package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.SegmentEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Segment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = SectionMapper.class)
public interface SegmentMapper {
    SegmentMapper INSTANCE = Mappers.getMapper(SegmentMapper.class);

    @Mapping(target = "itineraryId.id", source = "id.itineraryId")
    @Mapping(target = "position", source = "id.position")
    @Mapping(target = "sections", source = "sections", qualifiedByName = "defaultFromDB")
    Segment map(SegmentEntity itineraryEntity);

}
