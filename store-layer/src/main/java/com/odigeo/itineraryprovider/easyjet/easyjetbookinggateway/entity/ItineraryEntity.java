package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "ITINERARY")
@NamedQuery(name = "ItineraryEntity.findAll", query = "SELECT i FROM ItineraryEntity i")
public class ItineraryEntity implements Serializable {

    @Id
    private UUID id;

    @Column(name = "ARRIVAL_LOCATION")
    private BigDecimal arrivalLocation;

    @Column(name = "DEPARTURE_DATE")
    private String departureDate;

    @Column(name = "DEPARTURE_LOCATION")
    private BigDecimal departureLocation;

    @Column(name = "RETURN_DATE")
    private String returnDate;

    @Column(name = "TRIP_TYPE")
    private String tripType;

    @OneToOne(mappedBy = "itineraryEntity", cascade = CascadeType.ALL)
    private FareDetailsEntity fareDetailsEntity;

    @OneToOne(mappedBy = "itineraryEntity")
    private ItineraryBookingEntity itineraryBooking;

    @OneToMany(mappedBy = "itineraryEntity", cascade = CascadeType.ALL)
    @OrderBy("id.position ASC")
    private List<SegmentEntity> segments;

    public BigDecimal getArrivalLocation() {
        return arrivalLocation;
    }

    public void setArrivalLocation(BigDecimal arrivalLocation) {
        this.arrivalLocation = arrivalLocation;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public BigDecimal getDepartureLocation() {
        return departureLocation;
    }

    public void setDepartureLocation(BigDecimal departureLocation) {
        this.departureLocation = departureLocation;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public FareDetailsEntity getFareDetailsEntity() {
        return fareDetailsEntity;
    }

    public void setFareDetailsEntity(FareDetailsEntity fareDetailsEntity) {
        this.fareDetailsEntity = fareDetailsEntity;
    }

    public ItineraryBookingEntity getItineraryBooking() {
        return itineraryBooking;
    }

    public void setItineraryBooking(ItineraryBookingEntity itineraryBooking) {
        this.itineraryBooking = itineraryBooking;
    }

    public List<SegmentEntity> getSegments() {
        return segments;
    }

    public void setSegments(List<SegmentEntity> segments) {
        this.segments = segments;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }
}
