package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Embeddable
public class PassengerFaresEntityPK implements Serializable {

    @Column(name = "ITINERARY_ID", insertable = false, updatable = false)
    private UUID fareDetailsId;

    @Basic
    @Column(name = "TRAVELLER_TYPE", nullable = false, length = 3)
    private String travellerType;

    public UUID getFareDetailsId() {
        return fareDetailsId;
    }

    public void setFareDetailsId(UUID fareDetailsId) {
        this.fareDetailsId = fareDetailsId;
    }

    public String getTravellerType() {
        return travellerType;
    }

    public void setTravellerType(String travellerType) {
        this.travellerType = travellerType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PassengerFaresEntityPK passengerFaresEntityPK = (PassengerFaresEntityPK) o;
        if (fareDetailsId != passengerFaresEntityPK.fareDetailsId) {
            return false;
        }
        return Objects.equals(travellerType, passengerFaresEntityPK.travellerType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fareDetailsId, travellerType);
    }
}
