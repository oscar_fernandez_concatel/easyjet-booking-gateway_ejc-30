package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

@Mapper
public interface LocalDateTimeMapper {

    LocalDateTimeMapper INSTANCE = Mappers.getMapper(LocalDateTimeMapper.class);

    default LocalDateTime map(Timestamp time) {
        return LocalDateTime.ofInstant(time.toLocalDateTime().toInstant(ZoneOffset.UTC), ZoneId.of("UTC")); //TODO REMAP TIMESTAMP TO ZONEDDATETIME
    }

    default String mapToString(LocalDateTime time) {
        return DateMapper.INSTANCE.toString(time);
    }

    default LocalDateTime mapToLocalDateTime(String time) {
        return DateMapper.INSTANCE.toLocalDateTime(time);
    }
}
