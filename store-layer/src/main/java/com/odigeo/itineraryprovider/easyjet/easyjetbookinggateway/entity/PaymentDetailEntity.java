package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
@Table(name = "PAYMENT_DETAILS")
@NamedQueries({
        @NamedQuery(name = "PaymentDetailEntity.findByItineraryBookingId",
                query = "Select model from PaymentDetailEntity model where model.itineraryBookingId = :itineraryBookingId")})
public class PaymentDetailEntity implements Serializable {

    @Id
    @Column(name = "ID")
    private UUID id;

    @Basic
    @Column(name = "CREATED_DATE", updatable = false, insertable = false)
    private Timestamp created;

    @Basic
    @Column(name = "PAYMENT_INSTRUMENT_TOKEN", nullable = false)
    private String paymentInstrumentToken;

    @Column(name = "ITINERARY_BOOKING_ID")
    private UUID itineraryBookingId;

    @ManyToOne
    @JoinColumn(name = "ITINERARY_BOOKING_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    private ItineraryBookingEntity itineraryBookingEntity;

    @Basic
    @Column(name = "CARD_VENDOR_CODE", nullable = false)
    private String cardVendorCode;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public String getPaymentInstrumentToken() {
        return paymentInstrumentToken;
    }

    public void setPaymentInstrumentToken(String paymentInstrumentToken) {
        this.paymentInstrumentToken = paymentInstrumentToken;
    }

    public UUID getItineraryBookingId() {
        return itineraryBookingId;
    }

    public void setItineraryBookingId(UUID itineraryBookingId) {
        this.itineraryBookingId = itineraryBookingId;
    }

    public ItineraryBookingEntity getItineraryBookingEntity() {
        return itineraryBookingEntity;
    }

    public void setItineraryBookingEntity(ItineraryBookingEntity itineraryBookingEntity) {
        this.itineraryBookingEntity = itineraryBookingEntity;
    }

    public String getCardVendorCode() {
        return cardVendorCode;
    }

    public void setCardVendorCode(String cardVendorCode) {
        this.cardVendorCode = cardVendorCode;
    }
}
