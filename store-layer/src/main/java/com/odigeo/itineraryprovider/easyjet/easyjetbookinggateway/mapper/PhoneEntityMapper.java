package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.PhoneEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Phone;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.UUID;


@Mapper
public interface PhoneEntityMapper {

    PhoneEntityMapper INSTANCE = Mappers.getMapper(PhoneEntityMapper.class);

    default Phone toModel(PhoneEntity phoneEntity) {

        if (phoneEntity == null) {
            return null;
        }
        return Phone.builder()
                .setNumber(phoneEntity.getPhoneNumber())
                .setPhoneType(phoneEntity.getType())
                .build();
    }

    default PhoneEntity toEntity(Phone phoneModel) {

        PhoneEntity phoneEntity = new PhoneEntity();

        phoneEntity.setId(UUID.randomUUID());
        phoneEntity.setType(phoneModel.getPhoneType());
        phoneEntity.setPhoneNumber(phoneModel.getNumber());

        phoneEntity.setPrefix(phoneModel.getCountry() != null
                ? phoneModel.getCountry().getPhonePrefix() : null);

        return phoneEntity;
    }

}
