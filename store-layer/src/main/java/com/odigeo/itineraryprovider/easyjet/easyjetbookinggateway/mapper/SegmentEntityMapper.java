package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.SegmentEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.SegmentEntityPK;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Segment;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.stream.Collectors;


@Mapper
public interface SegmentEntityMapper {

    SegmentEntityMapper INSTANCE = Mappers.getMapper(SegmentEntityMapper.class);

    default SegmentEntity toEntity(Segment segmentModel) {

        SegmentEntity segment = new SegmentEntity();

        SegmentEntityPK segmentEntityPK = new SegmentEntityPK();
        segmentEntityPK.setItineraryId(segmentModel.getItineraryId().getId());
        segmentEntityPK.setPosition(segmentModel.getPosition());

        segment.setId(segmentEntityPK);

        segment.setSections(
                segmentModel.getSections().stream()
                        .map(flightSection -> SectionEntityMapper.INSTANCE.toEntity(
                                flightSection, segmentModel.getItineraryId().getId()))
                        .collect(Collectors.toList()));

        return segment;
    }

}
