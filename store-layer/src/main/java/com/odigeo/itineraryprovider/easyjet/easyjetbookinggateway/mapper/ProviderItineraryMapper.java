package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.ItineraryEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItinerary;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TripType;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {FareDetailsMapper.class, SegmentMapper.class, LocalDateTimeMapper.class})
public interface ProviderItineraryMapper {

    ProviderItineraryMapper INSTANCE = Mappers.getMapper(ProviderItineraryMapper.class);

    @Mappings({
            @Mapping(target = "itineraryId", source = "id"),
            @Mapping(target = "itinerary.departureGeoNodeId", source = "departureLocation"),
            @Mapping(target = "itinerary.arrivalGeoNodeId", source = "arrivalLocation"),
            @Mapping(target = "itinerary.departureDate", source = "departureDate"),
            @Mapping(target = "itinerary.returnDate", source = "returnDate"),
            @Mapping(target = "itinerary.fareDetails", source = "fareDetailsEntity"),
            @Mapping(target = "itinerary.segments", source = "segments"),
            @Mapping(target = "itinerary.id", source = "id"),
            @Mapping(target = "itinerary.tripType", source = "tripType", qualifiedByName = "tripTypeToTripType")
        })
    ProviderItinerary map(ItineraryEntity itineraryEntity);

    @Named("tripTypeToTripType")
    default TripType map(String value) {
        return TripType.getTripType(value);
    }

    ItineraryId map(java.util.UUID value);

}
