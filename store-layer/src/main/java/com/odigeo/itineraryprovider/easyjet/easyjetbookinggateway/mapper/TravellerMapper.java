package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.edreams.base.MissingElementException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.TravellerEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TravellerType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Traveller;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.TravellerGender;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.TravellerId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.TravellerIdentificationType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.TravellerTitle;
import org.apache.log4j.Logger;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.UUID;

@Mapper
public interface TravellerMapper {

    Logger LOGGER = Logger.getLogger(TravellerMapper.class);

    TravellerMapper INSTANCE = Mappers.getMapper(TravellerMapper.class);

    default TravellerEntity map(Traveller travellerModel, UUID itineraryBookingId) {

        TravellerEntity travellerEntity = new TravellerEntity();

        travellerEntity.setId(travellerModel.getId().getId());
        travellerEntity.setTravellerTitle(travellerModel.getTitle().getDatabaseCode());
        travellerEntity.setName(travellerModel.getName());
        travellerEntity.setTravellerNumber(BigDecimal.valueOf(travellerModel.getTravellerNumber()));
        travellerEntity.setIdentification(travellerModel.getIdentification());
        travellerEntity.setIdentificationExpirationDate(Date.valueOf(travellerModel.getIdentificationExpirationDate()));
        travellerEntity.setNationalityCountryCode(travellerModel.getNationalityCountryCode());
        travellerEntity.setIdentificationType(travellerModel.getIdentificationType().name());
        travellerEntity.setIdentificationCountryCode(travellerModel.getIdentificationIssueCountryCode());
        travellerEntity.setGender(travellerModel.getTravellerGender().name());
        travellerEntity.setAge(BigDecimal.valueOf(travellerModel.getAge()));
        travellerEntity.setSecondLastName(travellerModel.getSecondLastName());
        travellerEntity.setFirstLastName(travellerModel.getFirstLastName());
        travellerEntity.setDateOfBirth(Date.valueOf(travellerModel.getDateOfBirth()));
        travellerEntity.setType(travellerModel.getTravellerType().name());
        travellerEntity.setMiddleName(travellerModel.getMiddleName());
        travellerEntity.setPhoneEntity(
                travellerModel.getPhone() != null
                        ? PhoneEntityMapper.INSTANCE.toEntity(travellerModel.getPhone()) : null);
        travellerEntity.setItineraryBookingId(itineraryBookingId);

        return travellerEntity;
    }

    default Traveller map(TravellerEntity itineraryEntity) {

        return Traveller.builder()
                .setId(TravellerId.builder().setId(itineraryEntity.getId()).build())
                .setTravellerType(TravellerType.valueOf(itineraryEntity.getType()))
                .setIdentification(itineraryEntity.getIdentification())
                .setTravellerGender(TravellerGender.valueOf(itineraryEntity.getGender()))
                .setTravellerNumber(itineraryEntity.getTravellerNumber().intValue())
                .setItineraryBookingId(itineraryEntity.getItineraryBookingId())
                .setTitle(getTravellerTitle(itineraryEntity))
                .setPhone(PhoneEntityMapper.INSTANCE.toModel(itineraryEntity.getPhoneEntity()))
                .setName(itineraryEntity.getName())
                .setMiddleName(itineraryEntity.getMiddleName())
                .setIdentificationType(TravellerIdentificationType.valueOf(itineraryEntity.getIdentificationType()))
                .setIdentificationExpirationDate(itineraryEntity.getIdentificationExpirationDate().toLocalDate())
                .setFirstLastName(itineraryEntity.getFirstLastName())
                .setAge(itineraryEntity.getAge().intValue())
                .setDateOfBirth(itineraryEntity.getDateOfBirth().toLocalDate())
                .setIdentificationIssueCountryCode(itineraryEntity.getIdentificationCountryCode())
                .setNationalityCountryCode(itineraryEntity.getNationalityCountryCode())
                .setSecondLastName(itineraryEntity.getSecondLastName())
                .build();
    }

    private TravellerTitle getTravellerTitle(TravellerEntity itineraryEntity) {
        try {
            return TravellerTitle.getValue(itineraryEntity.getTravellerTitle());
        } catch (MissingElementException e) {
            LOGGER.warn(e);
            return TravellerTitle.MR;
        }
    }
}
