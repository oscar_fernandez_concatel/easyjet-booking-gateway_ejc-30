package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.SectionEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.SectionEntityPK;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightSection;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.UUID;


@Mapper
public interface SectionEntityMapper {

    SectionEntityMapper INSTANCE = Mappers.getMapper(SectionEntityMapper.class);

    default SectionEntity toEntity(FlightSection flightSection, UUID itineraryId) {

        SectionEntity sectionEntity = new SectionEntity();

        SectionEntityPK sectionEntityPK = new SectionEntityPK();
        sectionEntityPK.setItineraryId(itineraryId);
        sectionEntityPK.setFlightNumber(
                flightSection.getFlight() != null
                        ? flightSection.getFlight().getFlightNumber() : null);
        sectionEntityPK.setPositionId(flightSection.getSegmentPosition());

        sectionEntity.setId(sectionEntityPK);
        sectionEntity.setArrivalLocation(BigDecimal.valueOf(flightSection.getArrivalGeoNodeId()));
        sectionEntity.setDepartureLocation(BigDecimal.valueOf(flightSection.getDepartureGeoNodeId()));
        sectionEntity.setDepartureDateTime(Timestamp.valueOf(flightSection.getDepartureDate()));
        sectionEntity.setArrivalDateTime(Timestamp.valueOf(flightSection.getArrivalDate()));
        sectionEntity.setArrivalTerminal(flightSection.getArrivalTerminal());
        sectionEntity.setDepartureTerminal(flightSection.getDepartureTerminal());
        sectionEntity.setFareType(flightSection.getFlightFareInformation().getFareType().getCode());
        sectionEntity.setCabinClass(flightSection.getCabinClass().getName());
        sectionEntity.setFlightEntity(FlightEntityMapper.INSTANCE.toEntity(flightSection, sectionEntityPK));

        return sectionEntity;
    }

}
