package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "FLIGHT_FARE_INFORMATION")
public class FlightFareInformationEntity implements Serializable {

    @EmbeddedId
    private FlightFareInformationEntityPK id;

    @Basic
    @Column(name = "FARE_BASIS_ID", nullable = false, length = 11)
    private String fareBasisId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FLIGHT_NUMBER", referencedColumnName = "FLIGHT_NUMBER", insertable = false, updatable = false)
    @JoinColumn(name = "ITINERARY_ID", referencedColumnName = "ITINERARY_ID", insertable = false, updatable = false)
    @JoinColumn(name = "POSITION_ID", referencedColumnName = "POSITION_ID", insertable = false, updatable = false)
    private SectionEntity sectionEntity;

    public FlightFareInformationEntityPK getId() {
        return id;
    }

    public void setId(FlightFareInformationEntityPK id) {
        this.id = id;
    }

    public String getFareBasisId() {
        return fareBasisId;
    }

    public void setFareBasisId(String fareBasisId) {
        this.fareBasisId = fareBasisId;
    }

    public SectionEntity getSectionEntity() {
        return sectionEntity;
    }

    public void setSectionEntity(SectionEntity sectionEntity) {
        this.sectionEntity = sectionEntity;
    }
}
