package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;


import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "FARE_DETAILS")
public class FareDetailsEntity implements Serializable {

    @Basic
    @Column(name = "TOTAL_FARE_AMOUNT", nullable = false, precision = 2)
    private BigDecimal totalFareAmount;

    @Basic
    @Column(name = "CURRENCY", nullable = false, length = 3)
    private String currency;

    @Basic
    @Column(name = "TAX", nullable = false, precision = 2)
    private BigDecimal tax;

    @Basic
    @Column(name = "TAX_CURRENCY", nullable = false, length = 3)
    private String taxCurrency;

    @Id
    @Column(name = "ITINERARY_ID")
    private UUID itineraryId;

    @OneToOne
    @JoinColumn(name = "ITINERARY_ID", referencedColumnName = "ID")
    private ItineraryEntity itineraryEntity;

    @OneToMany(mappedBy = "fareDetailsEntity", cascade = CascadeType.ALL)
    private List<PassengerFaresEntity> passengerFares;

    public BigDecimal getTotalFareAmount() {
        return totalFareAmount;
    }

    public void setTotalFareAmount(BigDecimal totalFareAmount) {
        this.totalFareAmount = totalFareAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getTax() {
        return tax;
    }

    public void setTax(BigDecimal tax) {
        this.tax = tax;
    }

    public String getTaxCurrency() {
        return taxCurrency;
    }

    public void setTaxCurrency(String taxCurrency) {
        this.taxCurrency = taxCurrency;
    }

    public UUID getItineraryId() {
        return itineraryId;
    }

    public void setItineraryId(UUID itineraryId) {
        this.itineraryId = itineraryId;
    }

    public ItineraryEntity getItineraryEntity() {
        return itineraryEntity;
    }

    public void setItineraryEntity(ItineraryEntity itineraryEntity) {
        this.itineraryEntity = itineraryEntity;
    }

    public List<PassengerFaresEntity> getPassengerFares() {
        return passengerFares;
    }

    public void setPassengerFares(List<PassengerFaresEntity> passengerFares) {
        this.passengerFares = passengerFares;
    }
}
