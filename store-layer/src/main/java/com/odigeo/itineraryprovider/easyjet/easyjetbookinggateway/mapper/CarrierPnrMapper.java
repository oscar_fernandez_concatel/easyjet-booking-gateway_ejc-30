package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.commons.uuid.UUIDGenerator;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.CarrierPnrEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.SupplierLocator;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.UUID;

@Mapper
public interface CarrierPnrMapper {

    CarrierPnrMapper INSTANCE = Mappers.getMapper(CarrierPnrMapper.class);

    @Mapping(target = "code", source = "supplierLocatorCode")
    @Mapping(target = "carrierCode", source = "supplierCode")
    @Mapping(target = "id", expression = "java(com.odigeo.commons.uuid.UUIDGenerator.getInstance().generateUUID())")
    CarrierPnrEntity map(SupplierLocator supplierLocator);

    @InheritInverseConfiguration
    SupplierLocator map(CarrierPnrEntity ticketEntity);

    default UUID map() {
        return UUIDGenerator.getInstance().generateUUID();
    }
}
