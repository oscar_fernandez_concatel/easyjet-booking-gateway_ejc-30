package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Embeddable
public class SegmentEntityPK implements Serializable {

    @Column(name = "POSITION", insertable = false, updatable = false)
    private long position;

    @Column(name = "ITINERARY_ID", insertable = false, updatable = false)
    private UUID itineraryId;

    public long getPosition() {
        return position;
    }

    public void setPosition(long position) {
        this.position = position;
    }

    public UUID getItineraryId() {
        return itineraryId;
    }

    public void setItineraryId(UUID itineraryId) {
        this.itineraryId = itineraryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SegmentEntityPK segmentEntityPK = (SegmentEntityPK) o;
        if (position != segmentEntityPK.position) {
            return false;
        }
        return itineraryId == segmentEntityPK.itineraryId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(position, itineraryId);
    }
}
