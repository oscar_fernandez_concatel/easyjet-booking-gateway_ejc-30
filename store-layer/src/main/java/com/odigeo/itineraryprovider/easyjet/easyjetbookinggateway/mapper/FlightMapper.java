package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.FlightEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Carrier;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Flight;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface FlightMapper {
    FlightMapper INSTANCE = Mappers.getMapper(FlightMapper.class);

    @Mappings({
            @Mapping(target = "id.itineraryId", source = "itineraryId.id"),
            @Mapping(target = "id.positionId", source = "positionId"),
            @Mapping(target = "id.flightNumber", source = "flightNumber"),
            @Mapping(target = "operatingCarrier", source = "operatingCarrier.code"),
            @Mapping(target = "marketingCarrier", source = "marketingCarrier.code")
        })
    FlightEntity map(Flight flight);

    @InheritInverseConfiguration
    Flight map(FlightEntity flightEntity);

    default Carrier map(String code) {
        return Carrier.builder()
                .setCode(code)
                .setIataCode(code)
                .build();
    }
}
