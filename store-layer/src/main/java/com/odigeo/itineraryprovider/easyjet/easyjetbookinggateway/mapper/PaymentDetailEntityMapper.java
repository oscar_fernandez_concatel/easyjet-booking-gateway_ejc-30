package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.PaymentDetailEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.PaymentDetail;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PaymentDetailEntityMapper {

    PaymentDetailEntityMapper INSTANCE = Mappers.getMapper(PaymentDetailEntityMapper.class);

    default PaymentDetailEntity toEntity(PaymentDetail paymentDetailModel) {

        PaymentDetailEntity paymentDetailEntity = new PaymentDetailEntity();
        paymentDetailEntity.setId(paymentDetailModel.getId());
        paymentDetailEntity.setPaymentInstrumentToken(paymentDetailModel.getPaymentInstrumentToken());
        paymentDetailEntity.setItineraryBookingId(paymentDetailModel.getItineraryBookingId());
        paymentDetailEntity.setCardVendorCode(paymentDetailModel.getCardVendorCode());
        return paymentDetailEntity;
    }
}
