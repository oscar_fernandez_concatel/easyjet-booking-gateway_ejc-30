package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.itinerary;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.ItineraryNotFoundException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.PaymentDetail;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;

import java.util.Optional;
import java.util.UUID;

public interface ProviderItineraryBookingRepository {

    Optional<ProviderItineraryBooking> findItineraryBookingByItineraryId(UUID itineraryId);

    ProviderItineraryBooking find(UUID itineraryBookingId) throws ItineraryNotFoundException;

    void saveItineraryBooking(ProviderItineraryBooking providerItinerary);

    void updateItineraryBooking(ProviderItineraryBooking providerItinerary);

    void saveOrUpdateItineraryBookingPaymentDetail(PaymentDetail paymentDetail);
}
