package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.commons.uuid.UUIDGenerator;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.BuyerEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.PhoneEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Buyer;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.UUID;


@Mapper
public interface BuyerMapper {

    BuyerMapper INSTANCE = Mappers.getMapper(BuyerMapper.class);

    default BuyerEntity map(Buyer buyer, UUID itineraryId) {

        BuyerEntity entity = new BuyerEntity();
        entity.setItineraryBookingId(itineraryId);

        PhoneEntity phoneEntity = new PhoneEntity();
        phoneEntity.setId(UUIDGenerator.getInstance().generateUUID());
        phoneEntity.setPhoneNumber(buyer.getPhoneNumber());

        entity.setPhoneEntity(phoneEntity);
        entity.setAddress(buyer.getAddress());
        entity.setCity(buyer.getCityName());
        entity.setName(buyer.getName());
        entity.setLastName(buyer.getLastNames());
        entity.setCountryCode(buyer.getCountryCode());
        entity.setPhoneId(phoneEntity.getId());
        entity.setMail(buyer.getMail());
        entity.setZipCode(buyer.getZipCode());

        return entity;
    }

    default Buyer map(BuyerEntity entity) {
        return Buyer.builder()
                .setName(entity.getName())
                .setLastNames(entity.getLastName())
                .setMail(entity.getMail())
                .setPhoneNumber(entity.getPhoneEntity().getPhoneNumber())
                .setCountryCode(entity.getCountryCode())
                .setAddress(entity.getAddress())
                .setCityName(entity.getCity())
                .setZipCode(entity.getZipCode())
                .build();
    }

}
