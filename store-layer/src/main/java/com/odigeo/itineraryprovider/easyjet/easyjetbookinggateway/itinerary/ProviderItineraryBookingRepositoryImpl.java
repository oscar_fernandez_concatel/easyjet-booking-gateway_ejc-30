package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.itinerary;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.ItineraryBookingEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.PaymentDetailEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper.ItineraryBookingEntityMapper;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper.PaymentDetailEntityMapper;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper.ProviderItineraryBookingMapper;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayErrorType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.ItineraryNotFoundException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.PaymentDetail;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Optional;
import java.util.UUID;

public class ProviderItineraryBookingRepositoryImpl implements ProviderItineraryBookingRepository {

    private final EntityManager entityManager;
    private final ProviderItineraryBookingMapper providerItineraryBookingMapper;
    private final ItineraryBookingEntityMapper itineraryBookingEntityMapper;

    public ProviderItineraryBookingRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
        this.providerItineraryBookingMapper = ProviderItineraryBookingMapper.INSTANCE;
        this.itineraryBookingEntityMapper = ItineraryBookingEntityMapper.INSTANCE;
    }

    @Override
    public Optional<ProviderItineraryBooking> findItineraryBookingByItineraryId(UUID itineraryId) {

        TypedQuery<ItineraryBookingEntity> query =
                entityManager.createNamedQuery("ItineraryBookingEntity.findByItineraryId",
                        ItineraryBookingEntity.class);

        query.setParameter("itineraryId", itineraryId);

        return query.getResultList().stream().findFirst().map(itineraryBookingEntityMapper::map);
    }

    @Override
    public ProviderItineraryBooking find(UUID itineraryBookingId) throws ItineraryNotFoundException {

        ItineraryBookingEntity itineraryBookingEntity = getItineraryBookingEntity(itineraryBookingId);

        if (itineraryBookingEntity != null) {
            return itineraryBookingEntityMapper.map(itineraryBookingEntity);
        } else {
            throw new ItineraryNotFoundException("No ItineraryBookingEntity found with id "
                    + itineraryBookingId, EasyJetBookingGatewayErrorType.DATABASE_ERROR);
        }
    }

    private ItineraryBookingEntity getItineraryBookingEntity(UUID itineraryBookingId) {
        return entityManager.find(ItineraryBookingEntity.class, itineraryBookingId);
    }

    @Override
    public void saveItineraryBooking(ProviderItineraryBooking providerItinerary) {
        ItineraryBookingEntity entity = providerItineraryBookingMapper.mapToEntity(providerItinerary);
        entityManager.persist(entity);
    }

    @Override
    public void updateItineraryBooking(ProviderItineraryBooking providerItinerary) {
        ItineraryBookingEntity entity = providerItineraryBookingMapper.mapToEntity(providerItinerary);
        entityManager.merge(entity);
    }

    @Override
    public void saveOrUpdateItineraryBookingPaymentDetail(PaymentDetail paymentDetail) {

        PaymentDetailEntity entity = PaymentDetailEntityMapper.INSTANCE.toEntity(paymentDetail);

        TypedQuery<PaymentDetailEntity> query =
                entityManager.createNamedQuery("PaymentDetailEntity.findByItineraryBookingId",
                        PaymentDetailEntity.class);

        query.setParameter("itineraryBookingId", paymentDetail.getItineraryBookingId());

        PaymentDetailEntity existingEntity = query.getResultList().stream().findFirst().orElse(null);

        if (existingEntity != null) {
            existingEntity.setPaymentInstrumentToken(entity.getPaymentInstrumentToken());
            entityManager.merge(existingEntity);
        } else {
            entityManager.persist(entity);
        }
    }
}
