package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.redis;

public class RedisConnectionIssueException extends RuntimeException {

    public RedisConnectionIssueException(String message, Throwable cause) {
        super(message, cause);
    }

    public RedisConnectionIssueException(String message) {
        super(message);
    }

}
