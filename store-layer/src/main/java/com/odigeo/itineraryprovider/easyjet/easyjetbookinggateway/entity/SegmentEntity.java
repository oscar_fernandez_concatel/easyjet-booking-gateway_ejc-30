package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "SEGMENT")
public class SegmentEntity implements Serializable {

    @EmbeddedId
    private SegmentEntityPK id;


    @OneToMany(mappedBy = "segmentEntity",  cascade = CascadeType.ALL)
    @OrderBy("departureDateTime ASC")
    private List<SectionEntity> sections;

    @ManyToOne
    @JoinColumn(name = "ITINERARY_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    private ItineraryEntity itineraryEntity;

    public SegmentEntityPK getId() {
        return id;
    }

    public void setId(SegmentEntityPK id) {
        this.id = id;
    }

    public List<SectionEntity> getSections() {
        return sections;
    }

    public void setSections(List<SectionEntity> sections) {
        this.sections = sections;
    }

    public ItineraryEntity getItineraryEntity() {
        return itineraryEntity;
    }

    public void setItineraryEntity(ItineraryEntity itineraryEntity) {
        this.itineraryEntity = itineraryEntity;
    }
}
