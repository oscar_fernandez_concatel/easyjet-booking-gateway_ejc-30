package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.PassengerFaresEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.PassengerFaresEntityPK;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PassengerTypeFare;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.UUID;


@Mapper
public interface PassengerTypeEntityMapper {

    PassengerTypeEntityMapper INSTANCE = Mappers.getMapper(PassengerTypeEntityMapper.class);

    default PassengerFaresEntity toEntity(
            PassengerTypeFare passengerTypeFareModel,
            Integer numPassenger,
            UUID itineraryId) {

        PassengerFaresEntity passengerFaresEntity = new PassengerFaresEntity();

        PassengerFaresEntityPK passengerFaresEntityPK = new PassengerFaresEntityPK();
        passengerFaresEntityPK.setFareDetailsId(itineraryId);
        passengerFaresEntityPK.setTravellerType(passengerTypeFareModel.getTravellerType().getCode());

        passengerFaresEntity.setId(passengerFaresEntityPK);
        passengerFaresEntity.setNumPassenger(numPassenger);

        passengerFaresEntity.setFareAmount(
                passengerTypeFareModel.getFareAmount() != null
                        ? passengerTypeFareModel.getFareAmount().getAmount() : null);

        passengerFaresEntity.setFareCurrency(
                passengerTypeFareModel.getFareAmount() != null
                        && passengerTypeFareModel.getFareAmount().getCurrency() != null
                        ? passengerTypeFareModel.getFareAmount().getCurrency().getCurrencyCode() : null); //NOPMD

        passengerFaresEntity.setTaxAmount(
                passengerTypeFareModel.getTaxAmount() != null
                        ? passengerTypeFareModel.getTaxAmount().getAmount() : null); //NOPMD

        passengerFaresEntity.setTaxCurrency(
                passengerTypeFareModel.getTaxAmount() != null
                        && passengerTypeFareModel.getTaxAmount().getCurrency() != null
                        ? passengerTypeFareModel.getTaxAmount().getCurrency().getCurrencyCode() : null); //NOPMD

        passengerFaresEntity.setIsDiscount(this.getDiscountValue(passengerTypeFareModel.isDiscount()));

        return passengerFaresEntity;
    }

    default String getDiscountValue(boolean discount) {
        return discount ? "T" : "F";
    }

}
