package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "FLIGHT")
public class FlightEntity implements Serializable {

    @EmbeddedId
    private SectionEntityPK id;

    @Basic
    @Column(name = "OPERATING_CARRIER", nullable = false, length = 3)
    private String operatingCarrier;

    @Basic
    @Column(name = "MARKETING_CARRIER", nullable = false, length = 3)
    private String marketingCarrier;

    @Column(name = "POSITION_ID", insertable = false, updatable = false)
    private long positionId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "FLIGHT_NUMBER", referencedColumnName = "FLIGHT_NUMBER", insertable = false, updatable = false),
            @JoinColumn(name = "ITINERARY_ID", referencedColumnName = "ITINERARY_ID", insertable = false, updatable = false),
            @JoinColumn(name = "POSITION_ID", referencedColumnName = "POSITION_ID", insertable = false, updatable = false)
        })
    private SectionEntity sectionEntity;

    public SectionEntityPK getId() {
        return id;
    }

    public void setId(SectionEntityPK id) {
        this.id = id;
    }

    public String getOperatingCarrier() {
        return operatingCarrier;
    }

    public void setOperatingCarrier(String operatingCarrier) {
        this.operatingCarrier = operatingCarrier;
    }

    public String getMarketingCarrier() {
        return marketingCarrier;
    }

    public void setMarketingCarrier(String marketingCarrier) {
        this.marketingCarrier = marketingCarrier;
    }

    public SectionEntity getSectionEntity() {
        return sectionEntity;
    }

    public void setSectionEntity(SectionEntity sectionEntity) {
        this.sectionEntity = sectionEntity;
    }

    public long getPositionId() {
        return positionId;
    }

    public void setPositionId(long positionId) {
        this.positionId = positionId;
    }
}
