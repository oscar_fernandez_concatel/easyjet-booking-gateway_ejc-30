package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "PHONE")
public class PhoneEntity implements Serializable {

    @Basic
    @Column(name = "\"TYPE\"", length = 3)
    private String type;

    @Basic
    @Column(name = "PHONE_NUMBER", nullable = false, length = 30)
    private String phoneNumber;

    @Basic
    @Column(name = "\"PREFIX\"", nullable = false, length = 6)
    private String prefix;

    @OneToOne(mappedBy = "phoneEntity")
    private BuyerEntity buyer;

    @OneToOne(mappedBy = "phoneEntity")
    private TravellerEntity traveller;
    @Id
    private UUID id;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public BuyerEntity getBuyer() {
        return buyer;
    }

    public void setBuyer(BuyerEntity buyer) {
        this.buyer = buyer;
    }

    public TravellerEntity getTraveller() {
        return traveller;
    }

    public void setTraveller(TravellerEntity traveller) {
        this.traveller = traveller;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }
}
