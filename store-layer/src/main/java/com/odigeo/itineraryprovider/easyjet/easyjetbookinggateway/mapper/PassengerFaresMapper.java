package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.PassengerFaresEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PassengerTypeFare;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TravellerType;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PassengerFaresMapper {
    PassengerFaresMapper INSTANCE = Mappers.getMapper(PassengerFaresMapper.class);

    @Mappings({
            @Mapping(target = "id.travellerType", source = "travellerType"),
            @Mapping(target = "fareAmount", source = "fareAmount.amount"),
            @Mapping(target = "fareCurrency", source = "fareAmount.currency"),
            @Mapping(target = "taxAmount", source = "taxAmount.amount"),
            @Mapping(target = "taxCurrency", source = "taxAmount.currency"),
            @Mapping(target = "isDiscount", source = "discount"),
            @Mapping(target = "numPassenger", source = "passengersPerType")
        })
    PassengerFaresEntity map(PassengerTypeFare passengerTypeFare);

    @InheritInverseConfiguration
    PassengerTypeFare map(PassengerFaresEntity passengerFaresEntity);

    default TravellerType type(String value) {
        return TravellerType.getTravellerType(value);
    }

    default String type(TravellerType value) {
        return value.getCode();
    }

}
