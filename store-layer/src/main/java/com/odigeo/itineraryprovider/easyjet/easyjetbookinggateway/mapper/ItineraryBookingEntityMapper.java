package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.ItineraryBookingEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.BookingStatus;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.stream.Collectors;

@Mapper
public interface ItineraryBookingEntityMapper {

    ItineraryBookingEntityMapper INSTANCE = Mappers.getMapper(ItineraryBookingEntityMapper.class);

    default ProviderItineraryBooking map(ItineraryBookingEntity itineraryBookingEntity) {
        return ProviderItineraryBooking.builder()
                .setItineraryBookingId(itineraryBookingEntity.getId())
                .setItinerary(ItineraryMapper.INSTANCE.map(itineraryBookingEntity.getItineraryEntity()))
                .setTravellers(itineraryBookingEntity.getTravellers().stream()
                        .map(TravellerMapper.INSTANCE::map)
                        .collect(Collectors.toList()))
                .setBuyer(BuyerMapper.INSTANCE.map(itineraryBookingEntity.getBuyerEntity()))
                .setPnr(itineraryBookingEntity.getPnr())
                .setStatus(BookingStatus.valueOf(itineraryBookingEntity.getBookingStatus()))
                .setPaymentDetails(
                        itineraryBookingEntity.getPaymentDetails().stream()
                                .map(PaymentDetailMapper.INSTANCE::toModel)
                                .collect(Collectors.toList()))
                .setTotalFarePrice(itineraryBookingEntity.getTotalFarePrice())
                .build();
    }
}
