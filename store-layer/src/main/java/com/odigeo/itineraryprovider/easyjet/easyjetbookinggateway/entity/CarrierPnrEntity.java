package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "CARRIER_PNR")
public class CarrierPnrEntity implements Serializable {

    @Basic
    @Column(name = "CODE", nullable = false, length = 6)
    private String code;

    @Basic
    @Column(name = "CARRIER_CODE", nullable = false, length = 6)
    private String carrierCode;

    @Column(name = "ITINERARY_BOOKING_ID")
    private UUID itineraryBookingId;

    @ManyToOne
    @JoinColumn(name = "ITINERARY_BOOKING_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    private ItineraryBookingEntity itineraryBookingEntity;

    @Id
    @Column(name = "ID")
    private UUID id;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCarrierCode() {
        return carrierCode;
    }

    public void setCarrierCode(String carrierCode) {
        this.carrierCode = carrierCode;
    }

    public UUID getItineraryBookingId() {
        return itineraryBookingId;
    }

    public void setItineraryBookingId(UUID itineraryBookingId) {
        this.itineraryBookingId = itineraryBookingId;
    }

    public ItineraryBookingEntity getItineraryBookingEntity() {
        return itineraryBookingEntity;
    }

    public void setItineraryBookingEntity(ItineraryBookingEntity itineraryBookingEntity) {
        this.itineraryBookingEntity = itineraryBookingEntity;
    }
}
