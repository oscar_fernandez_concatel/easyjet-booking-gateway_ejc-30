package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "PASSENGER_FARES")
public class PassengerFaresEntity implements Serializable {

    @EmbeddedId
    private PassengerFaresEntityPK id;

    @Column(name = "NUM_PASSENGER")
    private long numPassenger;

    @Basic
    @Column(name = "FARE_AMOUNT", nullable = false, length = 3)
    private BigDecimal fareAmount;

    @Basic
    @Column(name = "FARE_CURRENCY", nullable = false, length = 3)
    private String fareCurrency;

    @Basic
    @Column(name = "TAX_AMOUNT", nullable = false)
    private BigDecimal taxAmount;

    @Basic
    @Column(name = "TAX_CURRENCY", nullable = false, length = 3)
    private String taxCurrency;

    @Basic
    @Column(name = "TAX_DETAILS", nullable = false, length = 1)
    private String taxDetails;

    @Basic
    @Column(name = "IS_DISCOUNT", nullable = true, length = 1)
    private String isDiscount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ITINERARY_ID", insertable = false, updatable = false)
    private FareDetailsEntity fareDetailsEntity;

    public PassengerFaresEntityPK getId() {
        return id;
    }

    public void setId(PassengerFaresEntityPK id) {
        this.id = id;
    }

    public long getNumPassenger() {
        return numPassenger;
    }

    public void setNumPassenger(long numPassenger) {
        this.numPassenger = numPassenger;
    }

    public BigDecimal getFareAmount() {
        return fareAmount;
    }

    public void setFareAmount(BigDecimal fareAmount) {
        this.fareAmount = fareAmount;
    }

    public String getFareCurrency() {
        return fareCurrency;
    }

    public void setFareCurrency(String fareCurrency) {
        this.fareCurrency = fareCurrency;
    }

    public BigDecimal getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }

    public String getTaxCurrency() {
        return taxCurrency;
    }

    public void setTaxCurrency(String taxCurrency) {
        this.taxCurrency = taxCurrency;
    }

    public String getTaxDetails() {
        return taxDetails;
    }

    public void setTaxDetails(String taxDetails) {
        this.taxDetails = taxDetails;
    }

    public String getIsDiscount() {
        return isDiscount;
    }

    public void setIsDiscount(String isDiscount) {
        this.isDiscount = isDiscount;
    }

    public FareDetailsEntity getFareDetailsEntity() {
        return fareDetailsEntity;
    }

    public void setFareDetailsEntity(FareDetailsEntity fareDetailsEntity) {
        this.fareDetailsEntity = fareDetailsEntity;
    }
}
