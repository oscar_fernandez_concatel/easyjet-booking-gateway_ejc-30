package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.ItineraryEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Itinerary;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TripType;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {FareDetailsMapper.class, SegmentMapper.class, LocalDateTimeMapper.class})
public interface ItineraryMapper {

    ItineraryMapper INSTANCE = Mappers.getMapper(ItineraryMapper.class);

    @Mappings({
            @Mapping(target = "id", source = "id"),
            @Mapping(target = "departureGeoNodeId", source = "departureLocation"),
            @Mapping(target = "arrivalGeoNodeId", source = "arrivalLocation"),
            @Mapping(target = "fareDetails", source = "fareDetailsEntity"),
            @Mapping(target = "tripType", source = "tripType", qualifiedByName = "tripTypeToTripType")
        })
    Itinerary map(ItineraryEntity itineraryEntity);

    @Named("tripTypeToTripType")
    default TripType map(String value) {
        return TripType.getTripType(value);
    }

    ItineraryId map(java.util.UUID value);
}
