package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.FlightFareInformationEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.SectionEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightFareInformationPassenger;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.HashMap;
import java.util.Map;

import static com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FareType.getFareTypeByCode;

@Mapper
public interface FlightFareInformationMapper {
    FlightFareInformationMapper INSTANCE = Mappers.getMapper(FlightFareInformationMapper.class);

    default Map<String, FlightFareInformationPassenger> map(SectionEntity sectionEntity) {
        Map<String, FlightFareInformationPassenger> flightFareInformationPassengerMap = new HashMap<>();
        for (FlightFareInformationEntity flightFareInformationEntity : sectionEntity.getFlightFareInformationEntities()) {
            FlightFareInformationPassenger flightFareInformationPassenger = FlightFareInformationPassenger.builder()
                    .setFareType(getFareTypeByCode(sectionEntity.getFareType()))
                    .setFareBasisId(flightFareInformationEntity.getFareBasisId())
                    .build();
            flightFareInformationPassengerMap.put(flightFareInformationEntity.getId().getFarePassengerType(), flightFareInformationPassenger);
        }
        return flightFareInformationPassengerMap;
    }
}
