package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.ItineraryEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Itinerary;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.math.BigDecimal;
import java.util.stream.Collectors;


@Mapper
public interface ItineraryEntityMapper {

    ItineraryEntityMapper INSTANCE = Mappers.getMapper(ItineraryEntityMapper.class);

    default ItineraryEntity toEntity(Itinerary itineraryModel) {

        ItineraryEntity itineraryEntity = new ItineraryEntity();

        itineraryEntity.setId(itineraryModel.getId().getId());
        itineraryEntity.setReturnDate(
                itineraryModel.getReturnDate() != null
                    ? itineraryModel.getReturnDate().toString() : null);

        itineraryEntity.setDepartureDate(
                itineraryModel.getDepartureDate() != null
                        ? itineraryModel.getDepartureDate().toString() : null);

        itineraryEntity.setDepartureLocation(BigDecimal.valueOf(itineraryModel.getDepartureGeoNodeId()));
        itineraryEntity.setArrivalLocation(BigDecimal.valueOf(itineraryModel.getArrivalGeoNodeId()));
        itineraryEntity.setTripType(itineraryModel.getTripType().getValue());

        itineraryEntity.setSegments(
                itineraryModel.getSegments().stream().map(SegmentEntityMapper.INSTANCE::toEntity)
                        .collect(Collectors.toList()));

        itineraryEntity.setFareDetailsEntity(FareDetailsEntityMapper.INSTANCE.toEntity(itineraryModel.getFareDetails()));

        return itineraryEntity;
    }

}
