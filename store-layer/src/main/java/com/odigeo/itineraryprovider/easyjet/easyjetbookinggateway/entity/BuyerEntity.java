package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;


import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "BUYER")
public class BuyerEntity implements Serializable {

    @Id
    @Column(name = "ITINERARY_BOOKING_ID", nullable = false)
    private UUID itineraryBookingId;

    @Basic
    @Column(name = "NAME", nullable = false, length = 40)
    private String name;

    @Basic
    @Column(name = "LAST_NAME", nullable = true, length = 60)
    private String lastName;

    @Basic
    @Column(name = "MAIL", nullable = false, length = 60)
    private String mail;

    @Basic
    @Column(name = "ADDRESS", nullable = false, length = 70)
    private String address;

    @Basic
    @Column(name = "ZIP_CODE", nullable = false, length = 10)
    private String zipCode;

    @Basic
    @Column(name = "CITY", nullable = false, length = 50)
    private String city;

    @Basic
    @Column(name = "COUNTRY_CODE", nullable = false, length = 64)
    private String countryCode;

    @OneToOne
    @JoinColumn(name = "ITINERARY_BOOKING_ID")
    private ItineraryBookingEntity itineraryBookingEntity;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "PHONE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    private PhoneEntity phoneEntity;

    @Column(name = "PHONE_ID")
    private UUID phoneId;

    public UUID getItineraryBookingId() {
        return itineraryBookingId;
    }

    public void setItineraryBookingId(UUID itineraryBookingId) {
        this.itineraryBookingId = itineraryBookingId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public ItineraryBookingEntity getItineraryBookingEntity() {
        return itineraryBookingEntity;
    }

    public void setItineraryBookingEntity(ItineraryBookingEntity itineraryBookingEntity) {
        this.itineraryBookingEntity = itineraryBookingEntity;
    }

    public PhoneEntity getPhoneEntity() {
        return phoneEntity;
    }

    public void setPhoneEntity(PhoneEntity phoneEntity) {
        this.phoneEntity = phoneEntity;
    }

    public UUID getPhoneId() {
        return phoneId;
    }

    public void setPhoneId(UUID phoneId) {
        this.phoneId = phoneId;
    }
}
