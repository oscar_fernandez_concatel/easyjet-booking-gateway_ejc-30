package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.FareDetailsEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.PassengerFaresEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FareDetails;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PassengerTypeFare;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;


@Mapper
public interface FareDetailsEntityMapper {

    FareDetailsEntityMapper INSTANCE = Mappers.getMapper(FareDetailsEntityMapper.class);

    default FareDetailsEntity toEntity(FareDetails fareDetailsModel) {

        FareDetailsEntity fareDetailsEntity = new FareDetailsEntity();

        fareDetailsEntity.setCurrency(fareDetailsModel.getTotalFareAmount().getCurrency().getCurrencyCode());
        fareDetailsEntity.setTaxCurrency(fareDetailsModel.getTotalTaxAmount().getCurrency().getCurrencyCode());
        fareDetailsEntity.setTax(fareDetailsModel.getTotalTaxAmount().getAmount());
        fareDetailsEntity.setTotalFareAmount(fareDetailsModel.getTotalFareAmount().getAmount());
        fareDetailsEntity.setItineraryId(fareDetailsModel.getItineraryId().getId());
        fareDetailsEntity.setPassengerFares(getPassengerFares(
                fareDetailsModel.getPassengerFares().values(),
                fareDetailsModel.getItineraryId().getId()));

        return fareDetailsEntity;
    }

    default List<PassengerFaresEntity> getPassengerFares(
            Collection<PassengerTypeFare> passengerTypeFares,
            UUID itineraryId) {

        List<PassengerFaresEntity> passengerFaresEntities = new ArrayList<>();

        Integer numPassenger = 1;
        for (PassengerTypeFare passengerTypeFare : passengerTypeFares) {
            passengerFaresEntities.add(PassengerTypeEntityMapper.INSTANCE.toEntity(
                    passengerTypeFare, numPassenger, itineraryId));
            numPassenger++;
        }

        return passengerFaresEntities;
    }

}
