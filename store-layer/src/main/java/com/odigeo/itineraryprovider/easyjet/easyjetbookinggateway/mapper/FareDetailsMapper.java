package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.FareDetailsEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.PassengerFaresEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FareDetails;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Money;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PassengerTypeFare;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TravellerType;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Mapper
public interface FareDetailsMapper {
    FareDetailsMapper INSTANCE = Mappers.getMapper(FareDetailsMapper.class);

    default FareDetails map(FareDetailsEntity fareDetailsEntity) {
        Money totalTaxAmount = Money.builder()
                .setAmount(fareDetailsEntity.getTax())
                .setCurrency(Currency.getInstance(fareDetailsEntity.getTaxCurrency()))
                .build();
        Money totalFareAmount = Money.builder()
                .setAmount(fareDetailsEntity.getTotalFareAmount())
                .setCurrency(Currency.getInstance(fareDetailsEntity.getCurrency()))
                .build();
        ItineraryId itineraryId = ItineraryId.builder()
                .setId(fareDetailsEntity.getItineraryId())
                .build();
        return FareDetails.builder()
                .setTotalTaxAmount(totalTaxAmount)
                .setPassengerFares(map(fareDetailsEntity.getPassengerFares()))
                .setTotalFareAmount(totalFareAmount)
                .setItineraryId(itineraryId)
                .build();
    }

    default Map<TravellerType, PassengerTypeFare> map(List<PassengerFaresEntity> passengerFares) {
        Map<TravellerType, PassengerTypeFare> passengerFare = new HashMap<>();
        for (PassengerFaresEntity value : passengerFares) {
            passengerFare.put(TravellerType.getTravellerType(value.getId().getTravellerType()), PassengerFaresMapper.INSTANCE.map(value));
        }
        return passengerFare;
    }
}
