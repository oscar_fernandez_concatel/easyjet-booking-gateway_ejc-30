package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.UUID;

@Entity
@Table(name = "TRAVELLER")
public class TravellerEntity implements Serializable {

    @Basic
    @Column(name = "TRAVELLER_NUMBER", nullable = false)
    private BigDecimal travellerNumber;

    @Basic
    @Column(name = "\"TYPE\"", nullable = false, length = 3)
    private String type;

    @Basic
    @Column(name = "TRAVELLER_TITLE", nullable = false, length = 3)
    private String travellerTitle;

    @Basic
    @Column(name = "NAME", nullable = false, length = 30)
    private String name;

    @Basic
    @Column(name = "MIDDLE_NAME", nullable = true, length = 30)
    private String middleName;

    @Basic
    @Column(name = "FIRST_LAST_NAME", nullable = true, length = 50)
    private String firstLastName;

    @Basic
    @Column(name = "SECOND_LAST_NAME", nullable = true, length = 50)
    private String secondLastName;

    @Basic
    @Column(name = "GENDER", nullable = false, length = 6)
    private String gender;

    @Column(name = "DATE_OF_BIRTH")
    private Date dateOfBirth;

    @Basic
    @Column(name = "AGE", nullable = true)
    private BigDecimal age;

    @Basic
    @Column(name = "NATIONALITY_COUNTRY_CODE", nullable = true, length = 3)
    private String nationalityCountryCode;

    @Basic
    @Column(name = "RESIDENCE_COUNTRY_CODE", length = 3)
    private String residenceCountryCode;

    @Basic
    @Column(name = "IDENTIFICATION", length = 20)
    private String identification;

    @Basic
    @Column(name = "IDENTIFICATION_TYPE")
    private String identificationType;

    @Basic
    @Column(name = "IDENTIFICATION_EXPIRATION_DATE")
    private Date identificationExpirationDate;

    @Basic
    @Column(name = "IDENTIFICATION_COUNTRY_CODE")
    private String identificationCountryCode;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "PHONE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    private PhoneEntity phoneEntity;

    @Column(name = "PHONE_ID")
    private UUID phoneId;

    @ManyToOne
    @JoinColumn(name = "ITINERARY_BOOKING_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    private ItineraryBookingEntity itineraryBookingEntity;

    @Basic
    @Column(name = "KEY", nullable = false)
    private String key;

    @Column(name = "ITINERARY_BOOKING_ID")
    private UUID itineraryBookingId;
    
    @Id
    private UUID id;

    public BigDecimal getTravellerNumber() {
        return travellerNumber;
    }

    public void setTravellerNumber(BigDecimal travellerNumber) {
        this.travellerNumber = travellerNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTravellerTitle() {
        return travellerTitle;
    }

    public void setTravellerTitle(String travellerTitle) {
        this.travellerTitle = travellerTitle;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getFirstLastName() {
        return firstLastName;
    }

    public void setFirstLastName(String firstLastName) {
        this.firstLastName = firstLastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public void setSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public BigDecimal getAge() {
        return age;
    }

    public void setAge(BigDecimal age) {
        this.age = age;
    }

    public String getNationalityCountryCode() {
        return nationalityCountryCode;
    }

    public void setNationalityCountryCode(String nationalityCountryCode) {
        this.nationalityCountryCode = nationalityCountryCode;
    }

    public String getResidenceCountryCode() {
        return residenceCountryCode;
    }

    public void setResidenceCountryCode(String residenceCountryCode) {
        this.residenceCountryCode = residenceCountryCode;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getIdentificationType() {
        return identificationType;
    }

    public void setIdentificationType(String identificationType) {
        this.identificationType = identificationType;
    }

    public Date getIdentificationExpirationDate() {
        return identificationExpirationDate;
    }

    public void setIdentificationExpirationDate(Date identificationExpirationDate) {
        this.identificationExpirationDate = identificationExpirationDate;
    }

    public String getIdentificationCountryCode() {
        return identificationCountryCode;
    }

    public void setIdentificationCountryCode(String identificationCountryCode) {
        this.identificationCountryCode = identificationCountryCode;
    }

    public PhoneEntity getPhoneEntity() {
        return phoneEntity;
    }

    public void setPhoneEntity(PhoneEntity phoneEntity) {
        this.phoneEntity = phoneEntity;
    }

    public UUID getPhoneId() {
        return phoneId;
    }

    public void setPhoneId(UUID phoneId) {
        this.phoneId = phoneId;
    }

    public ItineraryBookingEntity getItineraryBookingEntity() {
        return itineraryBookingEntity;
    }

    public void setItineraryBookingEntity(ItineraryBookingEntity itineraryBookingEntity) {
        this.itineraryBookingEntity = itineraryBookingEntity;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public UUID getItineraryBookingId() {
        return itineraryBookingId;
    }

    public void setItineraryBookingId(UUID itineraryBookingId) {
        this.itineraryBookingId = itineraryBookingId;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }
}
