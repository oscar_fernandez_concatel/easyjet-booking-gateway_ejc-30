package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.FlightEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.SectionEntityPK;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightSection;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;


@Mapper
public interface FlightEntityMapper {

    FlightEntityMapper INSTANCE = Mappers.getMapper(FlightEntityMapper.class);

    default FlightEntity toEntity(FlightSection flightSection, SectionEntityPK sectionEntityPK) {

        FlightEntity flightEntity = new FlightEntity();

        flightEntity.setId(sectionEntityPK);
        flightEntity.setOperatingCarrier(flightSection.getFlight().getOperatingCarrier().getCode());
        flightEntity.setMarketingCarrier(flightSection.getFlight().getMarketingCarrier().getCode());

        return flightEntity;
    }

}
