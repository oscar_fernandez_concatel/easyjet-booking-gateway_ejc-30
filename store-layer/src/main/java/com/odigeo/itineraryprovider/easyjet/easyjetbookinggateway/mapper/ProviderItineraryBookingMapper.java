package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.ItineraryBookingEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.BookingStatus;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.stream.Collectors;

@Mapper
public interface ProviderItineraryBookingMapper {

    ProviderItineraryBookingMapper INSTANCE = Mappers.getMapper(ProviderItineraryBookingMapper.class);

    default ItineraryBookingEntity mapToEntity(ProviderItineraryBooking providerItineraryBooking) {

        ItineraryBookingEntity itineraryBookingEntity = new ItineraryBookingEntity();

        itineraryBookingEntity.setId(providerItineraryBooking.getItineraryBookingId());

        itineraryBookingEntity.setBuyerEntity(
                BuyerMapper.INSTANCE.map(providerItineraryBooking.getBuyer(),
                        providerItineraryBooking.getItineraryBookingId()));

        itineraryBookingEntity.setTravellers(providerItineraryBooking.getTravellers().stream()
                .map(traveller -> TravellerMapper.INSTANCE.map(traveller, itineraryBookingEntity.getId()))
                .collect(Collectors.toList()));

        itineraryBookingEntity.setBookingStatus(providerItineraryBooking.getStatus() != null
                ? providerItineraryBooking.getStatus().name() : BookingStatus.PENDING.name());

        itineraryBookingEntity.setPnr(providerItineraryBooking.getPnr());

        itineraryBookingEntity.setCurrency(providerItineraryBooking.getItinerary().getFareDetails()
                .getTotalFareAmount().getCurrency().getCurrencyCode());

        itineraryBookingEntity.setTotalFarePrice(providerItineraryBooking.getItinerary().getFareDetails()
                .getTotalFareAmount().getAmount());

        itineraryBookingEntity.setItineraryEntity(
                ItineraryEntityMapper.INSTANCE.toEntity(providerItineraryBooking.getItinerary()));

        return itineraryBookingEntity;
    }
}
