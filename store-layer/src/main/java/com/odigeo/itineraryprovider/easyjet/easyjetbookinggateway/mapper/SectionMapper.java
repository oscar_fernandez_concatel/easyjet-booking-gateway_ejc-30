package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.SectionEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CabinClass;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FareType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Flight;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightFareInformation;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightFareInformationPassenger;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightSection;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderSection;
import org.mapstruct.Mapper;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Map;

@Mapper
public interface SectionMapper {
    SectionMapper INSTANCE = Mappers.getMapper(SectionMapper.class);

    default FlightSection map(SectionEntity sectionEntity) {
        Flight flight = FlightMapper.INSTANCE.map(sectionEntity.getFlightEntity());
        ProviderSection providerSection = ProviderSection.builder()
                .setFareKey(sectionEntity.getFareKey())
                .setBookingCode(sectionEntity.getBookingCode())
                .setClassOfService(sectionEntity.getClassOfService())
                .build();
        ItineraryId id = ItineraryId.builder()
                .setId(sectionEntity.getId().getItineraryId())
                .build();
        return FlightSection.builder()
                .setId(id)
                .setSegmentPosition((int) sectionEntity.getId().getPositionId())
                .setDepartureGeoNodeId(sectionEntity.getDepartureLocation().intValue())
                .setArrivalGeoNodeId(sectionEntity.getArrivalLocation().intValue())
                .setDepartureDate(sectionEntity.getDepartureDateTime().toLocalDateTime())
                .setArrivalDate(sectionEntity.getArrivalDateTime().toLocalDateTime())
                .setDepartureTerminal(sectionEntity.getDepartureTerminal())
                .setArrivalTerminal(sectionEntity.getArrivalTerminal())
                .setCabinClass(CabinClass.getCabinClass(sectionEntity.getCabinClass()))
                .setFlight(flight)
                .setProviderSection(providerSection)
                .build();
    }

    @Named("defaultFromDB")
    default FlightSection mapWithFlightFareInformation(SectionEntity sectionEntity) {
        FlightSection section = map(sectionEntity);
        final Map<String, FlightFareInformationPassenger> fareInformationPassengerMap = FlightFareInformationMapper.INSTANCE.map(sectionEntity);
        FareType fareType = FareType.PUBLIC;
        if (fareInformationPassengerMap.entrySet().stream().findAny().isPresent()) {
            fareType = fareInformationPassengerMap.entrySet().stream().findAny().get().getValue().getFareType();
        }
        FlightFareInformation flightFareInformation = FlightFareInformation.builder()
                .setFlightFareInformationPassenger(fareInformationPassengerMap)
                .setFareType(fareType)
                .build();
        return FlightSection.builder()
                .setFlight(section.getFlight())
                .setFlightFareInformation(flightFareInformation)
                .setProviderSection(section.getProviderSection())
                .setArrivalDate(section.getArrivalDate())
                .setArrivalTerminal(section.getArrivalTerminal())
                .setCabinClass(section.getCabinClass())
                .setArrivalGeoNodeId(section.getArrivalGeoNodeId())
                .setBaggageAllowance(section.getBaggageAllowance())
                .setDepartureDate(section.getDepartureDate())
                .setDepartureGeoNodeId(section.getDepartureGeoNodeId())
                .setDepartureTerminal(section.getDepartureTerminal())
                .setId(section.getId())
                .setProductType(section.getProductType())
                .setSegmentPosition(section.getSegmentPosition())
                .build();
    }

    default java.util.UUID map(ItineraryId value) {
        return value.getId();
    }

    default Timestamp map(LocalDateTime value) {
        return Timestamp.valueOf(value);
    }

    default LocalDateTime map(Timestamp value) {
        return value.toLocalDateTime();
    }

}
