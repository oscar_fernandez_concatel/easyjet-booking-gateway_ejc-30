package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "ITINERARY_BOOKING")
@NamedQueries({
        @NamedQuery(name = "ItineraryBookingEntity.findByItineraryId",
                query = "Select model from ItineraryBookingEntity model where model.itineraryEntity.id = :itineraryId")
    })
public class ItineraryBookingEntity implements Serializable {

    @Id
    private UUID id;

    @Column(name = "TOTAL_FARE_PRICE")
    private BigDecimal totalFarePrice;

    @Basic
    @Column(name = "PNR", nullable = false, length = 6)
    private String pnr;

    @Basic
    @Column(name = "CURRENCY", nullable = false, length = 3)
    private String currency;

    @Basic
    @Column(name = "BOOKING_STATUS", nullable = false, length = 15)
    private String bookingStatus;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ITINERARY_ID", referencedColumnName = "ID")
    private ItineraryEntity itineraryEntity;

    @OneToOne(mappedBy = "itineraryBookingEntity", cascade = CascadeType.ALL) //NOPMD
    private BuyerEntity buyerEntity;

    @OneToMany(mappedBy = "itineraryBookingEntity", cascade = CascadeType.ALL) //NOPMD
    private List<TravellerEntity> travellers;

    @OneToMany(mappedBy = "itineraryBookingEntity", cascade = CascadeType.ALL) //NOPMD
    private List<CarrierPnrEntity> carrierPnrs;

    @Basic
    @Column(name = "CREATED_DATE", updatable = false, insertable = false)
    private Timestamp created;

    @OneToMany(mappedBy = "itineraryBookingEntity", cascade = CascadeType.ALL) //NOPMD
    private List<PaymentDetailEntity> paymentDetails;

    public BigDecimal getTotalFarePrice() {
        return totalFarePrice;
    }

    public void setTotalFarePrice(BigDecimal totalFarePrice) {
        this.totalFarePrice = totalFarePrice;
    }

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public ItineraryEntity getItineraryEntity() {
        return itineraryEntity;
    }

    public void setItineraryEntity(ItineraryEntity itineraryEntity) {
        this.itineraryEntity = itineraryEntity;
    }

    public BuyerEntity getBuyerEntity() {
        return buyerEntity;
    }

    public void setBuyerEntity(BuyerEntity buyerEntity) {
        this.buyerEntity = buyerEntity;
    }

    public List<TravellerEntity> getTravellers() {
        return travellers;
    }

    public void setTravellers(List<TravellerEntity> travellers) {
        this.travellers = travellers;
    }

    public List<CarrierPnrEntity> getCarrierPnrs() {
        return carrierPnrs;
    }

    public void setCarrierPnrs(List<CarrierPnrEntity> carrierPnrs) {
        this.carrierPnrs = carrierPnrs;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public List<PaymentDetailEntity> getPaymentDetails() {
        return paymentDetails;
    }

    public void setPaymentDetails(List<PaymentDetailEntity> paymentDetails) {
        this.paymentDetails = paymentDetails;
    }
}
