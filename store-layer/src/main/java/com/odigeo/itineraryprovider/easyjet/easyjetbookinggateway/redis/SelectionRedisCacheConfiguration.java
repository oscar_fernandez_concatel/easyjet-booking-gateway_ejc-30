package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.redis;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.google.inject.Singleton;

@ConfiguredInPropertiesFile
@Singleton
public class SelectionRedisCacheConfiguration {

    private String cacheName;
    private String host;
    private int port;
    private int timeoutMillis;
    private int expirationTimeMillis;

    public SelectionRedisCacheConfiguration() {
    }

    public String getCacheName() {
        return cacheName;
    }

    public void setCacheName(String cacheName) {
        this.cacheName = cacheName;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getTimeoutMillis() {
        return timeoutMillis;
    }

    public void setTimeoutMillis(int timeoutMillis) {
        this.timeoutMillis = timeoutMillis;
    }

    public int getExpirationTimeMillis() {
        return expirationTimeMillis;
    }

    public void setExpirationTimeMillis(int expirationTimeMillis) {
        this.expirationTimeMillis = expirationTimeMillis;
    }
}
