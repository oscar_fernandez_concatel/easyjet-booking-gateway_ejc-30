package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Embeddable
public class FlightEntityPK implements Serializable {

    @Column(name = "POSITION_ID", insertable = false, updatable = false)
    private long positionId;

    @Column(name = "ITINERARY_ID", insertable = false, updatable = false)
    private UUID itineraryId;

    @Column(name = "FLIGHT_NUMBER")
    private String flightNumber;

    public long getPositionId() {
        return positionId;
    }

    public void setPositionId(long positionId) {
        this.positionId = positionId;
    }

    public UUID getItineraryId() {
        return itineraryId;
    }

    public void setItineraryId(UUID itineraryId) {
        this.itineraryId = itineraryId;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FlightEntityPK flightEntityPK = (FlightEntityPK) o;
        if (positionId != flightEntityPK.positionId) {
            return false;
        }
        if (itineraryId != flightEntityPK.itineraryId) {
            return false;
        }
        return flightNumber.equals(flightEntityPK.flightNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(positionId, itineraryId, flightNumber);
    }
}
