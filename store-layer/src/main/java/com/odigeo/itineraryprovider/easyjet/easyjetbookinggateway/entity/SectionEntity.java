package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "SECTION")
public class SectionEntity implements Serializable {

    @EmbeddedId
    private SectionEntityPK id;

    @Basic
    @Column(name = "DEPARTURE_LOCATION", nullable = false)
    private BigDecimal departureLocation;

    @Basic
    @Column(name = "ARRIVAL_LOCATION", nullable = false)
    private BigDecimal arrivalLocation;

    @Basic
    @Column(name = "DEPARTURE_DATE_TIME", nullable = false)
    private Timestamp departureDateTime;

    @Basic
    @Column(name = "ARRIVAL_DATE_TIME", nullable = false)
    private Timestamp arrivalDateTime;

    @Basic
    @Column(name = "DEPARTURE_TERMINAL", nullable = false, length = 100)
    private String departureTerminal;

    @Basic
    @Column(name = "ARRIVAL_TERMINAL", nullable = false, length = 100)
    private String arrivalTerminal;

    @Basic
    @Column(name = "BOOKING_CODE", nullable = false, length = 1)
    private String bookingCode;

    @Basic
    @Column(name = "FARE_KEY", nullable = false, length = 25)
    private String fareKey;

    @Basic
    @Column(name = "CABIN_CLASS", nullable = false, length = 1)
    private String cabinClass;

    @Basic
    @Column(name = "FARE_TYPE", nullable = false, columnDefinition = "char(1)")
    private String fareType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ITINERARY_ID", referencedColumnName = "ITINERARY_ID", insertable = false, updatable = false)
    @JoinColumn(name = "POSITION_ID", referencedColumnName = "POSITION", insertable = false, updatable = false)
    private SegmentEntity segmentEntity;

    @OneToOne(mappedBy = "sectionEntity", cascade = CascadeType.ALL)
    private FlightEntity flightEntity;

    @OneToMany(mappedBy = "sectionEntity", cascade = CascadeType.ALL)
    private List<FlightFareInformationEntity> flightFareInformationEntities;

    @Basic
    @Column(name = "CLASS_OF_SERVICE", length = 1)
    private String classOfService;

    public SectionEntityPK getId() {
        return id;
    }

    public void setId(SectionEntityPK id) {
        this.id = id;
    }

    public BigDecimal getDepartureLocation() {
        return departureLocation;
    }

    public void setDepartureLocation(BigDecimal departureLocation) {
        this.departureLocation = departureLocation;
    }

    public BigDecimal getArrivalLocation() {
        return arrivalLocation;
    }

    public void setArrivalLocation(BigDecimal arrivalLocation) {
        this.arrivalLocation = arrivalLocation;
    }

    public Timestamp getDepartureDateTime() {
        return departureDateTime;
    }

    public void setDepartureDateTime(Timestamp departureDateTime) {
        this.departureDateTime = departureDateTime;
    }

    public Timestamp getArrivalDateTime() {
        return arrivalDateTime;
    }

    public void setArrivalDateTime(Timestamp arrivalDateTime) {
        this.arrivalDateTime = arrivalDateTime;
    }

    public String getDepartureTerminal() {
        return departureTerminal;
    }

    public void setDepartureTerminal(String departureTerminal) {
        this.departureTerminal = departureTerminal;
    }

    public String getArrivalTerminal() {
        return arrivalTerminal;
    }

    public void setArrivalTerminal(String arrivalTerminal) {
        this.arrivalTerminal = arrivalTerminal;
    }

    public String getBookingCode() {
        return bookingCode;
    }

    public void setBookingCode(String bookingCode) {
        this.bookingCode = bookingCode;
    }

    public String getFareKey() {
        return fareKey;
    }

    public void setFareKey(String fareKey) {
        this.fareKey = fareKey;
    }

    public String getCabinClass() {
        return cabinClass;
    }

    public void setCabinClass(String cabinClass) {
        this.cabinClass = cabinClass;
    }

    public String getFareType() {
        return fareType;
    }

    public void setFareType(String fareType) {
        this.fareType = fareType;
    }

    public SegmentEntity getSegmentEntity() {
        return segmentEntity;
    }

    public void setSegmentEntity(SegmentEntity segmentEntity) {
        this.segmentEntity = segmentEntity;
    }

    public FlightEntity getFlightEntity() {
        return flightEntity;
    }

    public void setFlightEntity(FlightEntity flightEntity) {
        this.flightEntity = flightEntity;
    }

    public List<FlightFareInformationEntity> getFlightFareInformationEntities() {
        return flightFareInformationEntities;
    }

    public void setFlightFareInformationEntities(List<FlightFareInformationEntity> flightFareInformationEntities) {
        this.flightFareInformationEntities = flightFareInformationEntities;
    }

    public String getClassOfService() {
        return classOfService;
    }

    public void setClassOfService(String classOfService) {
        this.classOfService = classOfService;
    }

}
