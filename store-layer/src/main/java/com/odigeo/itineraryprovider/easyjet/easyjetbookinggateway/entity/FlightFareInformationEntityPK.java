package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Embeddable
public class FlightFareInformationEntityPK implements Serializable {

    @Column(name = "POSITION_ID", insertable = false, updatable = false)
    private long positionId;

    @Column(name = "ITINERARY_ID", insertable = false, updatable = false)
    private UUID itineraryId;

    @Column(name = "FLIGHT_NUMBER")
    private String flightNumber;

    @Basic
    @Column(name = "FARE_PASSENGER_TYPE", nullable = false, length = 6)
    private String farePassengerType;

    public long getPositionId() {
        return positionId;
    }

    public void setPositionId(long positionId) {
        this.positionId = positionId;
    }

    public UUID getItineraryId() {
        return itineraryId;
    }

    public void setItineraryId(UUID itineraryId) {
        this.itineraryId = itineraryId;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getFarePassengerType() {
        return farePassengerType;
    }

    public void setFarePassengerType(String farePassengerType) {
        this.farePassengerType = farePassengerType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FlightFareInformationEntityPK flightFareInformationEntityPK = (FlightFareInformationEntityPK) o;
        if (positionId != flightFareInformationEntityPK.positionId) {
            return false;
        }
        if (itineraryId != flightFareInformationEntityPK.itineraryId) {
            return false;
        }
        return flightNumber.equals(flightFareInformationEntityPK.flightNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(positionId, itineraryId, flightNumber);
    }
}
