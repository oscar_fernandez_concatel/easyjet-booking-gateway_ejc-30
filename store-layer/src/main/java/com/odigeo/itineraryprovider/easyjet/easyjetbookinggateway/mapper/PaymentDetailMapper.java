package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.PaymentDetailEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.PaymentDetail;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;


@Mapper
public interface PaymentDetailMapper {

    PaymentDetailMapper INSTANCE = Mappers.getMapper(PaymentDetailMapper.class);

    default PaymentDetail toModel(PaymentDetailEntity paymentDetailEntity) {

        return PaymentDetail.builder()
                .setId(paymentDetailEntity.getId())
                .setItineraryBookingId(paymentDetailEntity.getItineraryBookingId())
                .setPaymentInstrumentToken(paymentDetailEntity.getPaymentInstrumentToken())
                .setCardVendorCode(paymentDetailEntity.getCardVendorCode())
                .build();
    }

}
