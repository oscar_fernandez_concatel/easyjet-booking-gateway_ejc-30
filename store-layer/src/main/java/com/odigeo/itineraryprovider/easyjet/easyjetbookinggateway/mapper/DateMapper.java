package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Mapper
public abstract class DateMapper {
    public static final DateMapper INSTANCE = Mappers.getMapper(DateMapper.class);

    private static final DateTimeFormatter DATE_WITH_OFFSET_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSxxx");

    public LocalDateTime toLocalDateTime(String date) {
        if (date == null) {
            return null;
        }
        return LocalDateTime.parse(date, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }

    /**
     * Converts a {@link ZonedDateTime} into its {@link String} representation.
     *
     * @param date the date to be converted
     * @return the converted date, in the input date is defined,
     * {@code null} otherwise
     * @see DateTimeFormatter format(TemporalAccessor)
     */
    public String toString(ZonedDateTime date) {
        if (date == null) {
            return null;
        }
        return DATE_WITH_OFFSET_FORMATTER.format(date);
    }

    public String toString(LocalDateTime date) {
        if (date == null) {
            return null;
        }
        return DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(date);
    }
}
