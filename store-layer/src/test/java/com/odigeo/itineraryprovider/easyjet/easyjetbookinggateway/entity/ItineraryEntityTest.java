package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class ItineraryEntityTest {

    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";

    @Test
    public void testMethods() {

        final String expectedString = "SYSTEM";
        UUID uuid = UUID.fromString(IDENTIFIER);
        final BigDecimal expectedQuantity = new BigDecimal("500");

        ItineraryBookingEntity itineraryBookingEntity = new ItineraryBookingEntity();
        itineraryBookingEntity.setPnr(expectedString);

        FareDetailsEntity fareDetailsEntity = new FareDetailsEntity();
        fareDetailsEntity.setCurrency(expectedString);



        ItineraryEntity itineraryEntity = new ItineraryEntity();
        itineraryEntity.setItineraryBooking(itineraryBookingEntity);
        itineraryEntity.setArrivalLocation(expectedQuantity);
        itineraryEntity.setId(uuid);
        itineraryEntity.setFareDetailsEntity(fareDetailsEntity);
        itineraryEntity.setDepartureDate(expectedString);
        itineraryEntity.setReturnDate(expectedString);
        itineraryEntity.setArrivalLocation(expectedQuantity);
        itineraryEntity.setTripType(expectedString);
        itineraryEntity.setDepartureLocation(expectedQuantity);

        SegmentEntity segmentEntity = new SegmentEntity();
        segmentEntity.setItineraryEntity(itineraryEntity);
        List<SegmentEntity> segmentEntityList = new ArrayList<>();
        segmentEntityList.add(segmentEntity);

        itineraryEntity.setSegments(segmentEntityList);


        assertEquals(itineraryEntity.getItineraryBooking().getPnr(), expectedString);
        assertEquals(itineraryEntity.getArrivalLocation(), expectedQuantity);
        assertEquals(itineraryEntity.getId(), uuid);
        assertEquals(itineraryEntity.getFareDetailsEntity().getCurrency(), expectedString);
        assertEquals(itineraryEntity.getDepartureDate(), expectedString);
        assertEquals(itineraryEntity.getReturnDate(), expectedString);
        assertEquals(itineraryEntity.getArrivalLocation(), expectedQuantity);
        assertEquals(itineraryEntity.getTripType(), expectedString);
        assertTrue(itineraryEntity.getSegments().size() == 1);
        assertEquals(itineraryEntity.getDepartureLocation(), expectedQuantity);
    }

}
