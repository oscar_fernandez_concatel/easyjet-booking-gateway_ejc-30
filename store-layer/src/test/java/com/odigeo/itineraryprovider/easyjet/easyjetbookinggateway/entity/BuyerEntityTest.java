package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import org.testng.annotations.Test;

import java.util.UUID;

import static org.testng.Assert.assertEquals;

public class BuyerEntityTest {

    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";

    @Test
    public void testMethods() {

        final String expectedString = "SYSTEM";
        UUID uuid = UUID.fromString(IDENTIFIER);


        BuyerEntity buyerEntity = new BuyerEntity();
        buyerEntity.setName(expectedString);
        buyerEntity.setAddress(expectedString);
        buyerEntity.setCity(expectedString);
        buyerEntity.setCountryCode(expectedString);
        buyerEntity.setLastName(expectedString);
        buyerEntity.setMail(expectedString);
        buyerEntity.setZipCode(expectedString);
        TravellerEntity travellerEntity = new TravellerEntity();
        travellerEntity.setName(expectedString);

        ItineraryBookingEntity itineraryBookingEntity = new ItineraryBookingEntity();
        itineraryBookingEntity.setCurrency(expectedString);

        PhoneEntity phoneEntity = new PhoneEntity();
        phoneEntity.setBuyer(buyerEntity);
        phoneEntity.setPhoneNumber(expectedString);
        phoneEntity.setPrefix(expectedString);
        phoneEntity.setType(expectedString);
        phoneEntity.setTraveller(travellerEntity);
        buyerEntity.setPhoneEntity(phoneEntity);
        buyerEntity.setItineraryBookingId(uuid);
        buyerEntity.setItineraryBookingEntity(itineraryBookingEntity);
        buyerEntity.setPhoneId(uuid);

        assertEquals(expectedString, buyerEntity.getName());
        assertEquals(expectedString, buyerEntity.getAddress());
        assertEquals(expectedString, buyerEntity.getCity());
        assertEquals(expectedString, buyerEntity.getCountryCode());
        assertEquals(expectedString, buyerEntity.getLastName());
        assertEquals(expectedString, buyerEntity.getMail());
        assertEquals(expectedString, buyerEntity.getZipCode());
        assertEquals(uuid, buyerEntity.getItineraryBookingId());
        assertEquals(expectedString, buyerEntity.getItineraryBookingEntity().getCurrency());
        assertEquals(expectedString, buyerEntity.getPhoneEntity().getPhoneNumber());
        assertEquals(uuid, buyerEntity.getPhoneId());
    }
}
