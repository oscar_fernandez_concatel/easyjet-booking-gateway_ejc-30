package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.FlightFareInformationEntityPK;

import java.util.UUID;

public class FlightFareInformationEntityPKObjectMother {
    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";
    public static final FlightFareInformationEntityPK ANY = aFlightFareInformationEntityPK();

    private static FlightFareInformationEntityPK aFlightFareInformationEntityPK() {
        FlightFareInformationEntityPK flightFareInformationEntityPK = new FlightFareInformationEntityPK();
        flightFareInformationEntityPK.setItineraryId(UUID.fromString(IDENTIFIER));
        flightFareInformationEntityPK.setPositionId(1L);
        flightFareInformationEntityPK.setFlightNumber("flightNumber");
        flightFareInformationEntityPK.setFarePassengerType("ADULT");
        return flightFareInformationEntityPK;
    }

}
