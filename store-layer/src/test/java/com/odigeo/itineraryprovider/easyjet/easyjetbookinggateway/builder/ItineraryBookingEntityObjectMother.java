package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.ItineraryBookingEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.PaymentDetailEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.BookingStatus;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ItineraryBookingEntityObjectMother {

    public static final ItineraryBookingEntity ANY = aItineraryBookingEntity();

    private static ItineraryBookingEntity aItineraryBookingEntity() {

        PaymentDetailEntity paymentDetailEntity = new PaymentDetailEntity();
        paymentDetailEntity.setItineraryBookingId(UUID.randomUUID());
        paymentDetailEntity.setCardVendorCode("VI");
        paymentDetailEntity.setPaymentInstrumentToken(UUID.randomUUID().toString());
        paymentDetailEntity.setCreated(Timestamp.from(Instant.now()));

        ItineraryBookingEntity itineraryBookingEntity = new ItineraryBookingEntity();
        itineraryBookingEntity.setPnr("98JHJ");
        itineraryBookingEntity.setBuyerEntity(BuyerEntityObjectMother.ANY);
        itineraryBookingEntity.setTravellers(List.of(TravellerEntityObjectMother.ANY));
        itineraryBookingEntity.setBookingStatus(BookingStatus.PENDING.name());
        itineraryBookingEntity.setCurrency("EUR");
        itineraryBookingEntity.setCreated(Timestamp.from(Instant.now()));
        itineraryBookingEntity.setTotalFarePrice(BigDecimal.TEN);
        itineraryBookingEntity.setCarrierPnrs(new ArrayList<>());
        itineraryBookingEntity.setItineraryEntity(ItineraryEntityObjectMother.ANY);
        itineraryBookingEntity.setPaymentDetails(List.of(paymentDetailEntity));
        return itineraryBookingEntity;
    }
}
