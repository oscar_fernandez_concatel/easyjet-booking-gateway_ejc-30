package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;


import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.CarrierObjectModel;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.FlightObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.FlightEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Carrier;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Flight;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class FlightMapperTest {


    @Test
    public void testToEntity() {
        Flight flight = FlightObjectMother.ANY_INTERNAL;
        FlightEntity flightEntity = FlightMapper.INSTANCE.map(flight);
        assertEquals(flight.getFlightNumber(), flightEntity.getId().getFlightNumber());
    }

    @Test
    public void testToModel() {
        Carrier carrier = FlightMapper.INSTANCE.map("CODE");
        Carrier anyInternal = CarrierObjectModel.ANY_INTERNAL;
        assertEquals(anyInternal.getCode(), carrier.getCode());

    }

}
