package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.TravellerEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.TravellerTitle;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;

public class TravellerEntityObjectMother {

    public static final TravellerEntity ANY = aTravellerEntity();

    private static TravellerEntity aTravellerEntity() {
        TravellerEntity travellerEntity = new TravellerEntity();
        travellerEntity.setType("ADULT");
        travellerEntity.setTravellerTitle(TravellerTitle.MR.getDatabaseCode());
        travellerEntity.setName("Juan");
        travellerEntity.setFirstLastName("Perez");
        travellerEntity.setTravellerNumber(BigDecimal.ONE);
        travellerEntity.setDateOfBirth(Date.valueOf(LocalDate.of(1988, 8, 31)));
        travellerEntity.setAge(BigDecimal.valueOf(33));
        travellerEntity.setGender("MALE");
        travellerEntity.setNationalityCountryCode("ES");
        travellerEntity.setResidenceCountryCode("ES");
        travellerEntity.setIdentification("12345678R");
        travellerEntity.setIdentificationType("NIF");
        travellerEntity.setIdentificationExpirationDate(Date.valueOf(LocalDate.of(2034, 1, 1)));
        travellerEntity.setPhoneEntity(PhoneEntityObjectMother.ANY);
        return travellerEntity;
    }
}
