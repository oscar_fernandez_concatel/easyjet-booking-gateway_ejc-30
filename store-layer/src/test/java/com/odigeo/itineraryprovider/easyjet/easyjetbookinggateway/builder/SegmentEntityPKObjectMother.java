package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.SegmentEntityPK;

import java.util.UUID;

public class SegmentEntityPKObjectMother {
    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";
    public static final SegmentEntityPK ANY = aSegmentEntityPK();

    private static SegmentEntityPK aSegmentEntityPK() {
        SegmentEntityPK segmentEntityPK = new SegmentEntityPK();
        segmentEntityPK.setItineraryId(UUID.fromString(IDENTIFIER));
        segmentEntityPK.setPosition(1L);
        return segmentEntityPK;
    }
}
