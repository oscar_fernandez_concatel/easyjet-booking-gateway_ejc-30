package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.PassengerFaresEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.PassengerFaresEntityPK;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TravellerType;

import java.math.BigDecimal;
import java.util.UUID;

public class PassengerFaresEntityObjectMother {

    public static final PassengerFaresEntity ANY = aPassengerFaresEntity();

    private static PassengerFaresEntity aPassengerFaresEntity() {
        final String expectedString = "EUR";

        PassengerFaresEntityPK passengerFaresEntityPK = new PassengerFaresEntityPK();
        passengerFaresEntityPK.setTravellerType(TravellerType.ADULT.getCode());
        passengerFaresEntityPK.setFareDetailsId(UUID.randomUUID());

        PassengerFaresEntity passengerFaresEntity = new PassengerFaresEntity();
        passengerFaresEntity.setNumPassenger(1);
        passengerFaresEntity.setFareCurrency(expectedString);
        passengerFaresEntity.setFareAmount(BigDecimal.valueOf(33.99));
        passengerFaresEntity.setFareCurrency(expectedString);
        passengerFaresEntity.setTaxAmount(BigDecimal.valueOf(2.99));
        passengerFaresEntity.setTaxDetails(expectedString);
        passengerFaresEntity.setId(passengerFaresEntityPK);
        passengerFaresEntity.setTaxCurrency(expectedString);
        return passengerFaresEntity;
    }
}
