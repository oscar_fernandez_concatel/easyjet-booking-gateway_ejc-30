package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.FareDetailsEntity;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public class FareDetailsEntityObjectMother {

    public static final FareDetailsEntity ANY = aFareDetailsEntity();

    private static FareDetailsEntity aFareDetailsEntity() {
        final String expectedString = "EUR";
        FareDetailsEntity fareDetailsEntity = new FareDetailsEntity();
        fareDetailsEntity.setCurrency(expectedString);
        fareDetailsEntity.setItineraryId(UUID.randomUUID());
        fareDetailsEntity.setTotalFareAmount(BigDecimal.valueOf(33.99));
        fareDetailsEntity.setTax(BigDecimal.valueOf(2.99));
        fareDetailsEntity.setTaxCurrency(expectedString);
        fareDetailsEntity.setPassengerFares(List.of(PassengerFaresEntityObjectMother.ANY));
        return fareDetailsEntity;



    }
}
