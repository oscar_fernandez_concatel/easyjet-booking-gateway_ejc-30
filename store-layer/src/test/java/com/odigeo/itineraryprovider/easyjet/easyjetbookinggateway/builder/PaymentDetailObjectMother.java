package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.PaymentDetail;

import java.util.UUID;

public class PaymentDetailObjectMother {
    public static final PaymentDetail ANY = aPaymentDetail();

    private static PaymentDetail aPaymentDetail() {
        return PaymentDetail.builder()
            .setId(UUID.randomUUID())
            .setItineraryBookingId(ItineraryBookingIdObjectMother.ANY.getId())
            .setPaymentInstrumentToken("PAYMENTTOKEN")
            .build();
    }
}
