package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.ItineraryEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.SectionEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.SegmentEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.SegmentEntityPK;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Segment;

import java.util.ArrayList;
import java.util.List;

public class SegmentObjectMother {
    public static final Segment ANY = aSegment();
    public static final SegmentEntity ANY_ENTITY = aSegmentEntity();

    private static SegmentEntity aSegmentEntity() {
        ItineraryEntity itineraryEntity = ItineraryEntityObjectMother.ANY;
        SegmentEntityPK segmentEntityPK = SegmentEntityPKObjectMother.ANY;
        SegmentEntity segment = new SegmentEntity();
        segment.setId(segmentEntityPK);
        segment.setItineraryEntity(itineraryEntity);
        List<SectionEntity> sections = new ArrayList<>();
        SectionEntity section = SectionEntityObjectMother.ANY;
        sections.add(section);
        segment.setSections(sections);
        return segment;
    }

    public static Segment aSegment() {
        return Segment.builder()
                .setItineraryId(ItineraryIdObjectMother.ANY_INTERNAL)
                .setPosition(1)
                .setSections(List.of(FlightSectionObjectMother.ANY_INTERNAL))
                .build();
    }
}
