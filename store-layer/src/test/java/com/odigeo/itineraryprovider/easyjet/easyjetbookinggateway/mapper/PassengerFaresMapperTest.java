package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.PassengerFaresEntityObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.TravellerObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.PassengerFaresEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PassengerTypeFare;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TravellerType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Traveller;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class PassengerFaresMapperTest {

    @Test
    public void testToModel() {
        PassengerFaresEntity passengerFaresEntity = PassengerFaresEntityObjectMother.ANY;
        PassengerTypeFare passengerTypeFare = PassengerFaresMapper.INSTANCE.map(passengerFaresEntity);

        assertEquals(passengerFaresEntity.getTaxCurrency(), passengerTypeFare.getTaxAmount().getCurrency().getCurrencyCode());
    }

    @Test
    public void testToModel2() {
        Traveller traveller = TravellerObjectMother.ANY;
        String type = PassengerFaresMapper.INSTANCE.type(TravellerType.ADULT);
        assertEquals(traveller.getTravellerType().getCode(), type);

    }

}
