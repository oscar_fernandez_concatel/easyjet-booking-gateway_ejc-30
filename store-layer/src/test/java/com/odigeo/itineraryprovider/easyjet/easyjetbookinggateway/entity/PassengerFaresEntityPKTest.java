package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.testng.Assert.assertEquals;

public class PassengerFaresEntityPKTest {

    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";

    @Test
    public void testMethods() {

        final String expectedString = "SYSTEM";
        UUID uuid = UUID.fromString(IDENTIFIER);

        PassengerFaresEntityPK passengerFaresEntityPK = new PassengerFaresEntityPK();
        passengerFaresEntityPK.setTravellerType(expectedString);
        passengerFaresEntityPK.setFareDetailsId(uuid);

        assertEquals(passengerFaresEntityPK.getTravellerType(), expectedString);
        assertEquals(passengerFaresEntityPK.getFareDetailsId(), uuid);

        Assert.assertTrue(passengerFaresEntityPK.equals(passengerFaresEntityPK));
        Assert.assertFalse(passengerFaresEntityPK.equals(new Object()));
        Assert.assertFalse(passengerFaresEntityPK.equals(createEntity("travellerType", uuid)));
        Assert.assertFalse(passengerFaresEntityPK.equals(createEntity(expectedString, UUID.randomUUID())));
        Assert.assertEquals(passengerFaresEntityPK.hashCode(), createEntity(expectedString, uuid).hashCode());
    }

    private PassengerFaresEntityPK createEntity(String travellerType, UUID uuid) {
        PassengerFaresEntityPK passengerFaresEntityPK = new PassengerFaresEntityPK();
        passengerFaresEntityPK.setTravellerType(travellerType);
        passengerFaresEntityPK.setFareDetailsId(uuid);

        return passengerFaresEntityPK;
    }
}
