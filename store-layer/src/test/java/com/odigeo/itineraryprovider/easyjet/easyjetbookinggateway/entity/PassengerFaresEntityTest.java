package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;

import static org.testng.Assert.assertEquals;

public class PassengerFaresEntityTest {

    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";

    @Test
    public void testMethods() {

        final String expectedString = "SYSTEM";
        UUID uuid = UUID.fromString(IDENTIFIER);
        final Long expectedValue = 1L;
        final BigDecimal expectedQuantity = new BigDecimal("500");

        FareDetailsEntity fareDetailsEntity = new FareDetailsEntity();
        fareDetailsEntity.setCurrency(expectedString);

        PassengerFaresEntityPK passengerFaresEntityPK = new PassengerFaresEntityPK();
        passengerFaresEntityPK.setTravellerType(expectedString);


        PassengerFaresEntity passengerFaresEntity = new PassengerFaresEntity();
        passengerFaresEntity.setFareCurrency(expectedString);
        passengerFaresEntity.setNumPassenger(expectedValue);
        passengerFaresEntity.setFareDetailsEntity(fareDetailsEntity);
        passengerFaresEntity.setFareAmount(expectedQuantity);
        passengerFaresEntity.setId(passengerFaresEntityPK);
        passengerFaresEntity.setIsDiscount(expectedString);
        passengerFaresEntity.setTaxAmount(expectedQuantity);
        passengerFaresEntity.setTaxCurrency(expectedString);
        passengerFaresEntity.setTaxDetails(expectedString);


        assertEquals(passengerFaresEntity.getFareCurrency(), expectedString);
        assertEquals(Optional.of(passengerFaresEntity.getNumPassenger()), Optional.of(expectedValue));
        assertEquals(passengerFaresEntity.getFareDetailsEntity().getCurrency(), expectedString);
        assertEquals(passengerFaresEntity.getFareAmount(), expectedQuantity);
        assertEquals(passengerFaresEntity.getId().getTravellerType(), expectedString);
        assertEquals(passengerFaresEntity.getIsDiscount(), expectedString);
        assertEquals(passengerFaresEntity.getTaxAmount(), expectedQuantity);
        assertEquals(passengerFaresEntity.getTaxCurrency(), expectedString);
        assertEquals(passengerFaresEntity.getTaxDetails(), expectedString);

    }

}
