package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.PhoneEntityObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.PhoneEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Phone;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class PhoneEntityMapperTest {

    @Test
    public void testToModel() {
        PhoneEntity phoneEntity = PhoneEntityObjectMother.ANY;
        Phone phoneModel = PhoneEntityMapper.INSTANCE.toModel(phoneEntity);
        assertEquals(phoneModel.getNumber(), phoneEntity.getPhoneNumber());
    }

    @Test
    public void testToModelParamNull() {
        PhoneEntity phoneEntity = null;
        Phone phoneModel = PhoneEntityMapper.INSTANCE.toModel(phoneEntity);
        assertNull(phoneModel);
    }

    @Test
    public void testToEntity() {
        Phone phone = PhoneEntityObjectMother.ANY_MODEL;
        PhoneEntity phoneEntity = PhoneEntityMapper.INSTANCE.toEntity(phone);
        assertEquals(phone.getNumber(), phoneEntity.getPhoneNumber());
    }

}
