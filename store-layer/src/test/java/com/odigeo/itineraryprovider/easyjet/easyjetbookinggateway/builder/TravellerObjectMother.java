package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TravellerType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Phone;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Traveller;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.TravellerGender;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.TravellerId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.TravellerIdentificationType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.TravellerTitle;

import java.time.LocalDate;
import java.util.UUID;

public class TravellerObjectMother {
    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";
    public static final Traveller ANY = aTraveller();
    public static final com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Traveller ANY_EXTERNAL = aTravellerExternal();

    private static com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Traveller aTravellerExternal() {

        com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Phone phone = PhoneEntityObjectMother.ANY_EXTERNAL;
        com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Traveller traveller = new com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Traveller();
        traveller.setTravellerNumber(1);
        traveller.setTravellerType("ADULT");
        traveller.setTitle("MR");
        traveller.setName("name");
        traveller.setMiddleName("middleName");
        traveller.setFirstLastName("firstLastName");
        traveller.setSecondLastName("secondLastName");
        traveller.setTravellerGender("MALE");
        traveller.setDateOfBirth(LocalDate.of(1980, 12, 15));
        traveller.setAge(40);
        traveller.setNationalityCountryCode("nationalityCountryCode");
        traveller.setCountryCodeOfResidence("countryCodeOfResidence");
        traveller.setIdentification("identification");
        traveller.setIdentificationType("PASSPORT");
        traveller.setIdentificationExpirationDate(LocalDate.of(2023, 12, 15));
        traveller.setIdentificationIssueCountryCode("identificationIssueCountryCode");
        traveller.setLocalityCodeOfResidence("localityCodeOfResidence");
        traveller.setPhone(phone);
        return traveller;
    }

    private static Traveller aTraveller() {
        TravellerId id = TravellerIdObjectMother.ANY;
        Phone phone = PhoneEntityObjectMother.ANY_MODEL;
        return Traveller.builder()
                .setId(id)
                .setTravellerNumber(1)
                .setTravellerType(TravellerType.ADULT)
                .setTitle(TravellerTitle.MR)
                .setName("name")
                .setMiddleName("middleName")
                .setFirstLastName("firstLastName")
                .setSecondLastName("secondLastName")
                .setTravellerGender(TravellerGender.MALE)
                .setDateOfBirth(LocalDate.of(1980, 12, 15))
                .setAge(40)
                .setNationalityCountryCode("nationalityCountryCode")
                .setCountryCodeOfResidence("countryCodeOfResidence")
                .setIdentification("identification")
                .setIdentificationType(TravellerIdentificationType.PASSPORT)
                .setIdentificationExpirationDate(LocalDate.of(2023, 12, 15))
                .setIdentificationIssueCountryCode("identificationIssueCountryCode")
                .setLocalityCodeOfResidence("localityCodeOfResidence")
                .setPhone(phone)
                .setKey("key")
                .setItineraryBookingId(UUID.fromString(IDENTIFIER))
                .build();
    }
}
