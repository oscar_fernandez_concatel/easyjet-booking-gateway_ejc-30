package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class ItineraryBookingEntityTest {

    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";

    @Test
    public void testMethods() {

        final String expectedString = "SYSTEM";
        UUID uuid = UUID.fromString(IDENTIFIER);
        final Long expectedValue = 1L;
        Timestamp timestamp = new Timestamp(1000);
        final BigDecimal expectedQuantity = new BigDecimal("500");

        ItineraryEntity itineraryEntity = new ItineraryEntity();
        itineraryEntity.setTripType(expectedString);

        BuyerEntity buyerEntity = new BuyerEntity();
        buyerEntity.setLastName(expectedString);

        CarrierPnrEntity carrierPnrEntity = new CarrierPnrEntity();
        carrierPnrEntity.setCode(expectedString);

        List<CarrierPnrEntity> carrierPnrEntityList = new ArrayList<>();
        carrierPnrEntityList.add(carrierPnrEntity);

        TravellerEntity travellerEntity = new TravellerEntity();
        travellerEntity.setName(expectedString);
        List<TravellerEntity> travellerEntityList = new ArrayList<>();
        travellerEntityList.add(travellerEntity);

        List<PaymentDetailEntity> paymentDetails = new ArrayList<>();


        ItineraryBookingEntity itineraryBookingEntity = new ItineraryBookingEntity();
        itineraryBookingEntity.setItineraryEntity(itineraryEntity);
        itineraryBookingEntity.setBuyerEntity(buyerEntity);
        itineraryBookingEntity.setId(uuid);
        itineraryBookingEntity.setCurrency(expectedString);
        itineraryBookingEntity.setCreated(timestamp);
        itineraryBookingEntity.setCarrierPnrs(carrierPnrEntityList);
        itineraryBookingEntity.setPnr(expectedString);
        itineraryBookingEntity.setTotalFarePrice(expectedQuantity);
        itineraryBookingEntity.setTravellers(travellerEntityList);
        itineraryBookingEntity.setBookingStatus(expectedString);
        itineraryBookingEntity.setPaymentDetails(paymentDetails);

        assertEquals(itineraryBookingEntity.getItineraryEntity().getTripType(), expectedString);
        assertEquals(itineraryBookingEntity.getBuyerEntity().getLastName(), expectedString);
        assertEquals(itineraryBookingEntity.getId(), uuid);
        assertEquals(itineraryBookingEntity.getCurrency(), expectedString);
        assertEquals(itineraryBookingEntity.getCreated(), timestamp);
        assertTrue(itineraryBookingEntity.getCarrierPnrs().size() == 1);
        assertEquals(itineraryBookingEntity.getPnr(), expectedString);
        assertEquals(itineraryBookingEntity.getTotalFarePrice(), expectedQuantity);
        assertTrue(itineraryBookingEntity.getTravellers().size() == 1);
        assertEquals(itineraryBookingEntity.getBookingStatus(), expectedString);
        assertTrue(itineraryBookingEntity.getPaymentDetails().isEmpty());


    }


}
