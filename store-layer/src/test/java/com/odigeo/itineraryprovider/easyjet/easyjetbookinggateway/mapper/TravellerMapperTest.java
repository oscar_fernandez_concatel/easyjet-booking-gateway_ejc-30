package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.TravellerEntityObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.TravellerEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Traveller;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class TravellerMapperTest {
    @Test
    public void testMap() {
        TravellerEntity travellerEntity = TravellerEntityObjectMother.ANY;
        Traveller traveller = TravellerMapper.INSTANCE.map(travellerEntity);
        assertNotNull(traveller);
    }

}
