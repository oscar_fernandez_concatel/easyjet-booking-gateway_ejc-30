package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.CarrierPnrEntityObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.CarrierPnrEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.SupplierLocator;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class CarrierPnrMapperTest {

    @Test
    public void testToModel() {
        CarrierPnrEntity carrierPnrEntity = CarrierPnrEntityObjectMother.ANY;
        SupplierLocator supplierLocator = CarrierPnrMapper.INSTANCE.map(carrierPnrEntity);
        assertEquals(supplierLocator.getSupplierCode(), carrierPnrEntity.getCarrierCode());
    }
}
