package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.FlightFareInformationEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.SectionEntity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class SectionEntityObjectMother {
    public static final SectionEntity ANY = aSectionEntity();

    private static SectionEntity aSectionEntity() {
        SectionEntity sectionEntity = new SectionEntity();
        sectionEntity.setId(SectionEntityPKObjectMother.ANY);
        sectionEntity.setArrivalLocation(new BigDecimal(1234));
        sectionEntity.setDepartureLocation(new BigDecimal(4567));
        sectionEntity.setFlightEntity(FlightEntityObjectMother.ANY);
        sectionEntity.setArrivalDateTime(java.sql.Timestamp.valueOf("2022-02-20 10:10:10.0"));
        sectionEntity.setArrivalTerminal("ArrivalTerminal");
        sectionEntity.setCabinClass("TOURIST");
        sectionEntity.setDepartureDateTime(java.sql.Timestamp.valueOf("2022-02-19 10:10:10.0"));
        sectionEntity.setDepartureTerminal("DepartureTerminal");
        sectionEntity.setFareKey("FareKey");
        sectionEntity.setFareType("P");
        sectionEntity.setBookingCode("BookingCode");
        sectionEntity.setClassOfService("ClassOfService");
        List<FlightFareInformationEntity> flightFareInformationEntities = new ArrayList<>();
        FlightFareInformationEntity flightFareInformationEntity = FlightFareInformationEntityObjectMother.ANY;
        flightFareInformationEntities.add(flightFareInformationEntity);
        sectionEntity.setFlightFareInformationEntities(flightFareInformationEntities);
        return sectionEntity;
    }

}
