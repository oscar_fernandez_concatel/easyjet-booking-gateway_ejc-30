package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryBookingEntityObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.ItineraryBookingEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ItineraryBookingEntityMapperTest {

    @Test
    public void testMap() {
        ItineraryBookingEntity itineraryBookingEntity = ItineraryBookingEntityObjectMother.ANY;
        ProviderItineraryBooking providerItineraryBooking = ItineraryBookingEntityMapper.INSTANCE.map(itineraryBookingEntity);

        Assert.assertEquals(providerItineraryBooking.getPnr(), itineraryBookingEntity.getPnr());
        Assert.assertEquals(providerItineraryBooking.getBuyer().getName(), itineraryBookingEntity.getBuyerEntity().getName());
        Assert.assertEquals(providerItineraryBooking.getStatus().name(), itineraryBookingEntity.getBookingStatus());
        Assert.assertEquals(providerItineraryBooking.getTravellers().stream().count(), itineraryBookingEntity.getTravellers().stream().count());
        Assert.assertEquals(providerItineraryBooking.getTravellers().get(0).getName(), itineraryBookingEntity.getTravellers().get(0).getName());
        Assert.assertEquals(providerItineraryBooking.getItinerary().getDepartureDate().toString(), itineraryBookingEntity.getItineraryEntity().getDepartureDate());
        Assert.assertEquals(providerItineraryBooking.getItinerary().getFareDetails().getTotalFareAmount().getCurrency().toString(), itineraryBookingEntity.getItineraryEntity().getFareDetailsEntity().getTaxCurrency());


    }
}
