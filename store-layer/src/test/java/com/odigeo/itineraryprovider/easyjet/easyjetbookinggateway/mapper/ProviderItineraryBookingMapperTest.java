package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderItineraryBookingObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.ItineraryBookingEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class ProviderItineraryBookingMapperTest {
    @Test
    public void testMapToEntity() {
        ProviderItineraryBooking providerItineraryBooking = ProviderItineraryBookingObjectMother.ANY;
        ItineraryBookingEntity itineraryBookingEntity = ProviderItineraryBookingMapper.INSTANCE.mapToEntity(providerItineraryBooking);
        assertNotNull(itineraryBookingEntity);
    }

}
