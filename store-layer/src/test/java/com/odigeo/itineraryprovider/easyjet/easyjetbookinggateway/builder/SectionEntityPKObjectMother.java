package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.SectionEntityPK;

import java.util.UUID;

public class SectionEntityPKObjectMother {
    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";
    public static final SectionEntityPK ANY = aSectionEntityPK();

    private static SectionEntityPK aSectionEntityPK() {
        SectionEntityPK sectionEntityPK = new SectionEntityPK();
        sectionEntityPK.setItineraryId(UUID.fromString(IDENTIFIER));
        sectionEntityPK.setPositionId(1L);
        sectionEntityPK.setFlightNumber("flightNumber");
        return sectionEntityPK;
    }

}
