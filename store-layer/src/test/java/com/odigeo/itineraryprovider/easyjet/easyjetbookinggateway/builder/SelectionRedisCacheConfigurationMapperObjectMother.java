package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.redis.SelectionRedisCacheConfiguration;

public class SelectionRedisCacheConfigurationMapperObjectMother {

    public static final SelectionRedisCacheConfiguration ANY = getConfiguration();


    private static SelectionRedisCacheConfiguration getConfiguration() {
        SelectionRedisCacheConfiguration response = new SelectionRedisCacheConfiguration();
        response.setCacheName("TEST_CACHE");
        response.setHost("127.0.0.1");
        response.setPort(80);
        response.setExpirationTimeMillis(3000);
        response.setTimeoutMillis(500);
        return response;
    }
}
