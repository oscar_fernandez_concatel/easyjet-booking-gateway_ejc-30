package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryIdObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;
import org.testng.annotations.Test;

import java.time.LocalDateTime;

import static org.testng.Assert.assertNotNull;

public class SectionMapperTest {

    @Test
    public void testMap() {
        ItineraryId itineraryId = ItineraryIdObjectMother.ANY_INTERNAL;
        java.util.UUID id = SectionMapper.INSTANCE.map(itineraryId);
        assertNotNull(id);
    }

    @Test
    public void testMapTimeStamp() {
        java.sql.Timestamp timestamp = java.sql.Timestamp.valueOf("2022-02-19 00:00:00.0");
        LocalDateTime localDateTime = SectionMapper.INSTANCE.map(timestamp);
        assertNotNull(localDateTime);
    }

    @Test
    public void testMapLocalDateTime() {
        LocalDateTime localDateTime = LocalDateTime.of(2022, 02, 19, 0, 0, 0);
        java.sql.Timestamp timestamp = SectionMapper.INSTANCE.map(localDateTime);
        assertNotNull(timestamp);
    }

}
