package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;


import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.FareDetailsEntityObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.FareDetailsEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FareDetails;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FareDetailsEntityMapperTest {

    @Test
    public void testToModel() {
        FareDetailsEntity fareDetailsEntity = FareDetailsEntityObjectMother.ANY;
        FareDetails fareDetails = FareDetailsMapper.INSTANCE.map(fareDetailsEntity);
        Assert.assertEquals(fareDetailsEntity.getCurrency(), fareDetails.getTotalFareAmount().getCurrency().getCurrencyCode());
    }


}
