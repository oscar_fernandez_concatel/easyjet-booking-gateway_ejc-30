package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.SegmentObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.SegmentEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Segment;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class SegmentMapperTest {
    @Test
    public void testMap() {
        SegmentEntity segmentEntity = SegmentObjectMother.ANY_ENTITY;
        Segment segment = SegmentMapper.INSTANCE.map(segmentEntity);
        assertNotNull(segment);
    }

}
