package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Optional;
import java.util.UUID;

import static org.testng.Assert.assertEquals;


public class FlightFareInformationEntityPKTest {

    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";

    @Test
    public void testMethods() {

        final String expectedString = "SYSTEM";
        UUID uuid = UUID.fromString(IDENTIFIER);
        final Long expectedValue = 1L;


        FlightFareInformationEntityPK flightFareInformationEntityPK = new FlightFareInformationEntityPK();
        flightFareInformationEntityPK.setFlightNumber(expectedString);
        flightFareInformationEntityPK.setFarePassengerType(expectedString);
        flightFareInformationEntityPK.setItineraryId(uuid);
        flightFareInformationEntityPK.setPositionId(expectedValue);


        assertEquals(flightFareInformationEntityPK.getFlightNumber(), expectedString);
        assertEquals(flightFareInformationEntityPK.getFarePassengerType(), expectedString);
        assertEquals(flightFareInformationEntityPK.getItineraryId(), uuid);
        assertEquals(Optional.of(flightFareInformationEntityPK.getPositionId()), Optional.of(expectedValue));

        Assert.assertTrue(flightFareInformationEntityPK.equals(flightFareInformationEntityPK));
        Assert.assertFalse(flightFareInformationEntityPK.equals(new Object()));
        Assert.assertFalse(flightFareInformationEntityPK.equals(createEntity(expectedString, expectedString, uuid, 2L)));
        Assert.assertFalse(flightFareInformationEntityPK.equals(createEntity(expectedString, expectedString, UUID.randomUUID(), expectedValue)));
        Assert.assertTrue(flightFareInformationEntityPK.equals(createEntity(expectedString, expectedString, uuid, expectedValue)));
        Assert.assertEquals(flightFareInformationEntityPK.hashCode(), createEntity(expectedString, expectedString, uuid, expectedValue).hashCode());
    }

    private FlightFareInformationEntityPK createEntity(String flightNumber, String farePassengertype, UUID uuid, Long positionId) {
        FlightFareInformationEntityPK flightFareInformationEntityPK = new FlightFareInformationEntityPK();
        flightFareInformationEntityPK.setFlightNumber(flightNumber);
        flightFareInformationEntityPK.setFarePassengerType(farePassengertype);
        flightFareInformationEntityPK.setItineraryId(uuid);
        flightFareInformationEntityPK.setPositionId(positionId);

        return flightFareInformationEntityPK;
    }

}
