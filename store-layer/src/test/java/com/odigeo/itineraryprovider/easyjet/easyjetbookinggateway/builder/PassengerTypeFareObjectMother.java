package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Money;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PassengerTypeFare;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TravellerType;

public class PassengerTypeFareObjectMother {
    public static final PassengerTypeFare ANY = aPassengerTypeFare();

    public static PassengerTypeFare aPassengerTypeFare() {

        Money fareAmount = MoneyObjectMother.ANY;
        Money taxAmount = MoneyObjectMother.ANY;

        return PassengerTypeFare.builder()
                .setDiscount(Boolean.FALSE)
                .setPassengersPerType(1)
                .setFareAmount(fareAmount)
                .setTaxAmount(taxAmount)
                .setTravellerType(TravellerType.ADULT)
                .build();
    }
}
