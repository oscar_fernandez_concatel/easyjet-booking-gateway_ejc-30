package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FareDetails;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PassengerTypeFare;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TravellerType;

import java.util.HashMap;
import java.util.Map;

public class FareDetailsObjectMother {

    public static final FareDetails ANY = aFareDetails();

    private static FareDetails aFareDetails() {
//        final String expectedString = "SYSTEM";
//        FareDetails.Builder builder = new FareDetails.Builder();
//        builder.setTotalTaxAmount();
//        FareDetails fareDetails = new FareDetails();

        Map<TravellerType, PassengerTypeFare> passengerFares = new HashMap<>();
        passengerFares.put(TravellerType.ADULT, PassengerTypeFareObjectMother.ANY);

        return FareDetails.builder()
                .setTotalTaxAmount(MoneyObjectMother.ANY)
                .setPassengerFares(passengerFares)
                .setTotalFareAmount(MoneyObjectMother.ANY)
                .setItineraryId(ItineraryIdObjectMother.ANY_INTERNAL)
                .build();
    }
}
