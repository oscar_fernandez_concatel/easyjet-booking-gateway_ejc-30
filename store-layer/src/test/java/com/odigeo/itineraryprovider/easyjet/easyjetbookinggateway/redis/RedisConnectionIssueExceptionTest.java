package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.redis;

import org.testng.Assert;
import org.testng.annotations.Test;

public class RedisConnectionIssueExceptionTest {

    @Test
    public void redisConnectionIssueExceptionTestFirstConstructor() {
        try {
            throw new RedisConnectionIssueException("message", new Throwable("cause"));
        } catch (final Exception e) {
            Assert.assertTrue(e instanceof RedisConnectionIssueException);
            Assert.assertTrue(e.getMessage().contains("message"));
            Assert.assertTrue(e.getCause().getMessage().contains("cause"));

        }
    }

    @Test
    public void redisConnectionIssueExceptionTestSecondConstructor() {
        try {
            throw new RedisConnectionIssueException("message");
        } catch (final Exception e) {
            Assert.assertTrue(e instanceof RedisConnectionIssueException);
            Assert.assertTrue(e.getMessage().contains("message"));
        }
    }
}
