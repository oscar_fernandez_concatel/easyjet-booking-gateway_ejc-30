package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SegmentEntityTest {

    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";

    @Test
    public void testMethods() {

        final String expectedString = "SYSTEM";
        UUID uuid = UUID.fromString(IDENTIFIER);
        final Long expectedValue = 1L;
        final BigDecimal expectedQuantity = new BigDecimal("500");
        Timestamp timestamp = new Timestamp(1000);


        SegmentEntityPK segmentEntityPK = new SegmentEntityPK();
        segmentEntityPK.setPosition(expectedValue);
        segmentEntityPK.setItineraryId(uuid);

        ItineraryEntity itineraryEntity = new ItineraryEntity();
        itineraryEntity.setTripType(expectedString);

        SectionEntity sectionEntity = new SectionEntity();
        sectionEntity.setFareKey(expectedString);
        List<SectionEntity> sectionEntityList = new ArrayList<>();
        sectionEntityList.add(sectionEntity);


        SegmentEntity segmentEntity = new SegmentEntity();
        segmentEntity.setId(segmentEntityPK);
        segmentEntity.setItineraryEntity(itineraryEntity);
        segmentEntity.setSections(sectionEntityList);

        assertEquals(Optional.of(segmentEntity.getId().getPosition()), Optional.of(expectedValue));
        assertEquals(segmentEntity.getItineraryEntity().getTripType(), expectedString);
        assertTrue(segmentEntity.getSections().size() == 1);



    }

}
