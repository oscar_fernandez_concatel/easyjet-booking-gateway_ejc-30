package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.ItineraryEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TripType;

import java.math.BigDecimal;
import java.util.UUID;

public class ItineraryEntityObjectMother {

    public static final ItineraryEntity ANY = aItineraryEntity();

    private static ItineraryEntity aItineraryEntity() {
        ItineraryEntity itineraryEntity = new ItineraryEntity();
        itineraryEntity.setId(UUID.randomUUID());
        itineraryEntity.setArrivalLocation(BigDecimal.valueOf(3374));
        itineraryEntity.setDepartureLocation(BigDecimal.valueOf(9588));
        itineraryEntity.setTripType(TripType.ONE_WAY.getValue());
        itineraryEntity.setDepartureDate("2022-01-22T10:15:30");
        itineraryEntity.setFareDetailsEntity(FareDetailsEntityObjectMother.ANY);
        return itineraryEntity;
    }
}
