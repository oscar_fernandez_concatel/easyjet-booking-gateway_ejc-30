package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.CarrierPnrEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.ItineraryBookingEntity;

import java.util.UUID;

public class CarrierPnrEntityObjectMother {

    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";
    public static final CarrierPnrEntity ANY = aCarrierPnrEntity();

    private static CarrierPnrEntity aCarrierPnrEntity() {
        final String expectedString = "SYSTEM";
        UUID uuid = UUID.fromString(IDENTIFIER);

        ItineraryBookingEntity itineraryBookingEntity = new ItineraryBookingEntity();
        itineraryBookingEntity.setCurrency(expectedString);

        CarrierPnrEntity carrierPnrEntity = new CarrierPnrEntity();
        carrierPnrEntity.setCarrierCode(expectedString);
        carrierPnrEntity.setCode(expectedString);
        carrierPnrEntity.setItineraryBookingEntity(itineraryBookingEntity);
        carrierPnrEntity.setId(uuid);
        carrierPnrEntity.setItineraryBookingId(uuid);
        return  carrierPnrEntity;
    }
}
