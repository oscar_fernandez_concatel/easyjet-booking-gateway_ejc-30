package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.FlightFareInformationEntity;

public class FlightFareInformationEntityObjectMother {
    public static final FlightFareInformationEntity ANY = aFlightFareInformationEntity();

    private static FlightFareInformationEntity aFlightFareInformationEntity() {
        FlightFareInformationEntity flightFareInformationEntity = new FlightFareInformationEntity();
        flightFareInformationEntity.setId(FlightFareInformationEntityPKObjectMother.ANY);
        flightFareInformationEntity.setFareBasisId("FareBasisId");
        return flightFareInformationEntity;
    }
}
