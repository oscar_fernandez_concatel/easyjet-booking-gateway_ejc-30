package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryEntityObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.ItineraryEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItinerary;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class ProviderItineraryMapperTest {
    @Test
    public void testMap() {
        ItineraryEntity itineraryEntity = ItineraryEntityObjectMother.ANY;
        ProviderItinerary providerItinerary = ProviderItineraryMapper.INSTANCE.map(itineraryEntity);
        assertNotNull(providerItinerary);
    }

}
