package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.testng.Assert.assertEquals;


public class FareDetailsEntityTest {

    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";


    @Test
    public void testMethods() {

        final String expectedString = "SYSTEM";
        final BigDecimal expectedQuantity = new BigDecimal("500");
        UUID uuid = UUID.fromString(IDENTIFIER);
        final int expectedSize = 1;

        ItineraryEntity itineraryEntity = new ItineraryEntity();
        itineraryEntity.setTripType(expectedString);

        PassengerFaresEntity passengerFaresEntity = new PassengerFaresEntity();
        passengerFaresEntity.setFareCurrency(expectedString);
        List<PassengerFaresEntity> passengerFaresEntityList = new ArrayList<>();
        passengerFaresEntityList.add(passengerFaresEntity);

        FareDetailsEntity fareDetailsEntity = new FareDetailsEntity();
        fareDetailsEntity.setItineraryEntity(itineraryEntity);
        fareDetailsEntity.setCurrency(expectedString);
        fareDetailsEntity.setPassengerFares(passengerFaresEntityList);
        fareDetailsEntity.setTax(expectedQuantity);
        fareDetailsEntity.setTotalFareAmount(expectedQuantity);
        fareDetailsEntity.setItineraryId(uuid);
        fareDetailsEntity.setTaxCurrency(expectedString);

        assertEquals(expectedString, fareDetailsEntity.getCurrency());
        assertEquals(expectedSize, fareDetailsEntity.getPassengerFares().size());
        assertEquals(expectedQuantity, fareDetailsEntity.getTax());
        assertEquals(expectedQuantity, fareDetailsEntity.getTotalFareAmount());
        assertEquals(uuid, fareDetailsEntity.getItineraryId());
        assertEquals(expectedString, fareDetailsEntity.getTaxCurrency());
        assertEquals(expectedString, fareDetailsEntity.getItineraryEntity().getTripType());

    }

}
