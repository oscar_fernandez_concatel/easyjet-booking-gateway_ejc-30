package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.itinerary;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryBookingEntityObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.ItineraryNotFoundException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class ProviderItineraryBookingRepositoryImplTest {

    @Mock
    private EntityManager entityManager;

    @BeforeMethod
    public void setUp() {

        MockitoAnnotations.openMocks(this);

        ConfigurationEngine.init(new AbstractModule() {
            @Override
            protected void configure() {
                bind(ProviderItineraryBookingRepository.class).toInstance(new ProviderItineraryBookingRepositoryImpl(
                        entityManager));
            }
        });
        initEntityManager();
    }

    @Test()
    public void whenFindByItineraryBookingIdAndItineraryBookingEntityIsNotNull() throws ItineraryNotFoundException {

        when(entityManager.find(any(), any()))
                .thenReturn(ItineraryBookingEntityObjectMother.ANY);

        ProviderItineraryBookingRepository providerItineraryBookingRepository =
                ConfigurationEngine.getInstance(ProviderItineraryBookingRepository.class);

        ProviderItineraryBooking result =
                providerItineraryBookingRepository.find(UUID.randomUUID());

        Assert.assertNotNull(result);
    }

    @Test(expectedExceptions = ItineraryNotFoundException.class)
    public void whenFindByItineraryBookingIdAndItineraryBookingEntityIsNullThenThrowException()
            throws ItineraryNotFoundException {

        when(entityManager.find(any(), any()))
                .thenReturn(null);

        ProviderItineraryBookingRepository providerItineraryBookingRepository =
                ConfigurationEngine.getInstance(ProviderItineraryBookingRepository.class);

        providerItineraryBookingRepository.find(UUID.randomUUID());
    }

    @Test()
    public void whenSaveItineraryBooking() throws ItineraryNotFoundException {

        when(entityManager.find(any(), any()))
                .thenReturn(ItineraryBookingEntityObjectMother.ANY);

        ProviderItineraryBookingRepository providerItineraryBookingRepository =
                ConfigurationEngine.getInstance(ProviderItineraryBookingRepository.class);

        ProviderItineraryBooking result =
                providerItineraryBookingRepository.find(UUID.randomUUID());

        providerItineraryBookingRepository.saveItineraryBooking(result);
    }

    @Test()
    public void whenUpdateItineraryBooking() throws ItineraryNotFoundException {

        when(entityManager.find(any(), any()))
                .thenReturn(ItineraryBookingEntityObjectMother.ANY);

        ProviderItineraryBookingRepository providerItineraryBookingRepository =
                ConfigurationEngine.getInstance(ProviderItineraryBookingRepository.class);

        ProviderItineraryBooking result =
                providerItineraryBookingRepository.find(UUID.randomUUID());

        providerItineraryBookingRepository.updateItineraryBooking(result);
    }

    @Test()
    public void whenFindItineraryBookingByItineraryId() {

        ProviderItineraryBookingRepository providerItineraryBookingRepository =
                ConfigurationEngine.getInstance(ProviderItineraryBookingRepository.class);

        Optional<ProviderItineraryBooking> result;
        result = providerItineraryBookingRepository.findItineraryBookingByItineraryId(UUID.randomUUID());

        Assert.assertNotNull(result);
        Assert.assertTrue(result.isPresent());
    }

    private void initEntityManager() {
        TypedQuery query = Mockito.mock(TypedQuery.class);
        when(entityManager.createNamedQuery(anyString(), any())).thenReturn(query);
        when(query.getResultList()).thenReturn(List.of(ItineraryBookingEntityObjectMother.ANY));
    }
}
