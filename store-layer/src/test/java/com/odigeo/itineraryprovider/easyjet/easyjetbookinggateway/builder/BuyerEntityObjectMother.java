package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.BuyerEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Buyer;

public class BuyerEntityObjectMother {

    public static final BuyerEntity ANY = aBuyerEntity();
    public static final Buyer ANY_MODEL = aBuyer();

    private static BuyerEntity aBuyerEntity() {
        final String expectedString = "SYSTEM";
        BuyerEntity buyerEntity = new BuyerEntity();
        buyerEntity.setName(expectedString);
        buyerEntity.setLastName(expectedString);
        buyerEntity.setMail(expectedString);
        buyerEntity.setPhoneEntity(PhoneEntityObjectMother.ANY);
        buyerEntity.setCountryCode(expectedString);
        buyerEntity.setAddress(expectedString);
        buyerEntity.setCity(expectedString);
        buyerEntity.setZipCode(expectedString);
        return buyerEntity;
    }

    private static Buyer aBuyer() {
        return Buyer.builder()
                .setName("name")
                .setMail("mail@odigeo.com")
                .setPhoneNumber("phoneNumber")
                .setCountryCode("countryCode")
                .setAddress("address")
                .setCityName("cityName")
                .setZipCode("zipCode")
                .setLastNames("lastNames")
                .setPrimeMember(false)
                .build();
    }
}
