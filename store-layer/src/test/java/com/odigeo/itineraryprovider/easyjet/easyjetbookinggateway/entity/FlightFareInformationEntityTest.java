package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import org.testng.annotations.Test;

import java.util.UUID;

import static org.testng.Assert.assertEquals;


public class FlightFareInformationEntityTest {

    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";

    @Test
    public void testMethods() {

        final String expectedString = "SYSTEM";
        UUID uuid = UUID.fromString(IDENTIFIER);
        final Long expectedValue = 1L;

        FlightFareInformationEntityPK flightFareInformationEntityPK = new FlightFareInformationEntityPK();
        flightFareInformationEntityPK.setFlightNumber(expectedString);

        SectionEntityPK sectionEntityPK = new SectionEntityPK();
        sectionEntityPK.setFlightNumber(expectedString);
        sectionEntityPK.setItineraryId(uuid);
        sectionEntityPK.setPositionId(expectedValue);

        SectionEntity sectionEntity = new SectionEntity();
        sectionEntity.setId(sectionEntityPK);

        FlightFareInformationEntity flightFareInformationEntity = new FlightFareInformationEntity();
        flightFareInformationEntity.setFareBasisId(expectedString);
        flightFareInformationEntity.setSectionEntity(sectionEntity);
        flightFareInformationEntity.setId(flightFareInformationEntityPK);

        assertEquals(expectedString, flightFareInformationEntity.getFareBasisId());
        assertEquals(expectedString, flightFareInformationEntity.getSectionEntity().getId().getFlightNumber());
        assertEquals(expectedString, flightFareInformationEntity.getId().getFlightNumber());
    }

}
