package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Optional;
import java.util.UUID;

import static org.testng.Assert.assertEquals;


public class FlightEntityPKTest {

    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";

    @Test
    public void testMethods() {

        final String expectedString = "SYSTEM";
        UUID uuid = UUID.fromString(IDENTIFIER);
        final Long expectedValue = 1L;

        FlightEntityPK flightEntityPK = new FlightEntityPK();
        flightEntityPK.setFlightNumber(expectedString);
        flightEntityPK.setItineraryId(uuid);
        flightEntityPK.setPositionId(expectedValue);


        assertEquals(expectedString, flightEntityPK.getFlightNumber());
        assertEquals(Optional.of(expectedValue), Optional.of(flightEntityPK.getPositionId()));
        assertEquals(uuid, flightEntityPK.getItineraryId());

        Assert.assertTrue(flightEntityPK.equals(flightEntityPK));
        Assert.assertFalse(flightEntityPK.equals(null));
        Assert.assertFalse(flightEntityPK.equals(new Object()));
        Assert.assertFalse(flightEntityPK.equals(createFlightEntityPK(expectedString, uuid, 2L)));
        Assert.assertFalse(flightEntityPK.equals(createFlightEntityPK(expectedString, UUID.randomUUID(), expectedValue)));
        Assert.assertTrue(flightEntityPK.equals(createFlightEntityPK(expectedString, uuid, expectedValue)));
        Assert.assertEquals(flightEntityPK.hashCode(), createFlightEntityPK(expectedString, uuid, expectedValue).hashCode());
    }

    private FlightEntityPK createFlightEntityPK(String flightNumber, UUID uuid, Long positionId) {
        FlightEntityPK flightEntityPK = new FlightEntityPK();
        flightEntityPK.setFlightNumber(flightNumber);
        flightEntityPK.setItineraryId(uuid);
        flightEntityPK.setPositionId(positionId);

        return flightEntityPK;
    }
}
