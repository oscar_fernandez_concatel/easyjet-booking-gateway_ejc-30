package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.UUID;

import static org.testng.Assert.assertEquals;

public class PaymentDetailEntityTest {
    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";

    @Test
    public void testMethods() {

        final String expectedString = "SYSTEM";
        UUID uuid = UUID.fromString(IDENTIFIER);
        final Long expectedValue = 1L;
        final BigDecimal expectedQuantity = new BigDecimal("500");
        Date expectedDate = new Date(1L);
        Timestamp timestamp = new Timestamp(1000);

        ItineraryEntity itineraryEntity = new ItineraryEntity();
        itineraryEntity.setTripType(expectedString);

        ItineraryBookingEntity itineraryBookingEntity = new ItineraryBookingEntity();
        itineraryBookingEntity.setItineraryEntity(itineraryEntity);


        PaymentDetailEntity paymentDetail = new PaymentDetailEntity();
        paymentDetail.setId(uuid);
        paymentDetail.setPaymentInstrumentToken(expectedString);
        paymentDetail.setItineraryBookingEntity(itineraryBookingEntity);
        paymentDetail.setCreated(timestamp);
        paymentDetail.setItineraryBookingId(uuid);



        assertEquals(paymentDetail.getId(), uuid);
        assertEquals(paymentDetail.getPaymentInstrumentToken(), expectedString);
        assertEquals(paymentDetail.getItineraryBookingEntity().getItineraryEntity().getTripType(), expectedString);
        assertEquals(paymentDetail.getCreated(), timestamp);
        assertEquals(paymentDetail.getItineraryBookingId(), uuid);


    }

}
