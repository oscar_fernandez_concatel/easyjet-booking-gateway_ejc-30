package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Optional;
import java.util.UUID;

import static org.testng.Assert.assertEquals;

public class SectionEntityPKTest {

    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";

    @Test
    public void testMethods() {

        final String expectedString = "SYSTEM";
        UUID uuid = UUID.fromString(IDENTIFIER);
        final Long expectedValue = 1L;
        final BigDecimal expectedQuantity = new BigDecimal("500");
        Timestamp timestamp = new Timestamp(1000);


        SectionEntityPK sectionEntityPK = new SectionEntityPK();
        sectionEntityPK.setFlightNumber(expectedString);
        sectionEntityPK.setItineraryId(uuid);
        sectionEntityPK.setPositionId(expectedValue);

        assertEquals(sectionEntityPK.getFlightNumber(), expectedString);
        assertEquals(sectionEntityPK.getItineraryId(), uuid);
        assertEquals(Optional.of(sectionEntityPK.getPositionId()), Optional.of(expectedValue));

        Assert.assertTrue(sectionEntityPK.equals(sectionEntityPK));
        Assert.assertFalse(sectionEntityPK.equals(new Object()));
        Assert.assertFalse(sectionEntityPK.equals(createEntity(expectedString, uuid, 2L)));
        Assert.assertFalse(sectionEntityPK.equals(createEntity(expectedString, UUID.randomUUID(), expectedValue)));
        Assert.assertTrue(sectionEntityPK.equals(createEntity(expectedString, uuid, expectedValue)));
        Assert.assertEquals(sectionEntityPK.hashCode(), createEntity(expectedString, uuid, expectedValue).hashCode());

    }

    private SectionEntityPK createEntity(String flightNumber, UUID uuid, Long positionId) {
        SectionEntityPK sectionEntityPK = new SectionEntityPK();
        sectionEntityPK.setFlightNumber(flightNumber);
        sectionEntityPK.setItineraryId(uuid);
        sectionEntityPK.setPositionId(positionId);

        return sectionEntityPK;
    }

}
