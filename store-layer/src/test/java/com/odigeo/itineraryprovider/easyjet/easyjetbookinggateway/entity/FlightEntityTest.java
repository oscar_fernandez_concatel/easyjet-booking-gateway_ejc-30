package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import org.testng.annotations.Test;

import java.util.Optional;
import java.util.UUID;

import static org.testng.Assert.assertEquals;


public class FlightEntityTest {

    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";


    @Test
    public void testMethods() {

        final String expectedString = "SYSTEM";
        UUID uuid = UUID.fromString(IDENTIFIER);
        final Long expectedValue = 1L;

        SectionEntityPK sectionEntityPK = new SectionEntityPK();
        sectionEntityPK.setFlightNumber(expectedString);
        sectionEntityPK.setItineraryId(uuid);
        sectionEntityPK.setPositionId(expectedValue);

        SectionEntity sectionEntity = new SectionEntity();
        sectionEntity.setId(sectionEntityPK);


        FlightEntity flightEntity = new FlightEntity();
        flightEntity.setId(sectionEntityPK);
        flightEntity.setSectionEntity(sectionEntity);
        flightEntity.setMarketingCarrier(expectedString);
        flightEntity.setOperatingCarrier(expectedString);
        flightEntity.setPositionId(expectedValue);

        assertEquals(expectedString, flightEntity.getId().getFlightNumber());
        assertEquals(expectedString, flightEntity.getSectionEntity().getId().getFlightNumber());
        assertEquals(expectedString, flightEntity.getMarketingCarrier());
        assertEquals(expectedString, flightEntity.getOperatingCarrier());
        assertEquals(Optional.of(expectedValue), Optional.of(flightEntity.getPositionId()));

    }

}
