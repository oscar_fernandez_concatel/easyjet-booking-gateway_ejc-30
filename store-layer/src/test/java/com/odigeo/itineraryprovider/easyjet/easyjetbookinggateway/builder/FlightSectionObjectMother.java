package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CabinClass;
import com.odigeo.suppliergatewayapi.v25.itinerary.ItineraryId;
import com.odigeo.suppliergatewayapi.v25.itinerary.fare.FlightFareInformationPassenger;
import com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.BaggageAllowance;
import com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.Flight;
import com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.FlightSection;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Map;

public class FlightSectionObjectMother {
    public static final FlightSection ANY_EXTERNAL = aFlightSectionExternal();
    public static final com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightSection ANY_INTERNAL = aFlightSectionInternal();

    public static FlightSection aFlightSectionExternal() {

        BaggageAllowance baggageAllowance = BaggageAllowanceObjectMother.ANY_EXTERNAL;
        ItineraryId itineraryId = ItineraryIdObjectMother.ANY_EXTERNAL;
        Flight flight = FlightObjectMother.ANY_EXTERNAL;
        Map<String, FlightFareInformationPassenger> flightFareInformations = Collections.singletonMap("PUBLIC",
                FlightFareInformationPassengerObjectMother.ANY);

        FlightSection section = new FlightSection();
        section.setArrivalDate(LocalDateTime.of(2021, 12, 15, 10, 0, 0));
        section.setFlight(flight);
        section.setFlightFareInformations(flightFareInformations);
        section.setArrivalTerminal("BCN");
        section.setArrivalGeoNodeId(5678);
        section.setBaggageAllowance(baggageAllowance);
        section.setCabinClass("TOURIST");
        section.setDepartureDate(LocalDateTime.of(2021, 12, 15, 10, 0, 0));
        section.setDepartureGeoNodeId(1234);
        section.setDepartureTerminal("EZE");
        section.setId(itineraryId);
        section.setSegmentPosition(1);
        return section;
    }

    public static com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightSection aFlightSectionInternal() {

        com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.BaggageAllowance baggageAllowance = BaggageAllowanceObjectMother.ANY_INTERNAL;
        com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId itineraryId = ItineraryIdObjectMother.ANY_INTERNAL;

        return com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightSection.builder()
                .setFlight(FlightObjectMother.ANY_INTERNAL)
                .setFlightFareInformation(FlightFareInformationObjectMother.ANY)
                .setArrivalDate(LocalDateTime.of(2021, 12, 15, 10, 0, 0))
                .setArrivalTerminal("BCN")
                .setArrivalGeoNodeId(5678)
                .setBaggageAllowance(baggageAllowance)
                .setCabinClass(CabinClass.TOURIST)
                .setDepartureDate(LocalDateTime.of(2021, 12, 15, 10, 0, 0))
                .setDepartureGeoNodeId(1234)
                .setDepartureTerminal("EZE")
                .setId(itineraryId)
                .setSegmentPosition(1)
                .build();
    }
}
