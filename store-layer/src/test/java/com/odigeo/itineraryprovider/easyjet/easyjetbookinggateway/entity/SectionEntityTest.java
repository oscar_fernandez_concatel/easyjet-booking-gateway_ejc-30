package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SectionEntityTest {

    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";

    @Test
    public void testMethods() {

        final String expectedString = "SYSTEM";
        UUID uuid = UUID.fromString(IDENTIFIER);
        final Long expectedValue = 1L;
        final BigDecimal expectedQuantity = new BigDecimal("500");
        Timestamp timestamp = new Timestamp(1000);

        SectionEntityPK sectionEntityPK = new SectionEntityPK();
        sectionEntityPK.setFlightNumber(expectedString);

        SegmentEntityPK segmentEntityPK = new SegmentEntityPK();
        segmentEntityPK.setPosition(expectedValue);

        SegmentEntity segmentEntity = new SegmentEntity();
        segmentEntity.setId(segmentEntityPK);

        FlightEntity flightEntity = new FlightEntity();
        flightEntity.setOperatingCarrier(expectedString);

        FlightFareInformationEntity flightFareInformationEntity = new FlightFareInformationEntity();
        flightFareInformationEntity.setFareBasisId(expectedString);

        List<FlightFareInformationEntity> flightFareInformationEntityList = new ArrayList<>();
        flightFareInformationEntityList.add(flightFareInformationEntity);


        SectionEntity sectionEntity = new SectionEntity();
        sectionEntity.setId(sectionEntityPK);
        sectionEntity.setSegmentEntity(segmentEntity);
        sectionEntity.setFlightEntity(flightEntity);
        sectionEntity.setArrivalDateTime(timestamp);
        sectionEntity.setArrivalLocation(expectedQuantity);
        sectionEntity.setArrivalTerminal(expectedString);
        sectionEntity.setBookingCode(expectedString);
        sectionEntity.setCabinClass(expectedString);
        sectionEntity.setClassOfService(expectedString);
        sectionEntity.setDepartureDateTime(timestamp);
        sectionEntity.setFareKey(expectedString);
        sectionEntity.setFareType(expectedString);
        sectionEntity.setDepartureLocation(expectedQuantity);
        sectionEntity.setDepartureTerminal(expectedString);
        sectionEntity.setFlightFareInformationEntities(flightFareInformationEntityList);

        assertEquals(sectionEntity.getId().getFlightNumber(), expectedString);
        assertEquals(Optional.of(sectionEntity.getSegmentEntity().getId().getPosition()), Optional.of(expectedValue));
        assertEquals(sectionEntity.getFlightEntity().getOperatingCarrier(), expectedString);
        assertEquals(sectionEntity.getArrivalDateTime(), timestamp);
        assertEquals(sectionEntity.getArrivalLocation(), expectedQuantity);
        assertEquals(sectionEntity.getArrivalTerminal(), expectedString);
        assertEquals(sectionEntity.getBookingCode(), expectedString);
        assertEquals(sectionEntity.getCabinClass(), expectedString);
        assertEquals(sectionEntity.getClassOfService(), expectedString);
        assertEquals(sectionEntity.getDepartureDateTime(), timestamp);
        assertEquals(sectionEntity.getFareKey(), expectedString);
        assertEquals(sectionEntity.getFareType(), expectedString);
        assertEquals(sectionEntity.getDepartureLocation(), expectedQuantity);
        assertEquals(sectionEntity.getDepartureTerminal(), expectedString);
        assertTrue(sectionEntity.getFlightFareInformationEntities().size() == 1);

    }

}
