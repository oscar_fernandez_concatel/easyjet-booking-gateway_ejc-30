package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.FlightEntity;

public class FlightEntityObjectMother {
    public static final FlightEntity ANY = aFlightEntity();

    private static FlightEntity aFlightEntity() {
        FlightEntity flightEntity = new FlightEntity();
        flightEntity.setId(SectionEntityPKObjectMother.ANY);
        flightEntity.setMarketingCarrier("marketingCarrier");
        flightEntity.setOperatingCarrier("operatingCarrier");
        flightEntity.setPositionId(1L);
        return flightEntity;
    }
}
