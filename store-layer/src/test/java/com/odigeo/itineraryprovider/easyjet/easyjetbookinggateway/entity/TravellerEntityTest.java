package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.UUID;

import static org.testng.Assert.assertEquals;

public class TravellerEntityTest {

    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";

    @Test
    public void testMethods() {

        final String expectedString = "SYSTEM";
        UUID uuid = UUID.fromString(IDENTIFIER);
        final Long expectedValue = 1L;
        final BigDecimal expectedQuantity = new BigDecimal("500");
        Date expectedDate = new Date(1L);


        PhoneEntity phoneEntity = new PhoneEntity();
        phoneEntity.setPhoneNumber(expectedString);

        ItineraryBookingEntity itineraryBookingEntity = new ItineraryBookingEntity();
        itineraryBookingEntity.setPnr(expectedString);

        TravellerEntity travellerEntity = new TravellerEntity();
        travellerEntity.setName(expectedString);
        travellerEntity.setPhoneEntity(phoneEntity);
        travellerEntity.setTravellerTitle(expectedString);
        travellerEntity.setAge(expectedQuantity);
        travellerEntity.setId(uuid);
        travellerEntity.setDateOfBirth(expectedDate);
        travellerEntity.setFirstLastName(expectedString);
        travellerEntity.setGender(expectedString);
        travellerEntity.setIdentification(expectedString);
        travellerEntity.setIdentificationCountryCode(expectedString);
        travellerEntity.setIdentificationExpirationDate(expectedDate);
        travellerEntity.setIdentificationType(expectedString);
        travellerEntity.setItineraryBookingEntity(itineraryBookingEntity);
        travellerEntity.setGender(expectedString);
        travellerEntity.setItineraryBookingId(uuid);
        travellerEntity.setKey(expectedString);
        travellerEntity.setMiddleName(expectedString);
        travellerEntity.setPhoneId(uuid);
        travellerEntity.setNationalityCountryCode(expectedString);
        travellerEntity.setResidenceCountryCode(expectedString);
        travellerEntity.setSecondLastName(expectedString);
        travellerEntity.setTravellerNumber(expectedQuantity);
        travellerEntity.setType(expectedString);

        assertEquals(travellerEntity.getName(), expectedString);
        assertEquals(travellerEntity.getPhoneEntity().getPhoneNumber(), expectedString);
        assertEquals(travellerEntity.getTravellerTitle(), expectedString);
        assertEquals(travellerEntity.getAge(), expectedQuantity);
        assertEquals(travellerEntity.getId(), uuid);
        assertEquals(travellerEntity.getDateOfBirth(), expectedDate);
        assertEquals(travellerEntity.getFirstLastName(), expectedString);
        assertEquals(travellerEntity.getGender(), expectedString);
        assertEquals(travellerEntity.getIdentification(), expectedString);
        assertEquals(travellerEntity.getIdentificationCountryCode(), expectedString);
        assertEquals(travellerEntity.getIdentificationExpirationDate(), expectedDate);
        assertEquals(travellerEntity.getIdentificationType(), expectedString);
        assertEquals(travellerEntity.getItineraryBookingEntity().getPnr(), expectedString);
        assertEquals(travellerEntity.getGender(), expectedString);
        assertEquals(travellerEntity.getItineraryBookingId(), uuid);
        assertEquals(travellerEntity.getKey(), expectedString);
        assertEquals(travellerEntity.getMiddleName(), expectedString);
        assertEquals(travellerEntity.getPhoneId(), uuid);
        assertEquals(travellerEntity.getNationalityCountryCode(), expectedString);
        assertEquals(travellerEntity.getResidenceCountryCode(), expectedString);
        assertEquals(travellerEntity.getSecondLastName(), expectedString);
        assertEquals(travellerEntity.getTravellerNumber(), expectedQuantity);
        assertEquals(travellerEntity.getType(), expectedString);

    }

}
