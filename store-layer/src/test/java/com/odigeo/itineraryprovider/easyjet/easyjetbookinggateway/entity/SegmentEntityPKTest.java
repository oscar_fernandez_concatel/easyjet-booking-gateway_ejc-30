package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Optional;
import java.util.UUID;

import static org.testng.Assert.assertEquals;

public class SegmentEntityPKTest {

    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";

    @Test
    public void testMethods() {

        UUID uuid = UUID.fromString(IDENTIFIER);
        final Long expectedValue = 1L;

        SegmentEntityPK segmentEntityPK = new SegmentEntityPK();
        segmentEntityPK.setItineraryId(uuid);
        segmentEntityPK.setPosition(expectedValue);


        assertEquals(Optional.of(segmentEntityPK.getPosition()), Optional.of(expectedValue));
        assertEquals(segmentEntityPK.getItineraryId(), uuid);

        Assert.assertTrue(segmentEntityPK.equals(segmentEntityPK));
        Assert.assertFalse(segmentEntityPK.equals(new Object()));
        Assert.assertFalse(segmentEntityPK.equals(createEntity(uuid, 2L)));
        Assert.assertFalse(segmentEntityPK.equals(createEntity(UUID.randomUUID(), expectedValue)));
        Assert.assertEquals(segmentEntityPK.hashCode(), createEntity(uuid, expectedValue).hashCode());
    }

    private SegmentEntityPK createEntity(UUID uuid, Long position) {
        SegmentEntityPK segmentEntityPK = new SegmentEntityPK();
        segmentEntityPK.setItineraryId(uuid);
        segmentEntityPK.setPosition(position);

        return segmentEntityPK;
    }

}
