package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;


import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Carrier;

public class CarrierObjectMother {
    public static final Carrier ANY = aCarrier();

    private static Carrier aCarrier() {
        return Carrier.builder()
                .setCode("CODE")
                .setIataCode("IATACODE")
                .setName("NAME")
                .setShowBaggagePolicy(true)
                .build();
    }

}
