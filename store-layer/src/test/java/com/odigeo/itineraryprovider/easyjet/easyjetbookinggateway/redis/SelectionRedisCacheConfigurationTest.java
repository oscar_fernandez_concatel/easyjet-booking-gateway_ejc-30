package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.redis;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.SelectionRedisCacheConfigurationMapperObjectMother;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SelectionRedisCacheConfigurationTest {

    private SelectionRedisCacheConfiguration selectionRedisCacheConfiguration;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        selectionRedisCacheConfiguration = new SelectionRedisCacheConfigurationMapperObjectMother().ANY;
    }

    @Test
    public void testCheckIsCreated() {
        Assert.assertNotNull(selectionRedisCacheConfiguration);
        Assert.assertNotNull(selectionRedisCacheConfiguration.getCacheName());
        Assert.assertNotNull(selectionRedisCacheConfiguration.getExpirationTimeMillis());
        Assert.assertNotNull(selectionRedisCacheConfiguration.getHost());
        Assert.assertNotNull(selectionRedisCacheConfiguration.getPort());
        Assert.assertNotNull(selectionRedisCacheConfiguration.getTimeoutMillis());

        SelectionRedisCacheConfiguration sel = new SelectionRedisCacheConfiguration();
        sel.setTimeoutMillis(selectionRedisCacheConfiguration.getTimeoutMillis());
        sel.setPort(selectionRedisCacheConfiguration.getPort());
        sel.setHost(selectionRedisCacheConfiguration.getHost());
        sel.setExpirationTimeMillis(selectionRedisCacheConfiguration.getExpirationTimeMillis());
        sel.setTimeoutMillis(selectionRedisCacheConfiguration.getTimeoutMillis());

        selectionRedisCacheConfiguration.setCacheName("NEW_CACHE_NAME");
        selectionRedisCacheConfiguration.setHost("127.0.0.2");
        selectionRedisCacheConfiguration.setPort(90);
        selectionRedisCacheConfiguration.setTimeoutMillis(100);
        selectionRedisCacheConfiguration.setExpirationTimeMillis(200);

        Assert.assertNotEquals(selectionRedisCacheConfiguration.getCacheName(), sel.getCacheName());
        Assert.assertNotEquals(selectionRedisCacheConfiguration.getExpirationTimeMillis(), sel.getExpirationTimeMillis());
        Assert.assertNotEquals(selectionRedisCacheConfiguration.getHost(), sel.getHost());
        Assert.assertNotEquals(selectionRedisCacheConfiguration.getPort(), sel.getPort());
        Assert.assertNotEquals(selectionRedisCacheConfiguration.getTimeoutMillis(), sel.getTimeoutMillis());
    }
}
