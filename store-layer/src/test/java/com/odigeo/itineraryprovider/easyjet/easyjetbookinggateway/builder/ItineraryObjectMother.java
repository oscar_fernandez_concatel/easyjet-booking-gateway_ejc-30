package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FareDetails;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Itinerary;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Segment;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TripType;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ItineraryObjectMother {
    public static final Itinerary ANY = aItinerary();

    public static Itinerary aItinerary() {
        FareDetails fareDetails = FareDetailsObjectMother.ANY;
        Segment segment = SegmentObjectMother.ANY;
        List<Segment> segments = new ArrayList<>();
        segments.add(segment);

        ItineraryId id = ItineraryIdObjectMother.ANY_INTERNAL;

        return Itinerary.builder()
                .setFareDetails(fareDetails)
                .setSegments(segments)
                .setId(id)
                .setDepartureDate(LocalDateTime.of(2021, 12, 15, 10, 0, 0))
                .setArrivalGeoNodeId(5678)
                .setReturnDate(LocalDateTime.of(2021, 12, 15, 10, 0, 0))
                .setTripType(TripType.ONE_WAY)
                .setDepartureGeoNodeId(1234)
                .build();
    }
}
