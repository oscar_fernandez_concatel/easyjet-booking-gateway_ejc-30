package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.PaymentDetailObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.PaymentDetailEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.PaymentDetail;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PaymentDetailEntityMapperTest {

    @Test
    public void testToEntity() {
        PaymentDetail paymentDetail = PaymentDetailObjectMother.ANY;
        PaymentDetailEntity paymentDetailEntity = PaymentDetailEntityMapper.INSTANCE.toEntity(paymentDetail);

        Assert.assertNotNull(paymentDetailEntity);
        Assert.assertEquals(paymentDetailEntity.getId().toString(), paymentDetail.getId().toString());

    }
}
