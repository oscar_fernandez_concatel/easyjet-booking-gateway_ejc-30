package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryEntityObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.ItineraryEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Itinerary;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TripType;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.UUID;

public class ItineraryMapperTest {

    @Test
    public void testMapFromItineraryEntity() {
        ItineraryEntity itineraryEntity = ItineraryEntityObjectMother.ANY;
        Itinerary itinerary = ItineraryMapper.INSTANCE.map(itineraryEntity);

        Assert.assertNotNull(itinerary);
        Assert.assertEquals(itinerary.getArrivalGeoNodeId().intValue(), itineraryEntity.getArrivalLocation().intValue());

    }

    @Test
    public void testMapFromTripTypeString() {
        TripType tripType = ItineraryMapper.INSTANCE.map("ONE_WAY");

        Assert.assertNotNull(tripType);
        Assert.assertTrue(tripType.name().equals("ONE_WAY"));
    }

    @Test
    public void testMapFromItineraryId() {
        UUID uuid = UUID.randomUUID();
        ItineraryId itineraryId = ItineraryMapper.INSTANCE.map(uuid);

        Assert.assertNotNull(itineraryId);
    }
}
