package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.UUID;

import static org.testng.Assert.assertEquals;

public class PhoneEntityTest {

    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";

    @Test
    public void testMethods() {

        final String expectedString = "SYSTEM";
        UUID uuid = UUID.fromString(IDENTIFIER);
        final Long expectedValue = 1L;
        final BigDecimal expectedQuantity = new BigDecimal("500");

        TravellerEntity travellerEntity = new TravellerEntity();
        travellerEntity.setName(expectedString);

        BuyerEntity buyerEntity = new BuyerEntity();
        buyerEntity.setLastName(expectedString);

        PhoneEntity phoneEntity = new PhoneEntity();
        phoneEntity.setTraveller(travellerEntity);
        phoneEntity.setPhoneNumber(expectedString);
        phoneEntity.setType(expectedString);
        phoneEntity.setPrefix(expectedString);
        phoneEntity.setId(uuid);
        phoneEntity.setBuyer(buyerEntity);

        assertEquals(phoneEntity.getTraveller().getName(), expectedString);
        assertEquals(phoneEntity.getPhoneNumber(), expectedString);
        assertEquals(phoneEntity.getType(), expectedString);
        assertEquals(phoneEntity.getPrefix(), expectedString);
        assertEquals(phoneEntity.getId(), uuid);
        assertEquals(phoneEntity.getBuyer().getLastName(), expectedString);


    }

}
