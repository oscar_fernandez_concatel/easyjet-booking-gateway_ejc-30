package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.BuyerEntityObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity.BuyerEntity;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Buyer;
import static org.testng.Assert.assertEquals;
import org.testng.annotations.Test;

import java.util.UUID;

public class BuyerMapperTest {

    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";


    @Test
    public void testToModel() {

        BuyerEntity buyerEntity = BuyerEntityObjectMother.ANY;
        Buyer buyer = BuyerMapper.INSTANCE.map(buyerEntity);
        assertEquals(buyer.getName(), buyerEntity.getName());
    }

    @Test
    public void testToModel2() {
        UUID uuid = UUID.fromString(IDENTIFIER);

        BuyerEntity buyerEntity = BuyerEntityObjectMother.ANY;
        Buyer buyer = BuyerMapper.INSTANCE.map(buyerEntity);
        BuyerEntity buyerEntity1 = BuyerMapper.INSTANCE.map(buyer, uuid);

        assertEquals(buyer.getName(), buyerEntity1.getName());
    }

}
