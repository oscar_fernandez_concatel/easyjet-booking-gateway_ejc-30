package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.entity;

import org.testng.annotations.Test;

import java.util.UUID;

import static org.testng.Assert.assertEquals;


public class CarrierPnrEntityTest {

    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";

    @Test
    public void testMethods() {
        final String expectedString = "SYSTEM";
        UUID uuid = UUID.fromString(IDENTIFIER);

        ItineraryBookingEntity itineraryBookingEntity = new ItineraryBookingEntity();
        itineraryBookingEntity.setCurrency(expectedString);

        CarrierPnrEntity carrierPnrEntity = new CarrierPnrEntity();
        carrierPnrEntity.setCarrierCode(expectedString);
        carrierPnrEntity.setCode(expectedString);
        carrierPnrEntity.setItineraryBookingEntity(itineraryBookingEntity);
        carrierPnrEntity.setId(uuid);
        carrierPnrEntity.setItineraryBookingId(uuid);

        assertEquals(expectedString, carrierPnrEntity.getCarrierCode());
        assertEquals(expectedString, carrierPnrEntity.getCode());
        assertEquals(expectedString, carrierPnrEntity.getItineraryBookingEntity().getCurrency());
        assertEquals(uuid, carrierPnrEntity.getId());
        assertEquals(uuid, carrierPnrEntity.getItineraryBookingId());




    }

}
