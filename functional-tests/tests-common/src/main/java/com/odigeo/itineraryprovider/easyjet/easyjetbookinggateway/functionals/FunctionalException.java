package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.functionals;

public class FunctionalException extends RuntimeException {
    public FunctionalException(Throwable cause) {
        super(cause);
    }
}
