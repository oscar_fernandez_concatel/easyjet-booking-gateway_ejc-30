INSERT into GE_SCRIPTS_LOG (ID, EXECUTION_DATE, MODULE)
values ('EJC-44/01_create_table_phone.sql', systimestamp, 'easyjet-booking-gateway');

INSERT into GE_SCRIPTS_LOG (ID, EXECUTION_DATE, MODULE)
values ('EJC-44/02_create_table_itinerary.sql', systimestamp, 'easyjet-booking-gateway');

INSERT into GE_SCRIPTS_LOG (ID, EXECUTION_DATE, MODULE)
values ('EJC-44/03_create_table_itinerary_booking.sql', systimestamp, 'easyjet-booking-gateway');

INSERT into GE_SCRIPTS_LOG (ID, EXECUTION_DATE, MODULE)
values ('EJC-44/04_create_table_fare_details.sql', systimestamp, 'easyjet-booking-gateway');

INSERT into GE_SCRIPTS_LOG (ID, EXECUTION_DATE, MODULE)
values ('EJC-44/05_create_table_passenger_fares.sql', systimestamp, 'easyjet-booking-gateway');

INSERT into GE_SCRIPTS_LOG (ID, EXECUTION_DATE, MODULE)
values ('EJC-44/06_create_table_traveller.sql', systimestamp, 'easyjet-booking-gateway');

INSERT into GE_SCRIPTS_LOG (ID, EXECUTION_DATE, MODULE)
values ('EJC-44/07_create_table_segment.sql', systimestamp, 'easyjet-booking-gateway');

INSERT into GE_SCRIPTS_LOG (ID, EXECUTION_DATE, MODULE)
values ('EJC-44/08_create_table_section.sql', systimestamp, 'easyjet-booking-gateway');

INSERT into GE_SCRIPTS_LOG (ID, EXECUTION_DATE, MODULE)
values ('EJC-44/09_create_table_flight.sql', systimestamp, 'easyjet-booking-gateway');

INSERT into GE_SCRIPTS_LOG (ID, EXECUTION_DATE, MODULE)
values ('EJC-44/10_create_table_buyer.sql', systimestamp, 'easyjet-booking-gateway');

INSERT into GE_SCRIPTS_LOG (ID, EXECUTION_DATE, MODULE)
values ('EJC-44/11_create_flight_fare_Information.sql', systimestamp, 'easyjet-booking-gateway');

INSERT into GE_SCRIPTS_LOG (ID, EXECUTION_DATE, MODULE)
values ('EJC-44/12_create_table_carrier_pnr.sql', systimestamp, 'easyjet-booking-gateway');

INSERT into GE_SCRIPTS_LOG (ID, EXECUTION_DATE, MODULE)
values ('EJC-44/13_create_table_payment_details.sql', systimestamp, 'easyjet-booking-gateway');

INSERT into GE_SCRIPTS_LOG (ID, EXECUTION_DATE, MODULE)
values ('rollback/R01_drop_tables.sql', systimestamp, 'easyjet-booking-gateway');
