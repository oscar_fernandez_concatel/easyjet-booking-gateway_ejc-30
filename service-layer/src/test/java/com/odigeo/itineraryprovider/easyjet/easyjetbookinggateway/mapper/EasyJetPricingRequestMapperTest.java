package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.easyjet.b2b.request.OFTPricingRequestType;
import com.easyjet.b2b.request.State;
import com.odigeo.geoapi.last.responses.Airport;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.AirportObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.EasyJetBookingGatewayConfigurationMapperObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.EasyJetItinerarySelectionObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.configuration.EasyJetBookingGatewayConfiguration;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.EasyJetItinerarySelection;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FareClass;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.format.DateTimeFormatter;

public class EasyJetPricingRequestMapperTest {

    @Test
    public void testMap() {
        Airport airport = AirportObjectMother.ANY;
        EasyJetItinerarySelection easyJetItinerarySelection = EasyJetItinerarySelectionObjectMother.ANY;
        EasyJetBookingGatewayConfiguration configuration = EasyJetBookingGatewayConfigurationMapperObjectMother.ANY;

        EasyJetPricingRequestMapper mapper = new EasyJetPricingRequestMapper();
        OFTPricingRequestType request = mapper.map(easyJetItinerarySelection, airport, airport, configuration.getOftPricingLanguageCode(), configuration.getOftPricingBookingCodeInfant());

        Assert.assertEquals(request.getLanguageCode(), configuration.getOftPricingLanguageCode());
        Assert.assertEquals(request.getOriginatorBookingID(), easyJetItinerarySelection.getVisitId());
        Assert.assertEquals(request.getItinerary().get(0).getSegment().get(0).getSegmentReference(), easyJetItinerarySelection.getItinerary().getSegments().get(0).getSections().get(0).getId().getId().toString() + "#" + easyJetItinerarySelection.getItinerary().getSegments().get(0).getSections().get(0).getSegmentPosition());
        Assert.assertEquals(request.getItinerary().get(0).getSegment().get(0).getDepartureDateTime(), easyJetItinerarySelection.getItinerary().getSegments().get(0).getSections().get(0).getDepartureDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        Assert.assertEquals(request.getItinerary().get(0).getSegment().get(0).getDepartureLocation(), airport.getIataCode());
        Assert.assertEquals(request.getItinerary().get(0).getSegment().get(0).getArrivalDateTime(), easyJetItinerarySelection.getItinerary().getSegments().get(0).getSections().get(0).getArrivalDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        Assert.assertEquals(request.getItinerary().get(0).getSegment().get(0).getArrivalLocation(), airport.getIataCode());
        Assert.assertEquals(request.getItinerary().get(0).getSegment().get(0).getSegmentState(), State.NEW);
        Assert.assertEquals(request.getItinerary().get(0).getSegment().get(0).getFlightNumber(), easyJetItinerarySelection.getItinerary().getSegments().get(0).getSections().get(0).getFlight().getFlightNumber());
        Assert.assertEquals(request.getItinerary().get(0).getSegment().get(0).getFareClass(), FareClass.Y.name());

    }
}
