package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.redis;

import com.odigeo.persistance.cache.impl.distributed.RedisClientFactory;
import com.odigeo.persistance.cache.impl.distributed.SerializableSerializableRedisCodec;
import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.Serializable;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;

public class RedisCommandFactoryTest {
    private RedisCommandFactory redisCommandFactory;

    @Mock
    private SelectionRedisCacheConfiguration redisConfiguration;

    @Mock
    private RedisClientFactory redisClientFactory;

    @Mock
    private StatefulRedisConnection<Serializable, Serializable> redisConnection;

    @Mock
    private RedisClient redisClient;

    @Mock
    private RedisCommands<Serializable, Serializable> redisCommands;

    @Mock
    private SerializableSerializableRedisCodec serializableRedisCodec;


    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(redisConfiguration.getHost()).thenReturn("localhost");
        when(redisConfiguration.getPort()).thenReturn(6379);
        when(redisConfiguration.getTimeoutMillis()).thenReturn(10000);
        when(redisConfiguration.getExpirationTimeMillis()).thenReturn(10000);
        when(redisClientFactory.newInstance("localhost", 6379, 10000, 10000)).thenReturn(redisClient);
        when(redisClient.connect(any(SerializableSerializableRedisCodec.class))).thenReturn(redisConnection);
        doNothing().when(redisClient).shutdown();
        when(redisConnection.sync()).thenReturn(redisCommands);

        redisCommandFactory = new RedisCommandFactory(redisConfiguration, redisClientFactory, serializableRedisCodec);
    }


    @Test
    public void testGetRedisCommands() {
        RedisCommands<Serializable, Serializable> commands = redisCommandFactory.getRedisCommands();
        assertNotNull(commands);
    }

    @Test
    public void testCloseRedisClient() {
        redisCommandFactory.closeRedisClient();
    }

    @Test
    public void testGetRedisCommandsWithRedisConnectionIssue() {
        when(redisClient.connect(serializableRedisCodec)).thenThrow(new RuntimeException());
        try {
            redisCommandFactory.getRedisCommands();
        } catch (Exception x) {
            Assert.assertTrue(x instanceof RedisConnectionIssueException);
        }
    }

}
