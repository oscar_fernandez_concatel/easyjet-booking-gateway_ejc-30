package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.easyjet.b2b.request.OFTBookingRequestType;

public class OFTBookingRequestTypeObjectMother {
    public static final OFTBookingRequestType ANY = aOFTBookingRequestType();

    public static OFTBookingRequestType aOFTBookingRequestType() {
        return new OFTBookingRequestType();
    }

}
