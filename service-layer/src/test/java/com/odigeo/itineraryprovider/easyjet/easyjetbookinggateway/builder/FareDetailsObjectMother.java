package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FareDetails;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Money;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PassengerTypeFare;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TravellerType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class FareDetailsObjectMother {
    public static final FareDetails ANY = aFareDetails();

    public static FareDetails aFareDetails() {
        Money totalTaxAmount = MoneyObjectMother.ANY;
        Money totalFareAmount = MoneyObjectMother.ANY;
        ItineraryId id = ItineraryIdObjectMother.ANY;
        List<String> corporateCodes = new ArrayList<>();
        corporateCodes.add("CODE");
        Map<TravellerType, PassengerTypeFare> passengerFares = Collections.singletonMap(TravellerType.ADULT,
                PassengerTypeFareObjectMother.ANY);

        return FareDetails.builder()
                .setCorporateCodes(corporateCodes)
                .setTotalFareAmount(totalFareAmount)
                .setTotalTaxAmount(totalTaxAmount)
                .setPassengerFares(passengerFares)
                .setItineraryId(id)
                .build();
    }
}
