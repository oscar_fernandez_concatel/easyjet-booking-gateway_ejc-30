package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.easyjet.b2b.response.PaymentTypeElement;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.PaymentTypeObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderPaymentOptions;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class EasyJetPaymentTypeResponseMapperTest {

    @Test
    public void testMap() {
        List<PaymentTypeElement> paymentTypeElements = PaymentTypeObjectMother.getListPaymentTypeElementsBalanced();
        EasyJetPaymentTypeResponseMapper sut = new EasyJetPaymentTypeResponseMapper();

        ProviderPaymentOptions response = sut.map(paymentTypeElements);

        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getCreditCardPaymentOptions());
        Assert.assertNotNull(response.getCashPaymentOptions());
        Assert.assertEquals(response.getCreditCardPaymentOptions().size(), paymentTypeElements.size());
    }
}
