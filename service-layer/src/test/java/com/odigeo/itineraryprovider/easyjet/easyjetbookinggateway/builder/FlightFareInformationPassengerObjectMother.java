package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;


import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CabinClass;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FareType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightFareInformationPassenger;

public class FlightFareInformationPassengerObjectMother {
    public static final FlightFareInformationPassenger ANY = aFlightFareInformationPassenger();

    public static FlightFareInformationPassenger aFlightFareInformationPassenger() {

        return FlightFareInformationPassenger.builder()
                .setCabinClass(CabinClass.TOURIST)
                .setIsResident(false)
                .setFareRules("")
                .setFareType(FareType.PUBLIC)
                .setFareBasisId("")
                .setAgreementsType("")
                .setAvailableSeats(-1)
                .setServiceClass("")
                .setSubFareType("")
                .build();
    }
}
