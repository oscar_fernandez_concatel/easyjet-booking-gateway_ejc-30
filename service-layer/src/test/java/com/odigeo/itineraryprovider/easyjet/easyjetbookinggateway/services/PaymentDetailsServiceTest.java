package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.services;

import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.payments.paymentinstrument.contract.PaymentInstrumentResourceException;
import com.edreamsodigeo.payments.paymentinstrument.contract.creditcard.CreditCardPciScopeResource;
import com.edreamsodigeo.payments.paymentinstrument.token.PaymentInstrumentToken;
import com.google.inject.AbstractModule;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderDetailsObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderItineraryBookingObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderPaymentDetailObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.itinerary.ProviderItineraryBookingRepository;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Money;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;

public class PaymentDetailsServiceTest {

    @Mock
    private PricingService pricingService;

    @Mock
    private PaymentTypeService paymentTypeService;

    @Mock
    private CreditCardPciScopeResource creditCardPciScopeResource;

    @Mock
    private ProviderItineraryBookingRepository providerItineraryBookingRepository;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);

        ConfigurationEngine.init(new AbstractModule() {
            @Override
            protected void configure() {
                bind(PaymentDetailsService.class).toInstance(
                        new PaymentDetailsService(pricingService, paymentTypeService,
                                creditCardPciScopeResource,
                                providerItineraryBookingRepository));
            }
        });
    }

    @Test
    public void whenSetPaymentDetails() throws PaymentInstrumentResourceException {

        final PaymentInstrumentToken paymentInstrumentToken = new PaymentInstrumentToken();
        paymentInstrumentToken.setUuid(UUID.randomUUID());

        Mockito.when(pricingService.getCheapestFares(any()))
                .thenReturn(ProviderDetailsObjectMother.ANY);

        Mockito.when(creditCardPciScopeResource.tokenize(any()))
                .thenReturn(paymentInstrumentToken);

        final PaymentDetailsService paymentDetailsService =
                ConfigurationEngine.getInstance(PaymentDetailsService.class);

        final Money moneyResult = paymentDetailsService.setPaymentDetails(
                ProviderItineraryBookingObjectMother.ANY,
                ProviderPaymentDetailObjectMother.ANY);

        Assert.assertNotNull(moneyResult);
        Assert.assertEquals(moneyResult.getAmount(),
                ProviderDetailsObjectMother.ANY.getFareDetails().getTotalFareAmount().getAmount());
    }
}
