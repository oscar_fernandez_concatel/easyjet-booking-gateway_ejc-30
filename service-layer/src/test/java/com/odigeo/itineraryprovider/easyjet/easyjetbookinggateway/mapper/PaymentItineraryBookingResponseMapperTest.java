package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.MoneyObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderItineraryBookingObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderPaymentDetailObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Money;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderPaymentDetail;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.payment.PaymentItineraryBooking;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PaymentItineraryBookingResponseMapperTest {
    @Test
    public void testMap() {
        ProviderItineraryBooking providerItineraryBooking = ProviderItineraryBookingObjectMother.ANY;
        ProviderPaymentDetail providerPaymentDetail = ProviderPaymentDetailObjectMother.ANY;
        Money fee = MoneyObjectMother.ANY;
        PaymentItineraryBooking paymentItineraryBooking = PaymentItineraryBookingResponseMapper.INSTANCE.toApi(providerItineraryBooking, providerPaymentDetail, fee);
        Assert.assertNotNull(paymentItineraryBooking);
    }
}
