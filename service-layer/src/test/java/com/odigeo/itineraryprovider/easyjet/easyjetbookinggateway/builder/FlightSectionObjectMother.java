package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.BaggageAllowance;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CabinClass;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Flight;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightFareInformation;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightSection;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;

import java.time.LocalDateTime;

public class FlightSectionObjectMother {
    public static final FlightSection ANY = aFlightSection();

    public static FlightSection aFlightSection() {

        BaggageAllowance baggageAllowance = BaggageAllowanceObjectMother.ANY;
        ItineraryId itineraryId = ItineraryIdObjectMother.ANY;
        Flight flight = FlightObjectMother.ANY;
        FlightFareInformation flightFareInformation = FlightFareInformationObjectMother.ANY;

        return FlightSection.builder()
                .setArrivalDate(LocalDateTime.of(2021, 12, 15, 10, 0, 0))
                .setFlight(flight)
                .setFlightFareInformation(flightFareInformation)
                .setArrivalTerminal("BCN")
                .setArrivalGeoNodeId(5678)
                .setBaggageAllowance(baggageAllowance)
                .setCabinClass(CabinClass.TOURIST)
                .setDepartureDate(LocalDateTime.of(2021, 12, 15, 10, 0, 0))
                .setDepartureGeoNodeId(1234)
                .setDepartureTerminal("EZE")
                .setId(itineraryId)
                .setSegmentPosition(1)
                .build();
    }
}
