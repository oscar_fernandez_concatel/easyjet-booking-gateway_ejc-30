package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.CountryObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Country;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CountryMapperTest {

    @Test
    public void testToModel() {
        final com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Country country = CountryObjectMother.ANY_EXTERNAL;
        Country countryModel = CountryMapper.INSTANCE.toModel(country);
        Assert.assertNotNull(countryModel);
        Assert.assertEquals(countryModel.getCountryCode(), country.getCountryCode());
    }

    @Test
    public void testToApi() {
        Country countryModel = CountryObjectMother.ANY;
        final com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Country country = CountryMapper.INSTANCE.toApi(countryModel);
        Assert.assertNotNull(countryModel);
        Assert.assertEquals(countryModel.getCountryCode(), country.getCountryCode());
    }

}
