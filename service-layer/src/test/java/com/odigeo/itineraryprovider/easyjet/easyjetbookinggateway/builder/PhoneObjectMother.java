package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Country;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Phone;

public class PhoneObjectMother {
    public static final Phone ANY = aPhone();
    public static final com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Phone ANY_EXTERRNAL = aPhoneExternal();

    private static com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Phone aPhoneExternal() {
        com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Country country = CountryObjectMother.ANY_EXTERNAL;
        com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Phone phone = new com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Phone(country, "numberPhone");
        return phone;
    }

    private static Phone aPhone() {
        Country country = CountryObjectMother.ANY;
        return Phone.builder()
                .setPhoneType("phoneType")
                .setNumber("123456789")
                .setCountry(country)
                .build();
    }
}
