package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.adapter;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.EasyJetItinerarySelectionObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderDetailsObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderPaymentOptionsObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.SegmentObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.EasyJetItinerarySelection;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderDetails;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItinerary;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderPaymentOptions;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Segment;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TravellerType;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;

public class ProviderItineraryBuilderTest {

    @Mock
    private SegmentsBuilder segmentsBuilder;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        ConfigurationEngine.init(new AbstractModule() {
            @Override
            protected void configure() {
                bind(ProviderItineraryBuilder.class).toInstance(new ProviderItineraryBuilder(
                        segmentsBuilder));
            }
        });
    }

    @Test
    public void testWhenCreateProviderItinerary() {

        EasyJetItinerarySelection itinerarySelection = EasyJetItinerarySelectionObjectMother.ANY;
        ProviderDetails providerDetails = ProviderDetailsObjectMother.ANY;
        ProviderPaymentOptions providerPaymentOptions = ProviderPaymentOptionsObjectMother.ANY;
        Segment segment = SegmentObjectMother.ANY;
        List<Segment> segments = new ArrayList<>();
        segments.add(segment);

        when(segmentsBuilder.buildSegments(anyList(), any())).thenReturn(segments);

        final ProviderItineraryBuilder providerItineraryBuilder =
                ConfigurationEngine.getInstance(ProviderItineraryBuilder.class);

        final ProviderItinerary providerItinerary = providerItineraryBuilder.createProviderItinerary(itinerarySelection, providerDetails, providerPaymentOptions);

        Assert.assertNotNull(providerItinerary);

        Assert.assertEquals(providerItinerary.getItinerary().getArrivalGeoNodeId(), itinerarySelection.getItinerary().getArrivalGeoNodeId());
        Assert.assertEquals(providerItinerary.getItinerary().getDepartureGeoNodeId(), itinerarySelection.getItinerary().getDepartureGeoNodeId());
        Assert.assertEquals(providerItinerary.getItinerary().getDepartureDate(), itinerarySelection.getItinerary().getDepartureDate());
        Assert.assertEquals(providerItinerary.getItinerary().getReturnDate(), itinerarySelection.getItinerary().getReturnDate());
        Assert.assertEquals(providerItinerary.getItinerary().getTripType(), itinerarySelection.getItinerary().getTripType());

        Assert.assertEquals(providerItinerary.getItinerary().getFareDetails().getTotalTaxAmount().getAmount(), providerDetails.getFareDetails().getTotalTaxAmount().getAmount());
        Assert.assertEquals(providerItinerary.getItinerary().getFareDetails().getTotalFareAmount().getAmount(), providerDetails.getFareDetails().getTotalFareAmount().getAmount());

        Assert.assertEquals(providerItinerary.getItinerary().getFareDetails().getPassengerFares().get(TravellerType.ADULT).getTravellerType(), providerDetails.getFareDetails().getPassengerFares().get(TravellerType.ADULT).getTravellerType());
        Assert.assertEquals(providerItinerary.getItinerary().getFareDetails().getPassengerFares().get(TravellerType.ADULT).getFareAmount().getAmount(), providerDetails.getFareDetails().getPassengerFares().get(TravellerType.ADULT).getFareAmount().getAmount());
        Assert.assertEquals(providerItinerary.getItinerary().getFareDetails().getPassengerFares().get(TravellerType.ADULT).getTaxAmount().getAmount(), providerDetails.getFareDetails().getPassengerFares().get(TravellerType.ADULT).getTaxAmount().getAmount());
        Assert.assertEquals(providerItinerary.getItinerary().getFareDetails().getPassengerFares().get(TravellerType.ADULT).getPassengersPerType(), providerDetails.getFareDetails().getPassengerFares().get(TravellerType.ADULT).getPassengersPerType());

    }

    @Test
    public void testWhenCreateProviderItinerary2() {

        ProviderDetails providerDetails = ProviderDetailsObjectMother.ANY;
        ProviderPaymentOptions providerPaymentOptions = ProviderPaymentOptionsObjectMother.ANY;
        Segment segment = SegmentObjectMother.ANY;
        List<Segment> segments = new ArrayList<>();
        segments.add(segment);

        when(segmentsBuilder.buildSegments(anyList(), any())).thenReturn(segments);

        final ProviderItineraryBuilder providerItineraryBuilder =
                ConfigurationEngine.getInstance(ProviderItineraryBuilder.class);

        final ProviderItinerary providerItinerary = providerItineraryBuilder.createProviderItinerary(providerDetails, providerPaymentOptions);

        Assert.assertNotNull(providerItinerary);

        Assert.assertEquals(providerItinerary.getItinerary().getArrivalGeoNodeId(), providerDetails.getItinerary().getArrivalGeoNodeId());
        Assert.assertEquals(providerItinerary.getItinerary().getDepartureGeoNodeId(), providerDetails.getItinerary().getDepartureGeoNodeId());
        Assert.assertEquals(providerItinerary.getItinerary().getDepartureDate(), providerDetails.getItinerary().getDepartureDate());
        Assert.assertEquals(providerItinerary.getItinerary().getReturnDate(), providerDetails.getItinerary().getReturnDate());
        Assert.assertEquals(providerItinerary.getItinerary().getTripType(), providerDetails.getItinerary().getTripType());

        Assert.assertEquals(providerItinerary.getItinerary().getFareDetails().getTotalTaxAmount().getAmount(), providerDetails.getFareDetails().getTotalTaxAmount().getAmount());
        Assert.assertEquals(providerItinerary.getItinerary().getFareDetails().getTotalFareAmount().getAmount(), providerDetails.getFareDetails().getTotalFareAmount().getAmount());

        Assert.assertEquals(providerItinerary.getItinerary().getFareDetails().getPassengerFares().get(TravellerType.ADULT).getTravellerType(), providerDetails.getFareDetails().getPassengerFares().get(TravellerType.ADULT).getTravellerType());
        Assert.assertEquals(providerItinerary.getItinerary().getFareDetails().getPassengerFares().get(TravellerType.ADULT).getFareAmount().getAmount(), providerDetails.getFareDetails().getPassengerFares().get(TravellerType.ADULT).getFareAmount().getAmount());
        Assert.assertEquals(providerItinerary.getItinerary().getFareDetails().getPassengerFares().get(TravellerType.ADULT).getTaxAmount().getAmount(), providerDetails.getFareDetails().getPassengerFares().get(TravellerType.ADULT).getTaxAmount().getAmount());
        Assert.assertEquals(providerItinerary.getItinerary().getFareDetails().getPassengerFares().get(TravellerType.ADULT).getPassengersPerType(), providerDetails.getFareDetails().getPassengerFares().get(TravellerType.ADULT).getPassengersPerType());

    }

}
