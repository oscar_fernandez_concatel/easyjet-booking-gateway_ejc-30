package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderPaymentDetail;

public class ProviderPaymentDetailObjectMother {
    public static final ProviderPaymentDetail ANY = aProviderPaymentDetail();

    private static ProviderPaymentDetail aProviderPaymentDetail() {
        return ProviderPaymentDetail.builder()
                .setProviderPaymentMethod(ProviderPaymentMethodObjectMother.ANY)
                .build();
    }
}
