package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.BuyerObjectMother;
import org.testng.Assert;
import org.testng.annotations.Test;

public class BuyerMapperTest {

    @Test
    public void testToModel() {
        final com.odigeo.suppliergatewayapi.v25.itinerary.booking.Buyer buyer = BuyerObjectMother.ANY_EXTERNAL;
        com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Buyer buyerModel = BuyerMapper.INSTANCE.toModel(buyer);
        Assert.assertNotNull(buyerModel);
        Assert.assertEquals(buyerModel.getName(), buyer.getName());
    }

    @Test
    public void testToApi() {
        final com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Buyer buyerModel = BuyerObjectMother.ANY;
        com.odigeo.suppliergatewayapi.v25.itinerary.booking.Buyer buyer = BuyerMapper.INSTANCE.toApi(buyerModel);
        Assert.assertNotNull(buyerModel);
        Assert.assertEquals(buyerModel.getName(), buyer.getName());
    }

}
