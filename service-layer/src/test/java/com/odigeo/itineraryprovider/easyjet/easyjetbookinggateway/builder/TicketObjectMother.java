package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Carrier;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryBookingId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Money;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TaxDetail;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Ticket;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TicketAction;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TicketId;

import java.util.ArrayList;
import java.util.List;

public class TicketObjectMother {
    public static final Ticket ANY = aTicket();

    private static Ticket aTicket() {
        TicketId id = TicketIdObjectMother.ANY;
        Money totalAmount = MoneyObjectMother.ANY;
        Money taxes = MoneyObjectMother.ANY;
        Money commission = MoneyObjectMother.ANY;
        ItineraryBookingId itineraryBookingId = ItineraryBookingIdObjectMother.ANY;
        Carrier validatingCarrier = CarrierObjectMother.ANY;

        TicketAction ticketAction = TicketActionObjectMother.ANY;
        List<TicketAction> ticketActions = new ArrayList<>();
        ticketActions.add(ticketAction);

        TaxDetail taxDetail = TaxDetailObjectMother.ANY;
        List<TaxDetail> taxDetails = new ArrayList<>();
        taxDetails.add(taxDetail);

        return Ticket.builder()
                .setId(id)
                .setNumber("number")
                .setTravellerKey("travellerKey")
                .setTotalAmount(totalAmount)
                .setTaxes(taxes)
                .setCommission(commission)
                .setStatus("status")
                .setItineraryBookingId(itineraryBookingId)
                .setNumPassenger(1)
                .setValidatingCarrier(validatingCarrier)
                .setIataCode("iataCode")
                .setOfficeId("officeId")
                .setTicketActions(ticketActions)
                .setTaxDetails(taxDetails)
                .build();


    }
}
