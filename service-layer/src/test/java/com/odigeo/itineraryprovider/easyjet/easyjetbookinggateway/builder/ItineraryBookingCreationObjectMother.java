package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.suppliergatewayapi.v25.itinerary.booking.Buyer;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.creation.ItineraryBookingCreation;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Traveller;
import com.odigeo.suppliergatewayapi.v25.itinerary.selection.SelectedItineraryId;

import java.util.ArrayList;
import java.util.List;

public class ItineraryBookingCreationObjectMother {
    public static final ItineraryBookingCreation ANY = aItineraryBookingCreation();

    private static ItineraryBookingCreation aItineraryBookingCreation() {
        Buyer buyer = BuyerObjectMother.ANY_EXTERNAL;
        Traveller traveller = TravellerObjectMother.ANY_EXTERNAL;
        List<Traveller> travellers = new ArrayList<>();
        travellers.add(traveller);
        SelectedItineraryId itineraryId = SelectedItineraryIdObjectMother.ANY;
        ItineraryBookingCreation itineraryBookingCreation = new ItineraryBookingCreation();
        itineraryBookingCreation.setVisitId("visitId");
        itineraryBookingCreation.setVisitInformation("visitInformation");
        itineraryBookingCreation.setBookingId("bookingId");
        itineraryBookingCreation.setItineraryId(itineraryId);
        itineraryBookingCreation.setBuyer(buyer);
        itineraryBookingCreation.setTravellers(travellers);
        return itineraryBookingCreation;
    }
}
