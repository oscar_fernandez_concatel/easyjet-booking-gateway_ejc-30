package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.services;

import com.odigeo.easyjetadapter.adapter.boundary.EasyJetAdapterManager;
import com.odigeo.easyjetadapter.webservices.entity.exception.ProviderException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.EasyJetAdapterResponseRootObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.EasyJetBookingGatewayConfigurationMapperObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.configuration.EasyJetBookingGatewayConfiguration;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper.EasyJetPaymentTypeResponseMapper;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingInternalException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderPaymentOptions;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class PaymentTypeServiceTest {

    @Mock
    private EasyJetAdapterManager adapterManager;

    @Mock
    private EasyJetPaymentTypeResponseMapper easyJetPaymentTypeResponseMapper;

    @Mock
    private EasyJetBookingGatewayConfiguration easyJetBookingGatewayConfiguration;

    private PaymentTypeService sut;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        sut = new PaymentTypeService(adapterManager, easyJetPaymentTypeResponseMapper, easyJetBookingGatewayConfiguration);
    }

    @Test
    public void testWhenGetPaymentTypes() throws ProviderException, EasyJetBookingInternalException {
        when(adapterManager.getPaymentTypes()).thenReturn(EasyJetAdapterResponseRootObjectMother.getResponseForPaymentTypesBalanced());
        when(easyJetPaymentTypeResponseMapper.map(any())).thenCallRealMethod();
        when(easyJetBookingGatewayConfiguration.getSupportedPaymentMethodCodesList())
                .thenReturn(EasyJetBookingGatewayConfigurationMapperObjectMother.ANY.getSupportedPaymentMethodCodesList());

        ProviderPaymentOptions response = sut.getPaymentTypes();

        Assert.assertNotNull(response);
        Assert.assertEquals(response.getCreditCardPaymentOptions().size(),
                EasyJetAdapterResponseRootObjectMother.getResponseForPaymentTypesBalanced().getDataListRoot().getPaymentTypes().getPayment().size());
    }

    @Test
    public void testWhenGetPaymentTypesWithUnsupportedTypes() throws ProviderException, EasyJetBookingInternalException {
        when(adapterManager.getPaymentTypes()).thenReturn(EasyJetAdapterResponseRootObjectMother.getResponseForPaymentTypesWithUnsupportedTypes());
        when(easyJetPaymentTypeResponseMapper.map(any())).thenCallRealMethod();
        when(easyJetBookingGatewayConfiguration.getSupportedPaymentMethodCodesList())
                .thenReturn(EasyJetBookingGatewayConfigurationMapperObjectMother.ANY.getSupportedPaymentMethodCodesList());

        ProviderPaymentOptions response = sut.getPaymentTypes();

        Assert.assertNotNull(response);
        Assert.assertEquals(response.getCreditCardPaymentOptions().size(),
                EasyJetAdapterResponseRootObjectMother
                        .getResponseForPaymentTypesWithUnsupportedTypes()
                        .getDataListRoot()
                        .getPaymentTypes()
                        .getPayment()
                        .size() - 1);
        Assert.assertEquals(
                response
                        .getCreditCardPaymentOptions()
                        .stream()
                        .filter(opt -> opt.getCreditCardSpec().equals("UNSUPP")).count(), 0);
        Assert.assertNotNull(response.getCashPaymentOptions());
        Assert.assertEquals(response.getCashPaymentOptions().size(), 0);
    }

    @Test(expectedExceptions = EasyJetBookingInternalException.class)
    public void testWhenGetPaymentTypesWithErrorOnAdapter() throws ProviderException, EasyJetBookingInternalException {
        when(adapterManager.getPaymentTypes()).thenThrow(new ProviderException("...", new Exception()) { });
        sut.getPaymentTypes();
    }

    @Test
    public void testWhenGetPaymentTypesWithErrorList() throws ProviderException {
        when(adapterManager.getPaymentTypes()).thenReturn(EasyJetAdapterResponseRootObjectMother.getWithErrorList());

        try {
            sut.getPaymentTypes();
        } catch (Exception exception) {
            Assert.assertTrue(exception instanceof EasyJetBookingInternalException);
        }
    }
}
