package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryBookingId;

import java.util.UUID;

public class ItineraryBookingIdObjectMother {
    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";
    public static final ItineraryBookingId ANY = aItineraryBookingId();

    private static ItineraryBookingId aItineraryBookingId() {
        return ItineraryBookingId.builder()
                .setId(UUID.fromString(IDENTIFIER))
                .build();
    }
}
