package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PaymentDetails;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PaymentId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PaymentInstrumentToken;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PaymentMethod;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TicketActionId;

public class PaymentDetailsObjectMother {
    public static final PaymentDetails ANY = aPaymentDetails();

    private static PaymentDetails aPaymentDetails() {
        PaymentId paymentId = PaymentIdObjectMother.ANY;
        PaymentMethod paymentMethod = PaymentMethodObjectMother.ANY;
        PaymentInstrumentToken paymentInstrumentToken = PaymentInstrumentTokenObjectMother.ANY;
        TicketActionId ticketActionId = TicketActionIdObjectMother.ANY;
        return PaymentDetails.builder()
                .setPaymentId(paymentId)
                .setPaymentMethod(paymentMethod)
                .setPaymentInstrumentToken(paymentInstrumentToken)
                .setTicketActionId(ticketActionId)
                .setIsCash(false)
                .build();
    }
}
