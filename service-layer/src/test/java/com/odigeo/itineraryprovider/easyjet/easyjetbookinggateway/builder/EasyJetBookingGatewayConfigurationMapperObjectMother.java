package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.configuration.EasyJetBookingGatewayConfiguration;

public class EasyJetBookingGatewayConfigurationMapperObjectMother {

    public static final EasyJetBookingGatewayConfiguration ANY = getConfiguration();


    public static EasyJetBookingGatewayConfiguration getConfiguration() {
        EasyJetBookingGatewayConfiguration response = new EasyJetBookingGatewayConfiguration();
        response.setSupportedPaymentMethodCodes("AX,MC,DC,DL,VI,SW,EL,TP,DM");
        response.setOftPricingBookingCodeInfant("INF");
        response.setOftPricingLanguageCode("EN");
        return response;
    }
}
