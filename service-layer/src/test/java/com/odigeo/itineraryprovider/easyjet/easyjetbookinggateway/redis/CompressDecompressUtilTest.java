package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.redis;

import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.nio.charset.StandardCharsets;


public class CompressDecompressUtilTest {

    private CompressDecompressUtil compressDecompressUtil;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        compressDecompressUtil = new CompressDecompressUtil();
    }

    @Test
    public void testCompressDecompressBytes() {
        String code = "CODE";
        byte[] compressedArray = compressDecompressUtil.compressBytes(code.getBytes(StandardCharsets.UTF_8));
        Assert.assertNotNull(compressedArray);

        String compressedCode = new String(compressedArray, StandardCharsets.UTF_8);
        Assert.assertNotEquals(code, compressedCode);

        byte[] decompressedArray = compressDecompressUtil.decompressBytes(compressedArray);
        Assert.assertNotNull(decompressedArray);

        String decompressedCode = new String(decompressedArray, StandardCharsets.UTF_8);
        Assert.assertEquals(code, decompressedCode);
    }
}
