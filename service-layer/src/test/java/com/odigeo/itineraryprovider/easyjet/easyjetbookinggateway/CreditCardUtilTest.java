package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingInternalException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardVendorCode;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CreditCardUtilTest {

    @BeforeMethod
    public void setUp() {
    }

    @Test
    public void whenGetEasyJetVendorCodeThenVisaCredit() {
        String easyJetVendorCode =
                CreditCardUtil.getEasyJetVendorCode(CreditCardVendorCode.VISA_CREDIT);

        Assert.assertSame(Constants.VI, easyJetVendorCode);
    }

    @Test
    public void whenGetEasyJetVendorCodeThenVisaDebit() {
        String easyJetVendorCode =
                CreditCardUtil.getEasyJetVendorCode(CreditCardVendorCode.VISA_DEBIT);

        Assert.assertSame(Constants.DL, easyJetVendorCode);
    }

    @Test
    public void whenGetEasyJetVendorCodeThenVisaElectron() {
        String easyJetVendorCode =
                CreditCardUtil.getEasyJetVendorCode(CreditCardVendorCode.VISA_ELECTRON);

        Assert.assertSame(Constants.EL, easyJetVendorCode);
    }

    @Test
    public void whenGetEasyJetVendorCodeThenMastercard() {
        String easyJetVendorCode =
                CreditCardUtil.getEasyJetVendorCode(CreditCardVendorCode.MASTERCARD);

        Assert.assertSame(Constants.MC, easyJetVendorCode);
    }

    @Test
    public void whenGetEasyJetVendorCodeThenDinersClub() {
        String easyJetVendorCode =
                CreditCardUtil.getEasyJetVendorCode(CreditCardVendorCode.DINERS_CLUB);

        Assert.assertSame(Constants.DC, easyJetVendorCode);
    }

    @Test
    public void whenGetEasyJetVendorCodeThenAmex() {
        String easyJetVendorCode =
                CreditCardUtil.getEasyJetVendorCode(CreditCardVendorCode.AMEX);

        Assert.assertSame(Constants.AX, easyJetVendorCode);
    }

    @Test
    public void whenGetEasyJetVendorCodeThenAmericanExpress() {
        String easyJetVendorCode =
                CreditCardUtil.getEasyJetVendorCode(CreditCardVendorCode.AMERICAN_EXPRESS);

        Assert.assertSame(Constants.AX, easyJetVendorCode);
    }

    @Test(expectedExceptions = EasyJetBookingInternalException.class)
    public void whenGetEasyJetVendorCodeThenUnknownCode() {
        CreditCardUtil.getEasyJetVendorCode(CreditCardVendorCode.MAESTRO);
    }
}
