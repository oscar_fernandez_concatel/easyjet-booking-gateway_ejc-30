package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TicketActionId;

import java.util.UUID;

public class TicketActionIdObjectMother {
    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";
    public static final TicketActionId ANY = aTicketActionId();

    private static TicketActionId aTicketActionId() {
        return TicketActionId.builder()
                .setId(UUID.fromString(IDENTIFIER))
                .build();
    }
}
