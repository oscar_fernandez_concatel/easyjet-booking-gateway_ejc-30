package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FareType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightFareInformation;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightFareInformationPassenger;

import java.util.Collections;
import java.util.Map;

public class FlightFareInformationObjectMother {
    public static final FlightFareInformation ANY = aFlightFareInformation();

    public static FlightFareInformation aFlightFareInformation() {

        Map<String, FlightFareInformationPassenger> flightFareInformations = Collections.singletonMap("PUBLIC",
                FlightFareInformationPassengerObjectMother.ANY);

        return FlightFareInformation.builder()
                .setFareType(FareType.PUBLIC)
                .setFlightFareInformationPassenger(flightFareInformations)
                .build();
    }
}
