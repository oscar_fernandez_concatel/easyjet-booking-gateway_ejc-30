package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.services;

import com.edreams.configuration.ConfigurationEngine;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.AbstractModule;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.adapter.ProviderItineraryBuilder;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.EasyJetItinerarySelectionObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderDetailsObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderItineraryObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderPaymentOptionsObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayErrorType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingInternalException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.EasyJetItinerarySelection;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderDetails;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItinerary;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderPaymentOptions;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.redis.CompressDecompressUtil;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.redis.RedisCommandFactory;
import io.lettuce.core.api.sync.RedisCommands;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.Serializable;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class EasyJetSelectedItineraryServiceTest {

    @Mock
    private ProviderItineraryBuilder providerItineraryBuilder;
    @Mock
    private RedisCommandFactory redisCommandFactory;
    @Mock
    private RedisCommands<Serializable, Serializable> redisCommands;
    @Mock
    private ObjectMapper objectMapper;
    @Mock
    private PricingService pricingService;
    @Mock
    private PaymentTypeService paymentTypeService;
    @Mock
    private CompressDecompressUtil compressDecompressUtil;

    @BeforeMethod
    public void init() {

        MockitoAnnotations.openMocks(this);

        ConfigurationEngine.init(new AbstractModule() {
            @Override
            protected void configure() {
                bind(EasyJetSelectedItineraryService.class).toInstance(new EasyJetSelectedItineraryService(
                        redisCommandFactory,
                        objectMapper,
                        providerItineraryBuilder,
                        pricingService,
                        paymentTypeService, compressDecompressUtil));
            }
        });
    }

    @Test
    public void testWhenCreateSelectedItinerary() throws EasyJetBookingGatewayException, JsonProcessingException {

        EasyJetItinerarySelection easyJetItinerarySelection = EasyJetItinerarySelectionObjectMother.ANY;
        byte[] bytes = "test".getBytes();

        when(redisCommandFactory.getRedisCommands()).thenReturn(redisCommands);
        when(objectMapper.writeValueAsBytes(any())).thenReturn(bytes);
        when(providerItineraryBuilder.createProviderItinerary(any(EasyJetItinerarySelection.class), any(ProviderDetails.class), any(ProviderPaymentOptions.class)))
                .thenReturn(ProviderItineraryObjectMother.ANY);
        when(providerItineraryBuilder.createProviderItinerary(any(ProviderDetails.class), any(ProviderPaymentOptions.class)))
                .thenReturn(ProviderItineraryObjectMother.ANY);
        when(pricingService.getCheapestFares(any(EasyJetItinerarySelection.class)))
                .thenReturn(ProviderDetailsObjectMother.ANY);
        when(paymentTypeService.getPaymentTypes())
                .thenReturn(ProviderPaymentOptionsObjectMother.ANY);

        final EasyJetSelectedItineraryService service =
                ConfigurationEngine.getInstance(EasyJetSelectedItineraryService.class);

        ProviderItinerary providerItinerary = service.createSelectedItinerary(easyJetItinerarySelection);

        Assert.assertNotNull(providerItinerary);

    }

    @Test
    public void testWhenCreateSelectedItineraryAndFailProviderItineraryBuilder() throws EasyJetBookingGatewayException, JsonProcessingException {

        EasyJetItinerarySelection easyJetItinerarySelection = EasyJetItinerarySelectionObjectMother.ANY;
        byte[] bytes = "test".getBytes();

        when(redisCommandFactory.getRedisCommands()).thenReturn(redisCommands);
        when(objectMapper.writeValueAsBytes(any())).thenReturn(bytes);
        when(providerItineraryBuilder.createProviderItinerary(any(EasyJetItinerarySelection.class), any(ProviderDetails.class), any(ProviderPaymentOptions.class)))
                .thenReturn(ProviderItineraryObjectMother.ANY);
        when(providerItineraryBuilder.createProviderItinerary(any(ProviderDetails.class), any(ProviderPaymentOptions.class)))
                .thenThrow(new NullPointerException("NullPointerException error"));
        when(pricingService.getCheapestFares(any(EasyJetItinerarySelection.class)))
                .thenReturn(ProviderDetailsObjectMother.ANY);
        when(paymentTypeService.getPaymentTypes())
                .thenReturn(ProviderPaymentOptionsObjectMother.ANY);

        final EasyJetSelectedItineraryService service =
                ConfigurationEngine.getInstance(EasyJetSelectedItineraryService.class);

        ProviderItinerary providerItinerary = service.createSelectedItinerary(easyJetItinerarySelection);

        Assert.assertNotNull(providerItinerary);

    }

    @Test
    public void testWhenCreateSelectedItineraryAndThrowsInternalException() throws EasyJetBookingGatewayException, JsonProcessingException {

        EasyJetItinerarySelection easyJetItinerarySelection = EasyJetItinerarySelectionObjectMother.ANY;
        byte[] bytes = "test".getBytes();

        when(redisCommandFactory.getRedisCommands()).thenReturn(redisCommands);
        when(objectMapper.writeValueAsBytes(any())).thenReturn(bytes);
        when(providerItineraryBuilder.createProviderItinerary(any(EasyJetItinerarySelection.class), any(ProviderDetails.class), any(ProviderPaymentOptions.class)))
                .thenReturn(ProviderItineraryObjectMother.ANY);
        when(providerItineraryBuilder.createProviderItinerary(any(ProviderDetails.class), any(ProviderPaymentOptions.class)))
                .thenReturn(ProviderItineraryObjectMother.ANY);
        when(pricingService.getCheapestFares(any(EasyJetItinerarySelection.class)))
                .thenThrow(new EasyJetBookingInternalException("error message", EasyJetBookingGatewayErrorType.UNKNOWN));
        when(paymentTypeService.getPaymentTypes())
                .thenReturn(ProviderPaymentOptionsObjectMother.ANY);

        final EasyJetSelectedItineraryService service =
                ConfigurationEngine.getInstance(EasyJetSelectedItineraryService.class);

        try {
            service.createSelectedItinerary(easyJetItinerarySelection);
        } catch (Exception x) {
            Assert.assertTrue(x instanceof EasyJetBookingGatewayException);
        }

    }

    @Test
    public void testWhenCreateSelectedItineraryAndFailRedis() throws EasyJetBookingGatewayException, JsonProcessingException {

        EasyJetItinerarySelection easyJetItinerarySelection = EasyJetItinerarySelectionObjectMother.ANY;

        when(redisCommandFactory.getRedisCommands()).thenReturn(redisCommands);
        when(objectMapper.writeValueAsBytes(any())).thenThrow(new JsonProcessingException("", new Exception()) {
        });
        when(providerItineraryBuilder.createProviderItinerary(any(EasyJetItinerarySelection.class), any(ProviderDetails.class), any(ProviderPaymentOptions.class)))
                .thenReturn(ProviderItineraryObjectMother.ANY);
        when(providerItineraryBuilder.createProviderItinerary(any(ProviderDetails.class), any(ProviderPaymentOptions.class)))
                .thenReturn(ProviderItineraryObjectMother.ANY);
        when(pricingService.getCheapestFares(any(EasyJetItinerarySelection.class)))
                .thenReturn(ProviderDetailsObjectMother.ANY);
        when(paymentTypeService.getPaymentTypes())
                .thenReturn(ProviderPaymentOptionsObjectMother.ANY);

        final EasyJetSelectedItineraryService service =
                ConfigurationEngine.getInstance(EasyJetSelectedItineraryService.class);

        ProviderItinerary providerItinerary = service.createSelectedItinerary(easyJetItinerarySelection);

        Assert.assertNotNull(providerItinerary);

    }

}
