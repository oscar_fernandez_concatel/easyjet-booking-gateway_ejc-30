package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;


import com.easyjet.b2b.response.BookingCodesType;
import com.easyjet.b2b.response.DataListRootType;
import com.easyjet.b2b.response.ErrorListRootType;
import com.easyjet.b2b.response.ErrorListType;
import com.easyjet.b2b.response.ErrorType;
import com.easyjet.b2b.response.OFTPricingResponseType;
import com.easyjet.b2b.response.PassengerListType;
import com.easyjet.b2b.response.PassengerMonetaryDataGroupType;
import com.easyjet.b2b.response.PassengerMonetaryDataType;
import com.easyjet.b2b.response.PassengerType;
import com.easyjet.b2b.response.ResponseRootType;

public class OFTPricingResponseTypeObjectMother {
    public static final ResponseRootType ANY = aResponseRootType();
    public static final ResponseRootType ANY_ERROR = aResponseRootTypeError();

    public static ResponseRootType aResponseRootType() {

        ResponseRootType response = new ResponseRootType();

        OFTPricingResponseType oftPricingResponse = new OFTPricingResponseType();
        oftPricingResponse.setCurrency("EUR");
        oftPricingResponse.setTotalAmount("68.00");

        PassengerListType passengerList = new PassengerListType();
        PassengerType passenger = new PassengerType();
        passenger.setPassengerReference("1");
        BookingCodesType bookingCodesType = new BookingCodesType();
        bookingCodesType.setPassengerTypeCode("ADT");
        passenger.setBookingCodes(bookingCodesType);

        PassengerMonetaryDataGroupType passengerMonetaryDataGroup = new PassengerMonetaryDataGroupType();

        PassengerMonetaryDataType passengerMonetaryData = new PassengerMonetaryDataType();
        passengerMonetaryData.setAmount("58.00");
        passengerMonetaryData.setTax("12.00");
        passengerMonetaryData.setType("FARE");


        PassengerMonetaryDataType passengerMonetaryDataSSR = new PassengerMonetaryDataType();
        passengerMonetaryDataSSR.setType("SSR");
        passengerMonetaryDataSSR.setAmount("10.00");

        passengerMonetaryDataGroup.getPassengerMonetaryData().add(passengerMonetaryData);
        passengerMonetaryDataGroup.getPassengerMonetaryData().add(passengerMonetaryDataSSR);

        passenger.setPassengerMonetaryDataGroup(passengerMonetaryDataGroup);

        passengerList.getPassengers().add(passenger);
        oftPricingResponse.setPassengerList(passengerList);

        DataListRootType dataListRootType = new DataListRootType();
        dataListRootType.setOFTPricingResponse(oftPricingResponse);
        response.setDataListRoot(dataListRootType);

        return response;
    }

    public static ResponseRootType aResponseRootTypeError() {

        ResponseRootType response = new ResponseRootType();
        ErrorListRootType errorListRootType = new ErrorListRootType();
        response.setErrorListRoot(errorListRootType);
        response.setSuccess("0");
        ErrorListType errorListType = new ErrorListType();
        ErrorType errorType = new ErrorType();
        errorType.setNumber("400002");
        errorType.setDescription("400002");
        errorListType.setError(errorType);
        errorListRootType.getErrorList().add(errorListType);
        return response;
    }

}
