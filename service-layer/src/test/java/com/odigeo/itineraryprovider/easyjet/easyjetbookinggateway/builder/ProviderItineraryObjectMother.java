package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Itinerary;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItinerary;

public class ProviderItineraryObjectMother {
    public static final ProviderItinerary ANY = aProviderItinerary();

    public static ProviderItinerary aProviderItinerary() {
        Itinerary itinerary = ItineraryObjectMother.ANY;
        ItineraryId itineraryId = ItineraryIdObjectMother.ANY;
        return ProviderItinerary.builder()
                .setItineraryId(itineraryId)
                .setItinerary(itinerary)
                .build();
    }

}
