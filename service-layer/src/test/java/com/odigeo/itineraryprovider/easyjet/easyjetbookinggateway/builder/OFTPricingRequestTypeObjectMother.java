package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.easyjet.b2b.request.OFTPricingRequestType;

public class OFTPricingRequestTypeObjectMother {
    public static final OFTPricingRequestType ANY = aOFTPricingRequestType();

    public static OFTPricingRequestType aOFTPricingRequestType() {
        return new OFTPricingRequestType();
    }
}
