package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.services;

import com.edreams.configuration.ConfigurationEngine;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.AbstractModule;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.CreateProviderItineraryBookingRespBuilder;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryBookingCreationInternalObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderDetailsObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderItineraryBookingObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderItineraryObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.itinerary.ProviderItineraryBookingRepository;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingInternalException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.ItineraryNotFoundException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.command.CreateItineraryBookingCommand;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.redis.CompressDecompressUtil;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.redis.RedisCommandFactory;
import io.lettuce.core.api.sync.RedisCommands;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertNotNull;

public class CreateProviderItineraryBookingServiceTest {

    @Mock
    private ProviderItineraryBookingRepository providerItineraryBookingRepository;

    @Mock
    private RedisCommandFactory redisCommandFactory;

    @Mock
    private RedisCommands<Serializable, Serializable> redisCommands;

    @Mock
    private ObjectMapper objectMapper;

    @Mock
    private CompressDecompressUtil compressDecompressUtil;

    @Mock
    private PricingService pricingService;

    @Mock
    private CreateProviderItineraryBookingRespBuilder createProviderItineraryBookingRespBuilder;

    private Map<Serializable, Serializable> itineraryRedisMap;

    private byte[] bytes;

    @BeforeMethod
    public void setUp() throws JsonProcessingException {

        MockitoAnnotations.openMocks(this);

        bytes = new ObjectMapper().writeValueAsBytes(ProviderItineraryObjectMother.ANY);

        when(redisCommandFactory.getRedisCommands()).thenReturn(redisCommands);
        when(compressDecompressUtil.compressBytes(any(byte[].class))).thenReturn(bytes);
        when(compressDecompressUtil.decompressBytes(any(byte[].class))).thenReturn(bytes);

        itineraryRedisMap = new HashMap<>();
        itineraryRedisMap.put(ProviderItineraryObjectMother.ANY.getItineraryId().getId(), bytes);

        ConfigurationEngine.init(new AbstractModule() {
            @Override
            protected void configure() {
                bind(CreateProviderItineraryBookingService.class).toInstance(new CreateProviderItineraryBookingService(
                        providerItineraryBookingRepository,
                        redisCommandFactory,
                        objectMapper,
                        compressDecompressUtil,
                        pricingService,
                        createProviderItineraryBookingRespBuilder));
            }
        });
    }

    @Test
    public void whenCreateProviderItineraryBookingAndProviderItineraryBookingExists() throws EasyJetBookingGatewayException {

        Mockito.when(providerItineraryBookingRepository.findItineraryBookingByItineraryId(any()))
                .thenReturn(Optional.of(ProviderItineraryBookingObjectMother.ANY));

        final CreateProviderItineraryBookingService createProviderItineraryBookingService =
                ConfigurationEngine.getInstance(CreateProviderItineraryBookingService.class);

        CreateItineraryBookingCommand providerItineraryBookingResult =
                createProviderItineraryBookingService.createProviderItineraryBooking(
                        ItineraryBookingCreationInternalObjectMother.ANY
                );

        verify(providerItineraryBookingRepository).findItineraryBookingByItineraryId(any());
        assertNotNull(providerItineraryBookingResult);
    }

    @Test
    public void whenCreateProviderItineraryBookingAndProviderItineraryNotBookingExists()
            throws EasyJetBookingGatewayException, IOException {

        when(objectMapper.readValue(any(byte[].class), any(Class.class))).thenReturn(ProviderItineraryBookingObjectMother.ANY);
        when(redisCommands.hgetall(anyString())).thenReturn(itineraryRedisMap);

        Mockito.when(providerItineraryBookingRepository.findItineraryBookingByItineraryId(any()))
                .thenReturn(Optional.empty());

        Mockito.when(pricingService.getCheapestFares(any())).thenReturn(ProviderDetailsObjectMother.ANY);

        final CreateProviderItineraryBookingService createProviderItineraryBookingService =
                ConfigurationEngine.getInstance(CreateProviderItineraryBookingService.class);

        CreateItineraryBookingCommand providerItineraryBookingResult =
                createProviderItineraryBookingService.createProviderItineraryBooking(
                        ItineraryBookingCreationInternalObjectMother.ANY
                );

        verify(redisCommandFactory).getRedisCommands();
        assertNotNull(providerItineraryBookingResult);
    }

    @Test(expectedExceptions = EasyJetBookingInternalException.class)
    public void whenCreateProviderItineraryBookingAndProviderItineraryDeserializationFail()
            throws EasyJetBookingGatewayException, IOException {

        when(objectMapper.readValue(any(byte[].class), any(Class.class)))
                .thenThrow(new IOException());

        when(redisCommands.hgetall(anyString())).thenReturn(itineraryRedisMap);

        Mockito.when(providerItineraryBookingRepository.findItineraryBookingByItineraryId(any()))
                .thenReturn(Optional.empty());

        final CreateProviderItineraryBookingService createProviderItineraryBookingService =
                ConfigurationEngine.getInstance(CreateProviderItineraryBookingService.class);

        createProviderItineraryBookingService.createProviderItineraryBooking(
                ItineraryBookingCreationInternalObjectMother.ANY
        );
    }

    @Test(expectedExceptions = ItineraryNotFoundException.class)
    public void whenCreateProviderItineraryBookingAndProviderItineraryNotFoundExceptionOnRedis()
            throws EasyJetBookingGatewayException, IOException {

        when(objectMapper.readValue(any(byte[].class), any(Class.class)))
                .thenReturn(null);

        when(redisCommands.hgetall(anyString())).thenReturn(Map.of());

        final CreateProviderItineraryBookingService createProviderItineraryBookingService =
                ConfigurationEngine.getInstance(CreateProviderItineraryBookingService.class);

        createProviderItineraryBookingService.createProviderItineraryBooking(
                ItineraryBookingCreationInternalObjectMother.ANY
        );
    }
}
