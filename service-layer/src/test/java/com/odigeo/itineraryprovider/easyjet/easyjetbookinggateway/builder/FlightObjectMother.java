package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;


import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.BookingStatus;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Flight;

public class FlightObjectMother {
    public static final Flight ANY = aFlight();

    public static Flight aFlight() {
        return Flight.builder()
                .setFlightNumber("AR1401")
                .setStatus(BookingStatus.CONFIRMED.name())
                .build();
    }

}
