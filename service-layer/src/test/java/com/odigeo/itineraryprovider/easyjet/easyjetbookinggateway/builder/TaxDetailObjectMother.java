package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Money;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TaxDetail;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TaxDetailId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TicketActionId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TicketId;

public class TaxDetailObjectMother {
    public static final TaxDetail ANY = aTaxDetail();

    private static TaxDetail aTaxDetail() {
        TaxDetailId id = TaxDetailIdObjectMother.ANY;
        TicketId ticketId = TicketIdObjectMother.ANY;
        Money amount = MoneyObjectMother.ANY;
        TicketActionId ticketActionId = TicketActionIdObjectMother.ANY;
        return TaxDetail.builder()
                .setId(id)
                .setTicketId(ticketId)
                .setKey("key")
                .setCategory("category")
                .setAmount(amount)
                .setTicketActionId(ticketActionId)
                .build();
    }
}
