package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.BaggageAllowance;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.BaggageAllowanceType;


public class BaggageAllowanceObjectMother {
    public static final BaggageAllowance ANY = aBaggageAllowance();

    public static BaggageAllowance aBaggageAllowance() {
        return BaggageAllowance.builder()
                .setBaggageAllowanceType(BaggageAllowanceType.KG)
                .setQuantity(15)
                .build();
    }
}
