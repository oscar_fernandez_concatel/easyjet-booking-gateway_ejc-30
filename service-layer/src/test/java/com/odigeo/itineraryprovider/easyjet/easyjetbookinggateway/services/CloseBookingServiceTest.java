package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.services;

import com.easyjet.b2b.request.OFTBookingRequestType;
import com.easyjet.b2b.response.CommitBookingResponseType;
import com.easyjet.b2b.response.ResponseRootType;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.easyjetadapter.adapter.boundary.EasyJetAdapterManager;
import com.odigeo.easyjetadapter.webservices.entity.exception.ProviderException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.adapter.BookingRequestTypeBuilder;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.CommitBookingResponseTypeObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.IssuedItineraryResponseBuilder;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.OFTBookingRequestTypeObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.PaymentItineraryBookingIdObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderItineraryBookingObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.itinerary.ProviderItineraryBookingRepository;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayErrorType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.ItineraryNotFoundException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.IssuedItineraryBooking;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PaymentItineraryBookingId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class CloseBookingServiceTest {

    @Mock
    ProviderItineraryBookingRepository providerItineraryBookingRepository;

    @Mock
    BookingRequestTypeBuilder builder;

    @Mock
    IssuedItineraryResponseBuilder issuedItineraryResponseBuilder;

    @Mock
    EasyJetAdapterManager easyJetAdapterManager;

    @BeforeMethod
    public void init() {

        MockitoAnnotations.openMocks(this);

        ConfigurationEngine.init(new AbstractModule() {
            @Override
            protected void configure() {
                bind(CloseBookingService.class).toInstance(
                        new CloseBookingService(providerItineraryBookingRepository, builder,
                                issuedItineraryResponseBuilder, easyJetAdapterManager));
            }
        });
    }

    @Test
    public void testWhenClose() throws ProviderException {
        PaymentItineraryBookingId paymentItineraryBookingId = PaymentItineraryBookingIdObjectMother.ANY;

        ProviderItineraryBooking providerItineraryBooking = ProviderItineraryBookingObjectMother.ANY;
        when(providerItineraryBookingRepository.find(any(UUID.class)))
                .thenReturn(providerItineraryBooking);

        OFTBookingRequestType request = OFTBookingRequestTypeObjectMother.ANY;
        ResponseRootType response = CommitBookingResponseTypeObjectMother.ANY;
        when(builder.build(any(ProviderItineraryBooking.class))).thenReturn(request);
        when(easyJetAdapterManager.createBooking(any(OFTBookingRequestType.class))).thenReturn(response);
        when(issuedItineraryResponseBuilder.build(any(ProviderItineraryBooking.class),
                any(CommitBookingResponseType.class))).thenCallRealMethod();

        CloseBookingService service =
                ConfigurationEngine.getInstance(CloseBookingService.class);

        IssuedItineraryBooking issuedItineraryBooking1 = service.close(paymentItineraryBookingId);

        Assert.assertNotNull(issuedItineraryBooking1);
    }

    @Test
    public void testWhenCloseAndAssertFail() throws ProviderException {
        PaymentItineraryBookingId paymentItineraryBookingId = PaymentItineraryBookingIdObjectMother.ANY;
        when(providerItineraryBookingRepository.find(any(UUID.class)))
                .thenReturn(null);
        ResponseRootType response = CommitBookingResponseTypeObjectMother.ANY;
        when(easyJetAdapterManager.createBooking(any(OFTBookingRequestType.class))).thenReturn(response);
        CloseBookingService service =
                ConfigurationEngine.getInstance(CloseBookingService.class);
        try {
            service.close(paymentItineraryBookingId);
        } catch (Exception x) {
            Assert.assertTrue(x instanceof EasyJetBookingGatewayException);
        }
    }


    @Test
    public void testWhenCloseAndThrowsInternalException() throws ProviderException {
        PaymentItineraryBookingId paymentItineraryBookingId = PaymentItineraryBookingIdObjectMother.ANY;
        when(providerItineraryBookingRepository.find(any(UUID.class)))
                .thenReturn(ProviderItineraryBookingObjectMother.ANY);

        when(easyJetAdapterManager.createBooking(any()))
                .thenThrow(new ProviderException("", new Exception()) {
                });

        CloseBookingService service =
                ConfigurationEngine.getInstance(CloseBookingService.class);
        try {
            service.close(paymentItineraryBookingId);
        } catch (Exception x) {
            Assert.assertTrue(x instanceof EasyJetBookingGatewayException);
        }
    }


    @Test
    public void testWhenCloseAndThrowsInternalException2() {
        PaymentItineraryBookingId paymentItineraryBookingId = PaymentItineraryBookingIdObjectMother.ANY;
        when(providerItineraryBookingRepository.find(any(UUID.class)))
                .thenThrow(new ItineraryNotFoundException("", EasyJetBookingGatewayErrorType.DATABASE_ERROR) {
                });
        CloseBookingService service =
                ConfigurationEngine.getInstance(CloseBookingService.class);
        try {
            service.close(paymentItineraryBookingId);
        } catch (Exception x) {
            Assert.assertTrue(x instanceof EasyJetBookingGatewayException);
        }
    }

    @Test
    public void testWhenCloseAndReceivedErrorList() throws ProviderException {
        PaymentItineraryBookingId paymentItineraryBookingId = PaymentItineraryBookingIdObjectMother.ANY;

        ProviderItineraryBooking providerItineraryBooking = ProviderItineraryBookingObjectMother.ANY;
        when(providerItineraryBookingRepository.find(any(UUID.class)))
                .thenReturn(providerItineraryBooking);
        OFTBookingRequestType request = OFTBookingRequestTypeObjectMother.ANY;
        ResponseRootType response = CommitBookingResponseTypeObjectMother.ANY_ERROR;
        when(builder.build(any(ProviderItineraryBooking.class))).thenReturn(request);
        when(easyJetAdapterManager.createBooking(any(OFTBookingRequestType.class))).thenReturn(response);
        CloseBookingService service =
                ConfigurationEngine.getInstance(CloseBookingService.class);

        try {
            service.close(paymentItineraryBookingId);
        } catch (Exception x) {
            Assert.assertTrue(x instanceof EasyJetBookingGatewayException);
        }
    }

}
