package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.BaggageAllowanceObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.BaggageAllowance;
import org.testng.Assert;
import org.testng.annotations.Test;

public class BaggageAllowanceMapperTest {

    @Test
    public void testToApi() {
        final BaggageAllowance baggageAllowanceModel = BaggageAllowanceObjectMother.ANY;
        com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.BaggageAllowance baggageAllowance = BaggageAllowanceMapper.INSTANCE.toApi(baggageAllowanceModel);
        Assert.assertNotNull(baggageAllowance);
        Assert.assertEquals(Integer.valueOf(baggageAllowance.getQuantity()), Integer.valueOf(baggageAllowanceModel.getQuantity()));
    }

}
