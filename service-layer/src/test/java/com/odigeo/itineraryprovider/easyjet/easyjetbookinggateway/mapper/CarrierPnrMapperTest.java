package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.SupplierLocatorObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.SupplierLocator;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CarrierPnrMapperTest {

    @Test
    public void testToApi() {
        final SupplierLocator supplierLocator = SupplierLocatorObjectMother.ANY;
        com.odigeo.suppliergatewayapi.v25.itinerary.booking.CarrierPnr carrierPnr = CarrierPnrMapper.INSTANCE.toApi(supplierLocator);
        Assert.assertNotNull(carrierPnr);
        Assert.assertEquals(carrierPnr.getCarrierCode(), supplierLocator.getSupplierCode());
    }
}
