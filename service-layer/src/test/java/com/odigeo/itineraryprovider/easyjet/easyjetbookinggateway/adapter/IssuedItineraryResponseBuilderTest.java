package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.adapter;

import com.easyjet.b2b.response.ResponseRootType;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.CommitBookingResponseTypeObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.IssuedItineraryResponseBuilder;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderItineraryBookingObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.IssuedItineraryBooking;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class IssuedItineraryResponseBuilderTest {

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);

        ConfigurationEngine.init(new AbstractModule() {
            @Override
            protected void configure() {
                bind(IssuedItineraryResponseBuilder.class).toInstance(new IssuedItineraryResponseBuilder());
            }
        });
    }

    @Test
    public void testBuild() {
        final ProviderItineraryBooking providerItineraryBooking = ProviderItineraryBookingObjectMother.ANY;
        final ResponseRootType responseRootType = CommitBookingResponseTypeObjectMother.ANY;

        final IssuedItineraryResponseBuilder issuedItineraryResponseBuilder =
                ConfigurationEngine.getInstance(IssuedItineraryResponseBuilder.class);

        final IssuedItineraryBooking issuedItineraryBooking = issuedItineraryResponseBuilder.build(providerItineraryBooking, responseRootType.getDataListRoot().getCommitBookingResponse());

        Assert.assertNotNull(issuedItineraryBooking);

    }
}
