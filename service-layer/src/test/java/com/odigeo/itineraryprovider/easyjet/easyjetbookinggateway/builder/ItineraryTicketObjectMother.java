package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryTicket;

public class ItineraryTicketObjectMother {
    public static final ItineraryTicket ANY = aItineraryTicket();

    private static ItineraryTicket aItineraryTicket() {
        ItineraryId id = ItineraryIdObjectMother.ANY;
        return ItineraryTicket.builder()
                .setItineraryId(id)
                .build();
    }
}
