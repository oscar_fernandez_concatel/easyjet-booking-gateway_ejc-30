package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.PhoneObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Phone;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PhoneMapperTest {
    @Test
    public void testToModel() {
        com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Phone phone = PhoneObjectMother.ANY_EXTERRNAL;
        Phone phoneModel = PhoneMapper.INSTANCE.toModel(phone);
        Assert.assertNotNull(phoneModel);
        Assert.assertNotNull(phone);
        Assert.assertEquals(phoneModel.getNumber(), phone.getNumber());
    }
}
