package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.PaymentDetail;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.BookingStatus;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Buyer;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Itinerary;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryTicket;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.SupplierLocator;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Traveller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProviderItineraryBookingObjectMother {
    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";

    public static final ProviderItineraryBooking ANY = aProviderItineraryBooking();

    private static ProviderItineraryBooking aProviderItineraryBooking() {

        Itinerary itinerary = ItineraryObjectMother.ANY;
        ItineraryId itineraryId = ItineraryIdObjectMother.ANY;

        UUID itineraryBookingId = UUID.fromString(IDENTIFIER);
        Buyer buyer = BuyerObjectMother.ANY;
        Traveller traveller = TravellerObjectMother.ANY;
        List<Traveller> travellers = new ArrayList<>();
        travellers.add(traveller);

        ItineraryTicket itineraryTicket = ItineraryTicketObjectMother.ANY;
        List<ItineraryTicket> itineraryTickets = new ArrayList<>();
        itineraryTickets.add(itineraryTicket);

        SupplierLocator supplierLocator = SupplierLocatorObjectMother.ANY;
        List<SupplierLocator> supplierLocators = new ArrayList<>();
        supplierLocators.add(supplierLocator);

        return ProviderItineraryBooking.builder()
                .setItineraryBookingId(itineraryBookingId)
                .setPnr("pnr")
                .setUniversalLocator("universalLocator")
                .setReservationLocator("reservationLocator")
                .setStatus(BookingStatus.PENDING)
                .setBuyer(buyer)
                .setTravellers(travellers)
                .setItineraryTickets(itineraryTickets)
                .setSupplierLocators(supplierLocators)
                .setItinerary(itinerary)
                .setItineraryId(itineraryId)
                .setPaymentDetails(List.of(PaymentDetail.builder()
                        .setCardVendorCode("VI")
                        .setPaymentInstrumentToken(UUID.randomUUID().toString())
                        .setItineraryBookingId(itineraryBookingId)
                        .build()))
                .setTotalFarePrice(BigDecimal.valueOf(52.99))
                .build();
    }


}
