package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Buyer;

public class BuyerObjectMother {
    public static final Buyer ANY = aBuyer();
    public static final com.odigeo.suppliergatewayapi.v25.itinerary.booking.Buyer ANY_EXTERNAL = aBuyerExternal();

    private static com.odigeo.suppliergatewayapi.v25.itinerary.booking.Buyer aBuyerExternal() {
        com.odigeo.suppliergatewayapi.v25.itinerary.booking.Buyer buyer = new com.odigeo.suppliergatewayapi.v25.itinerary.booking.Buyer();
        buyer.setName("name");
        buyer.setMail("mail@odigeo.com");
        buyer.setPhoneNumber("phoneNumber");
        buyer.setCountryCode("countryCode");
        buyer.setAddress("address");
        buyer.setCityName("cityName");
        buyer.setZipCode("zipCode");
        buyer.setLastNames("lastNames");
        buyer.setPrimeMember(false);
        return buyer;
    }

    private static Buyer aBuyer() {
        return Buyer.builder()
                .setName("name")
                .setMail("mail@odigeo.com")
                .setPhoneNumber("phoneNumber")
                .setCountryCode("countryCode")
                .setAddress("address")
                .setCityName("cityName")
                .setZipCode("zipCode")
                .setLastNames("lastNames")
                .setPrimeMember(false)
                .build();
    }

}
