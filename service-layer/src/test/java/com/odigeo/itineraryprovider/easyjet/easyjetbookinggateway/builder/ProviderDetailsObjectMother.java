package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FareDetails;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Itinerary;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderDetails;

public class ProviderDetailsObjectMother {
    public static final ProviderDetails ANY = aProviderDetails();

    public static ProviderDetails aProviderDetails() {
        FareDetails fareDetails = FareDetailsObjectMother.ANY;
        Itinerary itinerary = ItineraryObjectMother.ANY;

        return ProviderDetails.builder()
                .setItinerary(itinerary)
                .setFareDetails(fareDetails)
                .build();
    }
}
