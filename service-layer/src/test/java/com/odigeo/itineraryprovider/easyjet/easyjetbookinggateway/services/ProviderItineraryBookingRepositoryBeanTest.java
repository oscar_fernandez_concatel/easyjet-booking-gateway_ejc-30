package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.services;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderItineraryBookingObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.itinerary.ProviderItineraryBookingRepository;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.ItineraryNotFoundException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.PaymentDetail;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;

public class ProviderItineraryBookingRepositoryBeanTest {

    @Mock
    private EntityManager entityManager;

    @Mock
    private ProviderItineraryBookingRepository providerItineraryBookingRepository;

    private final ProviderItineraryBookingRepositoryBean sut = new ProviderItineraryBookingRepositoryBean();

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);

        ConfigurationEngine.init(new AbstractModule() {
            @Override
            protected void configure() {
                bind(ProviderItineraryBookingRepositoryBean.class).toInstance(new ProviderItineraryBookingRepositoryBean(
                        entityManager,
                        providerItineraryBookingRepository));
            }
        });
    }

    @Test
    public void whenFindItineraryBookingByItineraryId() {

        Mockito.when(providerItineraryBookingRepository
                        .findItineraryBookingByItineraryId(any()))
                .thenReturn(Optional.of(ProviderItineraryBookingObjectMother.ANY));

        ProviderItineraryBookingRepositoryBean providerItineraryBookingRepositoryBean =
                ConfigurationEngine.getInstance(ProviderItineraryBookingRepositoryBean.class);

        Optional<ProviderItineraryBooking> result =
                providerItineraryBookingRepositoryBean.findItineraryBookingByItineraryId(UUID.randomUUID());

        Assert.assertNotNull(result);
        Assert.assertTrue(result.isPresent());
    }

    @Test
    public void whenFindById() throws ItineraryNotFoundException {

        Mockito.when(providerItineraryBookingRepository
                        .find(any()))
                .thenReturn(ProviderItineraryBookingObjectMother.ANY);

        ProviderItineraryBookingRepositoryBean providerItineraryBookingRepositoryBean =
                ConfigurationEngine.getInstance(ProviderItineraryBookingRepositoryBean.class);

        ProviderItineraryBooking result =
                providerItineraryBookingRepositoryBean.find(UUID.randomUUID());

        Assert.assertNotNull(result);
    }

    @Test
    public void whenSaveItineraryBooking() {

        ProviderItineraryBookingRepositoryBean providerItineraryBookingRepositoryBean =
                ConfigurationEngine.getInstance(ProviderItineraryBookingRepositoryBean.class);

        providerItineraryBookingRepositoryBean.saveItineraryBooking(ProviderItineraryBookingObjectMother.ANY);
    }

    @Test
    public void whenUpdateItineraryBooking() {

        ProviderItineraryBookingRepositoryBean providerItineraryBookingRepositoryBean =
                ConfigurationEngine.getInstance(ProviderItineraryBookingRepositoryBean.class);

        providerItineraryBookingRepositoryBean.updateItineraryBooking(ProviderItineraryBookingObjectMother.ANY);
    }

    @Test
    public void whenPostConstruct() {

        ProviderItineraryBookingRepositoryBean providerItineraryBookingRepositoryBean =
                ConfigurationEngine.getInstance(ProviderItineraryBookingRepositoryBean.class);

        providerItineraryBookingRepositoryBean.postConstruct();
    }

    @Test
    public void whenSaveOrUpdateItineraryBookingPaymentDetail() {

        ProviderItineraryBookingRepositoryBean providerItineraryBookingRepositoryBean =
                ConfigurationEngine.getInstance(ProviderItineraryBookingRepositoryBean.class);

        providerItineraryBookingRepositoryBean.saveOrUpdateItineraryBookingPaymentDetail(
                PaymentDetail.builder().build()
        );
    }
}
