package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.easyjet.b2b.response.PaymentTypeElement;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.Constants;

import java.util.ArrayList;
import java.util.List;

public class PaymentTypeObjectMother {

    public static final PaymentTypeElement ANY_EASYJET = aPaymentTypeElement("AX", "CCC");

    public static PaymentTypeElement aPaymentTypeElement(String code, String fee) {
        PaymentTypeElement paymentTypeElement = new PaymentTypeElement();
        paymentTypeElement.setCreditCardCode(code);
        paymentTypeElement.setFee(fee);
        paymentTypeElement.setBankNameRequiredFlag("FLAG");
        paymentTypeElement.setCardNoreq("NOREQ");
        return paymentTypeElement;
    }

    public static List<PaymentTypeElement> getListPaymentTypeElementsBalanced() {
        return new ArrayList<>() { {
                add(aPaymentTypeElement(Constants.AX, Constants.PAYMENT_TYPE_FEE_CREDIT));
                add(aPaymentTypeElement(Constants.MC, Constants.PAYMENT_TYPE_FEE_CREDIT));
                add(aPaymentTypeElement(Constants.DC, Constants.PAYMENT_TYPE_FEE_DEBIT));
                add(aPaymentTypeElement(Constants.DL, Constants.PAYMENT_TYPE_FEE_CREDIT));
                add(aPaymentTypeElement(Constants.SW, Constants.PAYMENT_TYPE_FEE_DEBIT));
            } };
    }
}
