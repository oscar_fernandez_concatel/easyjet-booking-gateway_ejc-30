package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PaymentInstrumentToken;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PaymentMethodType;

import java.util.UUID;

public class PaymentInstrumentTokenObjectMother {
    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";
    public static final PaymentInstrumentToken ANY = aPaymentInstrumentToken();

    private static PaymentInstrumentToken aPaymentInstrumentToken() {
        return PaymentInstrumentToken.builder()
                .setUuid(UUID.fromString(IDENTIFIER))
                .setType(PaymentMethodType.CREDITCARD)
                .build();
    }
}
