package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Buyer;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryBookingCreationInternal;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Traveller;

import java.util.ArrayList;
import java.util.List;

public class ItineraryBookingCreationInternalObjectMother {

    public static final ItineraryBookingCreationInternal ANY = aItineraryBookingCreationInternal();

    private static ItineraryBookingCreationInternal aItineraryBookingCreationInternal() {

        Buyer buyer = BuyerObjectMother.ANY;
        Traveller traveller = TravellerObjectMother.ANY;
        List<Traveller> travellers = new ArrayList<>();
        travellers.add(traveller);

        return ItineraryBookingCreationInternal.builder()
                .setBuyer(buyer)
                .setTravellers(travellers)
                .setSelectedItineraryId(ProviderItineraryObjectMother.ANY.getItineraryId().getId())
                .build();
    }
}
