package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightSection;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Segment;

import java.util.ArrayList;
import java.util.List;

public class SegmentObjectMother {
    public static final Segment ANY = aSegment();

    public static Segment aSegment() {

        FlightSection section = FlightSectionObjectMother.ANY;
        List<FlightSection> sections = new ArrayList<>();
        sections.add(section);
        ItineraryId itineraryId = ItineraryIdObjectMother.ANY;

        return Segment.builder()
                .setItineraryId(itineraryId)
                .setPosition(1)
                .setSections(sections)
                .build();
    }
}
