package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryBookingCreationObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryBookingCreationInternal;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.creation.ItineraryBookingCreation;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ItineraryBookingCreationMapperTest {
    @Test
    public void testToModel() {
        ItineraryBookingCreation itineraryBookingCreation = ItineraryBookingCreationObjectMother.ANY;
        ItineraryBookingCreationInternal itineraryBookingCreationInternal = ItineraryBookingCreationMapper.INSTANCE.toModel(itineraryBookingCreation);
        Assert.assertNotNull(itineraryBookingCreationInternal);
    }
}
