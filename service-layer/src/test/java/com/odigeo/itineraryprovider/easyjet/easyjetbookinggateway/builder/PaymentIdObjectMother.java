package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PaymentId;

import java.util.UUID;

public class PaymentIdObjectMother {
    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";
    public static final PaymentId ANY = aPaymentId();

    private static PaymentId aPaymentId() {
        return PaymentId.builder()
                .setId(UUID.fromString(IDENTIFIER))
                .build();
    }

}
