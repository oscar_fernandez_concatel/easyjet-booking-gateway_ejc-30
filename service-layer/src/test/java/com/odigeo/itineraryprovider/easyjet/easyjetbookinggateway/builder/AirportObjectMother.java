package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.geoapi.last.responses.Airport;

public class AirportObjectMother {
    public static final Airport ANY = aAirport();

    public static Airport aAirport() {
        Airport airport = new Airport();
        airport.setIataCode("IATACODE");
        return airport;
    }
}
