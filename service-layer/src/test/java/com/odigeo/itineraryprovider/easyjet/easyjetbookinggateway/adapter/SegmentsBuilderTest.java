package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.adapter;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryIdObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.SegmentObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Segment;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class SegmentsBuilderTest {

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        ConfigurationEngine.init(new AbstractModule() {
            @Override
            protected void configure() {
                bind(SegmentsBuilder.class).toInstance(new SegmentsBuilder());
            }
        });

    }

    @Test
    public void whenBuildSegments() {

        ItineraryId id = ItineraryIdObjectMother.ANY;
        Segment segment = SegmentObjectMother.ANY;
        List<Segment> segments = new ArrayList<Segment>();
        segments.add(segment);

        final SegmentsBuilder segmentsBuilder =
                ConfigurationEngine.getInstance(SegmentsBuilder.class);

        final List<Segment> list = segmentsBuilder.buildSegments(segments, id);

        Assert.assertNotNull(list);
        org.testng.Assert.assertFalse(list.isEmpty());

        Assert.assertEquals(list.get(0).getItineraryId().getId().toString(), id.getId().toString());
        Assert.assertNotNull(list.get(0).getPosition());

        Assert.assertEquals(list.get(0).getSections().get(0).getId().getId().toString(), id.getId().toString());
        Assert.assertNotNull(list.get(0).getSections().get(0).getSegmentPosition());

    }

}
