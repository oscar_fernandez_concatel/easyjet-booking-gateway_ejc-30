package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderPaymentOptions;

public class ProviderPaymentOptionsObjectMother {
    public static final ProviderPaymentOptions ANY = aProviderPaymentOptions();

    public static ProviderPaymentOptions aProviderPaymentOptions() {
        return ProviderPaymentOptions.builder()
                .build();
    }
}
