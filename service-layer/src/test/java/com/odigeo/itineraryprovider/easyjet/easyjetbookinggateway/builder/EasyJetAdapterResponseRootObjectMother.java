package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;


import com.easyjet.b2b.response.DataListRootType;
import com.easyjet.b2b.response.ErrorListRootType;
import com.easyjet.b2b.response.ErrorListType;
import com.easyjet.b2b.response.ErrorType;
import com.easyjet.b2b.response.PaymentTypeList;
import com.easyjet.b2b.response.ResponseRootType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.Constants;

public class EasyJetAdapterResponseRootObjectMother {


    public static ResponseRootType getResponseForPaymentTypesBalanced() {
        ResponseRootType response = getEmptyResponseRootType();
        PaymentTypeList paymentTypeList = new PaymentTypeList();
        paymentTypeList.getPayment()
                .add(PaymentTypeObjectMother.aPaymentTypeElement(Constants.AX, Constants.PAYMENT_TYPE_FEE_DEBIT));
        paymentTypeList.getPayment()
                .add(PaymentTypeObjectMother.aPaymentTypeElement(Constants.VI, Constants.PAYMENT_TYPE_FEE_CREDIT));
        paymentTypeList.getPayment()
                .add(PaymentTypeObjectMother.aPaymentTypeElement(Constants.MC, Constants.PAYMENT_TYPE_FEE_DEBIT));
        paymentTypeList.getPayment()
                .add(PaymentTypeObjectMother.aPaymentTypeElement(Constants.DC, Constants.PAYMENT_TYPE_FEE_DEBIT));
        paymentTypeList.getPayment()
                .add(PaymentTypeObjectMother.aPaymentTypeElement(Constants.DL, Constants.PAYMENT_TYPE_FEE_DEBIT));

        response.getDataListRoot().setPaymentTypes(paymentTypeList);

        return response;
    }

    public static ResponseRootType getResponseForPaymentTypesWithUnsupportedTypes() {
        ResponseRootType response = getEmptyResponseRootType();
        PaymentTypeList paymentTypeList = new PaymentTypeList();
        paymentTypeList.getPayment()
                .add(PaymentTypeObjectMother.aPaymentTypeElement("UNSUPP", Constants.PAYMENT_TYPE_FEE_DEBIT));
        paymentTypeList.getPayment()
                .add(PaymentTypeObjectMother.aPaymentTypeElement(Constants.VI, Constants.PAYMENT_TYPE_FEE_CREDIT));
        paymentTypeList.getPayment()
                .add(PaymentTypeObjectMother.aPaymentTypeElement(Constants.MC, Constants.PAYMENT_TYPE_FEE_DEBIT));
        paymentTypeList.getPayment()
                .add(PaymentTypeObjectMother.aPaymentTypeElement(Constants.DC, Constants.PAYMENT_TYPE_FEE_DEBIT));
        paymentTypeList.getPayment()
                .add(PaymentTypeObjectMother.aPaymentTypeElement(Constants.DL, Constants.PAYMENT_TYPE_FEE_DEBIT));

        response.getDataListRoot().setPaymentTypes(paymentTypeList);

        return response;
    }

    public static ResponseRootType getWithErrorList() {
        ResponseRootType response = new ResponseRootType();
        ErrorListRootType errorListRootType = new ErrorListRootType();
        response.setErrorListRoot(errorListRootType);
        response.setSuccess("0");
        ErrorListType errorListType = new ErrorListType();
        ErrorType errorType = new ErrorType();
        errorType.setNumber("11");
        errorType.setDescription("11");
        errorListType.setError(errorType);
        errorListRootType.getErrorList().add(errorListType);
        return response;
    }

    private static ResponseRootType getEmptyResponseRootType() {
        ResponseRootType response = new ResponseRootType();
        DataListRootType dataListRootType = new DataListRootType();
        response.setDataListRoot(dataListRootType);

        return response;
    }
}
