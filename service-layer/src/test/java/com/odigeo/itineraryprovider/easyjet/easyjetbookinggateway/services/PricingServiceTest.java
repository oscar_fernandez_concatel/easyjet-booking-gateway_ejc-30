package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.services;

import com.easyjet.b2b.request.OFTPricingRequestType;
import com.easyjet.b2b.response.ResponseRootType;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.easyjetadapter.adapter.boundary.EasyJetAdapterManager;
import com.odigeo.easyjetadapter.webservices.entity.exception.ProviderException;
import com.odigeo.geoapi.last.AirportService;
import com.odigeo.geoapi.last.GeoNodeNotFoundException;
import com.odigeo.geoapi.last.GeoServiceException;
import com.odigeo.geoapi.last.mock.AirportBuilder;
import com.odigeo.geoapi.last.responses.Airport;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.EasyJetBookingGatewayConfigurationMapperObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.EasyJetItinerarySelectionObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.OFTPricingRequestTypeObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.OFTPricingResponseTypeObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.configuration.EasyJetBookingGatewayConfiguration;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper.EasyJetPricingRequestMapper;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingInternalException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.EasyJetItinerarySelection;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderDetails;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Random;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class PricingServiceTest {

    @Mock
    private EasyJetAdapterManager easyJetAdapterManager;
    @Mock
    private EasyJetPricingRequestMapper easyJetPricingRequestMapper;
    @Mock
    private AirportService airportService;
    @Mock
    private EasyJetBookingGatewayConfiguration easyJetGatewayConfiguration;

    @BeforeMethod
    public void init() throws GeoNodeNotFoundException, GeoServiceException {

        MockitoAnnotations.openMocks(this);

        when(airportService.getEntityByGeoNodeId(anyInt()))
                .thenReturn(new AirportBuilder().iataCode("IATACODE").build(new Random()));

        ConfigurationEngine.init(new AbstractModule() {
            @Override
            protected void configure() {
                bind(PricingService.class).toInstance(new PricingService(
                        easyJetAdapterManager, easyJetPricingRequestMapper, airportService, easyJetGatewayConfiguration
                ));
                bind(AirportService.class).toInstance(airportService);
            }
        });
    }

    @Test
    public void testWhenGetCheapestFares() throws ProviderException, EasyJetBookingInternalException, GeoNodeNotFoundException, GeoServiceException {

        EasyJetItinerarySelection easyJetItinerarySelection = EasyJetItinerarySelectionObjectMother.ANY;

        when(easyJetAdapterManager.getPricingList(any(OFTPricingRequestType.class))).thenReturn(OFTPricingResponseTypeObjectMother.ANY);
        when(easyJetPricingRequestMapper.map(any(EasyJetItinerarySelection.class), any(Airport.class), any(Airport.class), anyString(), anyString())).thenReturn(OFTPricingRequestTypeObjectMother.ANY);
        when(easyJetGatewayConfiguration.getOftPricingLanguageCode())
                .thenReturn(EasyJetBookingGatewayConfigurationMapperObjectMother.ANY.getOftPricingLanguageCode());
        when(easyJetGatewayConfiguration.getOftPricingBookingCodeInfant())
                .thenReturn(EasyJetBookingGatewayConfigurationMapperObjectMother.ANY.getOftPricingBookingCodeInfant());

        PricingService service =
                ConfigurationEngine.getInstance(PricingService.class);

        ProviderDetails providerDetails = service.getCheapestFares(easyJetItinerarySelection);

        Assert.assertNotNull(providerDetails.getFareDetails());
        Assert.assertNotNull(providerDetails.getItinerary());

        Assert.assertEquals(providerDetails.getFareDetails().getTotalFareAmount().getAmount().toString(), "68.00");
        Assert.assertEquals(providerDetails.getItinerary().getFareDetails().getTotalFareAmount().getAmount().toString(), "68.00");
    }

    @Test
    public void testWhenGetCheapestFaresAndReceivedErrorList() throws ProviderException {

        ResponseRootType responseRootType = OFTPricingResponseTypeObjectMother.ANY_ERROR;

        when(easyJetAdapterManager.getPricingList(any(OFTPricingRequestType.class))).thenReturn(responseRootType);
        when(easyJetPricingRequestMapper.map(any(EasyJetItinerarySelection.class), any(Airport.class), any(Airport.class), anyString(), anyString())).thenReturn(OFTPricingRequestTypeObjectMother.ANY);
        when(easyJetGatewayConfiguration.getOftPricingLanguageCode())
                .thenReturn(EasyJetBookingGatewayConfigurationMapperObjectMother.ANY.getOftPricingLanguageCode());
        when(easyJetGatewayConfiguration.getOftPricingBookingCodeInfant())
                .thenReturn(EasyJetBookingGatewayConfigurationMapperObjectMother.ANY.getOftPricingBookingCodeInfant());

        PricingService service =
                ConfigurationEngine.getInstance(PricingService.class);

        try {
            service.getCheapestFares(EasyJetItinerarySelectionObjectMother.ANY);
        } catch (Exception x) {
            Assert.assertTrue(x instanceof EasyJetBookingInternalException);
        }
    }

    @Test
    public void testWhenGetCheapestFaresAndThrowsInternalException() throws ProviderException {
        when(easyJetAdapterManager.getPricingList(any(OFTPricingRequestType.class))).thenThrow(new ProviderException("", new Exception()) {
        });
        PricingService service =
                ConfigurationEngine.getInstance(PricingService.class);
        try {
            service.getCheapestFares(EasyJetItinerarySelectionObjectMother.ANY);
        } catch (Exception x) {
            Assert.assertTrue(x instanceof EasyJetBookingInternalException);
        }
    }

}
