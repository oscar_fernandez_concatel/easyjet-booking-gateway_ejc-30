package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.easyjet.b2b.response.CommitBookingResponseType;
import com.easyjet.b2b.response.DataListRootType;
import com.easyjet.b2b.response.ErrorListRootType;
import com.easyjet.b2b.response.ErrorListType;
import com.easyjet.b2b.response.ErrorType;
import com.easyjet.b2b.response.ResponseRootType;

import java.util.Collections;

public class CommitBookingResponseTypeObjectMother {
    public static final ResponseRootType ANY = aResponseRootType();
    public static final ResponseRootType ANY_ERROR = aResponseRootTypeError();

    private static ResponseRootType aResponseRootType() {

        ResponseRootType response = new ResponseRootType();

        ErrorListRootType errorListRootType = new ErrorListRootType();
        response.setErrorListRoot(errorListRootType);
        response.setSuccess("1");
        errorListRootType.getErrorList().addAll(Collections.EMPTY_LIST);


        CommitBookingResponseType commitBookingResponseType = new CommitBookingResponseType();
        commitBookingResponseType.setBookingNumber("bookingNUmber");
        DataListRootType dataListRootType = new DataListRootType();
        dataListRootType.setCommitBookingResponse(commitBookingResponseType);
        response.setDataListRoot(dataListRootType);

        return response;
    }

    private static ResponseRootType aResponseRootTypeError() {

        ResponseRootType response = new ResponseRootType();
        ErrorListRootType errorListRootType = new ErrorListRootType();
        response.setErrorListRoot(errorListRootType);
        response.setSuccess("0");
        ErrorListType errorListType = new ErrorListType();
        ErrorType errorType = new ErrorType();
        errorType.setNumber("400002");
        errorType.setDescription("400002");
        errorListType.setError(errorType);
        errorListRootType.getErrorList().add(errorListType);
        return response;
    }

}
