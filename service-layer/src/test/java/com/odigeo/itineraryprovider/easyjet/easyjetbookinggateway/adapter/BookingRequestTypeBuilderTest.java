package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.adapter;

import com.easyjet.b2b.request.OFTBookingRequestType;
import com.edreams.configuration.ConfigurationEngine;
import com.edreamsodigeo.payments.paymentinstrument.contract.PaymentInstrumentResourceException;
import com.edreamsodigeo.payments.paymentinstrument.contract.creditcard.CreditCardPciScope;
import com.edreamsodigeo.payments.paymentinstrument.contract.creditcard.CreditCardPciScopeResource;
import com.google.inject.AbstractModule;
import com.odigeo.geoapi.last.AirportService;
import com.odigeo.geoapi.last.GeoNodeNotFoundException;
import com.odigeo.geoapi.last.GeoServiceException;
import com.odigeo.geoapi.last.mock.AirportBuilder;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderItineraryBookingObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingInternalException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Random;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;

public class BookingRequestTypeBuilderTest {

    @Mock
    private AirportService airportService;

    @Mock
    private CreditCardPciScopeResource creditCardPciScopeResource;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);

        ConfigurationEngine.init(new AbstractModule() {
            @Override
            protected void configure() {
                bind(BookingRequestTypeBuilder.class).toInstance(
                        new BookingRequestTypeBuilder(airportService, creditCardPciScopeResource));
            }
        });
    }

    @Test
    public void testBuild()
            throws EasyJetBookingInternalException,
            GeoNodeNotFoundException,
            GeoServiceException, PaymentInstrumentResourceException {

        ProviderItineraryBooking providerItineraryBooking =
                ProviderItineraryBookingObjectMother.ANY;

        CreditCardPciScope creditCardPciScope = new CreditCardPciScope();
        creditCardPciScope.setNumber("5106906595054446");
        creditCardPciScope.setExpirationYear("2024");
        creditCardPciScope.setExpirationMonth("10");
        creditCardPciScope.setHolder("Juan Perez Galan");
        creditCardPciScope.setCvv("111");

        Mockito.when(creditCardPciScopeResource.getPciScope(any()))
                .thenReturn(creditCardPciScope);

        Mockito.when(airportService.getEntityByGeoNodeId(anyInt()))
                .thenReturn(new AirportBuilder().iataCode("IATACODE")
                        .build(new Random()));

        BookingRequestTypeBuilder bookingRequestTypeBuilder =
                ConfigurationEngine.getInstance(BookingRequestTypeBuilder.class);

        OFTBookingRequestType oftBookingRequestType =
                bookingRequestTypeBuilder.build(providerItineraryBooking);

        Assert.assertNotNull(oftBookingRequestType);

    }

    @Test(expectedExceptions = EasyJetBookingInternalException.class)
    public void testBuildAndPaymentInstrumentResourceException()
            throws EasyJetBookingInternalException, PaymentInstrumentResourceException,
            GeoNodeNotFoundException, GeoServiceException {

        ProviderItineraryBooking providerItineraryBooking =
                ProviderItineraryBookingObjectMother.ANY;

        Mockito.when(airportService.getEntityByGeoNodeId(anyInt()))
                .thenReturn(new AirportBuilder().iataCode("IATACODE")
                        .build(new Random()));

        Mockito.when(creditCardPciScopeResource.getPciScope(any()))
                .thenThrow(new PaymentInstrumentResourceException(""));

        BookingRequestTypeBuilder bookingRequestTypeBuilder =
                ConfigurationEngine.getInstance(BookingRequestTypeBuilder.class);

        bookingRequestTypeBuilder.build(providerItineraryBooking);

    }

    @Test(expectedExceptions = EasyJetBookingInternalException.class)
    public void testBuildAndGeoNodeException()
            throws EasyJetBookingInternalException, PaymentInstrumentResourceException,
            GeoNodeNotFoundException, GeoServiceException {

        ProviderItineraryBooking providerItineraryBooking =
                ProviderItineraryBookingObjectMother.ANY;

        Mockito.when(airportService.getEntityByGeoNodeId(anyInt()))
                .thenThrow(new GeoNodeNotFoundException(""));

        Mockito.when(creditCardPciScopeResource.getPciScope(any()))
                .thenThrow(new PaymentInstrumentResourceException(""));

        BookingRequestTypeBuilder bookingRequestTypeBuilder =
                ConfigurationEngine.getInstance(BookingRequestTypeBuilder.class);

        bookingRequestTypeBuilder.build(providerItineraryBooking);

    }

}
