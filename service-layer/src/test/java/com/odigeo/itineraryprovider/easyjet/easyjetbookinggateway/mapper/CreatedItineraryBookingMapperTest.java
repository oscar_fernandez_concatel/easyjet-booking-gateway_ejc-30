package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderItineraryBookingObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;
import com.odigeo.suppliergatewayapi.v25.ancillaries.baggage.entity.selection.BaggageSelection;
import com.odigeo.suppliergatewayapi.v25.ancillaries.seats.entity.selection.SeatSelection;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class CreatedItineraryBookingMapperTest {

    @Test
    public void testToApi() {
        ProviderItineraryBooking itineraryBooking = ProviderItineraryBookingObjectMother.ANY;
        List<SeatSelection> seatSelections = new ArrayList<>();
        List<BaggageSelection> baggageSelections = new ArrayList<>();
        com.odigeo.suppliergatewayapi.v25.itinerary.booking.creation.CreatedItineraryBooking createdItineraryBooking = CreatedItineraryBookingMapper.INSTANCE.toApi(itineraryBooking);
        Assert.assertNotNull(createdItineraryBooking);
        Assert.assertEquals(createdItineraryBooking.getId().getId(), itineraryBooking.getItineraryBookingId());
    }

}
