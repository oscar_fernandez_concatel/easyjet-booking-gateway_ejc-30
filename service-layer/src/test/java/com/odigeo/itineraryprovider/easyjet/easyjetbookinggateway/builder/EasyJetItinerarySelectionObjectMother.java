package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.EasyJetItinerarySelection;

public class EasyJetItinerarySelectionObjectMother {
    public static final EasyJetItinerarySelection ANY = aEasyJetItinerarySelection();

    public static EasyJetItinerarySelection aEasyJetItinerarySelection() {
        return EasyJetItinerarySelection.builder()
                .setVisitId("1")
                .setVisitInformation("VISITINFORMATION")
                .setItinerary(ItineraryObjectMother.ANY)
                .setProviderPaymentOptions(ProviderPaymentOptionsObjectMother.ANY)
                .setProviderPaymentDetail(ProviderPaymentDetailObjectMother.ANY)
                .build();
    }
}
