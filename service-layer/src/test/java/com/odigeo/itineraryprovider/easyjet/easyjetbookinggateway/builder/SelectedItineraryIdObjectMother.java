package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.suppliergatewayapi.v25.itinerary.selection.SelectedItineraryId;

import java.util.UUID;

public class SelectedItineraryIdObjectMother {
    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";
    public static final SelectedItineraryId ANY = aSelectedItineraryId();

    private static SelectedItineraryId aSelectedItineraryId() {
        SelectedItineraryId selectedItineraryId = new SelectedItineraryId();
        selectedItineraryId.setId(UUID.fromString(IDENTIFIER));
        return selectedItineraryId;
    }

}
