package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.adapter;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.CreateProviderItineraryBookingRespBuilder;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryBookingCreationInternalObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderDetailsObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderItineraryObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryBookingCreationInternal;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderDetails;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItinerary;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class CreateProviderItineraryBookingRespBuilderTest {

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);

        ConfigurationEngine.init(new AbstractModule() {
            @Override
            protected void configure() {
                bind(CreateProviderItineraryBookingRespBuilder.class).toInstance(new CreateProviderItineraryBookingRespBuilder());
            }
        });
    }

    @Test
    public void testBuild() {

        final ItineraryBookingCreationInternal itineraryBookingCreationInternal = ItineraryBookingCreationInternalObjectMother.ANY;
        final ProviderItinerary providerItinerary = ProviderItineraryObjectMother.ANY;
        final ProviderDetails providerDetails = ProviderDetailsObjectMother.ANY;

        final CreateProviderItineraryBookingRespBuilder createProviderItineraryBookingRespBuilder =
                ConfigurationEngine.getInstance(CreateProviderItineraryBookingRespBuilder.class);

        final ProviderItineraryBooking providerItineraryBooking = createProviderItineraryBookingRespBuilder.build(itineraryBookingCreationInternal, providerItinerary, providerDetails);

        Assert.assertNotNull(providerItineraryBooking);

    }
}
