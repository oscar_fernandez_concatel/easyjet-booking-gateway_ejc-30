package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.easyjet.b2b.response.OFTPricingResponseType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.OFTPricingResponseTypeObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Itinerary;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderDetails;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ProviderDetailsMapperTest {

    @Test
    public void testToApi() {
        Itinerary itinerary = ItineraryObjectMother.ANY;
        OFTPricingResponseType response = OFTPricingResponseTypeObjectMother.ANY.getDataListRoot().getOFTPricingResponse();

        ProviderDetails providerDetails = ProviderDetailsMapper.INSTANCE.mapProviderDetails(itinerary, response);

        Assert.assertNotNull(providerDetails.getFareDetails());
        Assert.assertNotNull(providerDetails.getItinerary());

        Assert.assertEquals(providerDetails.getFareDetails().getTotalFareAmount().getAmount().toString(), "68.00");
        Assert.assertEquals(providerDetails.getFareDetails().getTotalFareAmount().getCurrency().getCurrencyCode(), response.getCurrency());
        Assert.assertEquals(providerDetails.getFareDetails().getTotalTaxAmount().getAmount().toString(), response.getPassengerList().getPassengers().get(0).getPassengerMonetaryDataGroup().getPassengerMonetaryData().get(0).getTax());
        Assert.assertEquals(providerDetails.getFareDetails().getTotalTaxAmount().getCurrency().getCurrencyCode(), response.getCurrency());

        Assert.assertEquals(providerDetails.getItinerary().getFareDetails().getTotalFareAmount().getAmount().toString(), "68.00");
        Assert.assertEquals(providerDetails.getItinerary().getFareDetails().getTotalFareAmount().getCurrency().getCurrencyCode(), response.getCurrency());
        Assert.assertEquals(providerDetails.getItinerary().getFareDetails().getTotalTaxAmount().getAmount().toString(), response.getPassengerList().getPassengers().get(0).getPassengerMonetaryDataGroup().getPassengerMonetaryData().get(0).getTax());
        Assert.assertEquals(providerDetails.getItinerary().getFareDetails().getTotalTaxAmount().getCurrency().getCurrencyCode(), response.getCurrency());

    }

}
