package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.services;

import com.easyjet.b2b.request.OFTBookingRequestType;
import com.easyjet.b2b.response.CommitBookingResponseType;
import com.easyjet.b2b.response.ResponseRootType;
import com.google.inject.Inject;
import com.odigeo.commons.monitoring.metrics.Metric;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.easyjetadapter.adapter.boundary.EasyJetAdapterManager;
import com.odigeo.easyjetadapter.webservices.entity.exception.ProviderException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.adapter.BookingRequestTypeBuilder;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.IssuedItineraryResponseBuilder;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.commons.monitoring.metrics.MetricsConstants;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.itinerary.ProviderItineraryBookingRepository;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayErrorType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingInternalException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.ItineraryNotFoundException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.BookingStatus;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.IssuedItineraryBooking;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PaymentItineraryBookingId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;
import org.apache.log4j.Logger;

public class CloseBookingService extends ServiceBase {

    private static final Logger logger = Logger.getLogger(CloseBookingService.class);

    private final ProviderItineraryBookingRepository providerItineraryBookingRepository;
    private final BookingRequestTypeBuilder builder;
    private final IssuedItineraryResponseBuilder issuedItineraryResponseBuilder;
    private final EasyJetAdapterManager easyJetAdapterManager;

    @Inject
    public CloseBookingService(
            ProviderItineraryBookingRepository providerItineraryBookingRepository,
            BookingRequestTypeBuilder builder,
            IssuedItineraryResponseBuilder issuedItineraryResponseBuilder,
            EasyJetAdapterManager easyJetAdapterManager) {
        this.providerItineraryBookingRepository = providerItineraryBookingRepository;
        this.builder = builder;
        this.issuedItineraryResponseBuilder = issuedItineraryResponseBuilder;
        this.easyJetAdapterManager = easyJetAdapterManager;
    }

    public IssuedItineraryBooking close(
            PaymentItineraryBookingId paymentItineraryBookingId) throws EasyJetBookingGatewayException {
        try {

            ProviderItineraryBooking storedItineraryBooking =
                    providerItineraryBookingRepository.find(paymentItineraryBookingId.getId());

            assertItineraryBooking(storedItineraryBooking);

            OFTBookingRequestType request = builder.build(storedItineraryBooking);

            CommitBookingResponseType response = this.commitBooking(request);

            saveItineraryBooking(storedItineraryBooking, response);

            return issuedItineraryResponseBuilder.build(storedItineraryBooking, response);
        } catch (ItineraryNotFoundException e) {
            logger.error(e.getMessage());
            throw new EasyJetBookingGatewayException(e, EasyJetBookingGatewayErrorType.DATABASE_ERROR, e.getMessage());
        } catch (EasyJetBookingInternalException e) {
            logger.error(e.getMessage());
            throw new EasyJetBookingGatewayException(e, EasyJetBookingGatewayErrorType.ERROR_IN_ADAPTER, e.getMessage());
        }
    }

    private void saveItineraryBooking(
            ProviderItineraryBooking storedItineraryBooking,
            CommitBookingResponseType response) {

        ProviderItineraryBooking itineraryBooking = ProviderItineraryBooking.builder()
                .setPnr(response.getBookingNumber())
                .setItineraryBookingId(storedItineraryBooking.getItineraryBookingId())
                .setPaymentDetails(storedItineraryBooking.getPaymentDetails())
                .setBuyer(storedItineraryBooking.getBuyer())
                .setSupplierLocators(storedItineraryBooking.getSupplierLocators())
                .setStatus(BookingStatus.CONFIRMED)
                .setItinerary(storedItineraryBooking.getItinerary())
                .setItineraryId(storedItineraryBooking.getItineraryId())
                .build();

        this.providerItineraryBookingRepository.updateItineraryBooking(itineraryBooking);

    }

    private CommitBookingResponseType commitBooking(OFTBookingRequestType request) throws EasyJetBookingInternalException {
        Metric providerCallMetric = MetricsConstants.getProviderCall(MetricsConstants.PROVIDER_COMMIT_BOOKING);

        try {
            MetricsUtils.startTimer(providerCallMetric, MetricsConstants.METRICS_REGISTRY_NAME);
            ResponseRootType response = easyJetAdapterManager.createBooking(request);
            MetricsUtils.stopTimer(providerCallMetric, MetricsConstants.METRICS_REGISTRY_NAME);
            super.checkProviderResponseResult(response, MetricsConstants.PROVIDER_COMMIT_BOOKING);
            return response.getDataListRoot().getCommitBookingResponse();

        } catch (ProviderException e) {
            MetricsUtils.incrementCounter(MetricsConstants.getProviderException(
                    e.getClass().getSimpleName(), MetricsConstants.PROVIDER_COMMIT_BOOKING), MetricsConstants.METRICS_REGISTRY_NAME);
            throw new EasyJetBookingInternalException(e.getMessage(), EasyJetBookingGatewayErrorType.ERROR_IN_ADAPTER); //NOPMD - No interference with main flow
        }

    }

    private void assertItineraryBooking(ProviderItineraryBooking providerItineraryBooking)
            throws EasyJetBookingGatewayException {
        if (providerItineraryBooking == null) {
            throw new EasyJetBookingGatewayException("Itinerary Booking not found",
                    EasyJetBookingGatewayErrorType.ITINERARY_NOT_FOUND); //NOPMD
        }
    }
}
