package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryBookingCreationInternal;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.creation.ItineraryBookingCreation;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ItineraryBookingCreationMapper {

    ItineraryBookingCreationMapper INSTANCE = Mappers.getMapper(ItineraryBookingCreationMapper.class);

    default ItineraryBookingCreationInternal toModel(final ItineraryBookingCreation itineraryBookingCreation) {

        return ItineraryBookingCreationInternal.builder()
                .setBuyer(BuyerMapper.INSTANCE.toModel(itineraryBookingCreation.getBuyer()))
                .setTravellers(TravellerMapper.INSTANCE.toModel(itineraryBookingCreation.getTravellers()))
                .setSelectedItineraryId(itineraryBookingCreation.getItineraryId().getId())
                .build();
    }
}
