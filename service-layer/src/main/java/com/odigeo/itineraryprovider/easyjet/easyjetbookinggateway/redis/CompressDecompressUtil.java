package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.redis;

import com.google.inject.Singleton;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

@Singleton
public class CompressDecompressUtil {

    public byte[] compressBytes(byte[] bytes) {
        Deflater compressor = new Deflater();
        compressor.setLevel(Deflater.BEST_SPEED);
        compressor.setInput(bytes);
        compressor.finish();

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream(bytes.length)) {
            byte[] buf = new byte[1024];
            while (!compressor.finished()) {
                int count = compressor.deflate(buf);
                outputStream.write(buf, 0, count);
            }
            compressor.end();
            return outputStream.toByteArray();
        } catch (IOException e) { //NOPMD - No interference with main flow
            return new byte[0];
        }
    }

    public byte[] decompressBytes(byte[] bytes) {
        Inflater inflater = new Inflater();
        inflater.setInput(bytes);

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream(bytes.length)) {
            byte[] buffer = new byte[1024];

            while (!inflater.finished()) {
                int count = inflater.inflate(buffer);
                outputStream.write(buffer, 0, count);
            }
            inflater.end();
            return outputStream.toByteArray();
        } catch (IOException | DataFormatException e) {
            return new byte[0];
        }
    }
}
