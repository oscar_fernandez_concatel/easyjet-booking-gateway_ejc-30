package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.easyjet.b2b.response.OFTPricingResponseType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.BookingCodes;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FareDetails;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Itinerary;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Money;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PassengerTypeFare;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderDetails;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TravellerType;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;

@Mapper
public interface ProviderDetailsMapper {
    ProviderDetailsMapper INSTANCE = Mappers.getMapper(ProviderDetailsMapper.class);
    String FARE = "FARE";

    default ProviderDetails mapProviderDetails(Itinerary itinerary, OFTPricingResponseType response) {

        FareDetails fareDetails = FareDetails.builder()
                .setTotalFareAmount(Money.builder()
                        .setCurrency(Currency.getInstance(response.getCurrency()))
                        .setAmount(new BigDecimal(response.getTotalAmount()))
                        .build())
                .setTotalTaxAmount(updatedTotalTaxAmount(response))
                .setCorporateCodes(itinerary.getFareDetails().getCorporateCodes())
                .setItineraryId(itinerary.getId())
                .setPassengerFares(updatedPassengerTypeFareMap(itinerary, response))
                .build();

        Itinerary mappedItinerary = Itinerary.builder()
                .setFareDetails(fareDetails)
                .setId(itinerary.getId())
                .setDepartureDate(itinerary.getDepartureDate())
                .setSegments(itinerary.getSegments())
                .setDepartureGeoNodeId(itinerary.getDepartureGeoNodeId())
                .setReturnDate(itinerary.getReturnDate())
                .setTripType(itinerary.getTripType())
                .setProviderPaymentOptions(itinerary.getProviderPaymentOptions())
                .setArrivalGeoNodeId(itinerary.getArrivalGeoNodeId())
                .build();

        return ProviderDetails.builder()
                .setItinerary(mappedItinerary)
                .setFareDetails(fareDetails)
                .build();
    }

    default Money updatedTotalTaxAmount(OFTPricingResponseType response) {

        BigDecimal sumTaxAmount = response.getPassengerList().getPassengers().stream()
                .flatMap(passengerType -> passengerType.getPassengerMonetaryDataGroup().getPassengerMonetaryData().stream())
                .filter(x -> (x.getTax() != null) && FARE.equals(x.getType()))
                .map(x -> new BigDecimal(x.getTax()))
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        return Money.builder()
                .setAmount(sumTaxAmount)
                .setCurrency(Currency.getInstance(response.getCurrency()))
                .build();
    }

    default Map<TravellerType, PassengerTypeFare> updatedPassengerTypeFareMap(Itinerary itinerary, OFTPricingResponseType response) {
        Map<TravellerType, PassengerTypeFare> passengerFares = new HashMap<>();
        itinerary.getFareDetails().getPassengerFares().entrySet().stream()
                .forEach(entry -> {
                    PassengerTypeFare passengerTypeFare = entry.getValue();
                    String bookingCode = BookingCodes.getValue(passengerTypeFare.getTravellerType().name()).name();
                    BigDecimal tax = response.getPassengerList().getPassengers().stream()
                            .filter(passengerType -> passengerType.getBookingCodes().getPassengerTypeCode().equals(bookingCode))
                            .flatMap(passengerType -> passengerType.getPassengerMonetaryDataGroup().getPassengerMonetaryData().stream())
                            .filter(x -> (x.getTax() != null) && FARE.equals(x.getType()))
                            .map(x -> new BigDecimal(x.getTax()))
                            .reduce(BigDecimal.ZERO, BigDecimal::add);

                    BigDecimal fare = response.getPassengerList().getPassengers().stream()
                            .filter(passengerType -> passengerType.getBookingCodes().getPassengerTypeCode().equals(bookingCode))
                            .flatMap(passengerType -> passengerType.getPassengerMonetaryDataGroup().getPassengerMonetaryData().stream())
                            .filter(x -> (x.getAmount() != null) && FARE.equals(x.getType()))
                            .map(x -> new BigDecimal(x.getAmount()))
                            .reduce(BigDecimal.ZERO, BigDecimal::add);

                    Money fareAmount = Money.builder()
                            .setAmount(fare)
                            .setCurrency(Currency.getInstance(response.getCurrency()))
                            .build();
                    Money taxAmount = Money.builder()
                            .setAmount(tax)
                            .setCurrency(Currency.getInstance(response.getCurrency()))
                            .build();

                    PassengerTypeFare passengerTypeFare1 = PassengerTypeFare.builder()
                            .setFareAmount(fareAmount)
                            .setTaxAmount(taxAmount)
                            .setPassengersPerType(passengerTypeFare.getPassengersPerType())
                            .setDiscount(passengerTypeFare.isDiscount())
                            .setTravellerType(passengerTypeFare.getTravellerType())
                            .build();
                    passengerFares.put(entry.getKey(), passengerTypeFare1);
                });
        return passengerFares;
    }
}
