package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.adapter;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Flight;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightSection;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Segment;
import org.apache.commons.collections.CollectionUtils;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Singleton
public class SegmentsBuilder {

    public List<Segment> buildSegments(List<Segment> segments, ItineraryId itineraryId) {
        final AtomicInteger segmentPosition = new AtomicInteger(0);

        return segments.stream().map(s -> {
            final int segmentIndex = segmentPosition.getAndIncrement();
            List<FlightSection> flightSections = new ArrayList<>();
            if (CollectionUtils.isNotEmpty(s.getSections())) {
                flightSections = s.getSections().stream().map(secFlight -> buildFlightSection(itineraryId, segmentIndex, secFlight.getFlight(), secFlight)).collect(Collectors.toUnmodifiableList());
            }
            return buildSegment(itineraryId, segmentIndex, flightSections);
        }).collect(Collectors.toUnmodifiableList());
    }

    private Segment buildSegment(ItineraryId itineraryId, int position, List<FlightSection> flightSections) {
        return Segment.builder()
                .setItineraryId(itineraryId)
                .setPosition(position)
                .setSections(flightSections)
                .build();
    }

    private FlightSection buildFlightSection(ItineraryId itineraryId, int segmentPosition, Flight flight, FlightSection secFlight) {
        return FlightSection.builder()
                .setId(itineraryId)
                .setSegmentPosition(segmentPosition)
                .setFlight(flight)
                .setDepartureGeoNodeId(secFlight.getDepartureGeoNodeId())
                .setArrivalGeoNodeId(secFlight.getArrivalGeoNodeId())
                .setArrivalTerminal(secFlight.getArrivalTerminal())
                .setDepartureTerminal(secFlight.getDepartureTerminal())
                .setArrivalDate(secFlight.getArrivalDate())
                .setDepartureDate(secFlight.getDepartureDate())
                .setBaggageAllowance(secFlight.getBaggageAllowance())
                .setCabinClass(secFlight.getCabinClass())
                .setFlightFareInformation(secFlight.getFlightFareInformation())
                .build();
    }

}
