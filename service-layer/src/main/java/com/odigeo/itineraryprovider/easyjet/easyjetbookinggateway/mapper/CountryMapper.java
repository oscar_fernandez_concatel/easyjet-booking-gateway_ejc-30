package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Country;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CountryMapper {

    CountryMapper INSTANCE = Mappers.getMapper(CountryMapper.class);

    default Country toModel(final com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Country country) {

        if (country == null) {
            return null;
        }

        return Country.builder()
                .setCountryCode(country.getCountryCode())
                .setPhonePrefix(country.getPhonePrefix())
                .setCountryCode3Letters(country.getCountryCode3Letters())
                .setNumCountryCode(country.getNumCountryCode())
                .build();
    }

    default com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Country toApi(final Country countryModel) {

        if (countryModel == null) {
            return null;
        }

        com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Country country =
                new com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Country();

        country.setCountryCode(countryModel.getCountryCode());
        country.setPhonePrefix(countryModel.getPhonePrefix());
        country.setCountryCode3Letters(countryModel.getCountryCode3Letters());
        country.setNumCountryCode(countryModel.getNumCountryCode());

        return country;
    }
}
