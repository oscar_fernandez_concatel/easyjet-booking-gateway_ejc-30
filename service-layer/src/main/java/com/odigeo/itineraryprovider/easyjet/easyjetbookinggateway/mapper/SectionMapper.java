package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Section;
import com.odigeo.suppliergatewayapi.v25.itinerary.ItineraryId;
import com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.FlightSection;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface SectionMapper {

    SectionMapper INSTANCE = Mappers.getMapper(SectionMapper.class);

    default com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.Section toApi(
            final Section sectionModel) {

        com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.Section section =
                new FlightSection();

        ItineraryId itineraryId = new ItineraryId();
        itineraryId.setId(sectionModel.getId().getId());
        section.setId(itineraryId);

        section.setArrivalDate(sectionModel.getArrivalDate());
        section.setDepartureDate(sectionModel.getDepartureDate());
        section.setArrivalGeoNodeId(sectionModel.getArrivalGeoNodeId());
        section.setDepartureGeoNodeId(sectionModel.getDepartureGeoNodeId());
        section.setArrivalTerminal(sectionModel.getArrivalTerminal());
        section.setDepartureTerminal(sectionModel.getDepartureTerminal());
        section.setCabinClass(sectionModel.getCabinClass().name());
        section.setSegmentPosition(sectionModel.getSegmentPosition());
        section.setBaggageAllowance(BaggageAllowanceMapper.INSTANCE.toApi(sectionModel.getBaggageAllowance()));

        return section;
    }
}
