package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Money;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderPaymentDetail;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request.FlightSectionMapper;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request.MoneyMapper;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request.PhoneMapper;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request.ProviderPaymentMethodMapper;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.payment.PaymentItineraryBooking;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {FlightSectionMapper.class, PhoneMapper.class, ProviderPaymentMethodMapper.class})
public abstract class PaymentItineraryBookingResponseMapper {

    public static final PaymentItineraryBookingResponseMapper INSTANCE = Mappers.getMapper(PaymentItineraryBookingResponseMapper.class);

    @Mappings({
            @Mapping(target = "itineraryBooking.itinerary", source = "providerItineraryBooking.itinerary"),
            @Mapping(target = "itineraryBooking.buyer", source = "providerItineraryBooking.buyer"),
            @Mapping(target = "itineraryBooking.travellers", source = "providerItineraryBooking.travellers"),
            @Mapping(target = "itineraryBooking.id.id", source = "providerItineraryBooking.itineraryBookingId"),
            @Mapping(target = "id.id", source = "providerItineraryBooking.itineraryBookingId"),
            @Mapping(target = "itineraryBooking.pnr", source = "providerItineraryBooking.pnr"),
            @Mapping(target = "itineraryBooking.itinerary.fareDetails.passengerFares", source = "providerItineraryBooking.itinerary.fareDetails.passengerFares"),
            @Mapping(target = "paymentDetails.paymentMethod", source = "providerPaymentDetail.providerPaymentMethod"),
            @Mapping(target = "itineraryBooking.itinerary.providerCode", constant = "U2")
        })
    public abstract PaymentItineraryBooking toApi(ProviderItineraryBooking providerItineraryBooking, ProviderPaymentDetail providerPaymentDetail, Money fee);

    @AfterMapping
    public void afterMapping(Money fee, @MappingTarget PaymentItineraryBooking paymentItineraryBooking) {
        paymentItineraryBooking.getPaymentDetails().getPaymentMethod().setFee(MoneyMapper.INSTANCE.toApi(fee));
    }
}
