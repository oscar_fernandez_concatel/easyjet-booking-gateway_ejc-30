package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.adapter;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.EasyJetItinerarySelection;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FareDetails;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Itinerary;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderDetails;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItinerary;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderPaymentOptions;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Segment;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;

@Singleton
public class ProviderItineraryBuilder {

    private final SegmentsBuilder segmentsBuilder;

    @Inject
    public ProviderItineraryBuilder(SegmentsBuilder segmentsBuilder) {
        this.segmentsBuilder = segmentsBuilder;
    }

    public ProviderItinerary createProviderItinerary(EasyJetItinerarySelection itinerarySelection, ProviderDetails providerDetails, ProviderPaymentOptions providerPaymentOptions) {
        final ItineraryId itineraryId = itinerarySelection.getItinerary().getId();
        final List<Segment> segments = segmentsBuilder.buildSegments(itinerarySelection.getItinerary().getSegments(), itineraryId);
        final FareDetails fareDetails = setIdInFareDetails(providerDetails, itineraryId);
        final Itinerary itinerary = buildItinerary(itineraryId, itinerarySelection.getItinerary(), segments, fareDetails, providerPaymentOptions);
        return buildProviderItinerary(itineraryId, itinerary);
    }

    public ProviderItinerary createProviderItinerary(
            ProviderDetails providerDetails,
            ProviderPaymentOptions providerPaymentOptions) {
        final ItineraryId itineraryId = generateItineraryId();
        final FareDetails fareDetails = setIdInFareDetails(providerDetails, itineraryId);
        final Itinerary itinerary = buildItinerary(ItineraryId.generate(), providerDetails.getItinerary(), providerDetails.getItinerary().getSegments(), fareDetails, providerPaymentOptions);
        return buildProviderItinerary(itineraryId, itinerary);
    }

    private ProviderItinerary buildProviderItinerary(ItineraryId itineraryId, Itinerary itinerary) {
        return ProviderItinerary.builder()
                .setItinerary(itinerary)
                .setItineraryId(itineraryId).build();
    }

    private Itinerary buildItinerary(ItineraryId itineraryId, Itinerary itinerarySelection, List<Segment> segments, FareDetails fareDetails, ProviderPaymentOptions providerPaymentOptions) {
        return Itinerary.builder()
                .setId(itineraryId)
                .setArrivalGeoNodeId(itinerarySelection.getArrivalGeoNodeId())
                .setDepartureGeoNodeId(itinerarySelection.getDepartureGeoNodeId())
                .setDepartureDate(itinerarySelection.getDepartureDate())
                .setReturnDate(itinerarySelection.getReturnDate())
                .setTripType(itinerarySelection.getTripType())
                .setSegments(segments)
                .setFareDetails(fareDetails)
                .setProviderPaymentOptions(providerPaymentOptions)
                .build();
    }

    private ItineraryId generateItineraryId() {
        return ItineraryId.generate();
    }

    private FareDetails setIdInFareDetails(ProviderDetails providerDetails, ItineraryId itineraryId) {
        return FareDetails.builder()
                .setItineraryId(itineraryId)
                .setCorporateCodes(providerDetails.getFareDetails().getCorporateCodes())
                .setTotalTaxAmount(providerDetails.getFareDetails().getTotalTaxAmount())
                .setTotalFareAmount(providerDetails.getFareDetails().getTotalFareAmount())
                .setPassengerFares(providerDetails.getFareDetails().getPassengerFares())
                .build();
    }

}
