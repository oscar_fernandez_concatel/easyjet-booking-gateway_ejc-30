package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.creation.CreatedItineraryBooking;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.creation.CreatedItineraryBookingId;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CreatedItineraryBookingMapper {

    CreatedItineraryBookingMapper INSTANCE = Mappers.getMapper(CreatedItineraryBookingMapper.class);

    default CreatedItineraryBooking toApi(
            ProviderItineraryBooking itineraryBooking) {

        CreatedItineraryBooking createdItineraryBooking = new CreatedItineraryBooking();

        CreatedItineraryBookingId createdItineraryBookingId = new CreatedItineraryBookingId();
        createdItineraryBookingId.setId(itineraryBooking.getItineraryBookingId());
        createdItineraryBooking.setId(createdItineraryBookingId);

        createdItineraryBooking.setItineraryBooking(ItineraryBookingMapper.INSTANCE.toApi(itineraryBooking));

        return createdItineraryBooking;
    }
}
