package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.easyjet.b2b.response.PaymentTypeElement;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.Constants;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardCategory;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardPaymentOption;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardProduct;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardSpec;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderPaymentOptions;

import javax.inject.Singleton;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Singleton
public class EasyJetPaymentTypeResponseMapper {

    public ProviderPaymentOptions map(List<PaymentTypeElement> paymentTypes) {
        Set<CreditCardPaymentOption> creditCardPaymentOptions = new HashSet<>();
        paymentTypes.forEach(paymentTypeElement -> {
            CreditCardCategory creditCardCategory = CreditCardCategory.CREDIT;
            CreditCardSpec creditCardSpec = CreditCardSpec.builder()
                    .setCreditCardProduct(CreditCardProduct.DEFAULT)
                    .setCreditCardScheme(Constants.PAYMENT_TYPE_SCHEMA.get(paymentTypeElement.getCreditCardCode()))
                    .setCreditCardCategory(creditCardCategory)
                    .build();

            CreditCardPaymentOption creditCardPaymentOption = CreditCardPaymentOption.builder()
                    .setCreditCardSpec(creditCardSpec)
                            .build();
            creditCardPaymentOptions.add(creditCardPaymentOption);
        });

        return ProviderPaymentOptions.builder()
                .setCreditCardPaymentOptions(creditCardPaymentOptions)
                .setCashPaymentOptions(new HashSet<>())
                .build();
    }
}
