package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Phone;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PhoneMapper {

    PhoneMapper INSTANCE = Mappers.getMapper(PhoneMapper.class);

    default Phone toModel(final com.odigeo.suppliergatewayapi.v25
            .itinerary.booking.traveller.Phone phone) {

        return Phone.builder()
                .setCountry(CountryMapper.INSTANCE.toModel(phone.getCountry()))
                .setNumber(phone.getNumber())
                .setPhoneType(phone.getPhoneType())
                .build();
    }

    default com.odigeo.suppliergatewayapi.v25
            .itinerary.booking.traveller.Phone toApi(final Phone phoneModel) {

        if (phoneModel == null) {
            return null;
        }

        return new com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Phone(
                CountryMapper.INSTANCE.toApi(phoneModel.getCountry()),
                phoneModel.getNumber(),
                phoneModel.getPhoneType()
        );
    }
}
