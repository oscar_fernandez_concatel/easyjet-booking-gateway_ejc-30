package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.services;

import com.easyjet.b2b.response.OFTPricingResponseType;
import com.easyjet.b2b.response.ResponseRootType;
import com.google.inject.Inject;
import com.odigeo.commons.monitoring.metrics.Metric;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.easyjetadapter.adapter.boundary.EasyJetAdapterManager;
import com.odigeo.easyjetadapter.webservices.entity.exception.ProviderException;
import com.odigeo.geoapi.last.AirportService;
import com.odigeo.geoapi.last.GeoNodeNotFoundException;
import com.odigeo.geoapi.last.GeoServiceException;
import com.odigeo.geoapi.last.responses.Airport;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.commons.monitoring.metrics.MetricsConstants;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.configuration.EasyJetBookingGatewayConfiguration;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper.EasyJetPricingRequestMapper;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper.ProviderDetailsMapper;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayErrorType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingInternalException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.EasyJetItinerarySelection;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderDetails;

public class PricingService extends ServiceBase {

    private final EasyJetAdapterManager easyJetAdapterManager;
    private final EasyJetPricingRequestMapper easyJetPricingRequestMapper;
    private final AirportService airportService;
    private final EasyJetBookingGatewayConfiguration easyJetGatewayConfiguration;

    @Inject
    public PricingService(
            final EasyJetAdapterManager easyJetAdapterManager,
            final EasyJetPricingRequestMapper easyJetPricingRequestMapper,
            final AirportService airportService,
            final EasyJetBookingGatewayConfiguration easyJetGatewayConfiguration) {
        this.easyJetAdapterManager = easyJetAdapterManager;
        this.easyJetPricingRequestMapper = easyJetPricingRequestMapper;
        this.airportService = airportService;
        this.easyJetGatewayConfiguration = easyJetGatewayConfiguration;
    }

    public ProviderDetails getCheapestFares(
            final EasyJetItinerarySelection itinerarySelection) throws EasyJetBookingInternalException {
        try {
            Airport depAirport = airportService.getEntityByGeoNodeId(itinerarySelection.getItinerary().getDepartureGeoNodeId());
            Airport arrAirport = airportService.getEntityByGeoNodeId(itinerarySelection.getItinerary().getArrivalGeoNodeId());
            final com.easyjet.b2b.request.OFTPricingRequestType oftPricingRequest = easyJetPricingRequestMapper.map(itinerarySelection,
                    depAirport, arrAirport, easyJetGatewayConfiguration.getOftPricingLanguageCode(),
                    easyJetGatewayConfiguration.getOftPricingBookingCodeInfant());
            return this.mapPricingEasyJetResponse(
                    getPricingListFromEasyJetInternal(oftPricingRequest),
                    itinerarySelection);
        } catch (final ProviderException | RuntimeException | GeoServiceException | GeoNodeNotFoundException e) { //NOPMD - No interference with main flow
            MetricsUtils.incrementCounter(MetricsConstants.getProviderException(
                    e.getClass().getSimpleName(), MetricsConstants.PROVIDER_OFT_PRICING), MetricsConstants.METRICS_REGISTRY_NAME);
            throw new EasyJetBookingInternalException(e.getMessage(), EasyJetBookingGatewayErrorType.ERROR_IN_ADAPTER); //NOPMD - No interference with main flow
        }
    }

    private ProviderDetails mapPricingEasyJetResponse(
            final ResponseRootType responseRootType,
            final EasyJetItinerarySelection itinerarySelection) throws EasyJetBookingInternalException {

        super.checkProviderResponseResult(responseRootType);

        final OFTPricingResponseType response =
                responseRootType.getDataListRoot().getOFTPricingResponse();

        return ProviderDetailsMapper.INSTANCE.mapProviderDetails(itinerarySelection.getItinerary(), response);
    }

    private ResponseRootType getPricingListFromEasyJetInternal(
            final com.easyjet.b2b.request.OFTPricingRequestType oftPricingRequest) throws ProviderException {
        final Metric providerCallMetric = MetricsConstants.getProviderCall(MetricsConstants.PROVIDER_OFT_PRICING);
        try {
            MetricsUtils.startTimer(providerCallMetric, MetricsConstants.METRICS_REGISTRY_NAME);
            return easyJetAdapterManager.getPricingList(oftPricingRequest);
        } finally {
            MetricsUtils.stopTimer(providerCallMetric, MetricsConstants.METRICS_REGISTRY_NAME);
        }
    }

}
