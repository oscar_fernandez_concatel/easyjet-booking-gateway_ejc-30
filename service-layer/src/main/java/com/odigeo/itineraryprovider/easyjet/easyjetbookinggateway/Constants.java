package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway;

import com.google.common.collect.ImmutableMap;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardScheme;

import java.util.UUID;

public class Constants {

    public static final String OFFICE_ID = "eDO-API-" + UUID.randomUUID();
    public static final String IATA_NUMBER = "00030715";
    public static final String DEFAULT_LANGUAGE_CODE = "EN";

    public static final String PAYMENT_TYPE_FEE_CREDIT = "CCB";
    public static final String PAYMENT_TYPE_FEE_DEBIT = "DDD";
    public static final String PAYMENT_TYPE_CREDIT_CARD = "CC";

    public static final String AX = "AX";
    public static final String MC = "MC";
    public static final String DC = "DC";
    public static final String DL = "DL";
    public static final String VI = "VI";
    public static final String SW = "SW";
    public static final String EL = "EL";
    public static final String TP = "TP";
    public static final String DM = "DM";

    public static final String ITINERARY_REDIS_CACHE_NAME = "Selected_Itinerary";

    public static final ImmutableMap<String, CreditCardScheme> PAYMENT_TYPE_SCHEMA =
            ImmutableMap.<String, CreditCardScheme>builder()
                    .put(Constants.AX, CreditCardScheme.AMEX)
                    .put(Constants.MC, CreditCardScheme.MASTERCARD)
                    .put(Constants.DC, CreditCardScheme.DINERS_CLUB)
                    .put(Constants.DL, CreditCardScheme.VISA)
                    .put(Constants.VI, CreditCardScheme.VISA)
                    .put(Constants.SW, CreditCardScheme.MAESTRO)
                    .put(Constants.EL, CreditCardScheme.VISA)
                    .put(Constants.TP, CreditCardScheme.AIRPLUS)
                    .put(Constants.DM, CreditCardScheme.MASTERCARD)
                    .build();

}
