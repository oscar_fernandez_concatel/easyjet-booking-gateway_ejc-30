package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.configuration;

import com.google.inject.AbstractModule;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.configuration.jee.JeeServiceProvider;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.configuration.jee.UnavailableServiceException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.itinerary.ProviderItineraryBookingRepository;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.services.CloseBookingService;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.services.PaymentDetailsService;
import org.apache.log4j.Logger;

import java.util.Properties;

@SuppressWarnings("PMD")
public class ServiceLayerModule extends AbstractModule {

    private static final Logger LOGGER = Logger.getLogger(ServiceLayerModule.class);

    @Override
    protected void configure() {

        configureJeeService(ProviderItineraryBookingRepository.class);

        bind(PaymentDetailsService.class).asEagerSingleton();
        bind(CloseBookingService.class).asEagerSingleton();

        initStaxConfiguration();
    }

    private <T> void configureJeeService(Class<T> clazz) {
        configureJeeService(clazz, clazz);
    }

    private <T> void configureJeeService(Class<T> clazz, Class<? extends T> providerClazz) {
        try {
            bind(clazz).toProvider(JeeServiceProvider.getInstance(providerClazz));
        } catch (UnavailableServiceException e) {
            LOGGER.error("Unable to create provider " + providerClazz + " for class " + clazz, e);
        }
    }

    void initStaxConfiguration() {
        Properties props = System.getProperties();
        props.setProperty("org.apache.cxf.stax.allowInsecureParser", "1");
    }
}
