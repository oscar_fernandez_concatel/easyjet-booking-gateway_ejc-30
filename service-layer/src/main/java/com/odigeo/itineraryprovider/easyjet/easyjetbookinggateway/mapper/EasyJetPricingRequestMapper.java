package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.easyjet.b2b.request.OFTPricingRequestType;
import com.easyjet.b2b.request.State;
import com.google.inject.Singleton;
import com.odigeo.geoapi.last.responses.Airport;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.Constants;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.CreditCardUtil;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.BookingCodes;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCard;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.EasyJetItinerarySelection;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FareClass;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightSection;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PassengerTypeFare;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderPaymentDetail;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TravellerType;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Singleton
public class EasyJetPricingRequestMapper {

    public OFTPricingRequestType map(
            EasyJetItinerarySelection itinerarySelection,
            Airport depAirport,
            Airport arrAirport,
            String languageCode,
            String bookingCodeInfant) {

        OFTPricingRequestType oftPricingRequest = new OFTPricingRequestType();

        OFTPricingRequestType.FormOfPayment formOfPayment = getFormOfPayment(itinerarySelection);

        List<OFTPricingRequestType.Itinerary.Segment> segments = new ArrayList<>();
        itinerarySelection.getItinerary().getSections().forEach(flightSection -> {
            OFTPricingRequestType.Itinerary.Segment segment = new OFTPricingRequestType.Itinerary.Segment();
            segment.setSegmentReference(getSegmentReference(flightSection));
            segment.setOriginatorSegmentID("SEG" + getSegmentReference(flightSection));
            segment.setSegmentState(State.NEW);
            segment.setDepartureDateTime(flightSection.getDepartureDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            segment.setDepartureLocation(depAirport.getIataCode());
            segment.setArrivalDateTime(flightSection.getArrivalDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            segment.setArrivalLocation(arrAirport.getIataCode());
            segment.setFlightNumber(flightSection.getFlight().getFlightNumber());
            segment.setFareClass(FareClass.getValue("Y").name());
            segments.add(segment);
        });


        OFTPricingRequestType.PassengerList passengerList = new OFTPricingRequestType.PassengerList();

        int passengerReference = 1;
        for (Map.Entry<TravellerType, PassengerTypeFare> entry : itinerarySelection.getItinerary().getFareDetails().getPassengerFares().entrySet()) {

            TravellerType key = entry.getKey();
            PassengerTypeFare passengerTypeFare = entry.getValue();

            for (int i = 0; i < passengerTypeFare.getPassengersPerType(); i++) {

                OFTPricingRequestType.PassengerList.Passenger.BookingCodes bookingCodes = new OFTPricingRequestType.PassengerList.Passenger.BookingCodes();
                String bookingCode = BookingCodes.getValue(key.name()).name();
                bookingCodes.setPassengerTypeCode(bookingCode);

                OFTPricingRequestType.PassengerList.Passenger.SegmentData segmentData = new OFTPricingRequestType.PassengerList.Passenger.SegmentData();
                itinerarySelection.getItinerary().getSections().forEach(flightSection -> {
                    OFTPricingRequestType.PassengerList.Passenger.SegmentData.SegmentAssociation.Infant infant = new OFTPricingRequestType.PassengerList.Passenger.SegmentData.SegmentAssociation.Infant();
                    infant.setHasInfant(Boolean.toString(bookingCodeInfant.equals(bookingCode)));

                    OFTPricingRequestType.PassengerList.Passenger.SegmentData.SegmentAssociation segmentAssociation = new OFTPricingRequestType.PassengerList.Passenger.SegmentData.SegmentAssociation();
                    segmentAssociation.setSegmentReference(getSegmentReference(flightSection));
                    segmentAssociation.setSegmentAssociationState(State.NEW);
                    segmentAssociation.getInfant().add(infant);
                    segmentData.getSegmentAssociation().add(segmentAssociation);
                });

                OFTPricingRequestType.PassengerList.Passenger.PassengerMonetaryDataGroup
                        .PassengerMonetaryData passengerMonetaryData =
                        new OFTPricingRequestType.PassengerList.Passenger.PassengerMonetaryDataGroup
                                .PassengerMonetaryData();

                passengerMonetaryData.setPassengerMonDataState(State.NEW);
                passengerMonetaryData.setType("SSR");
                passengerMonetaryData.setSSRCode("LUG");
                passengerMonetaryData.setQuantity(0);

                itinerarySelection.getItinerary().getSections().forEach(flightSection ->
                        passengerMonetaryData.getSegmentReference()
                                .add(getSegmentReference(flightSection)));

                OFTPricingRequestType.PassengerList.Passenger.PassengerMonetaryDataGroup passengerMonetaryDataGroup = new OFTPricingRequestType.PassengerList.Passenger.PassengerMonetaryDataGroup();
                passengerMonetaryDataGroup.getPassengerMonetaryData().add(passengerMonetaryData);

                OFTPricingRequestType.PassengerList.Passenger passenger = new OFTPricingRequestType.PassengerList.Passenger();
                passenger.setPassengerReference(String.valueOf(passengerReference));
                passenger.setOriginatorPassengerID("PAX" + passengerReference);
                passenger.setPassengerState(State.NEW);
                passenger.setBookingCodes(bookingCodes);
                passenger.setSegmentData(segmentData);
                passenger.getPassengerMonetaryDataGroup().add(passengerMonetaryDataGroup);

                passengerList.getPassenger().add(passenger);

                passengerReference = passengerReference + 1;
            }

        }

        OFTPricingRequestType.Itinerary itinerary = new OFTPricingRequestType.Itinerary();
        itinerary.getSegment().addAll(segments);

        oftPricingRequest.setLanguageCode(languageCode);
        oftPricingRequest.setFormOfPayment(formOfPayment);
        oftPricingRequest.getItinerary().add(itinerary);
        oftPricingRequest.getPassengerList().add(passengerList);
        oftPricingRequest.setOriginatorBookingID(itinerarySelection.getVisitId());

        return oftPricingRequest;

    }

    private OFTPricingRequestType.FormOfPayment getFormOfPayment(EasyJetItinerarySelection itinerarySelection) {

        OFTPricingRequestType.FormOfPayment formOfPayment = new OFTPricingRequestType.FormOfPayment();

        if (itinerarySelection.getProviderPaymentDetail() != null) {

            OFTPricingRequestType.FormOfPayment.Payment payment = new OFTPricingRequestType.FormOfPayment.Payment();
            payment.setOriginatorPaymentID("PAY1");
            payment.setPaymentType(Constants.PAYMENT_TYPE_CREDIT_CARD);

            OFTPricingRequestType.FormOfPayment.Payment.CreditCard creditCard =
                    getCreditCard(itinerarySelection.getProviderPaymentDetail());

            payment.getCreditCard().add(creditCard);

            formOfPayment.setPayment(payment);
        }

        return formOfPayment;
    }

    private OFTPricingRequestType.FormOfPayment.Payment.CreditCard getCreditCard(
            ProviderPaymentDetail providerPaymentDetail) {

        if (!(providerPaymentDetail.getProviderPaymentMethod() instanceof CreditCard)) {
            return null;
        }

        OFTPricingRequestType.FormOfPayment.Payment.CreditCard creditCard =
                new OFTPricingRequestType.FormOfPayment.Payment.CreditCard();

        CreditCard receivedCreditCard = ((CreditCard) providerPaymentDetail.getProviderPaymentMethod());

        creditCard.setCardVendorCode(CreditCardUtil.getEasyJetVendorCode(
                receivedCreditCard.getVendorCode()));

        return creditCard;
    }

    private String getSegmentReference(FlightSection flightSection) {
        return flightSection.getId().getId().toString() + "#" + flightSection.getSegmentPosition().toString();
    }

}
