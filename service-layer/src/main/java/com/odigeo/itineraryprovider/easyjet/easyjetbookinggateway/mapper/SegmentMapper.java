package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Segment;
import com.odigeo.suppliergatewayapi.v25.itinerary.ItineraryId;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.stream.Collectors;

@Mapper
public interface SegmentMapper {

    SegmentMapper INSTANCE = Mappers.getMapper(SegmentMapper.class);

    default com.odigeo.suppliergatewayapi.v25.itinerary.segment.Segment toApi(final Segment segmentModel) {

        com.odigeo.suppliergatewayapi.v25.itinerary.segment.Segment segment =
                new com.odigeo.suppliergatewayapi.v25.itinerary.segment.Segment();

        ItineraryId itineraryId = new ItineraryId();
        itineraryId.setId(segmentModel.getItineraryId().getId());
        segment.setItineraryId(itineraryId);

        segment.setPosition(segmentModel.getPosition());
        segment.setSections(
                segmentModel.getSections().stream().map(SectionMapper.INSTANCE::toApi)
                        .collect(Collectors.toUnmodifiableList()));

        return segment;
    }
}
