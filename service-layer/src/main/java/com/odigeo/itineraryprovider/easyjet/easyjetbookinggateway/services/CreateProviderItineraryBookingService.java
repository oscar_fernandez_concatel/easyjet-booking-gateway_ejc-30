package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.services;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.Constants;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.CreateProviderItineraryBookingRespBuilder;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.itinerary.ProviderItineraryBookingRepository;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayErrorType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingInternalException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.ItineraryNotFoundException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.EasyJetItinerarySelection;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryBookingCreationInternal;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderDetails;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItinerary;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.command.CreateItineraryBookingCommand;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.redis.CompressDecompressUtil;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.redis.RedisCommandFactory;

import javax.inject.Inject;
import java.io.IOException;
import java.io.Serializable;
import java.util.Optional;

public class CreateProviderItineraryBookingService {

    private final ProviderItineraryBookingRepository providerItineraryBookingRepository;
    private final RedisCommandFactory redisCommandFactory;
    private final ObjectMapper objectMapper;
    private final CompressDecompressUtil compressDecompressUtil;
    private final PricingService pricingService;
    private final CreateProviderItineraryBookingRespBuilder createProviderItineraryBookingRespBuilder;

    @Inject
    public CreateProviderItineraryBookingService(
            final ProviderItineraryBookingRepository providerItineraryBookingRepository,
            final RedisCommandFactory redisCommandFactory,
            final ObjectMapper objectMapper,
            final CompressDecompressUtil compressDecompressUtil,
            final PricingService pricingService,
            final CreateProviderItineraryBookingRespBuilder createProviderItineraryBookingRespBuilder) {
        this.providerItineraryBookingRepository = providerItineraryBookingRepository;
        this.redisCommandFactory = redisCommandFactory;
        this.objectMapper = objectMapper;
        this.compressDecompressUtil = compressDecompressUtil;
        this.pricingService = pricingService;
        this.createProviderItineraryBookingRespBuilder = createProviderItineraryBookingRespBuilder;
        this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        this.objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        this.objectMapper.registerModule(new JavaTimeModule());
    }

    public CreateItineraryBookingCommand createProviderItineraryBooking(
            final ItineraryBookingCreationInternal itineraryBookingCreationInternal) throws EasyJetBookingGatewayException {

        ProviderItineraryBooking providerItineraryBooking;

        Optional<ProviderItineraryBooking> providerItineraryBookingOptional =
                providerItineraryBookingRepository.findItineraryBookingByItineraryId(
                        itineraryBookingCreationInternal.getItineraryId());

        if (providerItineraryBookingOptional.isPresent()) {
            providerItineraryBooking = providerItineraryBookingOptional.get();
        } else {
            ProviderItinerary redisProviderItinerary =
                    redisCommandFactory.getRedisCommands().hgetall(Constants.ITINERARY_REDIS_CACHE_NAME)
                            .entrySet()
                            .stream()
                            .filter(item -> item.getKey().equals(itineraryBookingCreationInternal.getItineraryId()))
                            .map(entry -> deserializeProviderItinerary(entry.getValue()))
                            .findFirst().orElse(null);

            if (redisProviderItinerary == null) {
                throw new ItineraryNotFoundException("Itinerary not found on redis.",
                        EasyJetBookingGatewayErrorType.ITINERARY_NOT_FOUND);
            }

            final ProviderDetails providerDetails = pricingService.getCheapestFares(EasyJetItinerarySelection.builder()
                    .setItinerary(redisProviderItinerary.getItinerary())
                    .build());

            providerItineraryBooking =
                    createProviderItineraryBookingRespBuilder.build(
                            itineraryBookingCreationInternal, redisProviderItinerary, providerDetails);

            providerItineraryBookingRepository.saveItineraryBooking(providerItineraryBooking);
        }

        return CreateItineraryBookingCommand
                .builder()
                .itineraryBooking(providerItineraryBooking)
                .build();
    }

    private ProviderItinerary deserializeProviderItinerary(Serializable value) {
        try {
            return objectMapper.readValue(compressDecompressUtil.decompressBytes((byte[]) value), ProviderItinerary.class);
        } catch (IOException e) {
            throw new EasyJetBookingInternalException("Error trying to convert from bytes to ProviderItinerary class", //NOPMD
                    EasyJetBookingGatewayErrorType.REDIS_DESERIALIZATION_EXCEPTION); //NOPMD
        }
    }

}
