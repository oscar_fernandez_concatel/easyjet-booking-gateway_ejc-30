package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.configuration.jee;

import org.apache.log4j.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;

public final class JeeServiceLocator {

    private static final String BEAN_SUFFIX = "Bean";

    private static final Logger LOGGER = Logger.getLogger(JeeServiceLocator.class);
    private static final JeeServiceLocator INSTANCE = new JeeServiceLocator();

    private InitialContext jndiContext;
    private String servicesContext;

    private JeeServiceLocator() {
        try {
            initContext();
        } catch (NamingException nex) {
            LOGGER.error("Unable to find initial JNDI context.", nex);
        }
    }

    public static JeeServiceLocator getInstance() {
        return INSTANCE;
    }

    private void initContext() throws NamingException {
        if (jndiContext == null) {
            jndiContext = new InitialContext();
            servicesContext = "java:global/" + getApplicationName() + "/service-layer";
        }
    }

    public String getApplicationName() {
        try {
            return (String) jndiContext.lookup("java:app/AppName");
        } catch (NamingException e) {
            LOGGER.error("Not found application name");
            return null;
        }
    }

    private Object getServiceByName(final String name) throws NamingException {
        initContext();
        return jndiContext.lookup(name);
    }

    public <T> T getService(final Class<T> serviceType) throws UnavailableServiceException {
        final String jndiServiceName = String.format("%s/%s%s", servicesContext, serviceType.getSimpleName(), BEAN_SUFFIX);
        try {
            return (T) getServiceByName(jndiServiceName);
        } catch (NamingException nex) {
            LOGGER.error("error loading " + jndiServiceName);
            throw new UnavailableServiceException(serviceType, nex);
        }
    }
}

