package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.redis;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.persistance.cache.impl.distributed.RedisClientFactory;
import com.odigeo.persistance.cache.impl.distributed.SerializableSerializableRedisCodec;
import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulConnection;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;

import java.io.Serializable;
import java.time.Duration;
import java.util.Optional;

@Singleton
@SuppressWarnings({"PMD.AvoidCatchingGenericException"})
public class RedisCommandFactory {

    private final RedisClient redisClient;

    private Optional<StatefulRedisConnection<Serializable, Serializable>> redisConnection;

    private final SerializableSerializableRedisCodec serializableRedisCodec;

    private final SelectionRedisCacheConfiguration redisConfiguration;

    @Inject
    public RedisCommandFactory(
            SelectionRedisCacheConfiguration redisConfiguration,
            RedisClientFactory redisClientFactory,
            SerializableSerializableRedisCodec serializableRedisCodec) {

        this.redisConfiguration = redisConfiguration;

        redisClient = redisClientFactory.newInstance(
                redisConfiguration.getHost(),
                redisConfiguration.getPort(),
                redisConfiguration.getExpirationTimeMillis(),
                redisConfiguration.getTimeoutMillis());

        redisConnection = Optional.empty();

        this.serializableRedisCodec = serializableRedisCodec;
    }

    public RedisCommands<Serializable, Serializable> getRedisCommands() {
        RedisCommands<Serializable, Serializable> sync = redisConnection.orElseGet(() -> createRedisConnection()).sync();
        sync.setTimeout(Duration.ofMillis(redisConfiguration.getExpirationTimeMillis()));
        return sync;
    }

    private StatefulRedisConnection<Serializable, Serializable> createRedisConnection() {
        synchronized (this) {
            if (!redisConnection.isPresent()) {
                try {
                    redisClient.setDefaultTimeout(Duration.ofMillis(redisConfiguration.getExpirationTimeMillis()));
                    redisConnection = Optional.ofNullable(redisClient.connect(serializableRedisCodec));
                } catch (Exception e) {
                    throw new RedisConnectionIssueException("Redis connection issue: " + e.getMessage(), e);
                }
            }
        }
        return redisConnection.orElseThrow(() -> new RedisConnectionIssueException("RedisConnection issue"));
    }

    public void closeRedisClient() {
        redisConnection.ifPresent(StatefulConnection::close);
        redisClient.shutdown();
    }

}
