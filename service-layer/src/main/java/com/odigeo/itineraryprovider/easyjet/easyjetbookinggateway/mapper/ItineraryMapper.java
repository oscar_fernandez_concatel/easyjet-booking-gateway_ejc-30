package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Itinerary;
import com.odigeo.suppliergatewayapi.v25.itinerary.ItineraryId;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.stream.Collectors;

@Mapper
public interface ItineraryMapper {

    ItineraryMapper INSTANCE = Mappers.getMapper(ItineraryMapper.class);

    default com.odigeo.suppliergatewayapi.v25.itinerary.Itinerary toApi(Itinerary itineraryModel) {

        com.odigeo.suppliergatewayapi.v25.itinerary.Itinerary itinerary =
                new com.odigeo.suppliergatewayapi.v25.itinerary.Itinerary();

        ItineraryId itineraryId = new ItineraryId();
        itineraryId.setId(itineraryModel.getId().getId());

        itinerary.setId(itineraryId);
        itinerary.setTripType(itineraryModel.getTripType().getValue());
        itinerary.setDepartureDate(itineraryModel.getDepartureDate());
        itinerary.setReturnDate(itineraryModel.getReturnDate());
        itinerary.setArrivalGeoNodeId(itineraryModel.getArrivalGeoNodeId());
        itinerary.setDepartureGeoNodeId(itineraryModel.getDepartureGeoNodeId());
        itinerary.setSegments(
                itineraryModel.getSegments().stream().map(SegmentMapper.INSTANCE::toApi)
                        .collect(Collectors.toUnmodifiableList()));

        return itinerary;
    }
}
