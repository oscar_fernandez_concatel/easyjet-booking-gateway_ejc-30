package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.services;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.Constants;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.adapter.ProviderItineraryBuilder;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingInternalException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.EasyJetItinerarySelection;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderDetails;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItinerary;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderPaymentOptions;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.redis.CompressDecompressUtil;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.redis.RedisCommandFactory;
import org.apache.log4j.Logger;

@Singleton
public class EasyJetSelectedItineraryService {

    private static final Logger logger = Logger.getLogger(EasyJetSelectedItineraryService.class);

    private final ProviderItineraryBuilder providerItineraryBuilder;
    private final RedisCommandFactory redisCommandFactory;
    private final ObjectMapper objectMapper;
    private final PricingService pricingService;
    private final PaymentTypeService paymentTypeService;
    private final CompressDecompressUtil compressDecompressUtil;

    @Inject
    public EasyJetSelectedItineraryService(
            final RedisCommandFactory redisCommandFactory,
            final ObjectMapper objectMapper,
            final ProviderItineraryBuilder providerItineraryBuilder,
            final PricingService pricingService,
            final PaymentTypeService paymentTypeService,
            final CompressDecompressUtil compressDecompressUtil) {
        this.redisCommandFactory = redisCommandFactory;
        this.providerItineraryBuilder = providerItineraryBuilder;
        this.objectMapper = objectMapper;
        this.paymentTypeService = paymentTypeService;
        this.compressDecompressUtil = compressDecompressUtil;
        this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        this.objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        this.objectMapper.registerModule(new JavaTimeModule());
        this.pricingService = pricingService;
    }

    public ProviderItinerary createSelectedItinerary(
            final EasyJetItinerarySelection itinerarySelection) throws EasyJetBookingGatewayException {
        final ProviderDetails providerDetails = pricingService.getCheapestFares(itinerarySelection);
        final ProviderItinerary providerItinerary = createProviderItinerary(itinerarySelection, providerDetails);
        saveProviderItinerary(providerItinerary);
        return providerItinerary;
    }

    private ProviderItinerary createProviderItinerary(
            final EasyJetItinerarySelection itinerarySelection,
            final ProviderDetails providerDetails) throws EasyJetBookingInternalException {

        final ProviderPaymentOptions providerPaymentOptions = paymentTypeService.getPaymentTypes();

        return providerItineraryBuilder.createProviderItinerary(
                itinerarySelection, providerDetails, providerPaymentOptions);
    }

    private void saveProviderItinerary(final ProviderItinerary providerItinerary) {
        try {
            byte[] bytes = objectMapper.writeValueAsBytes(providerItinerary);
            redisCommandFactory.getRedisCommands().hset(
                    Constants.ITINERARY_REDIS_CACHE_NAME,
                    providerItinerary.getItinerary().getId().getId(),
                    compressDecompressUtil.compressBytes(bytes));
        } catch (JsonProcessingException e) {
            logger.error(e, e);
        }
    }
}
