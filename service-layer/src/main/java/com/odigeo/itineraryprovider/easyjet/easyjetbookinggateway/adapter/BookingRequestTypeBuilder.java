package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.adapter;

import com.easyjet.b2b.request.OFTBookingRequestType;
import com.edreamsodigeo.payments.paymentinstrument.contract.PaymentInstrumentResourceException;
import com.edreamsodigeo.payments.paymentinstrument.contract.creditcard.CreditCardPciScope;
import com.edreamsodigeo.payments.paymentinstrument.contract.creditcard.CreditCardPciScopeResource;
import com.edreamsodigeo.payments.paymentinstrument.token.PaymentInstrumentToken;
import com.edreamsodigeo.payments.paymentinstrument.token.PaymentInstrumentType;
import com.google.inject.Inject;
import com.odigeo.geoapi.last.AirportService;
import com.odigeo.geoapi.last.GeoNodeNotFoundException;
import com.odigeo.geoapi.last.GeoServiceException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.Constants;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayErrorType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingInternalException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.PaymentDetail;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Buyer;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Itinerary;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Traveller;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.UUID;
import java.util.stream.Collectors;


public class BookingRequestTypeBuilder {

    private static final Logger logger = Logger.getLogger(BookingRequestTypeBuilder.class);

    private final AirportService airportService;
    private final CreditCardPciScopeResource creditCardPciScopeResource;

    @Inject
    public BookingRequestTypeBuilder(
            AirportService airportService,
            CreditCardPciScopeResource creditCardPciScopeResource) {
        this.airportService = airportService;
        this.creditCardPciScopeResource = creditCardPciScopeResource;
    }

    public OFTBookingRequestType build(
            ProviderItineraryBooking providerItineraryBooking) throws EasyJetBookingInternalException {

        OFTBookingRequestType request = new OFTBookingRequestType();

        try {
            setContactDetails(providerItineraryBooking.getBuyer(), request);

            request.setOfficeID(Constants.OFFICE_ID);
            request.setIATANumber(Constants.IATA_NUMBER);
            request.setLanguageCode(Constants.DEFAULT_LANGUAGE_CODE);
            request.setCurrencyCode(providerItineraryBooking.getItinerary()
                    .getFareDetails().getTotalFareAmount().getCurrency()
                    .getCurrencyCode());

            request.setPassengerList(new OFTBookingRequestType.PassengerList());
            request.setChargesList(new OFTBookingRequestType.ChargesList());
            request.setPaymentList(new OFTBookingRequestType.PaymentList());
            request.setCommentList(new OFTBookingRequestType.CommentList());

            getPassengers(providerItineraryBooking, request);

            request.getPaymentList().getPayment().addAll(
                    providerItineraryBooking.getPaymentDetails()
                            .stream().map(paymentDetail -> getPayment(
                                    providerItineraryBooking.getTotalFarePrice(),
                                    paymentDetail))
                            .collect(Collectors.toList()));

            OFTBookingRequestType.CommentList.Comment testComment = new OFTBookingRequestType.CommentList.Comment();
            testComment.setCommentText("Booking Created from eDO API");
            request.getCommentList().getComment().add(testComment);

            return request;

        } catch (GeoServiceException | GeoNodeNotFoundException e) {
            logger.error(e.getMessage());
            throw new EasyJetBookingInternalException(e.getMessage(),
                    EasyJetBookingGatewayErrorType.ERROR_ON_BUILD_ADAPTER_REQUEST, e); //NOPMD
        }
    }

    private void setContactDetails(
            Buyer buyer,
            OFTBookingRequestType request) {

        request.setFirstName(buyer.getName());
        request.setLastName(buyer.getLastNames());
        request.setContactEmailAddress(buyer.getMail());
        request.setAddress1(buyer.getAddress());
        request.setCity(buyer.getCityName());
        request.setPostalCode(buyer.getZipCode());
        request.setCountry(buyer.getCountryCode());
        request.setMobilePhone(buyer.getPhoneNumber());
        request.setStateProvince(buyer.getCityName());
    }

    private void getPassengers(
            ProviderItineraryBooking providerItineraryBooking,
            OFTBookingRequestType request) throws GeoNodeNotFoundException, GeoServiceException {

        for (Traveller traveller : providerItineraryBooking.getTravellers()) {

            OFTBookingRequestType.PassengerList.PassengerData passengerData =
                    new OFTBookingRequestType.PassengerList.PassengerData();

            passengerData.setOriginatorPassengerID("PAX" + traveller.getTravellerNumber());
            passengerData.setFirstName(traveller.getName());

            passengerData.setLastName(String.format("%s %s",
                    StringUtils.isNotEmpty(traveller.getFirstLastName())
                            ? traveller.getFirstLastName() : StringUtils.EMPTY,
                    StringUtils.isNotEmpty(traveller.getSecondLastName())
                            ? traveller.getSecondLastName() : StringUtils.EMPTY));

            passengerData.setTitleTypeCode(traveller.getTitle()
                    .getDatabaseCode().toUpperCase(Locale.ROOT));

            passengerData.getSegmentData().add(getSegmentData(
                    providerItineraryBooking.getItinerary()));

            request.getPassengerList().getPassengerData().add(passengerData);
        }
    }

    private OFTBookingRequestType.PassengerList.PassengerData.SegmentData getSegmentData(
            Itinerary itinerary) throws GeoNodeNotFoundException, GeoServiceException {

        String departureCode = airportService.getEntityByGeoNodeId(itinerary.getDepartureGeoNodeId()).getIataCode();
        String arrivalCode = airportService.getEntityByGeoNodeId(itinerary.getArrivalGeoNodeId()).getIataCode();

        OFTBookingRequestType.PassengerList.PassengerData.SegmentData segmentData =
                new OFTBookingRequestType.PassengerList.PassengerData.SegmentData();

        segmentData.setOriginatorSegmentID("SEG1");
        segmentData.setChargeAmount(itinerary.getFareDetails().getTotalFareAmount().getAmount());
        segmentData.setDepAirportCode(departureCode);
        segmentData.setArrAirportCode(arrivalCode);

        segmentData.setDepartureDateTime(itinerary.getDepartureDate()
                .format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

        segmentData.setFlightNumber(
                itinerary.getSegments().get(0).getSections().get(0).getFlight()
                        .getFlightNumber());

        return segmentData;
    }

    private OFTBookingRequestType.PaymentList.Payment getPayment(
            BigDecimal amount,
            PaymentDetail paymentDetail) {

        PaymentInstrumentToken paymentInstrumentToken = new PaymentInstrumentToken();
        paymentInstrumentToken.setUuid(UUID.fromString(paymentDetail.getPaymentInstrumentToken()));
        paymentInstrumentToken.setType(PaymentInstrumentType.CREDIT_CARD);

        CreditCardPciScope creditCardPciScopeResourcePciScope;
        try {
            creditCardPciScopeResourcePciScope = creditCardPciScopeResource.getPciScope(paymentInstrumentToken);
        } catch (PaymentInstrumentResourceException e) {
            logger.error("Error trying to get PaymentInstrumentToken.", e);
            throw new EasyJetBookingInternalException(
                    "Credit Card information is expired in PaymentInstrument service. Call SetPayments First",
                    EasyJetBookingGatewayErrorType.PAYMENT_INSTRUMENT_EXCEPTION, e);
        }

        OFTBookingRequestType.PaymentList.Payment payment = new OFTBookingRequestType.PaymentList.Payment();

        payment.setAmount(amount.toString());
        payment.setPaymentFormCode(Constants.PAYMENT_TYPE_CREDIT_CARD);
        payment.setNameOnCard(creditCardPciScopeResourcePciScope.getHolder());
        payment.setCreditCardCode(paymentDetail.getCardVendorCode());
        payment.setExpirationDate(String.format("%02d/%02d",
                Integer.valueOf(creditCardPciScopeResourcePciScope.getExpirationMonth()),
                Integer.valueOf(creditCardPciScopeResourcePciScope.getExpirationYear())));
        payment.setSecurityCode(creditCardPciScopeResourcePciScope.getCvv());
        payment.setCreditCardNumber(creditCardPciScopeResourcePciScope.getNumber());

        return payment;
    }
}
