package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.services;

import com.easyjet.b2b.response.PaymentTypeElement;
import com.easyjet.b2b.response.PaymentTypeList;
import com.easyjet.b2b.response.ResponseRootType;
import com.odigeo.commons.monitoring.metrics.Metric;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.easyjetadapter.adapter.boundary.EasyJetAdapterManager;
import com.odigeo.easyjetadapter.webservices.entity.exception.ProviderException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.commons.monitoring.metrics.MetricsConstants;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.configuration.EasyJetBookingGatewayConfiguration;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper.EasyJetPaymentTypeResponseMapper;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayErrorType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingInternalException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderPaymentOptions;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class PaymentTypeService extends ServiceBase {

    private final EasyJetAdapterManager easyJetAdapterManager;
    private final EasyJetPaymentTypeResponseMapper paymentTypeResponseMapper;
    private final EasyJetBookingGatewayConfiguration easyJetGatewayConfiguration;

    @Inject
    public PaymentTypeService(EasyJetAdapterManager easyJetAdapterManager, EasyJetPaymentTypeResponseMapper paymentTypeResponseMapper, EasyJetBookingGatewayConfiguration easyJetGatewayConfiguration) {
        this.easyJetAdapterManager = easyJetAdapterManager;
        this.paymentTypeResponseMapper = paymentTypeResponseMapper;
        this.easyJetGatewayConfiguration = easyJetGatewayConfiguration;
    }

    public ProviderPaymentOptions getPaymentTypes() throws EasyJetBookingInternalException {
        final Metric providerCallMetric = MetricsConstants.getProviderCall(MetricsConstants.PROVIDER_GET_PAYMENT_TYPES);

        try {
            MetricsUtils.startTimer(providerCallMetric, MetricsConstants.METRICS_REGISTRY_NAME);
            ResponseRootType response = easyJetAdapterManager.getPaymentTypes();
            MetricsUtils.stopTimer(providerCallMetric, MetricsConstants.METRICS_REGISTRY_NAME);
            super.checkProviderResponseResult(response, MetricsConstants.PROVIDER_GET_PAYMENT_TYPES);
            PaymentTypeList paymentTypesData = response.getDataListRoot().getPaymentTypes();
            return paymentTypeResponseMapper.map(filterAcceptedPaymentMethods(paymentTypesData.getPayment()));
        } catch (ProviderException e) {
            MetricsUtils.incrementCounter(MetricsConstants.getProviderException(
                    e.getClass().getSimpleName(), MetricsConstants.PROVIDER_GET_PAYMENT_TYPES), MetricsConstants.METRICS_REGISTRY_NAME);
            throw new EasyJetBookingInternalException(e.getMessage(), EasyJetBookingGatewayErrorType.ERROR_IN_ADAPTER); //NOPMD - No interference with main flow
        }
    }

    private List<PaymentTypeElement> filterAcceptedPaymentMethods(List<PaymentTypeElement> paymentTypes) {
        return paymentTypes.stream().filter(paymentTypeElement ->
                easyJetGatewayConfiguration.getSupportedPaymentMethodCodesList().contains(paymentTypeElement.getCreditCardCode())
        ).collect(Collectors.toUnmodifiableList());
    }
}
