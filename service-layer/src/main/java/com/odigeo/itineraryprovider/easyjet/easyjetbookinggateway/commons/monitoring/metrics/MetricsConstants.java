package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.commons.monitoring.metrics;

import com.odigeo.commons.monitoring.metrics.Metric;
import org.apache.commons.lang.StringUtils;

import java.util.regex.Pattern;

public class MetricsConstants {

    //Metrics
    public static final String METRICS_REGISTRY_NAME = "easyjet-booking-gateway";
    public static final String PROVIDER_OFT_PRICING = "provider-oft-pricing";
    public static final String PROVIDER_GET_PAYMENT_TYPES = "provider-payment-types";
    public static final String PROVIDER_COMMIT_BOOKING = "provider-commit-booking";
    public static final String NO_SYMBOLS_REGEX_PATTERN = Pattern.compile("[^a-zA-Z0-9]").pattern();

    //Tags
    public static final String RESPONSE = "response";
    public static final String SOURCE_CODE_ALIAS = "sourceCodeAlias";
    public static final String SOURCE_CODE = "statusCode";
    public static final String ERROR_MESSAGE = "errorMessage";


    // Provider calls
    public static Metric getProviderPricingResultError(final String sourceCode, final String sourceCodeAlias, final String metric) {
        return new MetricBuilder(metric)
                .withResponse(Response.RESULT_ERROR)
                .withSourceCodeAlias(sourceCodeAlias != null ? sourceCodeAlias : "sourceCodeNotMapped")
                .withSourceCode(sourceCode.replaceAll(NO_SYMBOLS_REGEX_PATTERN, StringUtils.EMPTY))
                .build();
    }

    public static Metric getProviderError(final String sourceCode, final String sourceCodeAlias, final String metric) {
        return new MetricBuilder(metric)
                .withResponse(Response.RESULT_ERROR)
                .withSourceCodeAlias(sourceCodeAlias != null ? sourceCodeAlias : "sourceCodeNotMapped")
                .withSourceCode(sourceCode.replaceAll(NO_SYMBOLS_REGEX_PATTERN, StringUtils.EMPTY))
                .build();
    }

    public static Metric getProviderCall(String metric) {
        return new MetricBuilder(metric)
                .build();
    }

    public static Metric getProviderResultErrorAll(String metric) {
        return new MetricBuilder(metric)
                .withResponse(Response.RESULT_ERROR)
                .build();
    }

    public static Metric getProviderException(final String errorMessage, String metric) {
        return new MetricBuilder(metric)
                .withResponse(Response.EXCEPTION)
                .withErrorMessage(errorMessage)
                .build();
    }

    public enum Response {
        OK, RESULT_ERROR, RESULT_WARNING, EXCEPTION
    }
}
