package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.configuration.jee;

import com.edreams.base.BaseRuntimeException;
import com.google.inject.Provider;

public final class JeeServiceProvider<T> implements Provider<T> {

    private final Class<T> type;

    private JeeServiceProvider(Class<T> type) {
        this.type = type;
    }

    @Override
    public T get() {
        try {
            return JeeServiceLocator.getInstance().getService(type);
        } catch (UnavailableServiceException e) {
            throw new BaseRuntimeException("Unable to obtain an instance of service " + type, e);
        }
    }

    private static <T> void checkThatServiceExists(Class<T> type) throws UnavailableServiceException {
        JeeServiceLocator.getInstance().getService(type);
    }

    public static <T> JeeServiceProvider<T> getInstance(Class<T> type) throws UnavailableServiceException {
        checkThatServiceExists(type);
        return new JeeServiceProvider<T>(type);
    }

}
