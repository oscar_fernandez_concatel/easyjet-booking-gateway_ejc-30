package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.commons.monitoring.metrics;

import com.odigeo.commons.monitoring.metrics.Metric;
import com.odigeo.commons.monitoring.metrics.MetricsPolicy;

public class MetricBuilder {
    private final Metric.Builder builder;

    MetricBuilder(final String method) {
        builder = new Metric.Builder(method).policy(MetricsPolicy.Precision.MINUTELY, MetricsPolicy.Retention.MONTH);
    }

    public Metric build() {
        return builder.build();
    }

    MetricBuilder withResponse(final MetricsConstants.Response response) {
        builder.tag(MetricsConstants.RESPONSE, response.name());
        return this;
    }

    MetricBuilder withSourceCode(final String sourceCode) {
        builder.tag(MetricsConstants.SOURCE_CODE, sourceCode);
        return this;
    }

    MetricBuilder withErrorMessage(final String errorMessage) {
        builder.tag(MetricsConstants.ERROR_MESSAGE, errorMessage);
        return this;
    }

    MetricBuilder withSourceCodeAlias(final String sourceCodeAlias) {
        builder.tag(MetricsConstants.SOURCE_CODE_ALIAS, sourceCodeAlias);
        return this;
    }
}
