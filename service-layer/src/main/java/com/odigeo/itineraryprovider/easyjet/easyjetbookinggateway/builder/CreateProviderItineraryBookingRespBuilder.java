package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.google.inject.Singleton;
import com.odigeo.commons.uuid.UUIDGenerator;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryBookingCreationInternal;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderDetails;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItinerary;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;

@Singleton
public class CreateProviderItineraryBookingRespBuilder {

    public ProviderItineraryBooking build(
            final ItineraryBookingCreationInternal itineraryBookingCreationInternal,
            final ProviderItinerary providerItinerary,
            final ProviderDetails providerDetails) {

        return ProviderItineraryBooking.builder()
                .setItineraryBookingId(UUIDGenerator.getInstance().generateUUID())
                .setBuyer(itineraryBookingCreationInternal.getBuyer())
                .setItineraryId(providerItinerary.getItinerary().getId())
                .setTravellers(itineraryBookingCreationInternal.getTravellers())
                .setItinerary(providerDetails.getItinerary())
                .build();
    }
}
