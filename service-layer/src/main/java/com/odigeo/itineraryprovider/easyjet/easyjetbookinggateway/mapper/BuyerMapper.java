package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Buyer;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface BuyerMapper {

    BuyerMapper INSTANCE = Mappers.getMapper(BuyerMapper.class);

    default Buyer toModel(final com.odigeo.suppliergatewayapi.v25.itinerary.booking.Buyer buyer) {

        return Buyer.builder()
                .setName(buyer.getName())
                .setLastNames(buyer.getLastNames())
                .setMail(buyer.getMail())
                .setPhoneNumber(buyer.getPhoneNumber())
                .setCountryCode(buyer.getCountryCode())
                .setAddress(buyer.getAddress())
                .setCityName(buyer.getCityName())
                .setZipCode(buyer.getZipCode())
                .setPrimeMember(buyer.isPrimeMember() != null && buyer.isPrimeMember())
                .build();
    }

    default com.odigeo.suppliergatewayapi.v25.itinerary.booking.Buyer toApi(final Buyer buyerModel) {

        com.odigeo.suppliergatewayapi.v25.itinerary.booking.Buyer buyer =
                new com.odigeo.suppliergatewayapi.v25.itinerary.booking.Buyer();

        buyer.setName(buyerModel.getName());
        buyer.setLastNames(buyerModel.getLastNames());
        buyer.setMail(buyerModel.getMail());
        buyer.setPhoneNumber(buyerModel.getPhoneNumber());
        buyer.setCountryCode(buyerModel.getCountryCode());
        buyer.setAddress(buyerModel.getAddress());
        buyer.setCityName(buyerModel.getCityName());
        buyer.setZipCode(buyerModel.getZipCode());
        buyer.setPrimeMember(buyerModel.isPrimeMember());

        return buyer;
    }
}
