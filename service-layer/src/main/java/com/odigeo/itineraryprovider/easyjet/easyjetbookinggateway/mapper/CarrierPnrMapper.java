package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.SupplierLocator;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CarrierPnrMapper {

    CarrierPnrMapper INSTANCE = Mappers.getMapper(CarrierPnrMapper.class);

    default com.odigeo.suppliergatewayapi.v25.itinerary.booking.CarrierPnr toApi(
            final SupplierLocator supplierLocator) {

        com.odigeo.suppliergatewayapi.v25.itinerary.booking.CarrierPnr carrierPnr =
                new com.odigeo.suppliergatewayapi.v25.itinerary.booking.CarrierPnr();

        carrierPnr.setCarrierCode(supplierLocator.getSupplierCode());
        carrierPnr.setPnr(supplierLocator.getSupplierLocatorCode());

        return carrierPnr;
    }
}
