package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.configuration;

import com.edreams.configuration.ConfiguredInPropertiesFile;

import javax.inject.Singleton;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Singleton
@ConfiguredInPropertiesFile
public class EasyJetBookingGatewayConfiguration {

    private String oftPricingLanguageCode;
    private String oftPricingBookingCodeInfant;
    private String supportedPaymentMethodCodes;

    public String getSupportedPaymentMethodCodes() {
        return supportedPaymentMethodCodes;
    }

    public void setSupportedPaymentMethodCodes(String supportedPaymentMethodCodes) {
        this.supportedPaymentMethodCodes = supportedPaymentMethodCodes;
    }

    public List<String> getSupportedPaymentMethodCodesList() {
        return Arrays.stream(getSupportedPaymentMethodCodes().split(Pattern.quote(","))).collect(Collectors.toUnmodifiableList());
    }

    public String getOftPricingLanguageCode() {
        return oftPricingLanguageCode;
    }

    public void setOftPricingLanguageCode(String oftPricingLanguageCode) {
        this.oftPricingLanguageCode = oftPricingLanguageCode;
    }

    public String getOftPricingBookingCodeInfant() {
        return oftPricingBookingCodeInfant;
    }

    public void setOftPricingBookingCodeInfant(String oftPricingBookingCodeInfant) {
        this.oftPricingBookingCodeInfant = oftPricingBookingCodeInfant;
    }
}
