package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.BaggageAllowance;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface BaggageAllowanceMapper {

    BaggageAllowanceMapper INSTANCE = Mappers.getMapper(BaggageAllowanceMapper.class);

    default com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.BaggageAllowance toApi(
            final BaggageAllowance baggageAllowanceModel) {

        com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.BaggageAllowance baggageAllowance =
                new com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.BaggageAllowance();

        baggageAllowance.setBaggageAllowanceType(
                baggageAllowanceModel != null && baggageAllowanceModel.getBaggageAllowanceType() != null
                        ? baggageAllowanceModel.getBaggageAllowanceType().name() : null); //NOPMD

        baggageAllowance.setQuantity(baggageAllowanceModel != null ? baggageAllowanceModel.getQuantity() : 0);

        return baggageAllowance;
    }
}
