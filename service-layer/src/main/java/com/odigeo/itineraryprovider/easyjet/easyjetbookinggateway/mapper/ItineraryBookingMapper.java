package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.ItineraryBooking;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.ItineraryBookingId;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.stream.Collectors;

@Mapper
public interface ItineraryBookingMapper {

    ItineraryBookingMapper INSTANCE = Mappers.getMapper(ItineraryBookingMapper.class);

    default ItineraryBooking toApi(final ProviderItineraryBooking providerItineraryBooking) {

        ItineraryBooking itineraryBooking = new ItineraryBooking();

        ItineraryBookingId itineraryBookingId = new ItineraryBookingId();
        itineraryBookingId.setId(providerItineraryBooking.getItineraryBookingId());
        itineraryBooking.setId(itineraryBookingId);

        itineraryBooking.setBuyer(BuyerMapper.INSTANCE.toApi(providerItineraryBooking.getBuyer()));
        itineraryBooking.setPnr(providerItineraryBooking.getPnr());

        itineraryBooking.setCarrierPnrs(
                providerItineraryBooking.getSupplierLocators()
                        .stream().map(CarrierPnrMapper.INSTANCE::toApi)
                        .collect(Collectors.toUnmodifiableList()));

        itineraryBooking.setItinerary(ItineraryMapper.INSTANCE.toApi(providerItineraryBooking.getItinerary()));
        itineraryBooking.setTravellers(TravellerMapper.INSTANCE.toApi(providerItineraryBooking.getTravellers()));

        return itineraryBooking;
    }
}
