package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.services;

import com.easyjet.b2b.response.ErrorListType;
import com.easyjet.b2b.response.ResponseRootType;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.commons.monitoring.metrics.MetricsConstants;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayErrorType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingInternalException;
import org.apache.commons.lang.StringUtils;

import java.util.List;

public abstract class ServiceBase {

    private static final String SUCCESS = "0";

    protected void checkProviderResponseResult(final ResponseRootType response) throws EasyJetBookingInternalException {

        if (StringUtils.isNotEmpty(response.getSuccess())
                && SUCCESS.equalsIgnoreCase(response.getSuccess())
                && response.getErrorListRoot() != null
                && !response.getErrorListRoot().getErrorList().isEmpty()) {

            final List<ErrorListType> errorList = response.getErrorListRoot().getErrorList();

            MetricsUtils.incrementCounter(
                    MetricsConstants.getProviderResultErrorAll(MetricsConstants.PROVIDER_OFT_PRICING),
                    MetricsConstants.METRICS_REGISTRY_NAME);

            errorList.forEach(errorListType ->
                    MetricsUtils.incrementCounter(MetricsConstants.getProviderPricingResultError(
                                    errorListType.getError().getDescription(),
                                    errorListType.getError().getNumber(),
                                    MetricsConstants.PROVIDER_OFT_PRICING),
                            MetricsConstants.METRICS_REGISTRY_NAME));

            throw new EasyJetBookingInternalException(
                    errorList.get(0).getError().getDescription(),
                    EasyJetBookingGatewayErrorType.ERROR_IN_ADAPTER,
                    errorList.get(0).getError().getNumber());
        }
    }

    protected void checkProviderResponseResult(final ResponseRootType response, String metric) throws EasyJetBookingInternalException {

        if (StringUtils.isNotEmpty(response.getSuccess())
                && SUCCESS.equalsIgnoreCase(response.getSuccess())
                && response.getErrorListRoot() != null
                && !response.getErrorListRoot().getErrorList().isEmpty()) {

            final List<ErrorListType> errorList = response.getErrorListRoot().getErrorList();

            MetricsUtils.incrementCounter(
                    MetricsConstants.getProviderResultErrorAll(metric),
                    MetricsConstants.METRICS_REGISTRY_NAME);

            errorList.forEach(errorListType ->
                    MetricsUtils.incrementCounter(MetricsConstants.getProviderError(
                                    errorListType.getError().getDescription(),
                                    errorListType.getError().getNumber(),
                                    metric),
                            MetricsConstants.METRICS_REGISTRY_NAME));

            throw new EasyJetBookingInternalException(
                    errorList.get(0).getError().getDescription(),
                    EasyJetBookingGatewayErrorType.ERROR_IN_ADAPTER,
                    errorList.get(0).getError().getNumber());
        }
    }
}
