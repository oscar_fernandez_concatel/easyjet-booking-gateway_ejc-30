package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayErrorType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingInternalException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardVendorCode;

public class CreditCardUtil {

    public static String getEasyJetVendorCode(CreditCardVendorCode type) {

        switch (type) {
        case VISA_CREDIT:
            return Constants.VI;
        case VISA_DEBIT:
            return Constants.DL;
        case VISA_ELECTRON:
            return Constants.EL;
        case MASTERCARD:
            return Constants.MC;
        case DINERS_CLUB:
            return Constants.DC;
        case AMEX:
        case AMERICAN_EXPRESS:
            return Constants.AX;
        default:
            throw new EasyJetBookingInternalException(type + " is not supported by EasyJet",
                    EasyJetBookingGatewayErrorType.PAYMENT_VENDOR_CODE_NOT_SUPPORTED);
        }
    }
}
