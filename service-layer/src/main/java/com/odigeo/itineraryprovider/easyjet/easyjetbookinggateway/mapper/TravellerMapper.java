package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TravellerType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Traveller;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.TravellerGender;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.TravellerId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.TravellerIdentificationType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.TravellerTitle;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Mapper
public interface TravellerMapper {

    TravellerMapper INSTANCE = Mappers.getMapper(TravellerMapper.class);

    default List<Traveller> toModel(final List<com.odigeo.suppliergatewayapi.v25
            .itinerary.booking.traveller.Traveller> travellers) {

        return travellers.stream().map(traveller -> Traveller.builder()
                .setId(TravellerId.builder().setId(UUID.randomUUID()).build())
                .setAge(traveller.getAge())
                .setDateOfBirth(traveller.getDateOfBirth())
                .setCountryCodeOfResidence(traveller.getCountryCodeOfResidence())
                .setFirstLastName(traveller.getFirstLastName())
                .setIdentificationExpirationDate(traveller.getIdentificationExpirationDate())
                .setIdentificationType(TravellerIdentificationType.valueOf(traveller.getIdentificationType()))
                .setIdentification(traveller.getIdentification())
                .setLocalityCodeOfResidence(traveller.getLocalityCodeOfResidence())
                .setName(traveller.getName())
                .setMiddleName(traveller.getMiddleName())
                .setPhone(PhoneMapper.INSTANCE.toModel(traveller.getPhone()))
                .setTitle(TravellerTitle.valueOf(traveller.getTitle()))
                .setTravellerType(TravellerType.valueOf(traveller.getTravellerType()))
                .setTravellerGender(TravellerGender.valueOf(traveller.getTravellerGender()))
                .setTravellerNumber(traveller.getTravellerNumber())
                .build()).collect(Collectors.toList());
    }

    default List<com.odigeo.suppliergatewayapi.v25
            .itinerary.booking.traveller.Traveller> toApi(final List<Traveller> travellers) {

        return travellers.stream().map(traveller -> {

            com.odigeo.suppliergatewayapi.v25
                    .itinerary.booking.traveller.Traveller travellerApi =
                    new com.odigeo.suppliergatewayapi.v25
                            .itinerary.booking.traveller.Traveller();

            travellerApi.setAge(traveller.getAge());
            travellerApi.setDateOfBirth(traveller.getDateOfBirth());
            travellerApi.setCountryCodeOfResidence(traveller.getCountryCodeOfResidence());
            travellerApi.setFirstLastName(traveller.getFirstLastName());
            travellerApi.setIdentificationExpirationDate(traveller.getIdentificationExpirationDate());
            travellerApi.setIdentificationType(traveller.getIdentificationType().name());
            travellerApi.setIdentification(traveller.getIdentification());
            travellerApi.setLocalityCodeOfResidence(traveller.getLocalityCodeOfResidence());
            travellerApi.setName(traveller.getName());
            travellerApi.setMiddleName(traveller.getMiddleName());
            travellerApi.setPhone(PhoneMapper.INSTANCE.toApi(traveller.getPhone()));
            travellerApi.setTitle(traveller.getTitle().getDatabaseCode());
            travellerApi.setTravellerType(traveller.getTravellerType().getCode());
            travellerApi.setTravellerGender(traveller.getTravellerGender().name());
            travellerApi.setTravellerNumber(traveller.getTravellerNumber());

            return travellerApi;

        }).collect(Collectors.toUnmodifiableList());

    }
}
