package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.easyjet.b2b.response.CommitBookingResponseType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.BookingStatus;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.IssuedItineraryBooking;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.IssuedItineraryBookingId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryBooking;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;

import javax.inject.Singleton;

@Singleton
public class IssuedItineraryResponseBuilder {

    public IssuedItineraryBooking build(
            ProviderItineraryBooking storedItineraryBooking,
            CommitBookingResponseType response) {
        
        ItineraryBooking itineraryBooking =
                ItineraryBooking
                        .builder()
                        .setItinerary(storedItineraryBooking.getItinerary())
                        .setBookingWithoutPaymentAllowed(false)
                        .setBuyer(storedItineraryBooking.getBuyer())
                        .setPnr(response.getBookingNumber())
                        .setTravellers(storedItineraryBooking.getTravellers())
                        .setSupplierLocators(storedItineraryBooking.getSupplierLocators())
                        .build();

        return IssuedItineraryBooking
                .builder()
                .setItineraryBooking(itineraryBooking)
                .setId(new IssuedItineraryBookingId(IssuedItineraryBookingId.builder().setId(storedItineraryBooking.getItineraryId().getId())))
                .setItineraryTickets(storedItineraryBooking.getItineraryTickets())
                .setStatus(BookingStatus.CONFIRMED.name())
                .build();
    }
}
