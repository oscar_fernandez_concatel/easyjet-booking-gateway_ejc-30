package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.services;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.itinerary.ProviderItineraryBookingRepository;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.itinerary.ProviderItineraryBookingRepositoryImpl;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.ItineraryNotFoundException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.PaymentDetail;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Optional;
import java.util.UUID;

@Stateless
@Local(ProviderItineraryBookingRepository.class)
public class ProviderItineraryBookingRepositoryBean implements ProviderItineraryBookingRepository {

    @PersistenceContext(unitName = "easyjet-booking-gateway")
    private EntityManager entityManager;

    private ProviderItineraryBookingRepository providerItineraryBookingRepository;

    public ProviderItineraryBookingRepositoryBean() {
    }

    public ProviderItineraryBookingRepositoryBean(
            EntityManager entityManager,
            ProviderItineraryBookingRepository providerItineraryBookingRepository) {
        this.entityManager = entityManager;
        this.providerItineraryBookingRepository = providerItineraryBookingRepository;
    }

    @PostConstruct
    public void postConstruct() {
        providerItineraryBookingRepository = new ProviderItineraryBookingRepositoryImpl(entityManager);
    }

    @Override
    public Optional<ProviderItineraryBooking> findItineraryBookingByItineraryId(UUID itineraryId) {
        return providerItineraryBookingRepository.findItineraryBookingByItineraryId(itineraryId);
    }

    @Override
    public ProviderItineraryBooking find(UUID itineraryBookingId) throws ItineraryNotFoundException {
        return providerItineraryBookingRepository.find(itineraryBookingId);
    }

    @Override
    public void saveItineraryBooking(ProviderItineraryBooking providerItinerary) {
        providerItineraryBookingRepository.saveItineraryBooking(providerItinerary);
    }

    @Override
    public void updateItineraryBooking(ProviderItineraryBooking providerItinerary) {
        providerItineraryBookingRepository.updateItineraryBooking(providerItinerary);
    }

    @Override
    public void saveOrUpdateItineraryBookingPaymentDetail(PaymentDetail paymentDetail) {
        providerItineraryBookingRepository.saveOrUpdateItineraryBookingPaymentDetail(paymentDetail);
    }

}
