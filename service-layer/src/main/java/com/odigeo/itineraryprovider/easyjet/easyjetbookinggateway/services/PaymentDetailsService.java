package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.services;

import com.edreamsodigeo.payments.paymentinstrument.contract.PaymentInstrumentResourceException;
import com.edreamsodigeo.payments.paymentinstrument.contract.creditcard.CreditCardPciScope;
import com.edreamsodigeo.payments.paymentinstrument.contract.creditcard.CreditCardPciScopeResource;
import com.edreamsodigeo.payments.paymentinstrument.token.PaymentInstrumentToken;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.CreditCardUtil;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.adapter.BookingRequestTypeBuilder;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.itinerary.ProviderItineraryBookingRepository;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayErrorType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingInternalException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.PaymentDetail;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCard;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.EasyJetItinerarySelection;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Money;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderDetails;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderPaymentDetail;
import org.apache.log4j.Logger;

import java.util.UUID;

@Singleton
public class PaymentDetailsService {

    private static final Logger LOGGER = Logger.getLogger(BookingRequestTypeBuilder.class);

    private final PricingService pricingService;
    private final PaymentTypeService paymentTypeService;
    private final CreditCardPciScopeResource creditCardPciScopeResource;
    private final ProviderItineraryBookingRepository providerItineraryBookingRepository;

    @Inject
    public PaymentDetailsService(
            PricingService pricingService,
            PaymentTypeService paymentTypeService,
            CreditCardPciScopeResource creditCardPciScopeResource,
            ProviderItineraryBookingRepository providerItineraryBookingRepository) {
        this.pricingService = pricingService;
        this.paymentTypeService = paymentTypeService;
        this.creditCardPciScopeResource = creditCardPciScopeResource;
        this.providerItineraryBookingRepository = providerItineraryBookingRepository;
    }

    public Money setPaymentDetails(
            ProviderItineraryBooking providerItineraryBooking,
            ProviderPaymentDetail providerPaymentDetail)
            throws EasyJetBookingGatewayException {

        ProviderDetails providerDetails = pricingService.getCheapestFares(
                EasyJetItinerarySelection.builder()
                        .setItinerary(providerItineraryBooking.getItinerary())
                        .setProviderPaymentDetail(providerPaymentDetail)
                        .setProviderPaymentOptions(paymentTypeService.getPaymentTypes())
                        .build());

        savePaymentInstrumentToken(providerItineraryBooking, providerPaymentDetail);

        ProviderItineraryBooking itineraryBooking = ProviderItineraryBooking.builder()
                .setItineraryBookingId(providerItineraryBooking.getItineraryBookingId())
                .setPaymentDetails(providerItineraryBooking.getPaymentDetails())
                .setBuyer(providerItineraryBooking.getBuyer())
                .setSupplierLocators(providerItineraryBooking.getSupplierLocators())
                .setStatus(providerItineraryBooking.getStatus())
                .setItinerary(providerItineraryBooking.getItinerary())
                .setItineraryId(providerItineraryBooking.getItineraryId())
                .setTotalFarePrice(providerDetails.getFareDetails()
                        .getTotalFareAmount().getAmount())
                .build();

        this.providerItineraryBookingRepository.updateItineraryBooking(itineraryBooking);

        return providerDetails.getFareDetails().getTotalFareAmount();
    }

    private void savePaymentInstrumentToken(
            ProviderItineraryBooking providerItineraryBooking,
            ProviderPaymentDetail providerPaymentDetail) {

        PaymentInstrumentToken paymentInstrumentToken = savePaymentDetails(providerPaymentDetail);

        PaymentDetail paymentDetail = PaymentDetail.builder()
                .setId(UUID.randomUUID())
                .setItineraryBookingId(providerItineraryBooking.getItineraryBookingId())
                .setPaymentInstrumentToken(paymentInstrumentToken.getUuid().toString())
                .setCardVendorCode(
                        CreditCardUtil.getEasyJetVendorCode(
                                getReceivedCreditCardInformation(providerPaymentDetail)
                                        .getVendorCode()))
                .build();

        providerItineraryBookingRepository.saveOrUpdateItineraryBookingPaymentDetail(paymentDetail);
    }

    private PaymentInstrumentToken savePaymentDetails(ProviderPaymentDetail providerPaymentDetail) {

        CreditCard receivedCreditCard = getReceivedCreditCardInformation(providerPaymentDetail);

        CreditCardPciScope creditCardPciScope = new CreditCardPciScope();
        creditCardPciScope.setCvv(receivedCreditCard.getVerificationValue());
        creditCardPciScope.setHolder(receivedCreditCard.getHolderName());
        creditCardPciScope.setNumber(receivedCreditCard.getNumber());
        creditCardPciScope.setExpirationMonth(String.valueOf(receivedCreditCard.getExpirationDate().getMonth().getValue()));
        creditCardPciScope.setExpirationYear(String.valueOf(receivedCreditCard.getExpirationDate().getYear()));

        try {
            return creditCardPciScopeResource.tokenize(creditCardPciScope);
        } catch (PaymentInstrumentResourceException e) {
            LOGGER.error(e);
            throw new EasyJetBookingInternalException("Error trying to secure credit card information",
                    EasyJetBookingGatewayErrorType.PAYMENT_INSTRUMENT_EXCEPTION, e);
        }
    }

    private CreditCard getReceivedCreditCardInformation(ProviderPaymentDetail providerPaymentDetail) {

        if (!(providerPaymentDetail.getProviderPaymentMethod() instanceof CreditCard)) {
            throw new EasyJetBookingInternalException("ProviderPaymentMethod not specified",
                    EasyJetBookingGatewayErrorType.PAYMENT_METHOD_NOT_SPECIFIED);
        }

        return ((CreditCard) providerPaymentDetail.getProviderPaymentMethod());
    }
}
