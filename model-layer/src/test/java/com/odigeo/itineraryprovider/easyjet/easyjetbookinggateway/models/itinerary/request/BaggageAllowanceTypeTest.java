package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import org.testng.Assert;
import org.testng.annotations.Test;

public class BaggageAllowanceTypeTest {
    private static BaggageAllowanceType baggageAllowance;

    @Test
    public void testGetValue() {
        baggageAllowance = BaggageAllowanceType.getValue(BaggageAllowanceType.KG.name());
        Assert.assertNotNull(BaggageAllowanceType.valueOf(baggageAllowance.name()));
        Assert.assertNull(BaggageAllowanceType.getValue(null));
        Assert.assertEquals(baggageAllowance.name().toLowerCase(), BaggageAllowanceType.KG.name().toLowerCase());
    }

    @Test
    public void testValues() {
        BaggageAllowanceType[] baggageAllowanceType = BaggageAllowanceType.values();
        Assert.assertNotNull(baggageAllowanceType);
        Assert.assertEquals(baggageAllowanceType[0], BaggageAllowanceType.KG);
        Assert.assertEquals(baggageAllowanceType[1], BaggageAllowanceType.PO);
        Assert.assertEquals(baggageAllowanceType[2], BaggageAllowanceType.NI);
        Assert.assertEquals(baggageAllowanceType[3], BaggageAllowanceType.SC);
        Assert.assertEquals(baggageAllowanceType[4], BaggageAllowanceType.NP);
        Assert.assertEquals(baggageAllowanceType[5], BaggageAllowanceType.SZ);
        Assert.assertEquals(baggageAllowanceType[6], BaggageAllowanceType.VL);
        Assert.assertEquals(baggageAllowanceType[7], BaggageAllowanceType.WE);
        Assert.assertEquals(baggageAllowanceType[8], BaggageAllowanceType.ND);
    }

    @Test
    public void testValueOf() {
        baggageAllowance = BaggageAllowanceType.getValue(BaggageAllowanceType.KG.name());
        Assert.assertNotNull(BaggageAllowanceType.valueOf(baggageAllowance.name()));
        Assert.assertEquals(BaggageAllowanceType.valueOf(baggageAllowance.name()), baggageAllowance);
    }
}
