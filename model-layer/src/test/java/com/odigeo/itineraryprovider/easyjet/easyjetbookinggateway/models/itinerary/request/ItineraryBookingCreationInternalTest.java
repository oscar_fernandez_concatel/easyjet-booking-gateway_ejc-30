package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.BuyerObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryBookingCreationInternalObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderItineraryObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.TravellerObjectMother;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.List;

public class ItineraryBookingCreationInternalTest {
    private static ItineraryBookingCreationInternal itineraryBookingCreationInternal;

    @Test
    public void testBuilder() {
        itineraryBookingCreationInternal = ItineraryBookingCreationInternalObjectMother.ANY;
        Assert.assertNotNull(itineraryBookingCreationInternal);
    }

    @Test
    public void testGetItineraryId() {
        Assert.assertNotNull(itineraryBookingCreationInternal.getItineraryId());
        Assert.assertEquals(itineraryBookingCreationInternal.getItineraryId(), ProviderItineraryObjectMother.ANY.getItineraryId().getId());
    }

    @Test
    public void testGetTravellers() {
        Assert.assertNotNull(itineraryBookingCreationInternal.getTravellers());
        Assert.assertEquals(itineraryBookingCreationInternal.getTravellers(), List.of(TravellerObjectMother.ANY));
    }

    @Test
    public void testGetBuyer() {
        Assert.assertNotNull(itineraryBookingCreationInternal.getBuyer());
        Assert.assertEquals(itineraryBookingCreationInternal.getBuyer(), BuyerObjectMother.ANY);
    }
}
