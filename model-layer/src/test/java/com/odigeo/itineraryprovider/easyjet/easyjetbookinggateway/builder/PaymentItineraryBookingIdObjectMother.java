package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PaymentItineraryBookingId;
import java.util.UUID;

public class PaymentItineraryBookingIdObjectMother {
    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";
    public static final PaymentItineraryBookingId ANY = aPaymentItineraryBookingId();
    public static final PaymentItineraryBookingId EMPTY = aPaymentItineraryBookingIdEmpty();

    private static PaymentItineraryBookingId aPaymentItineraryBookingId() {
        return new PaymentItineraryBookingId(UUID.fromString(IDENTIFIER));
    }

    private static PaymentItineraryBookingId aPaymentItineraryBookingIdEmpty() {
        return new PaymentItineraryBookingId();
    }

}
