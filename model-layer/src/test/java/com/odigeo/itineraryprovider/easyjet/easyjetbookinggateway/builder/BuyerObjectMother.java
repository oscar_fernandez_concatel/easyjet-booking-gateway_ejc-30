package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Buyer;

public class BuyerObjectMother {
    public static final Buyer ANY = aBuyer();

    public static Buyer aBuyer() {
        return Buyer.builder()
                .setName("NAME")
                .setLastNames("LASTNAMES")
                .setMail("MAIL")
                .setPhoneNumber("PHONE")
                .setAddress("ADDRESS")
                .setCityName("CITYNAME")
                .setCountryCode("COUNTRYCODE")
                .setZipCode("ZIPCODE")
                .setPrimeMember(Boolean.TRUE)
                .build();
    }
}

