package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderPaymentMethodObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderPaymentMethod;
import org.testng.Assert;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

public class ProviderPaymentMethodMapperTest {
    com.odigeo.suppliergatewayapi.v25.itinerary.booking.payment.PaymentMethod paymentMethod;

    @Ignore
    public void testToModel() {
        com.odigeo.suppliergatewayapi.v25.itinerary.booking.payment.CreditCard creditCard = (com.odigeo.suppliergatewayapi.v25.itinerary.booking.payment.CreditCard) paymentMethod;
        ProviderPaymentMethod providerPaymentMethod = ProviderPaymentMethodMapper.INSTANCE.toModel(creditCard);
        Assert.assertNotNull(providerPaymentMethod);
        Assert.assertEquals(providerPaymentMethod.getProviderPaymentMethodType().toString(), paymentMethod.getPaymentMethodType());
    }

    @Test
    public void testToApi() {
        ProviderPaymentMethod providerPaymentMethod = ProviderPaymentMethodObjectMother.ANY;
        paymentMethod = ProviderPaymentMethodMapper.INSTANCE.toApi(providerPaymentMethod);
        Assert.assertNotNull(providerPaymentMethod);
        Assert.assertNotNull(paymentMethod);
        Assert.assertEquals(providerPaymentMethod.getProviderPaymentMethodType().toString(), paymentMethod.getPaymentMethodType());
    }
}
