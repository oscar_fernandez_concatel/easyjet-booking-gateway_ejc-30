package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.exception;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayErrorType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingInternalException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingProviderException;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ExceptionTest {

    @Test
    public void testEasyJetBookingProviderException() {
        try {
            throw new EasyJetBookingProviderException("message");
        } catch (final Exception e) {
            Assert.assertTrue(e instanceof EasyJetBookingGatewayException);
            Assert.assertTrue(e.getMessage().contains("message"));
        }
    }

    @Test
    public void testEasyJetBookingInternalExceptionFirstContructor() {
        try {
            throw new EasyJetBookingInternalException("message", EasyJetBookingGatewayErrorType.UNKNOWN);
        } catch (final Exception e) {
            Assert.assertTrue(e instanceof EasyJetBookingGatewayException);
            Assert.assertTrue(e.getMessage().contains("message"));
            Assert.assertEquals(((EasyJetBookingInternalException) e).getErrorType(), EasyJetBookingGatewayErrorType.UNKNOWN);
        }
    }

    @Test
    public void testEasyJetBookingInternalExceptionSecondContructor() {
        try {
            Exception e = new Exception("message");
            throw new EasyJetBookingInternalException(e, EasyJetBookingGatewayErrorType.UNKNOWN);
        } catch (final Exception e) {
            Assert.assertTrue(e instanceof EasyJetBookingGatewayException);
            Assert.assertTrue(e.getMessage().contains("message"));
            Assert.assertEquals(((EasyJetBookingInternalException) e).getErrorType(), EasyJetBookingGatewayErrorType.UNKNOWN);
        }
    }

    @Test
    public void testEasyJetBookingInternalExceptionThirdContructor() {
        try {
            throw new EasyJetBookingInternalException("message", EasyJetBookingGatewayErrorType.UNKNOWN, "codeerror");
        } catch (final Exception e) {
            Assert.assertTrue(e instanceof EasyJetBookingGatewayException);
            Assert.assertTrue(e.getMessage().contains("message-codeerror"));
            Assert.assertEquals(((EasyJetBookingInternalException) e).getErrorType(), EasyJetBookingGatewayErrorType.UNKNOWN);
        }
    }

    @Test
    public void testEasyJetBookingGatewayFirstContructorException() {
        try {
            throw new EasyJetBookingGatewayException("message", EasyJetBookingGatewayErrorType.UNKNOWN);
        } catch (final Exception e) {
            Assert.assertTrue(e instanceof EasyJetBookingGatewayException);
            Assert.assertTrue(e.getMessage().contains("message"));
            Assert.assertEquals(((EasyJetBookingGatewayException) e).getErrorType(), EasyJetBookingGatewayErrorType.UNKNOWN);
        }
    }

    @Test
    public void testEasyJetBookingGatewaySecondContructorException() {
        try {
            Exception e = new Exception("message");
            throw new EasyJetBookingGatewayException(e, EasyJetBookingGatewayErrorType.UNKNOWN, "message");
        } catch (final Exception e) {
            Assert.assertTrue(e instanceof EasyJetBookingGatewayException);
            Assert.assertTrue(e.getMessage().contains("message"));
            Assert.assertEquals(((EasyJetBookingGatewayException) e).getErrorType(), EasyJetBookingGatewayErrorType.UNKNOWN);
        }
    }

    @Test
    public void testEasyJetBookingGatewayExceptionProviderId() {
        try {
            throw new EasyJetBookingGatewayException("message", EasyJetBookingGatewayErrorType.UNKNOWN);
        } catch (final Exception e) {
            Assert.assertTrue(e instanceof EasyJetBookingGatewayException);
            Assert.assertTrue(((EasyJetBookingGatewayException) e).getProviderId().contains("U2"));
        }
    }

    @Test
    public void testEasyJetBookingGatewayErrorType() {
        Assert.assertEquals(EasyJetBookingGatewayErrorType.getErrorType(0), EasyJetBookingGatewayErrorType.NOT_EXISTING_CODE);
    }

    @Test
    public void testEasyJetBookingGatewayErrorTypeFails() {
        Assert.assertEquals(EasyJetBookingGatewayErrorType.getErrorType(1000), EasyJetBookingGatewayErrorType.NOT_EXISTING_CODE);
    }
}
