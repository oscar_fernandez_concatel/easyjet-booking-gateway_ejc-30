package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import org.testng.Assert;
import org.testng.annotations.Test;


public class FareTypeTest {

    @Test
    public void testGetCode() {
        String fareTypeCode = FareType.PRIVATE.getCode();
        Assert.assertNotNull(fareTypeCode);
    }

    @Test
    public void testCompose() {
        String fareTypeCode = FareType.PUBLIC.getCode();
        Assert.assertEquals(fareTypeCode, FareType.PUBLIC.getCode());
        Assert.assertFalse(fareTypeCode.compareTo(FareType.PUBLIC.getCode()) > 0);
    }

    @Test
    public void testGetFareTypeByCode() {
        try {
            FareType fareType = FareType.getFareTypeByCode(FareType.PUBLIC.getCode());
            Assert.assertNotNull(fareType);
            Assert.assertEquals(fareType.getCode(), FareType.PUBLIC.getCode());
        } catch (Exception e) {
            Assert.fail("" + e);
        }
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testGetFareTypeByCodeThrowException() {
        FareType.getFareTypeByCode(null);
    }

    @Test
    public void testValues() {
        FareType[] fareTypes = FareType.values();
        Assert.assertNotNull(fareTypes);
        Assert.assertEquals(fareTypes[0], FareType.PUBLIC);
        Assert.assertEquals(fareTypes[1], FareType.NEGOTIATED);
        Assert.assertEquals(fareTypes[2], FareType.PRIVATE);
    }
}
