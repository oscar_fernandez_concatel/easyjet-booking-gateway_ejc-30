package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCard;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardVendorCode;

import java.time.LocalDate;

public class CreditCardObjectMother {
    public static final CreditCard ANY = aCreditCard();

    public static CreditCard aCreditCard() {
        return CreditCard.builder()
                .setHolderName("HOLDERNAME")
                .setVendorCode(CreditCardVendorCode.VISA_CREDIT)
                .setCardType("CARDTYPE")
                .setNumber("NUMBER")
                .setExpirationDate(LocalDate.now())
                .setVerificationValue("VERIFICATIONVALUE")
                .build();
    }
}

