package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TaxDetail;

public class TaxDetailObjectMother {
    public static final TaxDetail ANY = aTaxDetail();

    private static TaxDetail aTaxDetail() {
        return TaxDetail.builder()
                .setId(TaxDetailIdObjectMother.ANY)
                .setTicketId(TicketIdObjectMother.ANY)
                .setKey("KEY")
                .setCategory("CATEGORY")
                .setAmount(MoneyObjectMother.ANY)
                .setTicketActionId(TicketActionIdObjectMother.ANY)
                .build();
    }
}
