package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.BuyerObjectModel;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.Buyer;
import org.testng.Assert;
import org.testng.annotations.Test;

public class BuyerMapperTest {
    public static final String STRING_VALUE = "String";
    @Test
    public void testToApi() {

        final Buyer eBuyer = BuyerObjectModel.ANY_EXTERNAL;
        Assert.assertEquals(eBuyer.getName(), STRING_VALUE);
        Assert.assertEquals(eBuyer.getAddress(), STRING_VALUE);
        Assert.assertEquals(eBuyer.getCityName(), STRING_VALUE);
        Assert.assertEquals(eBuyer.getMail(), STRING_VALUE);
        Assert.assertEquals(eBuyer.getCountryCode(), STRING_VALUE);
        Assert.assertEquals(eBuyer.getLastNames(), STRING_VALUE);
        Assert.assertEquals(eBuyer.getPhoneNumber(), STRING_VALUE);
        Assert.assertEquals(eBuyer.getZipCode(), STRING_VALUE);

    }
}
