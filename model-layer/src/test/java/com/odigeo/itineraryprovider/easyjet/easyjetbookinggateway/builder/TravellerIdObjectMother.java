package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.TravellerId;

import java.util.UUID;

public class TravellerIdObjectMother {
    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";
    public static final TravellerId ANY = aTravellerId();

    private static TravellerId aTravellerId() {
        return TravellerId.builder()
                .setId(UUID.fromString(IDENTIFIER))
                .build();
    }

}
