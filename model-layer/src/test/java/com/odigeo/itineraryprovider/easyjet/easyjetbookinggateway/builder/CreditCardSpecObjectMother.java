package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardCategory;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardLevel;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardProduct;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardSpec;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardScheme;

public class CreditCardSpecObjectMother {
    public static final CreditCardSpec ANY = aCreditCardSpec();

    public static CreditCardSpec aCreditCardSpec() {
        return new CreditCardSpec.Builder()
                .setCreditCardCategory(CreditCardCategory.CREDIT)
                .setCreditCardLevel(CreditCardLevel.CORPORATE)
                .setCreditCardProduct(CreditCardProduct.DEFAULT)
                .setCreditCardScheme(CreditCardScheme.VISA)
                .build();
    }
}
