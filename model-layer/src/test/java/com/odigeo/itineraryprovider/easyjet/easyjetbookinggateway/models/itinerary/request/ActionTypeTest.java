package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ActionTypeTest {

    @Test
    public void testValuesREF() {
        ActionType actionType = ActionType.REFUNDED;
        Assert.assertEquals(actionType, ActionType.REFUNDED);
    }

    @Test
    public void testValuesVOI() {
        ActionType actionType = ActionType.VOIDED;
        Assert.assertEquals(actionType, ActionType.VOIDED);
    }

    @Test
    public void testValuesISSUE() {
        ActionType actionType = ActionType.ISSUED;
        Assert.assertEquals(actionType, ActionType.ISSUED);
    }
}
