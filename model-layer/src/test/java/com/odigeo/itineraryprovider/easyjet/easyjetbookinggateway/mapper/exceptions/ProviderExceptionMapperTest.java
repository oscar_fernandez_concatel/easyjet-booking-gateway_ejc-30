package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper.exceptions;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayErrorType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.exceptions.ProviderExceptionMapper;
import com.odigeo.suppliergatewayapi.v25.itinerary.exceptions.SupplierGatewayException;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ProviderExceptionMapperTest {

    @Test
    public void testFromModelToApi() {
        EasyJetBookingGatewayException exception = new EasyJetBookingGatewayException("...", EasyJetBookingGatewayErrorType.UNKNOWN);

        SupplierGatewayException result = ProviderExceptionMapper.INSTANCE.fromModelToApi(exception);

        Assert.assertNotNull(result);
        Assert.assertEquals(result.getMessage(), exception.getMessage());


    }
}
