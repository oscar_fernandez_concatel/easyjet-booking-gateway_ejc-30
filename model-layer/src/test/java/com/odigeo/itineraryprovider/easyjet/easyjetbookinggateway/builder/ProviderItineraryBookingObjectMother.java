package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.BookingStatus;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;

import java.util.List;

public class ProviderItineraryBookingObjectMother {

    public static final ProviderItineraryBooking ANY = aProviderItineraryBooking();

    private static ProviderItineraryBooking aProviderItineraryBooking() {
        return ProviderItineraryBooking.builder()
                .setItineraryBookingId(ItineraryBookingIdObjectMother.ANY.getId())
                .setPnr("PNR")
                .setUniversalLocator("UniversalLocator")
                .setReservationLocator("8HUDHW")
                .setBuyer(BuyerObjectMother.ANY)
                .setTravellers(List.of(TravellerObjectMother.ANY))
                .setStatus(BookingStatus.PENDING)
                .setItineraryTickets(List.of(ItineraryTicketObjectMother.ANY))
                .setSupplierLocators(List.of(SupplierLocatorObjectMother.ANY))
                .setPaymentDetails(List.of(PaymentDetailObjectMother.ANY))
                .setItineraryId(ItineraryObjectMother.ANY.getId())
                .setItinerary(ItineraryObjectMother.ANY)
                .build();
    }

}
