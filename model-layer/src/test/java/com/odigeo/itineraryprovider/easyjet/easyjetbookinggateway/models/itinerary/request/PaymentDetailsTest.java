package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.PaymentDetailsObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.PaymentIdObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.PaymentMethodObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.PaymentInstrumentTokenObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.TicketActionIdObjectMother;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PaymentDetailsTest {
    private static PaymentDetails paymentDetails;

    @Test
    public void testBuilder() {
        paymentDetails = PaymentDetailsObjectMother.ANY;
        Assert.assertNotNull(paymentDetails);
    }

    @Test
    public void testGetPaymentId() {
        Assert.assertNotNull(paymentDetails.getPaymentId());
        Assert.assertEquals(paymentDetails.getPaymentId(), PaymentIdObjectMother.ANY);
    }

    @Test
    public void testGetPaymentMethod() {
        Assert.assertNotNull(paymentDetails.getPaymentMethod());
        Assert.assertEquals(paymentDetails.getPaymentMethod(), PaymentMethodObjectMother.ANY);
    }

    @Test
    public void testGetPaymentInstrumentToken() {
        Assert.assertNotNull(paymentDetails.getPaymentInstrumentToken());
        Assert.assertEquals(paymentDetails.getPaymentInstrumentToken(), PaymentInstrumentTokenObjectMother.ANY);
    }

    @Test
    public void testGetTicketActionId() {
        Assert.assertNotNull(paymentDetails.getTicketActionId());
        Assert.assertEquals(paymentDetails.getTicketActionId(), TicketActionIdObjectMother.ANY);
    }

    @Test
    public void testIsCash() {
        Assert.assertFalse(paymentDetails.isCash());
    }
}
