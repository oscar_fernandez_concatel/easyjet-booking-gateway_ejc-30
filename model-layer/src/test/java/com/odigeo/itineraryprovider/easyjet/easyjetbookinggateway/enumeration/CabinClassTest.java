package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.enumeration;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CabinClass;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CabinClassTest {
    @Test
    public void testGetCabinClassNameNull() {
        CabinClass cabinClass = CabinClass.getCabinClass(null);
        Assert.assertNull(cabinClass);
    }

    @Test
    public void testGetCabinClassNameNotExists() {
        CabinClass cabinClass = CabinClass.getCabinClass("H");
        Assert.assertNull(cabinClass);
    }

    @Test
    public void testGetCabinClassNameExists() {
        CabinClass cabinClass = CabinClass.getCabinClass("TOURIST");
        Assert.assertNotNull(cabinClass);
        Assert.assertEquals(cabinClass, CabinClass.TOURIST);
    }
}
