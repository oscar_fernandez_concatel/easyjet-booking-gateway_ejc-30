package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.FlightFareInformationPassengerObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CabinClass;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FareType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request.FlightFareInformationMapper;
import com.odigeo.suppliergatewayapi.v25.itinerary.fare.FlightFareInformationPassenger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Map;

public class FlightFareInformationMapperTest {
    @Test
    public void testToApi() {
        Map<String, FlightFareInformationPassenger> eFlightFareInformations = Collections.singletonMap("PUBLIC",
                FlightFareInformationPassengerObjectMother.ANY);
        final Map<String, com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightFareInformationPassenger> map = FlightFareInformationMapper.INSTANCE.toModel(eFlightFareInformations);
        Assert.assertEquals(map.get("PUBLIC").getFareType(), FareType.PUBLIC);
        Assert.assertEquals(map.get("PUBLIC").getCabinClass(), CabinClass.TOURIST);
        Assert.assertFalse(map.get("PUBLIC").isResident());
        Assert.assertEquals(map.get("PUBLIC").getFareRules(), "");
        Assert.assertEquals(map.get("PUBLIC").getFareBasisId(), "");
        Assert.assertEquals(map.get("PUBLIC").getAgreementsType(), "");
        Assert.assertEquals(map.get("PUBLIC").getAvailableSeats(), -1);
        Assert.assertEquals(map.get("PUBLIC").getServiceClass(), "");
        Assert.assertEquals(map.get("PUBLIC").getSubFareType(), "");
    }
}
