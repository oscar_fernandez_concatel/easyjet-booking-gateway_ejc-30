package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.IssuedItineraryBooking;

import java.util.List;

public class IssuedItineraryBookingObjectMother {
    public static final IssuedItineraryBooking ANY = aIssuedItineraryBooking();

    public static IssuedItineraryBooking aIssuedItineraryBooking() {

        return IssuedItineraryBooking.builder()
                .setId(IssuedItineraryBookingIdObjectMother.ANY)
                .setItineraryBooking(ItineraryBookingObjectMother.ANY)
                .setItineraryTickets(List.of(ItineraryTicketObjectMother.ANY))
                .setStatus("STATUS")
                .build();
    }
}

