package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryBookingIdObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.PhoneObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.TravellerObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TravellerType;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.LocalDate;

public class TravellerTest {
    private Traveller traveller;

    @Test
    public void testBuilder() {
        traveller = TravellerObjectMother.ANY;
        Assert.assertNotNull(traveller);
    }

    @Test
    public void testGetId() {
        Assert.assertNotNull(traveller.getId());
        Assert.assertEquals(traveller.getId().getId().toString(), "15e45e7d-3e34-43df-9366-91c66a8cc9ae");
    }

    @Test
    public void testGetTravellerNumber() {
        Assert.assertNotNull(traveller.getTravellerNumber());
        Assert.assertEquals(traveller.getTravellerNumber(), (Integer) 1);
    }

    @Test
    public void testGetTravellerType() {
        Assert.assertNotNull(traveller.getTravellerType());
        Assert.assertEquals(traveller.getTravellerType(), TravellerType.ADULT);
    }

    @Test
    public void testGetTitle() {
        Assert.assertNotNull(traveller.getTitle());
        Assert.assertEquals(traveller.getTitle(), TravellerTitle.MR);
    }

    @Test
    public void testGetName() {
        Assert.assertNotNull(traveller.getName());
        Assert.assertEquals(traveller.getName(), "name");
    }

    @Test
    public void testGetMiddleName() {
        Assert.assertNotNull(traveller.getMiddleName());
        Assert.assertEquals(traveller.getMiddleName(), "middleName");
    }

    @Test
    public void testGetFirstLastName() {
        Assert.assertNotNull(traveller.getFirstLastName());
        Assert.assertEquals(traveller.getFirstLastName(), "firstLastName");
    }

    @Test
    public void testGetSecondLastName() {
        Assert.assertNotNull(traveller.getSecondLastName());
        Assert.assertEquals(traveller.getSecondLastName(), "secondLastName");
    }

    @Test
    public void testGetTravellerGender() {
        Assert.assertNotNull(traveller.getTravellerGender());
        Assert.assertEquals(traveller.getTravellerGender(), TravellerGender.MALE);
    }

    @Test
    public void testGetDateOfBirth() {
        Assert.assertNotNull(traveller.getDateOfBirth());
        Assert.assertEquals(traveller.getDateOfBirth(), LocalDate.of(1980, 12, 15));
    }

    @Test
    public void testGetAge() {
        Assert.assertNotNull(traveller.getAge());
        Assert.assertEquals(traveller.getAge(), (Integer) 40);
    }

    @Test
    public void testGetNationalityCountryCode() {
        Assert.assertNotNull(traveller.getNationalityCountryCode());
        Assert.assertEquals(traveller.getNationalityCountryCode(), "nationalityCountryCode");
    }

    @Test
    public void testGetCountryCodeOfResidence() {
        Assert.assertNotNull(traveller.getCountryCodeOfResidence());
        Assert.assertEquals(traveller.getCountryCodeOfResidence(), "countryCodeOfResidence");
    }

    @Test
    public void testGetIdentification() {
        Assert.assertNotNull(traveller.getIdentification());
        Assert.assertEquals(traveller.getIdentification(), "identification");
    }

    @Test
    public void testGetIdentificationType() {
        Assert.assertNotNull(traveller.getIdentificationType());
        Assert.assertEquals(traveller.getIdentificationType().toString(), TravellerIdentificationType.NIF.toString());
    }

    @Test
    public void testGetIdentificationExpirationDate() {
        Assert.assertNotNull(traveller.getIdentificationExpirationDate());
        Assert.assertEquals(traveller.getIdentificationExpirationDate(), LocalDate.of(2023, 12, 15));
    }

    @Test
    public void testGetIdentificationIssueCountryCode() {
        Assert.assertNotNull(traveller.getIdentificationIssueCountryCode());
        Assert.assertEquals(traveller.getIdentificationIssueCountryCode(), "identificationIssueCountryCode");
    }

    @Test
    public void testGetLocalityCodeOfResidence() {
        Assert.assertNotNull(traveller.getLocalityCodeOfResidence());
        Assert.assertEquals(traveller.getLocalityCodeOfResidence(), "localityCodeOfResidence");
    }

    @Test
    public void testGetPhone() {
        Assert.assertNotNull(traveller.getPhone());
        Assert.assertEquals(traveller.getPhone(), PhoneObjectMother.ANY);
    }

    @Test
    public void testGetKey() {
        Assert.assertNotNull(traveller.getKey());
        Assert.assertEquals(traveller.getKey(), "key");
    }

    @Test
    public void testGetItineraryBookingId() {
        Assert.assertNotNull(traveller.getItineraryBookingId());
        Assert.assertEquals(traveller.getItineraryBookingId(), ItineraryBookingIdObjectMother.ANY.getId());
    }
}
