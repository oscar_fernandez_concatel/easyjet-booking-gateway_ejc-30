package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.TicketActionIdObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.MoneyObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.TaxDetailIdObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.TaxDetailObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.TicketIdObjectMother;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TaxDetailTest {
    private static TaxDetail taxDetail;

    @Test
    public void testBuilder() {
        taxDetail = TaxDetailObjectMother.ANY;
        Assert.assertNotNull(taxDetail);
    }

    @Test
    public void testGetId() {
        Assert.assertNotNull(taxDetail.getId());
        Assert.assertEquals(taxDetail.getId(), TaxDetailIdObjectMother.ANY);
    }

    @Test
    public void testGetTicketId() {
        Assert.assertNotNull(taxDetail.getTicketId());
        Assert.assertEquals(taxDetail.getTicketId(), TicketIdObjectMother.ANY);
    }

    @Test
    public void testGetKey() {
        Assert.assertNotNull(taxDetail.getKey());
        Assert.assertEquals(taxDetail.getKey(), "KEY");
    }

    @Test
    public void testGetCategory() {
        Assert.assertNotNull(taxDetail.getCategory());
        Assert.assertEquals(taxDetail.getCategory(), "CATEGORY");
    }

    @Test
    public void testGetAmount() {
        Assert.assertNotNull(taxDetail.getAmount());
        Assert.assertEquals(taxDetail.getAmount(), MoneyObjectMother.ANY);
    }

    @Test
    public void testGetTicketActionId() {
        Assert.assertNotNull(taxDetail.getTicketActionId());
        Assert.assertEquals(taxDetail.getTicketActionId(), TicketActionIdObjectMother.ANY);
    }
}
