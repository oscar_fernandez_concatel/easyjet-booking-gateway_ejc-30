package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.enumeration;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.BookingCodes;
import org.testng.Assert;
import org.testng.annotations.Test;

public class BookingCodesTest {

    @Test
    public void testGetValueADT() {
        BookingCodes bookingCodes = BookingCodes.getValue("ADULT");
        Assert.assertEquals(bookingCodes, BookingCodes.ADT);
    }

    @Test
    public void testGetValueCHD() {
        BookingCodes bookingCodes = BookingCodes.getValue("CHILD");
        Assert.assertEquals(bookingCodes, BookingCodes.CHD);
    }

    @Test
    public void testGetValueINF() {
        BookingCodes bookingCodes = BookingCodes.getValue("INFANT");
        Assert.assertEquals(bookingCodes, BookingCodes.INF);
    }

    @Test
    public void testGetValueDefault() {
        BookingCodes bookingCodes = BookingCodes.getValue("XX");
        Assert.assertEquals(bookingCodes, BookingCodes.ADT);
    }

}
