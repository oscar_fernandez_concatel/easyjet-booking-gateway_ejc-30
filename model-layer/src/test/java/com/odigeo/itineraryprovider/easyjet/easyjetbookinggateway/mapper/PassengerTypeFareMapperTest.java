package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.PassengerTypeFareObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PassengerTypeFare;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request.PassengerTypeFareMapper;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PassengerTypeFareMapperTest {

    @Test
    public void testToApi() {
        com.odigeo.suppliergatewayapi.v25.itinerary.fare.PassengerTypeFare passengerTypeFare;
        passengerTypeFare = PassengerTypeFareObjectMother.aPassengerTypeFareExternal();
        final PassengerTypeFare cPassengerTypeFare = PassengerTypeFareMapper.INSTANCE.toModel(passengerTypeFare);
        Assert.assertNotNull(passengerTypeFare);
        Assert.assertNotNull(cPassengerTypeFare);
        Assert.assertEquals(cPassengerTypeFare.getFareAmount().getAmount(), passengerTypeFare.getFareAmount().getAmount());
        Assert.assertEquals(cPassengerTypeFare.getFareAmount().getCurrency(), passengerTypeFare.getFareAmount().getCurrency());
        Assert.assertEquals(cPassengerTypeFare.getPassengersPerType(), passengerTypeFare.getPassengersPerType());
        Assert.assertEquals(cPassengerTypeFare.getTaxAmount().getCurrency(), passengerTypeFare.getTaxAmount().getCurrency());
        Assert.assertEquals(cPassengerTypeFare.getTaxAmount().getAmount(), passengerTypeFare.getTaxAmount().getAmount());
        Assert.assertEquals(cPassengerTypeFare.getTravellerType().toString(), passengerTypeFare.getTravellerType());
    }
}
