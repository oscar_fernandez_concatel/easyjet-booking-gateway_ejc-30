package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Ticket;

import java.util.List;

public class TicketObjectMother {
    public static final Ticket ANY = aTicket();

    private static Ticket aTicket() {
        return Ticket.builder()
                .setId(TicketIdObjectMother.ANY)
                .setNumber("NUMBER")
                .setTravellerKey("TRAVELLERKEY")
                .setTotalAmount(MoneyObjectMother.ANY)
                .setTaxes(MoneyObjectMother.ANY)
                .setCommission(MoneyObjectMother.ANY)
                .setStatus("STATUS")
                .setItineraryBookingId(ItineraryBookingIdObjectMother.ANY)
                .setNumPassenger(1)
                .setValidatingCarrier(CarrierObjectMother.ANY)
                .setIataCode("IATACODE")
                .setOfficeId("OFFICEID")
                .setTicketActions(List.of(TicketActionObjectMother.ANY))
                .setTaxDetails(List.of(TaxDetailObjectMother.ANY))
                .build();
    }
}
