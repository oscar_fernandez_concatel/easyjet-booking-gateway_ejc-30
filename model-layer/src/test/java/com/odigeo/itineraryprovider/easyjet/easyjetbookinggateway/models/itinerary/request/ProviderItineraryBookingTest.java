package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.BuyerObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryBookingIdObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderItineraryBookingObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.TravellerObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryTicketObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.PaymentDetailObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.SupplierLocatorObjectMother;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class ProviderItineraryBookingTest {
    private static ProviderItineraryBooking providerItineraryBooking;

    @Test
    public void testBuilder() {
        providerItineraryBooking = ProviderItineraryBookingObjectMother.ANY;
        Assert.assertNotNull(providerItineraryBooking);
    }

    @Test
    public void testGetItineraryBookingId() {
        Assert.assertNotNull(providerItineraryBooking.getItineraryBookingId());
        Assert.assertEquals(providerItineraryBooking.getItineraryBookingId(), ItineraryBookingIdObjectMother.ANY.getId());
    }

    @Test
    public void testGetPnr() {
        Assert.assertNotNull(providerItineraryBooking.getPnr());
        Assert.assertEquals(providerItineraryBooking.getPnr(), "PNR");
    }

    @Test
    public void testGetUniversalLocator() {
        Assert.assertNotNull(providerItineraryBooking.getUniversalLocator());
        Assert.assertEquals(providerItineraryBooking.getUniversalLocator(), "UniversalLocator");
    }

    @Test
    public void testGetReservationLocator() {
        Assert.assertNotNull(providerItineraryBooking.getReservationLocator());
        Assert.assertEquals(providerItineraryBooking.getReservationLocator(), "8HUDHW");
    }

    @Test
    public void testGetBuyer() {
        Assert.assertNotNull(providerItineraryBooking.getBuyer());
        Assert.assertEquals(providerItineraryBooking.getBuyer(), BuyerObjectMother.ANY);
    }

    @Test
    public void testGetTravellers() {
        Assert.assertNotNull(providerItineraryBooking.getTravellers());
        Assert.assertEquals(providerItineraryBooking.getTravellers(), List.of(TravellerObjectMother.ANY));
    }

    @Test
    public void testGetStatus() {
        Assert.assertNotNull(providerItineraryBooking.getStatus());
        Assert.assertEquals(providerItineraryBooking.getStatus(), BookingStatus.PENDING);
    }

    @Test
    public void testGetItineraryTickets() {
        Assert.assertNotNull(providerItineraryBooking.getItineraryTickets());
        Assert.assertEquals(providerItineraryBooking.getItineraryTickets(), List.of(ItineraryTicketObjectMother.ANY));
    }

    @Test
    public void testGetSupplierLocators() {
        Assert.assertNotNull(providerItineraryBooking.getSupplierLocators());
        Assert.assertEquals(providerItineraryBooking.getSupplierLocators(), List.of(SupplierLocatorObjectMother.ANY));
    }

    @Test
    public void testGetPaymentDetails() {
        Assert.assertNotNull(providerItineraryBooking.getPaymentDetails());
        Assert.assertEquals(providerItineraryBooking.getPaymentDetails(), List.of(PaymentDetailObjectMother.ANY));
    }
}

