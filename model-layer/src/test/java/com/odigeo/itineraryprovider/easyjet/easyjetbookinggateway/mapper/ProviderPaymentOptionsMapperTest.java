package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.PaymentOptionObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CashPaymentOption;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardCategory;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardPaymentOption;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardScheme;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request.ProviderPaymentOptionsMapper;
import com.odigeo.suppliergatewayapi.v25.itinerary.paymentoption.CashProviderPaymentOption;
import com.odigeo.suppliergatewayapi.v25.itinerary.paymentoption.CreditCardProviderPaymentOption;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.math.BigDecimal;

public class ProviderPaymentOptionsMapperTest {

    @Test
    public void testToApiCreditCard() {
        CreditCardPaymentOption paymentOption = PaymentOptionObjectMother.getCardOption(CreditCardCategory.CREDIT, CreditCardScheme.VISA);
        CreditCardProviderPaymentOption response = ProviderPaymentOptionsMapper.INSTANCE.toApi(paymentOption);

        Assert.assertNotNull(response);
        Assert.assertEquals(response.getFee().getAmount(), new BigDecimal(0));
        Assert.assertNotNull(response.getCreditCardSpec());
        Assert.assertEquals(response.getCreditCardSpec().getCreditCardCategory().name(), paymentOption.getCreditCardSpec().getCreditCardCategory().name());
        Assert.assertEquals(response.getCreditCardSpec().getCreditCardLevel().name(), paymentOption.getCreditCardSpec().getCreditCardLevel().name());
        Assert.assertEquals(response.getCreditCardSpec().getCreditCardScheme().name(), paymentOption.getCreditCardSpec().getCreditCardScheme().name());
        Assert.assertEquals(response.getCreditCardSpec().getCreditCardProduct().getName(), paymentOption.getCreditCardSpec().getCreditCardProduct().getName());
    }

    @Test
    public void testToApiCash() {
        CashPaymentOption paymentOption = PaymentOptionObjectMother.getCashOption();
        CashProviderPaymentOption response = ProviderPaymentOptionsMapper.INSTANCE.toApi(paymentOption);

        Assert.assertNotNull(response);
        Assert.assertEquals(response.getFee().getAmount(), new BigDecimal(0));
    }
}
