package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderSection;

public class ProviderSectionObjectMother {
    public static final ProviderSection ANY = aProviderSection();

    public static ProviderSection aProviderSection() {
        return ProviderSection.builder()
                .setKey("KEY")
                .setBookingCode("BOOKINGCODE")
                .setClassOfService("CLASSOFSERVICE")
                .setFareKey("FAREKEY")
                .build();
    }
}
