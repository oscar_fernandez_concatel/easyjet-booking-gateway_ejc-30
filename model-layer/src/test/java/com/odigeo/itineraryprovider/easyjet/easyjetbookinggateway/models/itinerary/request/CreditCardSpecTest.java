package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.CreditCardSpecObjectMother;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CreditCardSpecTest {
    private static CreditCardSpec creditCardSpec;

    @Test
    public void testBuilder() {
        creditCardSpec = CreditCardSpecObjectMother.ANY;
        Assert.assertNotNull(creditCardSpec);
    }

    @Test
    public void testGetCreditCardScheme() {
        Assert.assertNotNull(creditCardSpec.getCreditCardScheme());
        Assert.assertEquals(creditCardSpec.getCreditCardScheme(), CreditCardScheme.VISA);
    }

    @Test
    public void testGetCreditCardProduct() {
        Assert.assertNotNull(creditCardSpec.getCreditCardProduct());
        Assert.assertEquals(creditCardSpec.getCreditCardProduct(), CreditCardProduct.DEFAULT);
    }

    @Test
    public void testGetCreditCardLevel() {
        Assert.assertNotNull(creditCardSpec.getCreditCardLevel());
        Assert.assertEquals(creditCardSpec.getCreditCardLevel(), CreditCardLevel.CORPORATE);
    }

    @Test
    public void testGetCreditCardCategory() {
        Assert.assertNotNull(creditCardSpec.getCreditCardCategory());
        Assert.assertEquals(creditCardSpec.getCreditCardCategory(), CreditCardCategory.CREDIT);
    }
}
