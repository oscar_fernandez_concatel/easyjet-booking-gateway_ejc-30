package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.EasyJetFareDetailsObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryIdObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FareDetails;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Itinerary;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.UUID;

public class ItineraryTest {

    @Test
    public void testSetId() {
        UUID uuid = UUID.fromString(ItineraryIdObjectMother.IDENTIFIER);
        ItineraryId id = ItineraryId.builder()
                .setId(uuid)
                .build();
        Itinerary itinerary = Itinerary.builder().setId(id)
                .build();

        Assert.assertTrue(itinerary.getId().getId().toString().equals(ItineraryIdObjectMother.IDENTIFIER));
    }

    @Test
    public void testSetFareDetails() {
        UUID uuid = UUID.fromString(ItineraryIdObjectMother.IDENTIFIER);
        ItineraryId id = ItineraryId.builder()
                .setId(uuid)
                .build();
        FareDetails fareDetails = EasyJetFareDetailsObjectMother.ANY;
        Itinerary itinerary = Itinerary.builder().setId(id)
                .setFareDetails(fareDetails)
                .build();

        Assert.assertNotNull(itinerary.getFareDetails());
    }

    @Test
    public void testBuilderSetId() {
        UUID uuid = UUID.fromString(ItineraryIdObjectMother.IDENTIFIER);
        ItineraryId id = ItineraryId.builder()
                .setId(uuid)
                .build();
        Itinerary itinerary = Itinerary.builder().setId(id).build();

        Assert.assertTrue(itinerary.getId().getId().toString().equals(ItineraryIdObjectMother.IDENTIFIER));

    }

}
