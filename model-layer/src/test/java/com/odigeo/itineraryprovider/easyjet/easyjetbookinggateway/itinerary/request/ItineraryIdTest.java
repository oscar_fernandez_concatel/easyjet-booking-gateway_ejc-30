package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.UUID;

public class ItineraryIdTest {

    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";

    @Test
    public void testSetId() {
        UUID uuid = UUID.fromString(IDENTIFIER);
        ItineraryId id = ItineraryId.builder()
                .setId(uuid)
                .build();
        Assert.assertTrue(id.getId().toString().equals(IDENTIFIER));
    }

    @Test
    public void testGenerate() {
        ItineraryId generate = ItineraryId.generate();
        Assert.assertNotNull(generate);
    }
}
