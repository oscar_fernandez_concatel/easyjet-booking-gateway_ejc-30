package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.BaggageAllowanceObjectMother;
import org.testng.Assert;
import org.testng.annotations.Test;

public class BaggageAllowanceTest {
    private static BaggageAllowance baggageAllowance;

    @Test
    public void testBuilder() {
        baggageAllowance = BaggageAllowanceObjectMother.ANY_INTERNAL;
        Assert.assertNotNull(baggageAllowance);
    }

    @Test
    public void testGetBaggageAllowanceType() {
        Assert.assertNotNull(baggageAllowance.getBaggageAllowanceType());
        Assert.assertEquals(baggageAllowance.getBaggageAllowanceType(), BaggageAllowanceType.KG);
    }

    @Test
    public void testGetQuantity() {
        Assert.assertNotNull(baggageAllowance.getQuantity());
        Assert.assertEquals(baggageAllowance.getQuantity(), (Integer) 15);
    }
}
