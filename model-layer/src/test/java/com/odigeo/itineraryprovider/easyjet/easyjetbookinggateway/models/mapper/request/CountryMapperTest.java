package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.CountryObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Country;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CountryMapperTest {

    @Test
    public void testMap() {
        Country countryModel = CountryObjectMother.ANY;
        final com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Country country = CountryMapper.INSTANCE.map(countryModel);
        Assert.assertNotNull(countryModel);
        Assert.assertEquals(countryModel.getCountryCode(), country.getCountryCode());
    }

}
