package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.FlightObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.BookingStatus;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Flight;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request.FlightMapper;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FlightMapperTest {
    @Test
    public void testToApi() {
        final com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.Flight eFlight = FlightObjectMother.ANY_EXTERNAL;
        final Flight flight = FlightMapper.INSTANCE.toModel(eFlight);

        Assert.assertNotNull(flight);
        Assert.assertEquals(flight.getFlightNumber(), "AR1401");
        Assert.assertEquals(flight.getStatus(), BookingStatus.CONFIRMED.name());

        Assert.assertEquals(flight.getMarketingCarrier().getIataCode(), "IATACODE");
        Assert.assertEquals(flight.getMarketingCarrier().getCode(), "CODE");
        Assert.assertEquals(flight.getMarketingCarrier().getName(), "NAME");
        Assert.assertTrue(flight.getMarketingCarrier().isShowBaggagePolicy());

        Assert.assertEquals(flight.getOperatingCarrier().getIataCode(), "IATACODE");
        Assert.assertEquals(flight.getOperatingCarrier().getCode(), "CODE");
        Assert.assertEquals(flight.getOperatingCarrier().getName(), "NAME");
        Assert.assertTrue(flight.getOperatingCarrier().isShowBaggagePolicy());
    }
}
