package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.PaymentDetailsObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.TicketActionIdObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.TicketActionObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.TicketIdObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.MoneyObjectMother;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.LocalDateTime;

public class TicketActionTest {
    private static TicketAction ticketAction;

    @Test
    public void testBuilder() {
        ticketAction = TicketActionObjectMother.ANY;
        Assert.assertNotNull(ticketAction);
    }

    @Test
    public void testGetId() {
        Assert.assertNotNull(ticketAction.getId());
        Assert.assertEquals(ticketAction.getId(), TicketActionIdObjectMother.ANY);
    }

    @Test
    public void testGetTicketId() {
        Assert.assertNotNull(ticketAction.getTicketId());
        Assert.assertEquals(ticketAction.getTicketId(), TicketIdObjectMother.ANY);
    }

    @Test
    public void testGetAction() {
        Assert.assertNotNull(ticketAction.getAction());
        Assert.assertEquals(ticketAction.getAction(), ActionType.ISSUED);
    }

    @Test
    public void testGetPaymentDetails() {
        Assert.assertNotNull(ticketAction.getPaymentDetails());
        Assert.assertEquals(ticketAction.getPaymentDetails(), PaymentDetailsObjectMother.ANY);
    }

    @Test
    public void testGetPrice() {
        Assert.assertNotNull(ticketAction.getPrice());
        Assert.assertEquals(ticketAction.getPrice(), MoneyObjectMother.ANY);
    }

    @Test
    public void testGetCommission() {
        Assert.assertNotNull(ticketAction.getCommission());
        Assert.assertEquals(ticketAction.getCommission(), MoneyObjectMother.ANY);
    }

    @Test
    public void testGetExecutionDate() {
        Assert.assertNotNull(ticketAction.getExecutionDate());
        Assert.assertEquals(ticketAction.getExecutionDate(), LocalDateTime.of(2022, 12, 15, 10, 0, 0));
    }
}
