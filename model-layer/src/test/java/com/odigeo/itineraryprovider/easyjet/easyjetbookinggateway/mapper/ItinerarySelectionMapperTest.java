package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.CarrierObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItinerarySelectionObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.EasyJetItinerarySelection;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Itinerary;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request.ItinerarySelectionMapper;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ItinerarySelectionMapperTest {
    @Test
    public void testToModel() {
        final com.odigeo.suppliergatewayapi.v25.itinerary.selection.ItinerarySelection eItinerarySelection = ItinerarySelectionObjectMother.ANY;

        final EasyJetItinerarySelection easyJetItinerarySelection = ItinerarySelectionMapper.INSTANCE.toModel(eItinerarySelection);
        Assert.assertEquals(easyJetItinerarySelection.getVisitId(), "1");
        Assert.assertEquals(easyJetItinerarySelection.getVisitInformation(), "hello world!");
        Assert.assertNotNull(easyJetItinerarySelection.getItinerary());
    }

    @Test
    public void testToItinerary() {
        final com.odigeo.suppliergatewayapi.v25.itinerary.selection.ItinerarySelection eItinerarySelection = ItinerarySelectionObjectMother.ANY;

        final Itinerary itinerary = ItinerarySelectionMapper.INSTANCE.toItinerary(eItinerarySelection);
        Assert.assertNotNull(itinerary);
        Assert.assertEquals(itinerary.getDepartureGeoNodeId().intValue(), 1234);
        Assert.assertEquals(itinerary.getArrivalGeoNodeId().intValue(), 5678);
        Assert.assertTrue(itinerary.getTripType().getValue().equals("ONE_WAY"));
        Assert.assertEquals(itinerary.getDepartureDate(), eItinerarySelection.getDepartureDate());
        Assert.assertEquals(itinerary.getReturnDate(), eItinerarySelection.getReturnDate());
    }


    @Test
    public void testGetCarrier() {
        final com.odigeo.suppliergatewayapi.v25.itinerary.selection.ItinerarySelection eItinerarySelection = ItinerarySelectionObjectMother.ANY;

        final com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.Carrier carrier = ItinerarySelectionMapper.INSTANCE.getCarrier(eItinerarySelection);
        Assert.assertNotNull(carrier);
        Assert.assertEquals(carrier.getName(), CarrierObjectMother.ANY.getName());
        Assert.assertEquals(carrier.getCode(), CarrierObjectMother.ANY.getCode());
        Assert.assertEquals(carrier.getIataCode(), CarrierObjectMother.ANY.getIataCode());
    }

}
