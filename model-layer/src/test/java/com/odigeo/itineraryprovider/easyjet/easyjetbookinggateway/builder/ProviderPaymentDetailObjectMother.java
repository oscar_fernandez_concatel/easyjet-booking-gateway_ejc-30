package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderPaymentDetail;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderPaymentMethod;

public class ProviderPaymentDetailObjectMother {
    public static final ProviderPaymentDetail ANY = aProviderPaymentDetail();

    private static ProviderPaymentDetail aProviderPaymentDetail() {
        ProviderPaymentMethod providerPaymentMethod = ProviderPaymentMethodObjectMother.ANY;
        return ProviderPaymentDetail.builder()
                .setProviderPaymentMethod(providerPaymentMethod)
                .build();
    }
}
