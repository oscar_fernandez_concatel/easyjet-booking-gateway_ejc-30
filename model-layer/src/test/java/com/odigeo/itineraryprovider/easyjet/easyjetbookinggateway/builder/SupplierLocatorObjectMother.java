package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.SupplierLocator;

import java.util.UUID;

public class SupplierLocatorObjectMother {
    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";
    public static final SupplierLocator ANY = aSupplierLocator();

    private static SupplierLocator aSupplierLocator() {
        return SupplierLocator.builder()
                .setSupplierCode("supplierCode")
                .setSupplierLocatorCode("supplierLocatorCode")
                .setItineraryBookingId(UUID.fromString(IDENTIFIER))
                .build();
    }
}

