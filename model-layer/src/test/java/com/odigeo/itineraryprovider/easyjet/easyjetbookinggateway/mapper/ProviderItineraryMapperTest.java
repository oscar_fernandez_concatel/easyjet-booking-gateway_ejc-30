package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.EasyJetItineraryIdObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderItineraryObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItinerary;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request.ProviderItineraryMapper;
import com.odigeo.suppliergatewayapi.v25.itinerary.selection.SelectedItinerary;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.LocalDateTime;

public class ProviderItineraryMapperTest {
    @Test
    public void testToApi() {
        ProviderItinerary providerItinerary = ProviderItineraryObjectMother.ANY;
        ItineraryId id = EasyJetItineraryIdObjectMother.ANY;

        SelectedItinerary selectedItinerary = ProviderItineraryMapper.INSTANCE.toApi(providerItinerary);

        Assert.assertEquals(selectedItinerary.getSelectedItineraryId().getId().toString(), id.getId().toString());
        Assert.assertNull(selectedItinerary.getTotalProviderAmountInFraudRisk());
        Assert.assertEquals(selectedItinerary.getItinerary().getArrivalGeoNodeId().intValue(), 5678);
        Assert.assertEquals(selectedItinerary.getItinerary().getId().getId().toString(), id.getId().toString());
        Assert.assertEquals(selectedItinerary.getItinerary().getDepartureDate(), LocalDateTime.of(2021, 12, 15, 10, 0, 0));
        Assert.assertEquals(selectedItinerary.getItinerary().getReturnDate(), LocalDateTime.of(2021, 12, 15, 10, 0, 0));
        Assert.assertEquals(selectedItinerary.getItinerary().getTripType(), "ONE_WAY");
        Assert.assertEquals(selectedItinerary.getItinerary().getDepartureGeoNodeId().intValue(), 1234);
    }
}
