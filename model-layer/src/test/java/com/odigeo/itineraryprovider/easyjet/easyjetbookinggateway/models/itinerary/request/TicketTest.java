package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.MoneyObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.TicketIdObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.TicketObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.CarrierObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryBookingIdObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.TaxDetailObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.TicketActionObjectMother;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class TicketTest {
    private static Ticket ticket;

    @Test
    public void testBuilder() {
        ticket = TicketObjectMother.ANY;
        Assert.assertNotNull(ticket);
    }

    @Test
    public void testGetId() {
        Assert.assertNotNull(ticket.getId());
        Assert.assertEquals(ticket.getId(), TicketIdObjectMother.ANY);
    }

    @Test
    public void testGetNumber() {
        Assert.assertNotNull(ticket.getNumber());
        Assert.assertEquals(ticket.getNumber(), "NUMBER");
    }

    @Test
    public void testGetTravellerKey() {
        Assert.assertNotNull(ticket.getTravellerKey());
        Assert.assertEquals(ticket.getTravellerKey(), "TRAVELLERKEY");
    }

    @Test
    public void testGetTotalAmount() {
        Assert.assertNotNull(ticket.getTotalAmount());
        Assert.assertEquals(ticket.getTotalAmount(), MoneyObjectMother.ANY);
    }

    @Test
    public void testGetTaxes() {
        Assert.assertNotNull(ticket.getTaxes());
        Assert.assertEquals(ticket.getTaxes(), MoneyObjectMother.ANY);
    }

    @Test
    public void testGetCommission() {
        Assert.assertNotNull(ticket.getCommission());
        Assert.assertEquals(ticket.getCommission(), MoneyObjectMother.ANY);
    }

    @Test
    public void testGetStatus() {
        Assert.assertNotNull(ticket.getStatus());
        Assert.assertEquals(ticket.getStatus(), "STATUS");
    }

    @Test
    public void testGetItineraryBookingId() {
        Assert.assertNotNull(ticket.getItineraryBookingId());
        Assert.assertEquals(ticket.getItineraryBookingId(), ItineraryBookingIdObjectMother.ANY);
    }

    @Test
    public void testGetNumPassenger() {
        Assert.assertNotNull(ticket.getNumPassenger());
        Assert.assertEquals(ticket.getNumPassenger(), (Integer) 1);
    }

    @Test
    public void testGetValidatingCarrier() {
        Assert.assertNotNull(ticket.getValidatingCarrier());
        Assert.assertEquals(ticket.getValidatingCarrier(), CarrierObjectMother.ANY);
    }

    @Test
    public void testGetIataCode() {
        Assert.assertNotNull(ticket.getIataCode());
        Assert.assertEquals(ticket.getIataCode(), "IATACODE");
    }

    @Test
    public void testGetOfficeId() {
        Assert.assertNotNull(ticket.getOfficeId());
        Assert.assertEquals(ticket.getOfficeId(), "OFFICEID");
    }

    @Test
    public void testGetTicketActions() {
        Assert.assertNotNull(ticket.getTicketActions());
        Assert.assertEquals(ticket.getTicketActions(), List.of(TicketActionObjectMother.ANY));
    }

    @Test
    public void testGetTaxDetails() {
        Assert.assertNotNull(ticket.getTaxDetails());
        Assert.assertEquals(ticket.getTaxDetails(), List.of(TaxDetailObjectMother.ANY));
    }
}
