package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FareDetails;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PassengerTypeFare;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Money;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TravellerType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class FareDetailsObjectMother {

    public static final FareDetails ANY = aFareDetails();
    public static final com.odigeo.suppliergatewayapi.v25.itinerary.fare.FareDetails ANY_EXTERNAL = aFareDetailsExternal();

    public static FareDetails aFareDetails() {
        Money totalTaxAmount = MoneyObjectMother.ANY;
        Money totalFareAmount = MoneyObjectMother.ANY;
        List<String> corporateCodes = new ArrayList<>();
        corporateCodes.add("CODE");
        Map<TravellerType, PassengerTypeFare> passengerFares = Collections.singletonMap(TravellerType.ADULT, PassengerTypeFareObjectMother.ANY);

        return FareDetails.builder()
                .setCorporateCodes(corporateCodes)
                .setTotalFareAmount(totalFareAmount)
                .setTotalTaxAmount(totalTaxAmount)
                .setPassengerFares(passengerFares)
                .build();
    }

    public static com.odigeo.suppliergatewayapi.v25.itinerary.fare.FareDetails aFareDetailsExternal() {
        com.odigeo.suppliergatewayapi.v25.itinerary.fare.FareDetails fareDetails = new com.odigeo.suppliergatewayapi.v25.itinerary.fare.FareDetails();
        com.odigeo.suppliergatewayapi.v25.itinerary.fare.Money totalTaxAmount = MoneyObjectMother.ANY_EXTERNAL;
        com.odigeo.suppliergatewayapi.v25.itinerary.fare.Money totalFareAmount = MoneyObjectMother.ANY_EXTERNAL;

        List<String> corporateCodes = new ArrayList<>();
        corporateCodes.add("CODE");

        Map<String, com.odigeo.suppliergatewayapi.v25.itinerary.fare.PassengerTypeFare> passengerFares =
            Collections.singletonMap("ADULT", PassengerTypeFareObjectMother.ANY_EXTERNAL);

        fareDetails.setCorporateCodes(corporateCodes);
        fareDetails.setTotalFareAmount(totalFareAmount);
        fareDetails.setTotalTaxAmount(totalTaxAmount);
        fareDetails.setPassengerFares(passengerFares);

        return fareDetails;
    }
}
