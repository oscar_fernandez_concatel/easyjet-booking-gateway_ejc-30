package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.BuyerObjectMother;
import org.testng.Assert;
import org.testng.annotations.Test;

public class BuyerTest {
    private static Buyer buyer;

    @Test
    public void testBuilder() {
        buyer = BuyerObjectMother.ANY;
        Assert.assertNotNull(buyer);
    }

    @Test
    public void testGetName() {
        Assert.assertNotNull(buyer.getName());
        Assert.assertEquals(buyer.getName(), "NAME");
    }

    @Test
    public void testGetMail() {
        Assert.assertNotNull(buyer.getMail());
        Assert.assertEquals(buyer.getMail(), "MAIL");
    }

    @Test
    public void testGetPhoneNumber() {
        Assert.assertNotNull(buyer.getPhoneNumber());
        Assert.assertEquals(buyer.getName(), "NAME");
    }

    @Test
    public void testGetCountryCode() {
        Assert.assertNotNull(buyer.getCountryCode());
        Assert.assertEquals(buyer.getCountryCode(), "COUNTRYCODE");
    }

    @Test
    public void testGetAddress() {
        Assert.assertNotNull(buyer.getAddress());
        Assert.assertEquals(buyer.getAddress(), "ADDRESS");
    }

    @Test
    public void testGetCityName() {
        Assert.assertNotNull(buyer.getCityName());
        Assert.assertEquals(buyer.getCityName(), "CITYNAME");
    }

    @Test
    public void testGetZipCode() {
        Assert.assertNotNull(buyer.getZipCode());
        Assert.assertEquals(buyer.getZipCode(), "ZIPCODE");
    }

    @Test
    public void testGetLastNames() {
        Assert.assertNotNull(buyer.getLastNames());
        Assert.assertEquals(buyer.getLastNames(), "LASTNAMES");
    }

    @Test
    public void testIsPrimeMember() {
        Assert.assertTrue(buyer.isPrimeMember());
    }
}
