package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.EasyJetItinerarySelectionObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderPaymentDetailObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderPaymentOptionsObjectMother;
import org.testng.Assert;
import org.testng.annotations.Test;

public class EasyJetItinerarySelectionTest {
    private static EasyJetItinerarySelection easyJetItinerarySelection;

    @Test
    public void testBuilder() {
        easyJetItinerarySelection = EasyJetItinerarySelectionObjectMother.ANY;
        Assert.assertNotNull(easyJetItinerarySelection);
    }

    @Test
    public void testGetVisitId() {
        Assert.assertNotNull(easyJetItinerarySelection.getVisitId());
        Assert.assertEquals(easyJetItinerarySelection.getVisitId(), "1");
    }

    @Test
    public void testGetVisitInformation() {
        Assert.assertNotNull(easyJetItinerarySelection.getVisitInformation());
        Assert.assertEquals(easyJetItinerarySelection.getVisitInformation(), "VISITINFORMATION");
    }

    @Test
    public void testGetItinerary() {
        Assert.assertNotNull(easyJetItinerarySelection.getItinerary());
        Assert.assertEquals(easyJetItinerarySelection.getItinerary(), ItineraryObjectMother.ANY);
    }

    @Test
    public void testGetProviderPaymentDetail() {
        Assert.assertNotNull(easyJetItinerarySelection.getProviderPaymentDetail());
        Assert.assertEquals(easyJetItinerarySelection.getProviderPaymentDetail(), ProviderPaymentDetailObjectMother.ANY);
    }

    @Test
    public void testGetProviderPaymentOptions() {
        Assert.assertNotNull(easyJetItinerarySelection.getProviderPaymentOptions());
        Assert.assertEquals(easyJetItinerarySelection.getProviderPaymentOptions(), ProviderPaymentOptionsObjectMother.ANY);
    }
}
