package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.Flight;

public class FlightObjectMother {
    public static final Flight ANY_EXTERNAL = aFlightExternal();
    public static final com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Flight ANY_INTERNAL = aFlightInternal();

    public static Flight aFlightExternal() {
        Flight flight = new Flight();

        flight.setFlightNumber("AR1401");
        flight.setMarketingCarrier(CarrierObjectModel.ANY_EXTERNAL);
        flight.setOperatingCarrier(CarrierObjectModel.ANY_EXTERNAL);
        flight.setStatus("CONFIRMED");
        flight.setTypeOfAircraft("aircraft");

        return flight;
    }

    public static com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Flight aFlightInternal() {
        com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Carrier marketing = CarrierObjectModel.ANY_INTERNAL;
        return com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Flight.builder()
                .setFlightNumber("AR1401")
                .setMarketingCarrier(marketing)
                .setStatus("CONFIRMED")
                .setPositionId(1)
                .setItineraryId(ItineraryIdObjectMother.ANY_INTERNAL)
                .build();
    }

}
