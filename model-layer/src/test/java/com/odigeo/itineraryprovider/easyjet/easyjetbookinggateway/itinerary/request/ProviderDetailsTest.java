package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.EasyJetFareDetailsObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.EasyJetItineraryIdObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.EasyJetPassengerTypeFareObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderDetailsObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FareDetails;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PassengerTypeFare;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderDetails;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TravellerType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TripType;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.LocalDateTime;

public class ProviderDetailsTest {
    @Test
    public void testProviderDetails() {
        ProviderDetails providerDetails = ProviderDetailsObjectMother.ANY;
        ItineraryId id = EasyJetItineraryIdObjectMother.ANY;
        FareDetails fareDetails = EasyJetFareDetailsObjectMother.ANY;
        PassengerTypeFare passengerTypeFare = EasyJetPassengerTypeFareObjectMother.ANY;

        Assert.assertEquals(providerDetails.getItinerary().getArrivalGeoNodeId().intValue(), 5678);
        Assert.assertEquals(providerDetails.getItinerary().getId().getId().toString(), id.getId().toString());
        Assert.assertEquals(providerDetails.getItinerary().getDepartureDate(), LocalDateTime.of(2021, 12, 15, 10, 0, 0));
        Assert.assertEquals(providerDetails.getItinerary().getReturnDate(), LocalDateTime.of(2021, 12, 15, 10, 0, 0));
        Assert.assertEquals(providerDetails.getItinerary().getTripType(), TripType.ONE_WAY);
        Assert.assertEquals(providerDetails.getItinerary().getDepartureGeoNodeId().intValue(), 1234);

        /* FareDetails */
        Assert.assertNotNull(providerDetails.getItinerary().getFareDetails());
        Assert.assertNotNull(providerDetails.getItinerary().getFareDetails().getCorporateCodes());
        Assert.assertEquals(providerDetails.getItinerary().getFareDetails().getCorporateCodes().size(), 1);
        Assert.assertEquals(providerDetails.getItinerary().getFareDetails().getCorporateCodes().get(0), "CODE");
        Assert.assertEquals(providerDetails.getItinerary().getFareDetails().getTotalFareAmount().getAmount(), fareDetails.getTotalFareAmount().getAmount());
        Assert.assertEquals(providerDetails.getItinerary().getFareDetails().getTotalTaxAmount().getAmount(), fareDetails.getTotalTaxAmount().getAmount());
        Assert.assertNotNull(providerDetails.getItinerary().getFareDetails().getItineraryId());
        Assert.assertEquals(providerDetails.getItinerary().getFareDetails().getItineraryId().getId().toString(), "15e45e7d-3e34-43df-9366-91c66a8cc9ae");

        Assert.assertNotNull(providerDetails.getItinerary().getFareDetails().getPassengerFares());
        Assert.assertEquals(providerDetails.getItinerary().getFareDetails().getPassengerFares().size(), 1);
        Assert.assertEquals(providerDetails.getItinerary().getFareDetails().getPassengerFares().get(TravellerType.ADULT).getPassengersPerType().intValue(), 1);
        Assert.assertEquals(providerDetails.getItinerary().getFareDetails().getPassengerFares().get(TravellerType.ADULT).getTravellerType(), TravellerType.ADULT);
        Assert.assertEquals(providerDetails.getItinerary().getFareDetails().getPassengerFares().get(TravellerType.ADULT).getFareAmount().getAmount(), passengerTypeFare.getFareAmount().getAmount());
        Assert.assertEquals(providerDetails.getItinerary().getFareDetails().getPassengerFares().get(TravellerType.ADULT).getTaxAmount().getAmount(), passengerTypeFare.getTaxAmount().getAmount());
        Assert.assertFalse(providerDetails.getItinerary().getFareDetails().getPassengerFares().get(TravellerType.ADULT).isDiscount());

        Assert.assertNotNull(providerDetails.getFareDetails());
        Assert.assertNotNull(providerDetails.getFareDetails().getCorporateCodes());
        Assert.assertEquals(providerDetails.getFareDetails().getCorporateCodes().size(), 1);
        Assert.assertEquals(providerDetails.getFareDetails().getCorporateCodes().get(0), "CODE");
        Assert.assertEquals(providerDetails.getFareDetails().getTotalFareAmount().getAmount(), fareDetails.getTotalFareAmount().getAmount());
        Assert.assertEquals(providerDetails.getFareDetails().getTotalTaxAmount().getAmount(), fareDetails.getTotalTaxAmount().getAmount());
        Assert.assertNotNull(providerDetails.getFareDetails().getItineraryId());
        Assert.assertEquals(providerDetails.getFareDetails().getItineraryId().getId().toString(), "15e45e7d-3e34-43df-9366-91c66a8cc9ae");

        Assert.assertNotNull(providerDetails.getFareDetails().getPassengerFares());
        Assert.assertEquals(providerDetails.getFareDetails().getPassengerFares().size(), 1);
        Assert.assertEquals(providerDetails.getFareDetails().getPassengerFares().get(TravellerType.ADULT).getPassengersPerType().intValue(), 1);
        Assert.assertEquals(providerDetails.getFareDetails().getPassengerFares().get(TravellerType.ADULT).getTravellerType(), TravellerType.ADULT);
        Assert.assertEquals(providerDetails.getFareDetails().getPassengerFares().get(TravellerType.ADULT).getFareAmount().getAmount(), passengerTypeFare.getFareAmount().getAmount());
        Assert.assertEquals(providerDetails.getFareDetails().getPassengerFares().get(TravellerType.ADULT).getTaxAmount().getAmount(), passengerTypeFare.getTaxAmount().getAmount());
        Assert.assertFalse(providerDetails.getFareDetails().getPassengerFares().get(TravellerType.ADULT).isDiscount());
    }

}
