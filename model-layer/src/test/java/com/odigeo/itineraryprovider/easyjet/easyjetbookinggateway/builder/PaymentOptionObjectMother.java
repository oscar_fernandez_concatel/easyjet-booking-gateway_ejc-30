package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CashPaymentOption;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardPaymentOption;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardProduct;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderPaymentOptions;
import com.odigeo.suppliergatewayapi.v25.itinerary.paymentoption.CashProviderPaymentOption;
import com.odigeo.suppliergatewayapi.v25.itinerary.paymentoption.CreditCardProviderPaymentOption;
import com.odigeo.suppliergatewayapi.v25.itinerary.paymentoption.creditcard.CreditCardCategory;
import com.odigeo.suppliergatewayapi.v25.itinerary.paymentoption.creditcard.CreditCardLevel;
import com.odigeo.suppliergatewayapi.v25.itinerary.paymentoption.creditcard.CreditCardScheme;
import com.odigeo.suppliergatewayapi.v25.itinerary.paymentoption.creditcard.CreditCardSpec;

import java.util.HashSet;

public class PaymentOptionObjectMother {

    public static ProviderPaymentOptions getInternal() {
        return ProviderPaymentOptions.builder()
                .setCashPaymentOptions(new HashSet<>() {
                    {
                        add(getCashOption());
                    }
                })
                .setCreditCardPaymentOptions(new HashSet<>() {
                    {
                        add(getCardOption(com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardCategory.CREDIT, com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardScheme.AIRPLUS));
                        add(getCardOption(com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardCategory.CREDIT, com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardScheme.MAESTRO));
                        add(getCardOption(com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardCategory.DEBIT, com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardScheme.VISA));
                        add(getCardOption(com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardCategory.DEBIT, com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardScheme.MASTERCARD));
                        add(getCardOption(com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardCategory.CREDIT, com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardScheme.AMEX));
                    }
                })
                .build();
    }

    public static com.odigeo.suppliergatewayapi.v25.itinerary.paymentoption.ProviderPaymentOptions getExternal() {
        com.odigeo.suppliergatewayapi.v25.itinerary.paymentoption.ProviderPaymentOptions response = new com.odigeo.suppliergatewayapi.v25.itinerary.paymentoption.ProviderPaymentOptions();
        response.setCashProviderPaymentOptions(new HashSet<>() {
            {
                add(getCashProviderOption());
            }
        });

        response.setCreditCardProviderPaymentOptions(new HashSet<>() {
            {
                add(getCardProviderOption(CreditCardCategory.CREDIT, CreditCardScheme.AIRPLUS));
                add(getCardProviderOption(CreditCardCategory.DEBIT, CreditCardScheme.AMEX));
                add(getCardProviderOption(CreditCardCategory.CREDIT, CreditCardScheme.MASTERCARD));
                add(getCardProviderOption(CreditCardCategory.CREDIT, CreditCardScheme.MAESTRO));
                add(getCardProviderOption(CreditCardCategory.DEBIT, CreditCardScheme.JCB));
            }
        });
        return response;

    }

    public static CashPaymentOption getCashOption() {
        return new CashPaymentOption();
    }

    public static CreditCardPaymentOption getCardOption(com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardCategory category, com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardScheme scheme) {
        com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardSpec creditCardSpec = new com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardSpec.Builder()
                .setCreditCardCategory(category)
                .setCreditCardScheme(scheme)
                .setCreditCardLevel(com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardLevel.CUSTOMER)
                .setCreditCardProduct(CreditCardProduct.DEFAULT)
                .build();
        return new CreditCardPaymentOption.Builder()
                .setCreditCardSpec(creditCardSpec)
                        .build();
    }

    public static CashProviderPaymentOption getCashProviderOption() {
        CashProviderPaymentOption response = new CashProviderPaymentOption();
        //response.setFee(MoneyObjectMother.ANY);
        return response;
    }

    public static CreditCardProviderPaymentOption getCardProviderOption(CreditCardCategory category, CreditCardScheme scheme) {
        CreditCardProviderPaymentOption response = new CreditCardProviderPaymentOption();
        //response.setFee(MoneyObjectMother.ANY);
        com.odigeo.suppliergatewayapi.v25.itinerary.paymentoption.creditcard.CreditCardSpec creditCardSpec = new CreditCardSpec();
        creditCardSpec.setCreditCardCategory(category);
        creditCardSpec.setCreditCardScheme(scheme);
        creditCardSpec.setCreditCardLevel(CreditCardLevel.CUSTOMER);
        creditCardSpec.setCreditCardProduct(com.odigeo.suppliergatewayapi.v25.itinerary.paymentoption.creditcard.CreditCardProduct.DEFAULT);
        response.setCreditCardSpec(creditCardSpec);
        return response;
    }
}
