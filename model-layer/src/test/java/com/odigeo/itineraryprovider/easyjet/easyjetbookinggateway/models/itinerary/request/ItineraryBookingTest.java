package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.BuyerObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryBookingIdObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryBookingObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.SupplierLocatorObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.TravellerObjectMother;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class ItineraryBookingTest {
    private static ItineraryBooking itineraryBooking;

    @Test
    public void testBuilder() {
        itineraryBooking = ItineraryBookingObjectMother.ANY;
        Assert.assertNotNull(itineraryBooking);
    }

    @Test
    public void testGetId() {
        Assert.assertNotNull(itineraryBooking.getId());
        Assert.assertEquals(itineraryBooking.getId(), ItineraryBookingIdObjectMother.ANY);
    }

    @Test
    public void testGetItinerary() {
        Assert.assertNotNull(itineraryBooking.getItinerary());
        Assert.assertEquals(itineraryBooking.getItinerary(), ItineraryObjectMother.ANY);
    }

    @Test
    public void testGetBuyer() {
        Assert.assertNotNull(itineraryBooking.getBuyer());
        Assert.assertEquals(itineraryBooking.getBuyer(), BuyerObjectMother.ANY);
    }

    @Test
    public void testGetTravellers() {
        Assert.assertNotNull(itineraryBooking.getTravellers());
        Assert.assertEquals(itineraryBooking.getTravellers(), List.of(TravellerObjectMother.ANY));
    }

    @Test
    public void testGetSupplierLocators() {
        Assert.assertNotNull(itineraryBooking.getSupplierLocators());
        Assert.assertEquals(itineraryBooking.getSupplierLocators(), List.of(SupplierLocatorObjectMother.ANY));
    }

    @Test
    public void testGetPnr() {
        Assert.assertNotNull(itineraryBooking.getPnr());
        Assert.assertEquals(itineraryBooking.getPnr(), "PNR");
    }

    @Test
    public void testGetBookingWithoutPaymentAllowed() {
        Assert.assertNotNull(itineraryBooking.getBookingWithoutPaymentAllowed());
        Assert.assertTrue(itineraryBooking.getBookingWithoutPaymentAllowed());
    }
}

