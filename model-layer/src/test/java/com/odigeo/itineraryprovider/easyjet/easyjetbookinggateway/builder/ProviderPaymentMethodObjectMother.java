package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCard;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardVendorCode;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderPaymentMethod;

import java.time.LocalDate;

public class ProviderPaymentMethodObjectMother {
    public static final ProviderPaymentMethod ANY = aProviderPaymentMethod();

    private static ProviderPaymentMethod aProviderPaymentMethod() {
        return CreditCard.builder()
                .setVendorCode(CreditCardVendorCode.VISA_CREDIT)
                .setNumber("number")
                .setExpirationDate(LocalDate.of(2023, 12, 15))
                .setVerificationValue("verificationValue")
                .setHolderName("holderName")
                .setCardType("cardType")
                .build();
    }
}
