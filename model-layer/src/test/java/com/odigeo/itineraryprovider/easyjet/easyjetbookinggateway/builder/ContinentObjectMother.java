package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Continent;

public class ContinentObjectMother {
    public static final Continent ANY = aContinent();

    private static Continent aContinent() {
        return Continent.builder()
                .setContinentId(1)
                .build();
    }
}

