package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.suppliergatewayapi.v25.itinerary.booking.Buyer;

public class BuyerObjectModel {

    public static final Buyer ANY_EXTERNAL = aBuyerExternal();
    public static final String STRING_VALUE = "String";
    public static final com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Buyer ANY_INTERNAL = aCarrierInternal();

    public static Buyer aBuyerExternal() {

        Buyer buyer = new Buyer();

        buyer.setName(STRING_VALUE);
        buyer.setLastNames(STRING_VALUE);
        buyer.setMail(STRING_VALUE);
        buyer.setPhoneNumber(STRING_VALUE);
        buyer.setCountryCode(STRING_VALUE);
        buyer.setAddress(STRING_VALUE);
        buyer.setCityName(STRING_VALUE);
        buyer.setZipCode(STRING_VALUE);
        buyer.setPrimeMember(Boolean.TRUE);

        return buyer;
    }

    public static com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Buyer aCarrierInternal() {
        return com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Buyer.builder()
                .setName(STRING_VALUE)
                .setLastNames(STRING_VALUE)
                .setMail(STRING_VALUE)
                .setPhoneNumber(STRING_VALUE)
                .setCountryCode(STRING_VALUE)
                .setAddress(STRING_VALUE)
                .setCityName(STRING_VALUE)
                .setZipCode(STRING_VALUE)
                .setPrimeMember(Boolean.TRUE)
                .build();
    }

}
