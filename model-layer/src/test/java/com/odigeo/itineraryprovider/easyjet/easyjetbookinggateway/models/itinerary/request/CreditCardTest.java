package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.CreditCardObjectMother;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.LocalDate;

public class CreditCardTest {
    private static CreditCard creditCard;

    @Test
    public void testBuilder() {
        creditCard = CreditCardObjectMother.ANY;
        Assert.assertNotNull(creditCard);
    }

    @Test
    public void testGetVendorCode() {
        Assert.assertNotNull(creditCard.getVendorCode());
        Assert.assertEquals(creditCard.getVendorCode(), CreditCardVendorCode.VISA_CREDIT);
    }

    @Test
    public void testGetNumber() {
        Assert.assertNotNull(creditCard.getNumber());
        Assert.assertEquals(creditCard.getNumber(), "NUMBER");
    }

    @Test
    public void testGetExpirationDate() {
        Assert.assertNotNull(creditCard.getExpirationDate());
        Assert.assertEquals(creditCard.getExpirationDate(), LocalDate.now());
    }

    @Test
    public void testGetVerificationValue() {
        Assert.assertNotNull(creditCard.getVerificationValue());
        Assert.assertEquals(creditCard.getVerificationValue(), "VERIFICATIONVALUE");
    }

    @Test
    public void testGetHolderName() {
        Assert.assertNotNull(creditCard.getHolderName());
        Assert.assertEquals(creditCard.getHolderName(), "HOLDERNAME");
    }

    @Test
    public void testGetCardType() {
        Assert.assertNotNull(creditCard.getCardType());
        Assert.assertEquals(creditCard.getCardType(), "CARDTYPE");
    }

    @Test
    public void testGetProviderPaymentMethodType() {
        Assert.assertNotNull(creditCard.getProviderPaymentMethodType());
        Assert.assertEquals(creditCard.getProviderPaymentMethodType(), ProviderPaymentMethodType.CREDITCARD);
    }
}
