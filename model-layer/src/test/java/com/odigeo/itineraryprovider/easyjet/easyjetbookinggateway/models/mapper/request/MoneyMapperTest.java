package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.MoneyObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Money;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MoneyMapperTest {
    private com.odigeo.suppliergatewayapi.v25.itinerary.fare.Money money;

    @Test
    public void testToModel() {
        money = MoneyObjectMother.ANY_EXTERNAL;
        Money cMoney = MoneyMapper.INSTANCE.toModel(money);
        Assert.assertNotNull(cMoney);
        Assert.assertNotNull(money);
        Assert.assertEquals(cMoney.getAmount(), money.getAmount());
        Assert.assertEquals(cMoney.getCurrency(), money.getCurrency());
    }

    @Test
    public void testToApi() {
        Money cMoney = MoneyObjectMother.ANY;
        money = MoneyMapper.INSTANCE.toApi(cMoney);
        Assert.assertNotNull(cMoney);
        Assert.assertNotNull(money);
        Assert.assertEquals(cMoney.getAmount(), money.getAmount());
        Assert.assertEquals(cMoney.getCurrency(), money.getCurrency());
    }

}
