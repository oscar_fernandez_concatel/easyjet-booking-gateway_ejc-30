package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryBookingCreationInternal;

import java.util.List;

public class ItineraryBookingCreationInternalObjectMother {
    public static final ItineraryBookingCreationInternal ANY = aItineraryBookingCreationInternal();

    private static ItineraryBookingCreationInternal aItineraryBookingCreationInternal() {
        return ItineraryBookingCreationInternal.builder()
                .setBuyer(BuyerObjectMother.ANY)
                .setTravellers(List.of(TravellerObjectMother.ANY))
                .setSelectedItineraryId(ProviderItineraryObjectMother.ANY.getItineraryId().getId())
                .build();
    }
}
