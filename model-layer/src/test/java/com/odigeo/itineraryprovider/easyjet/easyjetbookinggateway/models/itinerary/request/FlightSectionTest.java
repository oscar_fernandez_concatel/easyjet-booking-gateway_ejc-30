package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.BaggageAllowanceObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.FlightSectionObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryIdObjectMother;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.LocalDateTime;

public class FlightSectionTest {
    private static FlightSection flightSection;

    @Test
    public void testBuilder() {
        flightSection = FlightSectionObjectMother.ANY_INTERNAL;
        Assert.assertNotNull(flightSection);
    }

    @Test
    public void testGetId() {
        Assert.assertNotNull(flightSection.getId());
        Assert.assertEquals(flightSection.getId(), ItineraryIdObjectMother.ANY_INTERNAL);
    }

    @Test
    public void testGetSegmentPosition() {
        Assert.assertNotNull(flightSection.getSegmentPosition());
        Assert.assertEquals(flightSection.getSegmentPosition(), (Integer) 1);
    }

    @Test
    public void testGetDepartureGeoNodeId() {
        Assert.assertNotNull(flightSection.getDepartureGeoNodeId());
        Assert.assertEquals(flightSection.getDepartureGeoNodeId(), (Integer) 1234);
    }

    @Test
    public void testGetArrivalGeoNodeId() {
        Assert.assertNotNull(flightSection.getArrivalGeoNodeId());
        Assert.assertEquals(flightSection.getArrivalGeoNodeId(), (Integer) 5678);
    }

    @Test
    public void testGetDepartureDate() {
        Assert.assertNotNull(flightSection.getDepartureDate());
        Assert.assertEquals(flightSection.getDepartureDate(), LocalDateTime.of(2021, 12, 15, 10, 0, 0));
    }

    @Test
    public void testGetArrivalDate() {
        Assert.assertNotNull(flightSection.getArrivalDate());
        Assert.assertEquals(flightSection.getArrivalDate(), LocalDateTime.of(2021, 12, 15, 10, 0, 0));
    }

    @Test
    public void testGetArrivalTerminal() {
        Assert.assertNotNull(flightSection.getArrivalTerminal());
        Assert.assertEquals(flightSection.getArrivalTerminal(), "BCN");
    }

    @Test
    public void testGetDepartureTerminal() {
        Assert.assertNotNull(flightSection.getDepartureTerminal());
        Assert.assertEquals(flightSection.getDepartureTerminal(), "EZE");
    }

    @Test
    public void testGetCabinClass() {
        Assert.assertNotNull(flightSection.getCabinClass());
        Assert.assertEquals(flightSection.getCabinClass(), CabinClass.TOURIST);
    }

    @Test
    public void testGetBaggageAllowance() {
        Assert.assertNotNull(flightSection.getBaggageAllowance());
        Assert.assertEquals(flightSection.getBaggageAllowance(), BaggageAllowanceObjectMother.ANY_INTERNAL);
    }

    @Test
    public void testGetProductType() {
        Assert.assertNull(flightSection.getProductType());
    }

    @Test
    public void testTestBuilder() {
        com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightSection flightSectionExternal = FlightSectionObjectMother.ANY_INTERNAL;
        Assert.assertNotNull(flightSectionExternal);
    }

    @Test
    public void testGetFlight() {
        Assert.assertNotNull(flightSection.getFlight());
    }

    @Test
    public void testGetFlightFareInformation() {
        Assert.assertNotNull(flightSection.getFlightFareInformation());
    }

    @Test
    public void testGetProviderSection() {
        Assert.assertNull(flightSection.getProviderSection());
    }
}
