package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CabinClass;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FareType;
import com.odigeo.suppliergatewayapi.v25.itinerary.fare.FlightFareInformationPassenger;

import java.util.HashMap;
import java.util.Map;

public class FlightFareInformationPassengerObjectMother {
    public static final FlightFareInformationPassenger ANY = aFlightFareInformationPassenger();

    public static FlightFareInformationPassenger aFlightFareInformationPassenger() {

        FlightFareInformationPassenger flightFareInformationPassenger = new FlightFareInformationPassenger();

        flightFareInformationPassenger.setCabinClass("TOURIST");
        flightFareInformationPassenger.setResident(false);
        flightFareInformationPassenger.setFareRules("");
        flightFareInformationPassenger.setFareType("PUBLIC");
        flightFareInformationPassenger.setFareBasisId("");
        flightFareInformationPassenger.setAgreementsType("");
        flightFareInformationPassenger.setAvailableSeats(-1);
        flightFareInformationPassenger.setServiceClass("");
        flightFareInformationPassenger.setSubFareType("");
        return flightFareInformationPassenger;
    }

    public static com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightFareInformationPassenger internalModelFlightFareInformationPassengerPublicFare() {

        return com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightFareInformationPassenger.builder()
                .setCabinClass(CabinClass.TOURIST)
                .setIsResident(false)
                .setFareRules("")
                .setFareType(FareType.PUBLIC)
                .setFareBasisId("")
                .setAgreementsType("")
                .setAvailableSeats(-1)
                .setServiceClass("")
                .setSubFareType("")
                .build();
    }

    public static com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightFareInformationPassenger internalModelFlightFareInformationPassengerPrivateFare() {

        return com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightFareInformationPassenger.builder()
                .setCabinClass(CabinClass.TOURIST)
                .setIsResident(false)
                .setFareRules("")
                .setFareType(FareType.PRIVATE)
                .setFareBasisId("")
                .setAgreementsType("")
                .setAvailableSeats(-1)
                .setServiceClass("")
                .setSubFareType("")
                .build();
    }

    public static com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightFareInformationPassenger internalModelFlightFareInformationPassengerNegotiatedFare() {

        return com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightFareInformationPassenger.builder()
                .setCabinClass(CabinClass.TOURIST)
                .setIsResident(false)
                .setFareRules("")
                .setFareType(FareType.NEGOTIATED)
                .setFareBasisId("")
                .setAgreementsType("")
                .setAvailableSeats(-1)
                .setServiceClass("")
                .setSubFareType("")
                .build();
    }

    public static Map<String, com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightFareInformationPassenger> mapFareFlightFareInformationPassenger() {

        Map<String, com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightFareInformationPassenger> response = new HashMap<>();
        response.put("ANY-FARE", internalModelFlightFareInformationPassengerPublicFare());
        return response;
    }

    public static Map<String, com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightFareInformationPassenger> mapFareFlightFareInformationPassengerPubAndPriv() {

        Map<String, com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightFareInformationPassenger> response = new HashMap<>();
        response.put("ANY-FARE-PUB", internalModelFlightFareInformationPassengerPublicFare());
        response.put("ANY-FARE-PRIV", internalModelFlightFareInformationPassengerPrivateFare());
        return response;
    }

}
