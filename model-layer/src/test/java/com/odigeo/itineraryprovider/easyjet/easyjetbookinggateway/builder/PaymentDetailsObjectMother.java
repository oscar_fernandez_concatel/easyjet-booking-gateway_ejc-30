package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PaymentDetails;

public class PaymentDetailsObjectMother {
    public static final PaymentDetails ANY = aPaymentDetails();

    private static PaymentDetails aPaymentDetails() {
        return PaymentDetails.builder()
                .setPaymentId(PaymentIdObjectMother.ANY)
                .setPaymentMethod(PaymentMethodObjectMother.ANY)
                .setPaymentInstrumentToken(PaymentInstrumentTokenObjectMother.ANY)
                .setTicketActionId(TicketActionIdObjectMother.ANY)
                .setIsCash(Boolean.FALSE)
                .build();
    }
}
