package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TravellerType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Traveller;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.TravellerGender;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.TravellerIdentificationType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.TravellerTitle;

import java.time.LocalDate;

public class TravellerObjectMother {

    public static final Traveller ANY = aTraveller();

    private static Traveller aTraveller() {
        return Traveller.builder()
                .setId(TravellerIdObjectMother.ANY)
                .setTravellerNumber(1)
                .setTravellerType(TravellerType.ADULT)
                .setTitle(TravellerTitle.MR)
                .setName("name")
                .setMiddleName("middleName")
                .setFirstLastName("firstLastName")
                .setSecondLastName("secondLastName")
                .setTravellerGender(TravellerGender.MALE)
                .setDateOfBirth(LocalDate.of(1980, 12, 15))
                .setAge(40)
                .setNationalityCountryCode("nationalityCountryCode")
                .setCountryCodeOfResidence("countryCodeOfResidence")
                .setIdentification("identification")
                .setIdentificationType(TravellerIdentificationType.NIF)
                .setIdentificationExpirationDate(LocalDate.of(2023, 12, 15))
                .setIdentificationIssueCountryCode("identificationIssueCountryCode")
                .setLocalityCodeOfResidence("localityCodeOfResidence")
                .setPhone(PhoneObjectMother.ANY)
                .setKey("key")
                .setItineraryBookingId(ItineraryBookingIdObjectMother.ANY.getId())
                .build();
    }
}

