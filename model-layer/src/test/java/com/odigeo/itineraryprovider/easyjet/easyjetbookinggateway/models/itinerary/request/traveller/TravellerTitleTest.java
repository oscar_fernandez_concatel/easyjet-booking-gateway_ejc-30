package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller;

import com.edreams.base.MissingElementException;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TravellerTitleTest {
    private static TravellerTitle travellerTitle;

    @Test
    public void testTravellerTitle() {
        travellerTitle = TravellerTitle.MRS;
        Assert.assertNotNull(travellerTitle);
    }

    @Test
    public void testValueOf() {
        String travellerTitleStr = String.valueOf(TravellerTitle.MRS);
        Assert.assertNotNull(travellerTitleStr);
        Assert.assertEquals(travellerTitle.toString(), travellerTitleStr);
    }

    @Test
    public void testGetDatabaseCode() {
        String databaseCode = TravellerTitle.MRS.getDatabaseCode();
        Assert.assertNotNull(databaseCode);
    }

    @Test
    public void testGetValue() {
        try {
            Assert.assertEquals(TravellerTitle.MR, TravellerTitle.getValue(TravellerTitle.MR.getDatabaseCode()));
            Assert.assertEquals(TravellerTitle.MS, TravellerTitle.getValue(TravellerTitle.MS.getDatabaseCode()));
            Assert.assertEquals(TravellerTitle.MRS, TravellerTitle.getValue(TravellerTitle.MRS.getDatabaseCode()));
        } catch (MissingElementException e) {
            e.printStackTrace();
        }
    }

    @Test(expectedExceptions = MissingElementException.class)
    public void testGetValueThrowsException() throws MissingElementException {
        TravellerTitle.getValue(null);
    }

    @Test
    public void testValues() {
        TravellerTitle[] travellerTitles = TravellerTitle.values();
        Assert.assertNotNull(travellerTitles);
        Assert.assertEquals(travellerTitles[0], TravellerTitle.MR);
        Assert.assertEquals(travellerTitles[1], TravellerTitle.MRS);
        Assert.assertEquals(travellerTitles[2], TravellerTitle.MS);
    }

}
