package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.IssuedItineraryBookingIdObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.IssuedItineraryBookingObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryBookingObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryTicketObjectMother;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class IssuedItineraryBookingTest {
    private static IssuedItineraryBooking issuedItineraryBooking;

    @Test
    public void testBuilder() {
        issuedItineraryBooking = IssuedItineraryBookingObjectMother.ANY;
        Assert.assertNotNull(issuedItineraryBooking);
    }

    @Test
    public void testGetId() {
        Assert.assertNotNull(issuedItineraryBooking.getId());
        Assert.assertEquals(issuedItineraryBooking.getId(), IssuedItineraryBookingIdObjectMother.ANY);
    }

    @Test
    public void testGetItineraryBooking() {
        Assert.assertNotNull(issuedItineraryBooking.getItineraryBooking());
        Assert.assertEquals(issuedItineraryBooking.getItineraryBooking(), ItineraryBookingObjectMother.ANY);
    }

    @Test
    public void testGetItineraryTickets() {
        Assert.assertNotNull(issuedItineraryBooking.getItineraryTickets());
        Assert.assertEquals(issuedItineraryBooking.getItineraryTickets(), List.of(ItineraryTicketObjectMother.ANY));
    }

    @Test
    public void testGetStatus() {
        Assert.assertNotNull(issuedItineraryBooking.getStatus());
        Assert.assertEquals(issuedItineraryBooking.getStatus(), "STATUS");
    }
}
