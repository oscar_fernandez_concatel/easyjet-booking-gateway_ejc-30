package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryBooking;

import java.util.List;

public class ItineraryBookingObjectMother {
    public static final ItineraryBooking ANY = aItineraryBooking();

    public static ItineraryBooking aItineraryBooking() {
        return ItineraryBooking.builder()
                .setId(ItineraryBookingIdObjectMother.ANY)
                .setItinerary(ItineraryObjectMother.ANY)
                .setBookingWithoutPaymentAllowed(Boolean.TRUE)
                .setBuyer(BuyerObjectMother.ANY)
                .setPnr("PNR")
                .setTravellers(List.of(TravellerObjectMother.ANY))
                .setSupplierLocators(List.of(SupplierLocatorObjectMother.ANY))
                .build();
    }
}

