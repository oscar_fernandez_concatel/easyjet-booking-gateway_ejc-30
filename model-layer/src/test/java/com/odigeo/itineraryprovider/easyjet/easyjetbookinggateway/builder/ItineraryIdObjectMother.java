package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.suppliergatewayapi.v25.itinerary.ItineraryId;

public class ItineraryIdObjectMother {

    public static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";
    public static final ItineraryId ANY_EXTERNAL = aItineraryIdExternal();
    public static final com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId ANY_INTERNAL = aItineraryIdInternal();

    public static ItineraryId aItineraryIdExternal() {
        return ItineraryId.valueOf("15e45e7d-3e34-43df-9366-91c66a8cc9ae");
    }

    public static com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId aItineraryIdInternal() {
        return com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId.builder()
                .setId(java.util.UUID.randomUUID())
                .build();
    }


}
