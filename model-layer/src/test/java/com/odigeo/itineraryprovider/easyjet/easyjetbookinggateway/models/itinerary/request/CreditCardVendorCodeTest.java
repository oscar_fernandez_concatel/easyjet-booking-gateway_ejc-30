package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import org.testng.Assert;
import org.testng.annotations.Test;

public class CreditCardVendorCodeTest {
    private static CreditCardVendorCode creditCardVendorCode;

    @Test
    public void testGetType() {
        creditCardVendorCode = CreditCardVendorCode.getValue(CreditCardVendorCode.VISA_CREDIT.getType());
        Assert.assertNotNull(creditCardVendorCode);
        Assert.assertEquals(creditCardVendorCode.getType(), CreditCardVendorCode.VISA_CREDIT.getType());
    }

    @Test
    public void testGetValue() {
        try {
            String type = CreditCardVendorCode.VISA_CREDIT.getType();
            Assert.assertNotNull(creditCardVendorCode);
            Assert.assertEquals(creditCardVendorCode.getType(), type);
        } catch (Exception e) {
            Assert.fail("Unexpected enum constant: " + e);
        }
    }
    @Test
    public void testValues() {
        CreditCardVendorCode[] creditCardVendorCode = CreditCardVendorCode.values();
        Assert.assertNotNull(creditCardVendorCode);
        Assert.assertEquals(creditCardVendorCode[0], CreditCardVendorCode.VISA_CREDIT);
        Assert.assertEquals(creditCardVendorCode[1], CreditCardVendorCode.VISA_DEBIT);
        Assert.assertEquals(creditCardVendorCode[2], CreditCardVendorCode.VISA_ELECTRON);
        Assert.assertEquals(creditCardVendorCode[3], CreditCardVendorCode.MASTERCARD);
        Assert.assertEquals(creditCardVendorCode[4], CreditCardVendorCode.MASTERCARD_PREPAID);
        Assert.assertEquals(creditCardVendorCode[5], CreditCardVendorCode.MAESTRO);
        Assert.assertEquals(creditCardVendorCode[6], CreditCardVendorCode.MASTERCARD_DEBIT);
        Assert.assertEquals(creditCardVendorCode[7], CreditCardVendorCode.DINERS_CLUB);
        Assert.assertEquals(creditCardVendorCode[8], CreditCardVendorCode.AMERICAN_EXPRESS);
        Assert.assertEquals(creditCardVendorCode[9], CreditCardVendorCode.AMEX);
    }

    @Test
    public void testValueOf() {
        creditCardVendorCode = CreditCardVendorCode.valueOf(CreditCardVendorCode.VISA_CREDIT.name());
        Assert.assertNotNull(creditCardVendorCode);
        Assert.assertEquals(creditCardVendorCode.name(), CreditCardVendorCode.VISA_CREDIT.name());
    }
}
