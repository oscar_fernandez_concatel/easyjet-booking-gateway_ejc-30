package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.PhoneObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Phone;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PhoneMapperTest {

    @Test
    public void testMap() {
        Phone phone = PhoneObjectMother.ANY;
        PhoneMapper phoneMapper = new PhoneMapperImpl();
        com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Phone phoneModel = phoneMapper.map(phone);
        Assert.assertNotNull(phone);
        Assert.assertNotNull(phoneModel);
        Assert.assertEquals(phoneModel.getPhoneType(), phone.getPhoneType());
        Assert.assertEquals(phoneModel.getNumber(), phone.getNumber());
        Assert.assertEquals(phoneModel.getCountry().getCountryCode(), phone.getCountry().getCountryCode());
    }

    @Test
    public void testTestMap() {
        com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Phone phone = PhoneObjectMother.ANY_EXTERNAL;
        PhoneMapper phoneMapper = new PhoneMapperImpl();
        Phone phoneModel = phoneMapper.map(phone);
        Assert.assertNotNull(phone);
        Assert.assertNotNull(phoneModel);
        Assert.assertEquals(phoneModel.getPhoneType(), phone.getPhoneType());
        Assert.assertEquals(phoneModel.getNumber(), phone.getNumber());
        Assert.assertEquals(phoneModel.getCountry().getCountryCode(), phone.getCountry().getCountryCode());
    }
}
