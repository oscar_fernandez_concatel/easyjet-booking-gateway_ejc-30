package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.itinerary.request.command;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderItineraryBookingObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.command.CreateItineraryBookingCommand;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class CreateItineraryBookingCommandTest {

    @Test
    public void testCreateItineraryBookingCommandConstructor() {
        ProviderItineraryBooking providerItineraryBooking = ProviderItineraryBookingObjectMother.ANY;
        CreateItineraryBookingCommand createItineraryBookingCommand = CreateItineraryBookingCommand
            .builder()
            .itineraryBooking(providerItineraryBooking)
            .seatSelections(List.of())
            .baggageSelections(List.of())
            .build();

        Assert.assertNotNull(createItineraryBookingCommand);
        Assert.assertEquals(createItineraryBookingCommand.getItineraryBooking().getItineraryBookingId().toString(), providerItineraryBooking.getItineraryBookingId().toString());
        Assert.assertEquals(createItineraryBookingCommand.getItineraryBooking().getPnr(), providerItineraryBooking.getPnr());
        Assert.assertEquals(createItineraryBookingCommand.getItineraryBooking().getUniversalLocator(), providerItineraryBooking.getUniversalLocator());
        Assert.assertEquals(createItineraryBookingCommand.getItineraryBooking().getReservationLocator(), providerItineraryBooking.getReservationLocator());
        Assert.assertEquals(createItineraryBookingCommand.getItineraryBooking().getTravellers().stream().count(), providerItineraryBooking.getTravellers().stream().count());
        Assert.assertEquals(createItineraryBookingCommand.getItineraryBooking().getStatus(), providerItineraryBooking.getStatus());
        Assert.assertEquals(createItineraryBookingCommand.getItineraryBooking().getItineraryTickets().stream().count(), providerItineraryBooking.getItineraryTickets().stream().count());
        Assert.assertEquals(createItineraryBookingCommand.getItineraryBooking().getSupplierLocators().stream().count(), providerItineraryBooking.getSupplierLocators().stream().count());
        Assert.assertEquals(createItineraryBookingCommand.getItineraryBooking().getPaymentDetails().stream().count(), providerItineraryBooking.getPaymentDetails().stream().count());
        Assert.assertEquals(createItineraryBookingCommand.getItineraryBooking().getItineraryId().toString(), providerItineraryBooking.getItineraryId().toString());
        Assert.assertEquals(createItineraryBookingCommand.getItineraryBooking().getItinerary().getId().getId().toString(), providerItineraryBooking.getItinerary().getId().getId().toString());
        Assert.assertNotNull(createItineraryBookingCommand.getSeatSelections());
        Assert.assertNotNull(createItineraryBookingCommand.getBaggageSelections());
    }

}
