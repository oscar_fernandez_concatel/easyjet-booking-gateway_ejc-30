package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CabinClass;
import org.testng.Assert;
import org.testng.annotations.Ignore;

public class CabinClassMapperTest {

    @Ignore
    public void testToModel() {
        final CabinClass cabinClassName = CabinClass.getCabinClass(CabinClass.TOURIST.getName());
        CabinClass cabinClass = CabinClassMapperImpl.INSTANCE.toModel(cabinClassName.getName());
        Assert.assertNotNull(cabinClass);
        Assert.assertNotNull(cabinClassName);
        Assert.assertEquals(cabinClass.getName(), cabinClassName.getName());
    }
}
