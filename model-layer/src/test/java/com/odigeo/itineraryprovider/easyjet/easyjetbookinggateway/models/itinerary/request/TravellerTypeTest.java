package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.TravellerObjectMother;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TravellerTypeTest {
    private static TravellerType travellerType;

    @Test
    public void testBuilder() {
        travellerType = TravellerObjectMother.ANY.getTravellerType();
        Assert.assertNotNull(travellerType);
    }

    @Test
    public void testTestToString() {
        String travellerType = TravellerType.ADULT.toString();
        Assert.assertNotNull(travellerType);
        Assert.assertEquals(travellerType, TravellerType.ADULT.toString());
    }

    @Test
    public void testTestEquals() {
        travellerType = TravellerType.ADULT;
        Assert.assertNotNull(travellerType);
        Assert.assertEquals(travellerType, TravellerType.ADULT);
    }

    @Test
    public void testValueOf() {
        TravellerType[] travellerTypes = TravellerType.values();
        Assert.assertNotNull(travellerTypes);
        Assert.assertEquals(travellerTypes[0], TravellerType.ADULT);
        Assert.assertEquals(travellerTypes[1], TravellerType.INFANT);
        Assert.assertEquals(travellerTypes[2], TravellerType.CHILD);
    }

    @Test
    public void testGetCode() {
        String travellerTypeStr = TravellerType.ADULT.getCode();
        Assert.assertNotNull(travellerType);
        Assert.assertEquals(travellerType.getCode(), travellerTypeStr);
        Assert.assertNotEquals(travellerType.getCode(), TravellerType.INFANT.getCode());
        Assert.assertNotEquals(travellerType.getCode(), TravellerType.CHILD.getCode());
    }

    @Test
    public void testGetTravellerType() {
        String travellerCode = TravellerType.ADULT.getCode();
        Assert.assertNotNull(travellerCode);
        Assert.assertEquals(travellerCode, travellerType.getCode());
    }
}
