package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.FlightSectionObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightSection;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FlightSectionMapperTest {
    private com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.FlightSection eFlightSection;
    private FlightSection flightSection;

    @Test
    public void testToModel() {
        eFlightSection = FlightSectionObjectMother.ANY_EXTERNAL;
        FlightSection internalFlightSection = FlightSectionMapper.INSTANCE.toModel(eFlightSection);
        Assert.assertNotNull(internalFlightSection);
        Assert.assertEquals(internalFlightSection.getArrivalDate(), eFlightSection.getArrivalDate());
        Assert.assertEquals(internalFlightSection.getFlight().getFlightNumber(), eFlightSection.getFlight().getFlightNumber());
        Assert.assertEquals(internalFlightSection.getFlightFareInformation().getFareType().name(), eFlightSection.getFlightFareInformations().get("PUBLIC").getFareType().toString());
        Assert.assertEquals(internalFlightSection.getArrivalTerminal(), eFlightSection.getArrivalTerminal());
        Assert.assertEquals(internalFlightSection.getArrivalGeoNodeId(), eFlightSection.getArrivalGeoNodeId());
        Assert.assertEquals(internalFlightSection.getBaggageAllowance().getBaggageAllowanceType().toString(), eFlightSection.getBaggageAllowance().getBaggageAllowanceType().toString());
        Assert.assertEquals(internalFlightSection.getCabinClass().getName(), eFlightSection.getCabinClass().toString());
        Assert.assertEquals(internalFlightSection.getDepartureDate(), eFlightSection.getDepartureDate());
        Assert.assertEquals(internalFlightSection.getDepartureGeoNodeId(), eFlightSection.getDepartureGeoNodeId());
        Assert.assertEquals(internalFlightSection.getDepartureTerminal(), eFlightSection.getDepartureTerminal());
        Assert.assertEquals(internalFlightSection.getId().getId().toString(), eFlightSection.getId().getId().toString());
        Assert.assertEquals(internalFlightSection.getSegmentPosition(), eFlightSection.getSegmentPosition());
        Assert.assertEquals(internalFlightSection.getProductType(), eFlightSection.getProductType());
    }

    @Test
    public void testToApi() {
        flightSection = FlightSectionObjectMother.ANY_INTERNAL;
        com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.FlightSection externalFlightSection = FlightSectionMapper.INSTANCE.toApi(flightSection);;
        Assert.assertNotNull(externalFlightSection);
        Assert.assertEquals(externalFlightSection.getArrivalDate(), flightSection.getArrivalDate());
        Assert.assertEquals(externalFlightSection.getFlight().getFlightNumber(), flightSection.getFlight().getFlightNumber());
        Assert.assertEquals(externalFlightSection.getFlightFareInformations().get("ANY-FARE").getFareType(), flightSection.getFlightFareInformation().getFareType().name());
        Assert.assertEquals(externalFlightSection.getArrivalTerminal(), flightSection.getArrivalTerminal());
        Assert.assertEquals(externalFlightSection.getArrivalGeoNodeId(), flightSection.getArrivalGeoNodeId());
        Assert.assertEquals(externalFlightSection.getBaggageAllowance().getBaggageAllowanceType().toString(), flightSection.getBaggageAllowance().getBaggageAllowanceType().toString());
        Assert.assertEquals(externalFlightSection.getCabinClass().toString(), flightSection.getCabinClass().getName());
        Assert.assertEquals(externalFlightSection.getDepartureDate(), flightSection.getDepartureDate());
        Assert.assertEquals(externalFlightSection.getDepartureGeoNodeId(), flightSection.getDepartureGeoNodeId());
        Assert.assertEquals(externalFlightSection.getDepartureTerminal(), flightSection.getDepartureTerminal());
        Assert.assertEquals(externalFlightSection.getId().getId().toString(), flightSection.getId().getId().toString());
        Assert.assertEquals(externalFlightSection.getSegmentPosition(), flightSection.getSegmentPosition());
        Assert.assertEquals(externalFlightSection.getProductType(), flightSection.getProductType());
    }

}
