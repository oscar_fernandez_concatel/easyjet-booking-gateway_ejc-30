package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.PaymentOptionObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderPaymentOptions;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class ProviderPaymentOptionTest {

    ProviderPaymentOptions providerPaymentOptions;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        providerPaymentOptions = PaymentOptionObjectMother.getInternal();
    }

    @Test
    public void testGet() {
        Assert.assertNotNull(providerPaymentOptions.getCashPaymentOptions());
        Assert.assertNotNull(providerPaymentOptions.getCreditCardPaymentOptions());
    }

}
