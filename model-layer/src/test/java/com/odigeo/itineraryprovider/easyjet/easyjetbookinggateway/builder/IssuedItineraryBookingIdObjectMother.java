package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.IssuedItineraryBookingId;

public class IssuedItineraryBookingIdObjectMother {
    public static final IssuedItineraryBookingId ANY = aIssuedItineraryBookingId();

    public static IssuedItineraryBookingId aIssuedItineraryBookingId() {
        return IssuedItineraryBookingId.builder()
                .setId(java.util.UUID.randomUUID())
                .build();
    }
}
