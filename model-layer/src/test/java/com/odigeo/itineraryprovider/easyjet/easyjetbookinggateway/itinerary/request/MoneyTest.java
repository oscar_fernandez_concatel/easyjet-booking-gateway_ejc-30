package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Money;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Currency;

public class MoneyTest {

    @Test
    public void testMoneyConstructor() {
        Money money = Money.builder()
                .setAmount(BigDecimal.ONE)
                .setCurrency(Currency.getInstance("EUR"))
                .build();
        Assert.assertEquals(money.getAmount(), BigDecimal.ONE);
        Assert.assertEquals(money.getCurrency().getCurrencyCode(), "EUR");
    }

}
