package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PaymentMethod;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PaymentMethodType;

public class PaymentMethodObjectMother {
    public static final PaymentMethod ANY = aPaymentMethod();

    private static PaymentMethod aPaymentMethod() {
        return PaymentMethod.builder()
                .setPaymentMethodType(PaymentMethodType.CREDITCARD)
                .build();
    }
}
