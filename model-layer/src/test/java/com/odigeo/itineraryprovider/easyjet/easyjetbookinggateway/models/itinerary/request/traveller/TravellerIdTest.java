package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.TravellerIdObjectMother;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.UUID;

public class TravellerIdTest {
    private TravellerId travellerId;

    @Test
    public void testBuilder() {
        travellerId = TravellerIdObjectMother.ANY;
        Assert.assertNotNull(travellerId);
    }

    @Test
    public void testGetId() {
        Assert.assertNotNull(travellerId.getId());
        Assert.assertEquals(travellerId.getId(),  UUID.fromString("15e45e7d-3e34-43df-9366-91c66a8cc9ae"));
    }
}
