package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ProviderPaymentMethodTypeTest {
    private final ProviderPaymentMethodType providerPaymentMethodType = ProviderPaymentMethodType.CREDITCARD;

    @Test
    public void testProviderPaymentMethodType() {
        String code = ProviderPaymentMethodType.CREDITCARD.getCode();
        Assert.assertNotNull(providerPaymentMethodType.getCode());
        Assert.assertNotNull(code);
        Assert.assertEquals(code, providerPaymentMethodType.getCode());
    }

    @Test
    public void testValueOf() {
        String code = String.valueOf(ProviderPaymentMethodType.CREDITCARD);
        Assert.assertNotNull(providerPaymentMethodType.getCode());
        Assert.assertNotNull(code);
        Assert.assertEquals(code, providerPaymentMethodType.toString());
    }

    @Test
    public void testGetCode() {
        String code = String.valueOf(ProviderPaymentMethodType.CREDITCARD.getCode());
        Assert.assertNotNull(providerPaymentMethodType.getCode());
        Assert.assertNotNull(code);
        Assert.assertEquals(code, providerPaymentMethodType.getCode());
    }

    @Test
    public void testValues() {
        ProviderPaymentMethodType[] providerPaymentMethodTypes = ProviderPaymentMethodType.values();
        Assert.assertNotNull(providerPaymentMethodTypes);
        Assert.assertEquals(providerPaymentMethodTypes[0], ProviderPaymentMethodType.CASH);
        Assert.assertEquals(providerPaymentMethodTypes[1], ProviderPaymentMethodType.CREDITCARD);
        Assert.assertEquals(providerPaymentMethodTypes[2], ProviderPaymentMethodType.SECURE3D);
        Assert.assertEquals(providerPaymentMethodTypes[3], ProviderPaymentMethodType.INSTALLMENT);
        Assert.assertEquals(providerPaymentMethodTypes[4], ProviderPaymentMethodType.DELAYED_PAYMENT);
        Assert.assertEquals(providerPaymentMethodTypes[5], ProviderPaymentMethodType.NO_PAYMENT_NEEDED);
        Assert.assertEquals(providerPaymentMethodTypes[6], ProviderPaymentMethodType.TRAVEL_ACCOUNT);
    }

}
