package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryIdObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryTicketObjectMother;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ItineraryTicketTest {
    private static  ItineraryTicket itineraryTicket;

    @Test
    public void testBuilder() {
        itineraryTicket = ItineraryTicketObjectMother.ANY;
        Assert.assertNotNull(itineraryTicket);
    }

    @Test
    public void testGetId() {
        Assert.assertNull(itineraryTicket.getId());
    }

    @Test
    public void testGetNumber() {
        Assert.assertNull(itineraryTicket.getNumber());
    }

    @Test
    public void testGetTravellerKey() {
        Assert.assertNull(itineraryTicket.getTravellerKey());
    }

    @Test
    public void testGetTotalAmount() {
        Assert.assertNull(itineraryTicket.getTotalAmount());
    }

    @Test
    public void testGetTaxes() {
        Assert.assertNull(itineraryTicket.getTaxes());
    }

    @Test
    public void testGetCommission() {
        Assert.assertNull(itineraryTicket.getCommission());
    }

    @Test
    public void testGetStatus() {
        Assert.assertNull(itineraryTicket.getStatus());
    }

    @Test
    public void testGetItineraryBookingId() {
        Assert.assertNotNull(itineraryTicket.getItineraryId());
    }

    @Test
    public void testGetNumPassenger() {
        Assert.assertNull(itineraryTicket.getNumPassenger());
    }

    @Test
    public void testGetValidatingCarrier() {
        Assert.assertNull(itineraryTicket.getValidatingCarrier());
    }

    @Test
    public void testGetIataCode() {
        Assert.assertNull(itineraryTicket.getIataCode());
    }

    @Test
    public void testGetOfficeId() {
        Assert.assertNull(itineraryTicket.getOfficeId());
    }

    @Test
    public void testGetTicketActions() {
        Assert.assertNull(itineraryTicket.getTicketActions());
    }

    @Test
    public void testGetTaxDetails() {
        Assert.assertNull(itineraryTicket.getTaxDetails());
    }

    @Test
    public void testGetItineraryId() {
        Assert.assertNotNull(itineraryTicket.getItineraryId());
        Assert.assertEquals(itineraryTicket.getItineraryId(), ItineraryIdObjectMother.ANY_INTERNAL);
    }
}
