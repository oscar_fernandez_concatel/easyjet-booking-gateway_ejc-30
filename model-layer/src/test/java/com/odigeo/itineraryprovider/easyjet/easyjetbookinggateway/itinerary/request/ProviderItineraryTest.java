package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItinerary;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.UUID;

public class ProviderItineraryTest {
    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";

    @Test
    public void testGetId() {
        UUID uuid = UUID.fromString(IDENTIFIER);
        ItineraryId id = new ItineraryId.Builder()
                .setId(uuid)
                .build();
        ProviderItinerary providerItinerary = new ProviderItinerary.Builder().setItineraryId(id).build();

        Assert.assertTrue(providerItinerary.getItineraryId().getId().toString().equals(IDENTIFIER));
    }
}
