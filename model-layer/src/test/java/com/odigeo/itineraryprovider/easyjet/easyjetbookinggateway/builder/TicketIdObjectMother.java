package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TicketId;

import java.util.UUID;

public class TicketIdObjectMother {
    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";
    public static final TicketId ANY = aTicketId();

    private static TicketId aTicketId() {
        return TicketId.builder()
                .setId(UUID.fromString(IDENTIFIER))
                .build();
    }
}
