package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Money;

import java.math.BigDecimal;
import java.util.Currency;

public class EasyJetMoneyObjectMother {
    public static final Money ANY = aMoney();

    public static Money aMoney() {
        return new Money.Builder()
                .setAmount(BigDecimal.ONE)
                .setCurrency(Currency.getInstance("EUR"))
                .build();
    }

}
