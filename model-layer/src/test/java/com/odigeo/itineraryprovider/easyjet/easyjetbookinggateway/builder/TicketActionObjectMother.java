package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PaymentDetails;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TicketAction;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TicketActionId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TicketId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ActionType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Money;

import java.time.LocalDateTime;

public class TicketActionObjectMother {
    public static final TicketAction ANY = aTicketAction();

    private static TicketAction aTicketAction() {
        TicketActionId id = TicketActionIdObjectMother.ANY;
        TicketId ticketId = TicketIdObjectMother.ANY;
        PaymentDetails paymentDetails = PaymentDetailsObjectMother.ANY;
        Money price = MoneyObjectMother.ANY;
        Money commission = MoneyObjectMother.ANY;
        return TicketAction.builder()
                .setId(id)
                .setTicketId(ticketId)
                .setAction(ActionType.ISSUED)
                .setPaymentDetails(paymentDetails)
                .setPrice(price)
                .setCommission(commission)
                .setExecutionDate(LocalDateTime.of(2022, 12, 15, 10, 0, 0))
                .build();
    }
}
