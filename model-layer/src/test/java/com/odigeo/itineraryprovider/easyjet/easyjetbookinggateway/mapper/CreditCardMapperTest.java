package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.CreditCardObjectModel;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCard;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardVendorCode;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request.CreditCardMapper;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CreditCardMapperTest {

    public static final String STRING_VALUE = "String";

    @Test
    public void testToApi() {
        final com.odigeo.suppliergatewayapi.v25.itinerary.booking.payment.CreditCard eCreditCard = CreditCardObjectModel.ANY_EXTERNAL;
        final CreditCard creditCard = CreditCardMapper.INSTANCE.toModel(eCreditCard);

        Assert.assertEquals(creditCard.getCardType(), STRING_VALUE);
        Assert.assertNotNull(creditCard.getExpirationDate());
        Assert.assertEquals(creditCard.getNumber(), STRING_VALUE);
        Assert.assertEquals(creditCard.getVendorCode(), CreditCardVendorCode.MASTERCARD);
        Assert.assertEquals(creditCard.getVerificationValue(), STRING_VALUE);
        Assert.assertEquals(creditCard.getHolderName(), STRING_VALUE);
    }


}
