package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.SupplierLocatorObjectMother;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.UUID;

public class SupplierLocatorTest {
    private SupplierLocator supplierLocator;

    @Test
    public void testBuilder() {
        supplierLocator = SupplierLocatorObjectMother.ANY;
        Assert.assertNotNull(supplierLocator);
    }

    @Test
    public void testGetSupplierCode() {
        Assert.assertNotNull(supplierLocator.getSupplierCode());
        Assert.assertEquals(supplierLocator.getSupplierCode(), "supplierCode");
    }

    @Test
    public void testGetSupplierLocatorCode() {
        Assert.assertNotNull(supplierLocator.getSupplierLocatorCode());
        Assert.assertEquals(supplierLocator.getSupplierLocatorCode(), "supplierLocatorCode");
    }

    @Test
    public void testGetItineraryBookingId() {
        Assert.assertNotNull(supplierLocator.getItineraryBookingId());
        Assert.assertEquals(supplierLocator.getItineraryBookingId(), UUID.fromString("15e45e7d-3e34-43df-9366-91c66a8cc9ae"));
    }
}
