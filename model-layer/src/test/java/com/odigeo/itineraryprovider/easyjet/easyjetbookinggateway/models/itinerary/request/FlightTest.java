package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.CarrierObjectModel;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.FlightObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryIdObjectMother;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FlightTest {
    private static Flight flight;

    @Test
    public void testBuilder() {
        flight = FlightObjectMother.ANY_INTERNAL;
        Assert.assertNotNull(flight);
    }

    @Test
    public void testGetFlightNumber() {
        Assert.assertNotNull(flight.getFlightNumber());
        Assert.assertEquals(flight.getFlightNumber(), "AR1401");
    }

    @Test
    public void testGetStatus() {
        Assert.assertNotNull(flight.getStatus());
        Assert.assertEquals(flight.getStatus(), "CONFIRMED");
    }

    @Test
    public void testGetOperatingCarrier() {
        Assert.assertNull(flight.getOperatingCarrier());
    }

    @Test
    public void testGetMarketingCarrier() {
        Assert.assertNotNull(flight.getMarketingCarrier());
        Assert.assertEquals(flight.getMarketingCarrier().getCode(), CarrierObjectModel.ANY_EXTERNAL.getCode());
        Assert.assertEquals(flight.getMarketingCarrier().getIataCode(), CarrierObjectModel.ANY_EXTERNAL.getIataCode());
        Assert.assertEquals(flight.getMarketingCarrier().getName(), CarrierObjectModel.ANY_EXTERNAL.getName());
    }

    @Test
    public void testGetItineraryId() {
        Assert.assertEquals(flight.getItineraryId().getId().toString(), ItineraryIdObjectMother.ANY_INTERNAL.getId().toString());
    }

    @Test
    public void testGetPositionId() {
        Assert.assertEquals(flight.getPositionId(), 1);
    }
}
