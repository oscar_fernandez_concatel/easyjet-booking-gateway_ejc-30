package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.BaggageAllowanceObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.BaggageAllowance;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.BaggageAllowanceType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request.BaggageAllowanceMapper;
import org.testng.Assert;
import org.testng.annotations.Test;

public class BaggageAllowanceMapperTest {
    @Test
    public void testToApi() {
        final com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.BaggageAllowance eBaggageAllowance = BaggageAllowanceObjectMother.ANY_EXTERNAL;
        final BaggageAllowance baggageAllowance = BaggageAllowanceMapper.INSTANCE.toModel(eBaggageAllowance);
        Assert.assertEquals(baggageAllowance.getQuantity().intValue(), 15);
        Assert.assertEquals(baggageAllowance.getBaggageAllowanceType(), BaggageAllowanceType.KG);
    }
}
