package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;


import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Money;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PassengerTypeFare;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TravellerType;

public class EasyJetPassengerTypeFareObjectMother {

    public static final PassengerTypeFare ANY = aPassengerTypeFare();

    public static PassengerTypeFare aPassengerTypeFare() {

        Money fareAmount = EasyJetMoneyObjectMother.ANY;
        Money taxAmount = EasyJetMoneyObjectMother.ANY;

        return new PassengerTypeFare.Builder()
                .setDiscount(false)
                .setPassengersPerType(1)
                .setFareAmount(fareAmount)
                .setTaxAmount(taxAmount)
                .setTravellerType(TravellerType.ADULT)
                .build();
    }
}
