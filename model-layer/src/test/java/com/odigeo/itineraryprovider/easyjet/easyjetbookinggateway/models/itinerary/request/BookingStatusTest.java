package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import org.testng.Assert;
import org.testng.annotations.Test;

public class BookingStatusTest {

    @Test
    public void testValuesCancelled() {
        BookingStatus bookingStatus = BookingStatus.CANCELLED;
        Assert.assertEquals(bookingStatus, BookingStatus.CANCELLED);
    }

    @Test
    public void testValuesPending() {
        BookingStatus bookingStatus = BookingStatus.PENDING;
        Assert.assertEquals(bookingStatus, BookingStatus.PENDING);
    }

}
