package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import org.testng.Assert;
import org.testng.annotations.Test;

public class PaymentMethodTypeTest {

    @Test
    public void testValuesCash() {
        PaymentMethodType paymentMethodType = PaymentMethodType.CASH;
        Assert.assertEquals(paymentMethodType, PaymentMethodType.CASH);
    }

    @Test
    public void testValueOf() {
        PaymentMethodType paymentMethodType = PaymentMethodType.CREDITCARD;
        Assert.assertEquals(paymentMethodType, PaymentMethodType.CREDITCARD);
    }
}
