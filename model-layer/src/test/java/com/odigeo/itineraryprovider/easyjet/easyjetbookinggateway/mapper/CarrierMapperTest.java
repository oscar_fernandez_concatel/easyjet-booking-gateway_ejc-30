package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.CarrierObjectModel;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Carrier;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request.CarrierMapper;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CarrierMapperTest {
    @Test
    public void testToApi() {
        final com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.Carrier eCarrier = CarrierObjectModel.ANY_EXTERNAL;
        final Carrier carrier = CarrierMapper.INSTANCE.toModel(eCarrier);
        Assert.assertEquals(carrier.getIataCode(), "IATACODE");
        Assert.assertEquals(carrier.getCode(), "CODE");
        Assert.assertEquals(carrier.getName(), "NAME");
        Assert.assertTrue(carrier.isShowBaggagePolicy());
    }
}
