package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Itinerary;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItinerary;

public class ProviderItineraryObjectMother {
    public static final ProviderItinerary ANY = aProviderItinerary();

    public static ProviderItinerary aProviderItinerary() {
        ItineraryId id = EasyJetItineraryIdObjectMother.ANY;
        Itinerary itinerary = EasyJetItineraryObjectMother.ANY;

        return ProviderItinerary.builder()
                .setItinerary(itinerary)
                .setItineraryId(id)
                .build();
    }

}
