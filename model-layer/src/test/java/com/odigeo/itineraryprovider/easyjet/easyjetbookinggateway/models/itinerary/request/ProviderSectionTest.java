package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderSectionObjectMother;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ProviderSectionTest {
    private static ProviderSection providerSection;

    @Test
    public void testBuilder() {
        providerSection = ProviderSectionObjectMother.ANY;
        Assert.assertNotNull(providerSection);
    }

    @Test
    public void testGetKey() {
        Assert.assertNotNull(providerSection.getKey());
        Assert.assertEquals(providerSection.getKey(), "KEY");
    }

    @Test
    public void testGetFareKey() {
        Assert.assertNotNull(providerSection.getFareKey());
        Assert.assertEquals(providerSection.getFareKey(), "FAREKEY");
    }

    @Test
    public void testGetBookingCode() {
        Assert.assertNotNull(providerSection.getBookingCode());
        Assert.assertEquals(providerSection.getBookingCode(), "BOOKINGCODE");
    }

    @Test
    public void testGetClassOfService() {
        Assert.assertNotNull(providerSection.getClassOfService());
        Assert.assertEquals(providerSection.getClassOfService(), "CLASSOFSERVICE");
    }
}
