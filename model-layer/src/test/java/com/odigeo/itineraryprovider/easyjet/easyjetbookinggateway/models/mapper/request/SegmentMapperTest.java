package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.FlightSectionObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryIdObjectMother;
import com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.Section;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class SegmentMapperTest {

    @Test
    public void testToModel() {
        com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.FlightSection section = FlightSectionObjectMother.ANY_EXTERNAL;
        List<Section> sections = new ArrayList<>();
        sections.add(section);
        com.odigeo.suppliergatewayapi.v25.itinerary.ItineraryId itineraryId = ItineraryIdObjectMother.ANY_EXTERNAL;
        com.odigeo.suppliergatewayapi.v25.itinerary.segment.Segment segmentEntity = new com.odigeo.suppliergatewayapi.v25.itinerary.segment.Segment();
        segmentEntity.setItineraryId(itineraryId);
        segmentEntity.setPosition(1);
        segmentEntity.setSections(sections);

        com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Segment segment = SegmentMapper.INSTANCE.toModel(segmentEntity);

        Assert.assertNotNull(segment);
        Assert.assertEquals(segment.getSections().get(0).getId().getId(), segmentEntity.getSections().get(0).getId().getId());
        Assert.assertEquals(segment.getPosition(), segmentEntity.getPosition());
        Assert.assertEquals(segment.getItineraryId().getId(), segmentEntity.getItineraryId().getId());
    }
}
