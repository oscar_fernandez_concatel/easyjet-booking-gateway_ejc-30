package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.EasyJetFareDetailsObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.EasyJetPassengerTypeFareObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.FareDetailsObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FareDetails;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PassengerTypeFare;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TravellerType;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FareDetailsMapperTest {
    @Test
    public void testToApi() {

        FareDetails fareDetails = EasyJetFareDetailsObjectMother.ANY;
        FareDetails cFareDetails = FareDetailsObjectMother.ANY;
        PassengerTypeFare cPassengerTypeFare = EasyJetPassengerTypeFareObjectMother.ANY;

        Assert.assertNotNull(fareDetails);
        Assert.assertNotNull(fareDetails.getCorporateCodes());
        Assert.assertEquals(fareDetails.getCorporateCodes().size(), 1);
        Assert.assertEquals(fareDetails.getCorporateCodes().get(0), "CODE");
        Assert.assertEquals(fareDetails.getTotalFareAmount().getAmount(), cFareDetails.getTotalFareAmount().getAmount());
        Assert.assertEquals(fareDetails.getTotalTaxAmount().getAmount(), cFareDetails.getTotalTaxAmount().getAmount());
        Assert.assertNotNull(fareDetails.getItineraryId());

        Assert.assertNotNull(fareDetails.getPassengerFares());
        Assert.assertEquals(fareDetails.getPassengerFares().size(), 1);
        Assert.assertEquals(fareDetails.getPassengerFares().get(TravellerType.ADULT).getPassengersPerType().intValue(), 1);
        Assert.assertEquals(fareDetails.getPassengerFares().get(TravellerType.ADULT).getTravellerType(), TravellerType.ADULT);
        Assert.assertEquals(fareDetails.getPassengerFares().get(TravellerType.ADULT).getFareAmount().getAmount(), cPassengerTypeFare.getFareAmount().getAmount());
        Assert.assertEquals(fareDetails.getPassengerFares().get(TravellerType.ADULT).getTaxAmount().getAmount(), cPassengerTypeFare.getTaxAmount().getAmount());
        Assert.assertFalse(fareDetails.getPassengerFares().get(TravellerType.ADULT).isDiscount());
    }
}
