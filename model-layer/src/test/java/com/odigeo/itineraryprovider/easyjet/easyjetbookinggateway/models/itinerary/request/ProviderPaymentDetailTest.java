package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderPaymentDetailObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderPaymentMethodObjectMother;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ProviderPaymentDetailTest {
    private static ProviderPaymentDetail providerPaymentDetail;

    @Test
    public void testBuilder() {
        providerPaymentDetail = ProviderPaymentDetailObjectMother.ANY;
        Assert.assertNotNull(providerPaymentDetail);
    }

    @Test
    public void testGetProviderPaymentMethod() {
        Assert.assertNotNull(providerPaymentDetail.getProviderPaymentMethod());
        Assert.assertEquals(providerPaymentDetail.getProviderPaymentMethod(), ProviderPaymentMethodObjectMother.ANY);
    }
}
