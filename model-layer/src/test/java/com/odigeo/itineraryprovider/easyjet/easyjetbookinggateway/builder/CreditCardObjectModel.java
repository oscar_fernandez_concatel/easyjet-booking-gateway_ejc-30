package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;


import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardVendorCode;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.payment.CreditCard;
import com.odigeo.suppliergatewayapi.v25.itinerary.fare.Money;

import java.time.LocalDate;

public class CreditCardObjectModel {
    public static final String STRING_VALUE = "String";
    public static final CreditCard ANY_EXTERNAL = aCreditCardExternal();
    public static CreditCard aCreditCardExternal() {

        CreditCard creditCard = new CreditCard();
        creditCard.setCardType(STRING_VALUE);
        creditCard.setExpirationDate(LocalDate.now());
        creditCard.setNumber(STRING_VALUE);
        creditCard.setHolderName(STRING_VALUE);
        creditCard.setVendorCode(CreditCardVendorCode.MASTERCARD.getType());
        creditCard.setVerificationValue(STRING_VALUE);
        creditCard.setFee(new Money());

        return creditCard;
    }

    public static com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCard aCreditCardInternal() {
        return com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCard.builder()
                .setCardType(STRING_VALUE)
                .setExpirationDate(LocalDate.now())
                .setNumber(STRING_VALUE)
                .setHolderName(STRING_VALUE)
                .setVerificationValue(STRING_VALUE)
                .build();
    }

}
