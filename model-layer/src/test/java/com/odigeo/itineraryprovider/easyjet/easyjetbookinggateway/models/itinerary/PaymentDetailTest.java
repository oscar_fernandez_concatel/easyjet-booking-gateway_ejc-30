package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryBookingIdObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.PaymentDetailObjectMother;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.UUID;

public class PaymentDetailTest {
    private static PaymentDetail paymentDetail;
    @Test
    public void testBuilder() {
        paymentDetail = PaymentDetailObjectMother.ANY;
        Assert.assertNotNull(paymentDetail);
    }

    @Test
    public void testGetId() {
        Assert.assertNotNull(paymentDetail.getId());
        Assert.assertNotEquals(paymentDetail.getId(), UUID.randomUUID());
    }

    @Test
    public void testGetPaymentInstrumentToken() {
        Assert.assertNotNull(paymentDetail.getPaymentInstrumentToken());
        Assert.assertEquals(paymentDetail.getPaymentInstrumentToken(), "PAYMENTTOKEN");
    }

    @Test
    public void testGetItineraryBookingId() {
        Assert.assertNotNull(paymentDetail.getItineraryBookingId());
        Assert.assertEquals(paymentDetail.getItineraryBookingId(), ItineraryBookingIdObjectMother.ANY.getId());
    }
}
