package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;


import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FareDetails;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Itinerary;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TripType;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class EasyJetItineraryObjectMother {
    public static final Itinerary ANY = aItinerary();

    public static Itinerary aItinerary() {
        FareDetails fareDetails = EasyJetFareDetailsObjectMother.ANY;
        ItineraryId id = EasyJetItineraryIdObjectMother.ANY;

        return Itinerary.builder()
                .setFareDetails(fareDetails)
                .setId(id)
                .setDepartureDate(LocalDateTime.of(2021, 12, 15, 10, 0, 0))
                .setArrivalGeoNodeId(5678)
                .setReturnDate(LocalDateTime.of(2021, 12, 15, 10, 0, 0))
                .setTripType(TripType.ONE_WAY)
                .setDepartureGeoNodeId(1234)
                .setSegments(new ArrayList<>())
                .build();
    }
}
