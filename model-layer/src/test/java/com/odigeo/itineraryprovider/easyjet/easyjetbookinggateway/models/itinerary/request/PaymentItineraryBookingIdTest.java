package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.PaymentItineraryBookingIdObjectMother;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.UUID;

public class PaymentItineraryBookingIdTest {
    private PaymentItineraryBookingId paymentItineraryBookingId;

    @Test
    public void testPaymentItineraryBookingId() {
        paymentItineraryBookingId = PaymentItineraryBookingIdObjectMother.ANY;
        Assert.assertNotNull(paymentItineraryBookingId);
    }

    @Test
    public void testValueOf() {
        String id = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";
        PaymentItineraryBookingId paymentItineraryBookingId = PaymentItineraryBookingId.valueOf(id);
        Assert.assertNotNull(paymentItineraryBookingId);
        Assert.assertEquals(paymentItineraryBookingId.getId().toString(), id);
    }

    @Test
    public void testGetId() {
        PaymentItineraryBookingId paymentItineraryBookingId = PaymentItineraryBookingIdObjectMother.ANY;
        UUID uuid = UUID.fromString("15e45e7d-3e34-43df-9366-91c66a8cc9ae");
        Assert.assertNotNull(paymentItineraryBookingId.getId());
        Assert.assertEquals(paymentItineraryBookingId.getId(), uuid);
    }

    @Test
    public void testSetId() {
        paymentItineraryBookingId.setId(UUID.fromString("15e45e7d-3e34-43df-9366-91c66a8cc9ae"));
        Assert.assertNotNull(paymentItineraryBookingId.getId());
    }

    @Test
    public void testTestToString() {
        Assert.assertNotNull(paymentItineraryBookingId.getId());
        Assert.assertEquals(paymentItineraryBookingId.getId().toString(), "15e45e7d-3e34-43df-9366-91c66a8cc9ae");
    }

    @Test
    public void testTestValueOf() {
        Assert.assertEquals(String.valueOf(paymentItineraryBookingId.getId()), "15e45e7d-3e34-43df-9366-91c66a8cc9ae");
    }
}
