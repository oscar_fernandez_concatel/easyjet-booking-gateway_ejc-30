package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.PaymentMethodObjectMother;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PaymentMethodTest {
    private PaymentMethod paymentMethod;

    @Test
    public void testBuilder() {
        paymentMethod = PaymentMethodObjectMother.ANY;
        Assert.assertNotNull(paymentMethod);
    }

    @Test
    public void testGetPaymentMethodType() {
        Assert.assertNotNull(paymentMethod);
        Assert.assertEquals(paymentMethod.getPaymentMethodType().toString(), PaymentMethodType.CREDITCARD.toString());
        Assert.assertNotEquals(paymentMethod.getPaymentMethodType().toString(), PaymentMethodType.CASH.toString());
    }
}
