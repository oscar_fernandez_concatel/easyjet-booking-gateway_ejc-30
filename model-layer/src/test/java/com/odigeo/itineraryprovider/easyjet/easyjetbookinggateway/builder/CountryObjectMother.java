package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Continent;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Country;

public class CountryObjectMother {
    public static final Country ANY = aCountry();
    public static final com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Country ANY_EXTERNAL = aCountryExternal();

    private static Country aCountry() {
        Continent continent = ContinentObjectMother.ANY;
        return Country.builder()
                .setCountryCode("countryCode")
                .setCountryCode3Letters("countryCode3Letters")
                .setNumCountryCode(1)
                .setContinent(continent)
                .setPhonePrefix("phonePrefix")
                .build();
    }

    private static com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Country aCountryExternal() {
        com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Continent continent = new com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Continent();
        continent.setContinentId(1);
        com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Country country = new com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Country();
        country.setCountryCode("countryCode");
        country.setCountryCode3Letters("countryCode3Letters");
        country.setNumCountryCode(1);
        country.setContinent(continent);
        country.setPhonePrefix("phonePrefix");
        return country;
    }

}
