package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.enumeration;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FareClass;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FareClassTest {

    @Test
    public void testGetValueY() {
        FareClass fareClass = FareClass.getValue("Y");
        Assert.assertEquals(fareClass, FareClass.Y);
    }

    @Test
    public void testGetValueM() {
        FareClass fareClass = FareClass.getValue("M");
        Assert.assertEquals(fareClass, FareClass.M);
    }

    @Test
    public void testGetValueW() {
        FareClass fareClass = FareClass.getValue("W");
        Assert.assertEquals(fareClass, FareClass.W);
    }

    @Test
    public void testGetValueB() {
        FareClass fareClass = FareClass.getValue("B");
        Assert.assertEquals(fareClass, FareClass.B);
    }

    @Test
    public void testGetValueDefault() {
        FareClass fareClass = FareClass.getValue("XX");
        Assert.assertEquals(fareClass, FareClass.Y);
    }

}
