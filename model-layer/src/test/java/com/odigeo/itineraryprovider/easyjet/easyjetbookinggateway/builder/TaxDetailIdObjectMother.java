package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TaxDetailId;

import java.util.UUID;

public class TaxDetailIdObjectMother {
    private static final String IDENTIFIER = "15e45e7d-3e34-43df-9366-91c66a8cc9ae";
    public static final TaxDetailId ANY = aTaxDetailId();

    private static TaxDetailId aTaxDetailId() {
        return TaxDetailId.builder()
                .setId(UUID.fromString(IDENTIFIER))
                .build();
    }
}
