package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Country;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Phone;

public class PhoneObjectMother {
    public static final Phone ANY = aPhone();
    public static final com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Phone ANY_EXTERNAL = aPhoneExternal();

    private static Phone aPhone() {
        Country country = CountryObjectMother.ANY;
        return Phone.builder()
                .setPhoneType("phoneType")
                .setNumber("123456789")
                .setCountry(country)
                .build();
    }

    private static com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Phone aPhoneExternal() {
        com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Country eCountry = new com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Country();
        eCountry.setCountryCode("ES");

        return new com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Phone(eCountry, "654654654", "CELL");
    }
}
