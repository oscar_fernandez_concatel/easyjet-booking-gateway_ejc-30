package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Traveller;

import java.util.List;

public class ItineraryBooking {

    private final ItineraryBookingId id;
    private final Itinerary itinerary;
    private final Buyer buyer;
    private final List<Traveller> travellers;
    private final List<SupplierLocator> supplierLocators;
    private final String pnr;
    private final Boolean bookingWithoutPaymentAllowed;

    public ItineraryBooking(final ItineraryBooking.Builder builder) {
        this.id = builder.id;
        this.itinerary = builder.itinerary;
        this.buyer = builder.buyer;
        this.travellers = builder.travellers;
        this.supplierLocators = builder.supplierLocators;
        this.pnr = builder.pnr;
        this.bookingWithoutPaymentAllowed = builder.bookingWithoutPaymentAllowed;
    }

    public static ItineraryBooking.Builder builder() {
        return new ItineraryBooking.Builder();
    }

    public ItineraryBookingId getId() {
        return id;
    }

    public Itinerary getItinerary() {
        return itinerary;
    }

    public Buyer getBuyer() {
        return buyer;
    }

    public List<Traveller> getTravellers() {
        return travellers;
    }

    public List<SupplierLocator> getSupplierLocators() {
        return supplierLocators;
    }

    public String getPnr() {
        return pnr;
    }

    public Boolean getBookingWithoutPaymentAllowed() {
        return bookingWithoutPaymentAllowed;
    }

    public static final class Builder {
        private ItineraryBookingId id;
        private Itinerary itinerary;
        private Buyer buyer;
        private List<Traveller> travellers;
        private List<SupplierLocator> supplierLocators;
        private String pnr;
        private Boolean bookingWithoutPaymentAllowed;

        public Builder setId(ItineraryBookingId id) {
            this.id = id;
            return this;
        }

        public Builder setItinerary(Itinerary itinerary) {
            this.itinerary = itinerary;
            return this;
        }

        public Builder setBuyer(Buyer buyer) {
            this.buyer = buyer;
            return this;
        }

        public Builder setTravellers(List<Traveller> travellers) {
            this.travellers = travellers;
            return this;
        }

        public Builder setSupplierLocators(List<SupplierLocator> supplierLocators) {
            this.supplierLocators = supplierLocators;
            return this;
        }

        public Builder setPnr(String pnr) {
            this.pnr = pnr;
            return this;
        }

        public Builder setBookingWithoutPaymentAllowed(Boolean bookingWithoutPaymentAllowed) {
            this.bookingWithoutPaymentAllowed = bookingWithoutPaymentAllowed;
            return this;
        }

        public ItineraryBooking build() {
            return new ItineraryBooking(this);
        }
    }
}
