package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.BaggageAllowance;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface BaggageAllowanceMapper {
    BaggageAllowanceMapper INSTANCE = Mappers.getMapper(BaggageAllowanceMapper.class);

    BaggageAllowance toModel(com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.BaggageAllowance baggageAllowance);

    @InheritInverseConfiguration
    com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.BaggageAllowance toApi(BaggageAllowance baggageAllowance);
}
