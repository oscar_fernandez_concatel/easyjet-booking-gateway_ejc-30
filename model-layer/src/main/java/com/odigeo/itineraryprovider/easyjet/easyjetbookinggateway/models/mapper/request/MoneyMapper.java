package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Money;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface MoneyMapper {
    MoneyMapper INSTANCE = Mappers.getMapper(MoneyMapper.class);

    Money toModel(final com.odigeo.suppliergatewayapi.v25.itinerary.fare.Money money);

    com.odigeo.suppliergatewayapi.v25.itinerary.fare.Money toApi(Money money);
}
