package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import java.time.LocalDate;

public class CreditCard implements ProviderPaymentMethod {

    private final CreditCardVendorCode vendorCode;
    private final String number;
    private final LocalDate expirationDate;
    private final String verificationValue;
    private final String holderName;
    private final String cardType;

    public CreditCard(final Builder builder) {
        this.vendorCode = builder.vendorCode;
        this.number = builder.number;
        this.expirationDate = builder.expirationDate;
        this.verificationValue = builder.verificationValue;
        this.holderName = builder.holderName;
        this.cardType = builder.cardType;
    }

    public static Builder builder() {
        return new Builder();
    }

    public CreditCardVendorCode getVendorCode() {
        return vendorCode;
    }

    public String getNumber() {
        return number;
    }

    public LocalDate getExpirationDate() {
        return expirationDate;
    }

    public String getVerificationValue() {
        return verificationValue;
    }

    public String getHolderName() {
        return holderName;
    }

    public String getCardType() {
        return cardType;
    }

    @Override
    public ProviderPaymentMethodType getProviderPaymentMethodType() {
        return ProviderPaymentMethodType.CREDITCARD;
    }

    public static final class Builder {
        private CreditCardVendorCode vendorCode;
        private String number;
        private LocalDate expirationDate;
        private String verificationValue;
        private String holderName;
        private String cardType;

        public Builder setVendorCode(CreditCardVendorCode vendorCode) {
            this.vendorCode = vendorCode;
            return this;
        }

        public Builder setNumber(String number) {
            this.number = number;
            return this;
        }

        public Builder setExpirationDate(LocalDate expirationDate) {
            this.expirationDate = expirationDate;
            return this;
        }

        public Builder setVerificationValue(String verificationValue) {
            this.verificationValue = verificationValue;
            return this;
        }

        public Builder setHolderName(String holderName) {
            this.holderName = holderName;
            return this;
        }

        public Builder setCardType(String cardType) {
            this.cardType = cardType;
            return this;
        }

        public CreditCard build() {
            return new CreditCard(this);
        }
    }
}
