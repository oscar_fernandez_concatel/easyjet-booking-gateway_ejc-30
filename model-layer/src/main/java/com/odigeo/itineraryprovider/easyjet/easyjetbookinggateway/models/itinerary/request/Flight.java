package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = Flight.Builder.class)
public class Flight {

    private final ItineraryId itineraryId;
    private final int positionId;
    private final String flightNumber;
    private final String status;
    private final Carrier operatingCarrier;
    private final Carrier marketingCarrier;

    public Flight(final Flight.Builder builder) {
        this.flightNumber = builder.flightNumber;
        this.status = builder.status;
        this.operatingCarrier = builder.operatingCarrier;
        this.marketingCarrier = builder.marketingCarrier;
        this.itineraryId = builder.itineraryId;
        this.positionId = builder.positionId;
    }

    public static Flight.Builder builder() {
        return new Flight.Builder();
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public String getStatus() {
        return status;
    }

    public Carrier getOperatingCarrier() {
        return operatingCarrier;
    }

    public Carrier getMarketingCarrier() {
        return marketingCarrier;
    }

    public ItineraryId getItineraryId() {
        return itineraryId;
    }

    public int getPositionId() {
        return positionId;
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static final class Builder {
        private String flightNumber;
        private String status;
        private Carrier operatingCarrier;
        private Carrier marketingCarrier;
        private ItineraryId itineraryId;
        private int positionId;

        public Builder setFlightNumber(String flightNumber) {
            this.flightNumber = flightNumber;
            return this;
        }

        public Builder setStatus(String status) {
            this.status = status;
            return this;
        }

        public Builder setOperatingCarrier(Carrier operatingCarrier) {
            this.operatingCarrier = operatingCarrier;
            return this;
        }

        public Builder setMarketingCarrier(Carrier marketingCarrier) {
            this.marketingCarrier = marketingCarrier;
            return this;
        }

        public Builder setItineraryId(ItineraryId itineraryId) {
            this.itineraryId = itineraryId;
            return this;
        }

        public Builder setPositionId(int positionId) {
            this.positionId = positionId;
            return this;
        }

        public Flight build() {
            return new Flight(this);
        }
    }
}
