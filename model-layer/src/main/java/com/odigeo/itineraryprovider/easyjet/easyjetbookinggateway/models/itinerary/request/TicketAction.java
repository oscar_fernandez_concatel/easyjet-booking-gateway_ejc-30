package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import java.time.LocalDateTime;

public class TicketAction {
    private final TicketActionId id;
    private final TicketId ticketId;
    private final ActionType action;
    private final PaymentDetails paymentDetails;
    private final Money price;
    private final Money commission;
    private final LocalDateTime executionDate;

    public TicketAction(final Builder builder) {
        this.id = builder.id;
        this.ticketId = builder.ticketId;
        this.action = builder.action;
        this.paymentDetails = builder.paymentDetails;
        this.price = builder.price;
        this.commission = builder.commission;
        this.executionDate = builder.executionDate;
    }

    public static Builder builder() {
        return new Builder();
    }

    public TicketActionId getId() {
        return id;
    }

    public TicketId getTicketId() {
        return ticketId;
    }

    public ActionType getAction() {
        return action;
    }

    public PaymentDetails getPaymentDetails() {
        return paymentDetails;
    }

    public Money getPrice() {
        return price;
    }

    public Money getCommission() {
        return commission;
    }

    public LocalDateTime getExecutionDate() {
        return executionDate;
    }


    public static final class Builder {
        private TicketActionId id;
        private TicketId ticketId;
        private ActionType action;
        private PaymentDetails paymentDetails;
        private Money price;
        private Money commission;
        private LocalDateTime executionDate;

        public Builder setId(TicketActionId id) {
            this.id = id;
            return this;
        }

        public Builder setTicketId(TicketId ticketId) {
            this.ticketId = ticketId;
            return this;
        }

        public Builder setAction(ActionType action) {
            this.action = action;
            return this;
        }

        public Builder setPaymentDetails(PaymentDetails paymentDetails) {
            this.paymentDetails = paymentDetails;
            return this;
        }

        public Builder setPrice(Money price) {
            this.price = price;
            return this;
        }

        public Builder setCommission(Money commission) {
            this.commission = commission;
            return this;
        }

        public Builder setExecutionDate(LocalDateTime executionDate) {
            this.executionDate = executionDate;
            return this;
        }

        public TicketAction build() {
            return new TicketAction(this);
        }
    }
}
