package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Country;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CountryMapper {
    CountryMapper INSTANCE = Mappers.getMapper(CountryMapper.class);

    com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Country map(Country country);

    Country map(com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Country country);

}
