package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.exceptions;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayException;
import com.odigeo.suppliergatewayapi.v25.itinerary.exceptions.ErrorType;
import com.odigeo.suppliergatewayapi.v25.itinerary.exceptions.SupplierGatewayException;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ProviderExceptionMapper {

    ProviderExceptionMapper INSTANCE = Mappers.getMapper(ProviderExceptionMapper.class);

    default SupplierGatewayException fromModelToApi(EasyJetBookingGatewayException easyJetBookingGatewayException) {
        return new SupplierGatewayException(easyJetBookingGatewayException.getMessage(), easyJetBookingGatewayException,
                easyJetBookingGatewayException.getProviderId(), ErrorType.getErrorType(easyJetBookingGatewayException.getErrorType().getErrorCode()), null);

    }

}
