package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

public enum CreditCardVendorCode {
    
    VISA_CREDIT("VI"),
    VISA_DEBIT("VD"),
    VISA_ELECTRON("VE"),
    MASTERCARD("CA"),
    MASTERCARD_PREPAID("MP"),
    MAESTRO("MA"),
    MASTERCARD_DEBIT("MD"),
    DINERS_CLUB("DC"),
    AMERICAN_EXPRESS("AX"),
    AMEX("AX");

    private final String type;

    CreditCardVendorCode(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public static CreditCardVendorCode getValue(String type) {
        for (CreditCardVendorCode creditCardVendorCode : CreditCardVendorCode.values()) {
            if (creditCardVendorCode.getType().equals(type)) {
                return creditCardVendorCode;
            }
        }
        throw new IllegalArgumentException("Unexpected enum constant: " + type);
    }
}
