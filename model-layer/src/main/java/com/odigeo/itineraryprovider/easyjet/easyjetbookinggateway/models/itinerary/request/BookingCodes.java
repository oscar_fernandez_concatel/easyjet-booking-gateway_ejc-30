package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

public enum BookingCodes {
    ADT,
    CHD,
    INF;

    public static BookingCodes getValue(String representation) {
        switch (representation) {
        case "INFANT":
            return INF;
        case "CHILD":
            return CHD;
        default:
            return ADT;
        }
    }
}
