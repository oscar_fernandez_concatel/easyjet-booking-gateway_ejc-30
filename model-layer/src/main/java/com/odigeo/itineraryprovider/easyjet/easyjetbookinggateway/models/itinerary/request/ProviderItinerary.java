package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = ProviderItinerary.Builder.class)
public class ProviderItinerary {

    private final ItineraryId itineraryId;
    private final Itinerary itinerary;

    public ProviderItinerary(ProviderItinerary.Builder<?> builder) {
        this.itinerary = builder.itinerary;
        this.itineraryId = builder.itineraryId;
    }

    public static ProviderItinerary.Builder builder() {
        return new ProviderItinerary.Builder();
    }

    public ItineraryId getItineraryId() {
        return itineraryId;
    }

    public Itinerary getItinerary() {
        return itinerary;
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static class Builder<T extends Builder<T>> {

        private ItineraryId itineraryId;
        private Itinerary itinerary;

        public T setItinerary(Itinerary itinerary) {
            this.itinerary = itinerary;
            return (T) this;
        }

        public T setItineraryId(ItineraryId itineraryId) {
            this.itineraryId = itineraryId;
            return (T) this;
        }

        public ProviderItinerary build() {
            return new ProviderItinerary(this);
        }

    }

}
