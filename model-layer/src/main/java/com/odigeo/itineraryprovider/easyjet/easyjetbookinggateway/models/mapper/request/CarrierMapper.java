package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Carrier;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CarrierMapper {

    CarrierMapper INSTANCE = Mappers.getMapper(CarrierMapper.class);

    Carrier toModel(com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.Carrier carrier);

}
