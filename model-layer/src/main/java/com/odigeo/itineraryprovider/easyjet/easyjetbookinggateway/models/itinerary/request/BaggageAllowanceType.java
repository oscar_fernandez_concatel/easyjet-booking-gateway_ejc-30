package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

public enum BaggageAllowanceType {
    KG, // Kilos
    PO, // Pounds
    NI, // Nil
    SC, // Special Charge
    NP, // Number of pieces
    SZ, // Size
    VL, // Value
    WE, // Weight
    ND; // Not Defined

    public static BaggageAllowanceType getValue(String name) {
        if (name != null) {
            for (BaggageAllowanceType baggageAllowanceType : BaggageAllowanceType.values()) {
                if (baggageAllowanceType.toString().equalsIgnoreCase(name)) {
                    return baggageAllowanceType;
                }
            }
        }
        return null;
    }
}
