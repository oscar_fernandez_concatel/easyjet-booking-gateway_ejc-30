package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

public class PaymentDetails {
    private final PaymentId paymentId;
    private final PaymentMethod paymentMethod;
    private final PaymentInstrumentToken paymentInstrumentToken;
    private final TicketActionId ticketActionId;
    private final boolean isCash;

    public PaymentDetails(final Builder builder) {
        this.paymentId = builder.paymentId;
        this.paymentMethod = builder.paymentMethod;
        this.paymentInstrumentToken = builder.paymentInstrumentToken;
        this.ticketActionId = builder.ticketActionId;
        this.isCash = builder.isCash;
    }

    public static Builder builder() {
        return new Builder();
    }

    public PaymentId getPaymentId() {
        return paymentId;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public PaymentInstrumentToken getPaymentInstrumentToken() {
        return paymentInstrumentToken;
    }

    public TicketActionId getTicketActionId() {
        return ticketActionId;
    }

    public boolean isCash() {
        return isCash;
    }


    public static final class Builder {
        private PaymentId paymentId;
        private PaymentMethod paymentMethod;
        private PaymentInstrumentToken paymentInstrumentToken;
        private TicketActionId ticketActionId;
        private boolean isCash;

        public Builder setPaymentId(PaymentId paymentId) {
            this.paymentId = paymentId;
            return this;
        }

        public Builder setPaymentMethod(PaymentMethod paymentMethod) {
            this.paymentMethod = paymentMethod;
            return this;
        }

        public Builder setPaymentInstrumentToken(PaymentInstrumentToken paymentInstrumentToken) {
            this.paymentInstrumentToken = paymentInstrumentToken;
            return this;
        }

        public Builder setTicketActionId(TicketActionId ticketActionId) {
            this.ticketActionId = ticketActionId;
            return this;
        }

        public Builder setIsCash(boolean isCash) {
            this.isCash = isCash;
            return this;
        }

        public PaymentDetails build() {
            return new PaymentDetails(this);
        }
    }
}
