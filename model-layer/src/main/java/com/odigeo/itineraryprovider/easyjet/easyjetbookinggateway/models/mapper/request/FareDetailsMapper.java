package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FareDetails;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {MoneyMapper.class, PassengerTypeFareMapper.class})
public interface FareDetailsMapper {

    FareDetailsMapper INSTANCE = Mappers.getMapper(FareDetailsMapper.class);

    FareDetails toModel(final com.odigeo.suppliergatewayapi.v25.itinerary.fare.FareDetails fareDetails);

}
