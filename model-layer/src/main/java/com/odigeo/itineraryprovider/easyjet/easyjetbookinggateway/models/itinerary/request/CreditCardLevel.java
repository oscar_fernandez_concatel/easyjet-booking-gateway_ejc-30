package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

public enum CreditCardLevel {
    CUSTOMER,
    CORPORATE
}
