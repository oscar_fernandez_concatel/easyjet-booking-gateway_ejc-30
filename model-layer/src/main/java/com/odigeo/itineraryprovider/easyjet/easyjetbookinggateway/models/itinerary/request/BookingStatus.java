package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

public enum BookingStatus {
    PENDING,
    CANCELLED,
    CONFIRMED
}
