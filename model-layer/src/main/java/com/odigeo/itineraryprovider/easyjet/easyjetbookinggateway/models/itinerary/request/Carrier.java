package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.io.Serializable;

@JsonDeserialize(builder = Carrier.Builder.class)
public class Carrier implements Serializable {

    private final String name;
    private final String code;
    private final String iataCode;
    private final boolean showBaggagePolicy;

    public Carrier(final Builder builder) {
        this.code = builder.code;
        this.iataCode = builder.iataCode;
        this.name = builder.name;
        this.showBaggagePolicy = builder.showBaggagePolicy;
    }

    public static Carrier.Builder builder() {
        return new Carrier.Builder();
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public String getIataCode() {
        return iataCode;
    }

    public boolean isShowBaggagePolicy() {
        return showBaggagePolicy;
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static final class Builder {

        private String name;
        private String code;
        private String iataCode;
        private boolean showBaggagePolicy;

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setCode(String code) {
            this.code = code;
            return this;
        }

        public Builder setIataCode(String iataCode) {
            this.iataCode = iataCode;
            return this;
        }

        public Builder setShowBaggagePolicy(boolean showBaggagePolicy) {
            this.showBaggagePolicy = showBaggagePolicy;
            return this;
        }

        public Carrier build() {
            return new Carrier(this);
        }
    }
}
