package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderPaymentDetail;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.payment.ItineraryBookingPaymentDetails;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {ProviderPaymentMethodMapper.class})
public interface ProviderPaymentDetailsMapper {
    ProviderPaymentDetailsMapper INSTANCE = Mappers.getMapper(ProviderPaymentDetailsMapper.class);

    default ProviderPaymentDetail toModel(ItineraryBookingPaymentDetails itineraryBookingPaymentDetails) {
        return ProviderPaymentDetail.builder()
                .setProviderPaymentMethod(ProviderPaymentMethodMapper.INSTANCE.toModel(
                        itineraryBookingPaymentDetails.getPaymentDetails().getPaymentMethod()))
                .build();
    }
}
