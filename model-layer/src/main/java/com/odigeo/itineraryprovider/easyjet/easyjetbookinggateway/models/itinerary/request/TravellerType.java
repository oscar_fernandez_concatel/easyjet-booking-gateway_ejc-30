package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

public enum TravellerType {
    ADULT("ADT"),
    INFANT("INF"),
    CHILD("CNN");

    private final String code;

    TravellerType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public static TravellerType getTravellerType(String name) {
        if (CHILD.name().equals(name)) {
            return CHILD;
        } else if (INFANT.name().equals(name)) {
            return INFANT;
        } else {
            return ADULT;
        }
    }
}
