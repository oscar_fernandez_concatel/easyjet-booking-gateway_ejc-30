package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

public class ProviderDetails {

    private final FareDetails fareDetails;
    private final Itinerary itinerary;

    public ProviderDetails(final ProviderDetails.Builder builder) {
        this.fareDetails = builder.fareDetails;
        this.itinerary = builder.itinerary;
    }

    public static ProviderDetails.Builder builder() {
        return new ProviderDetails.Builder();
    }

    public FareDetails getFareDetails() {
        return fareDetails;
    }

    public Itinerary getItinerary() {
        return itinerary;
    }

    public static final class Builder {

        private FareDetails fareDetails;
        private Itinerary itinerary;

        public Builder setFareDetails(FareDetails fareDetails) {
            this.fareDetails = fareDetails;
            return this;
        }

        public Builder setItinerary(Itinerary itinerary) {
            this.itinerary = itinerary;
            return this;
        }

        public ProviderDetails build() {
            return new ProviderDetails(this);
        }
    }
}
