package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.odigeo.suppliergatewayapi.v25.itinerary.booking.ItineraryBookingId;

import java.util.UUID;

public class PaymentItineraryBookingId extends ItineraryBookingId {
    public PaymentItineraryBookingId() {
    }

    public PaymentItineraryBookingId(UUID uuid) {
        super(uuid);
    }

    public static PaymentItineraryBookingId valueOf(String id) {
        UUID uuid = UUID.fromString(id);
        return new PaymentItineraryBookingId(uuid);
    }
}
