package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryBookingId;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.creation.CreatedItineraryBookingId;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CreatedItineraryBookingIdMapper {
    CreatedItineraryBookingIdMapper INSTANCE = Mappers.getMapper(CreatedItineraryBookingIdMapper.class);

    ItineraryBookingId toModel(CreatedItineraryBookingId createdItineraryBookingId);
}
