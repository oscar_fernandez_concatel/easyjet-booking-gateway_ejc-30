package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

public class TaxDetail {
    private final TaxDetailId id;
    private final TicketId ticketId;
    private final String key;
    private final String category;
    private final Money amount;
    private final TicketActionId ticketActionId;

    public TaxDetail(final Builder builder) {
        this.id = builder.id;
        this.ticketId = builder.ticketId;
        this.key = builder.key;
        this.category = builder.category;
        this.amount = builder.amount;
        this.ticketActionId = builder.ticketActionId;
    }

    public static Builder builder() {
        return new Builder();
    }

    public TaxDetailId getId() {
        return id;
    }

    public TicketId getTicketId() {
        return ticketId;
    }

    public String getKey() {
        return key;
    }

    public String getCategory() {
        return category;
    }

    public Money getAmount() {
        return amount;
    }

    public TicketActionId getTicketActionId() {
        return ticketActionId;
    }


    public static final class Builder {
        private TaxDetailId id;
        private TicketId ticketId;
        private String key;
        private String category;
        private Money amount;
        private TicketActionId ticketActionId;

        public Builder setId(TaxDetailId id) {
            this.id = id;
            return this;
        }

        public Builder setTicketId(TicketId ticketId) {
            this.ticketId = ticketId;
            return this;
        }

        public Builder setKey(String key) {
            this.key = key;
            return this;
        }

        public Builder setCategory(String category) {
            this.category = category;
            return this;
        }

        public Builder setAmount(Money amount) {
            this.amount = amount;
            return this;
        }

        public Builder setTicketActionId(TicketActionId ticketActionId) {
            this.ticketActionId = ticketActionId;
            return this;
        }

        public TaxDetail build() {
            return new TaxDetail(this);
        }
    }
}
