package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

public class IssuedItineraryBookingId extends ItineraryBookingId {

    public IssuedItineraryBookingId(final IssuedItineraryBookingId.Builder builder) {
        super(builder);
    }

    public static IssuedItineraryBookingId.Builder builder() {
        return new IssuedItineraryBookingId.Builder();
    }


    public static class Builder extends ItineraryBookingId.Builder<Builder> {

        public IssuedItineraryBookingId build() {
            return new IssuedItineraryBookingId(this);
        }
    }
}
