package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Segment;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.stream.Collectors;

@Mapper
public interface SegmentMapper {

    SegmentMapper INSTANCE = Mappers.getMapper(SegmentMapper.class);

    default Segment toModel(com.odigeo.suppliergatewayapi.v25.itinerary.segment.Segment segment) {
        return Segment.builder()
                .setItineraryId(ItineraryId.builder().setId(segment.getItineraryId().getId()).build())
                .setPosition(segment.getPosition())
                .setSections(segment.getSections().stream().map(FlightSectionMapper.INSTANCE::toModel)
                        .collect(Collectors.toList()))
                .build();
    }

}
