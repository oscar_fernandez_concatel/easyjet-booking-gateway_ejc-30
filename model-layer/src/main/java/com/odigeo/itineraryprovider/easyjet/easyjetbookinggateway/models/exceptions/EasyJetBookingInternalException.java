package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions;

public class EasyJetBookingInternalException extends EasyJetBookingGatewayException {

    public EasyJetBookingInternalException(String message, EasyJetBookingGatewayErrorType errorType) {
        super(message, errorType);
    }

    public EasyJetBookingInternalException(Exception e, EasyJetBookingGatewayErrorType errorType) {
        super(e.getMessage(), errorType);
    }

    public EasyJetBookingInternalException(String message, EasyJetBookingGatewayErrorType errorType, String codeError) {
        super(message + "-" + codeError, errorType);
    }

    public EasyJetBookingInternalException(
            String message,
            EasyJetBookingGatewayErrorType errorType, Throwable e) {
        super(e, errorType, message);
    }

}
