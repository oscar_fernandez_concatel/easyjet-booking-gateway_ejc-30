package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.time.LocalDateTime;

@JsonDeserialize(builder = Section.Builder.class)
public class Section {

    private final ItineraryId id;
    private final Integer segmentPosition;
    private final Integer departureGeoNodeId;
    private final Integer arrivalGeoNodeId;

    private final LocalDateTime departureDate;
    private final LocalDateTime arrivalDate;

    private final String arrivalTerminal;
    private final String departureTerminal;
    private final CabinClass cabinClass;
    private final BaggageAllowance baggageAllowance;
    private final String productType;

    public Section(final Section.Builder<?> builder) {
        this.id = builder.id;
        this.segmentPosition = builder.segmentPosition;
        this.departureGeoNodeId = builder.departureGeoNodeId;
        this.arrivalGeoNodeId = builder.arrivalGeoNodeId;
        this.productType = builder.productType;
        this.departureDate = builder.departureDate;
        this.arrivalDate = builder.arrivalDate;
        this.arrivalTerminal = builder.arrivalTerminal;
        this.departureTerminal = builder.departureTerminal;
        this.cabinClass = builder.cabinClass;
        this.baggageAllowance = builder.baggageAllowance;
    }

    public ItineraryId getId() {
        return id;
    }

    public Integer getSegmentPosition() {
        return segmentPosition;
    }

    public Integer getDepartureGeoNodeId() {
        return departureGeoNodeId;
    }

    public Integer getArrivalGeoNodeId() {
        return arrivalGeoNodeId;
    }

    public LocalDateTime getDepartureDate() {
        return departureDate;
    }

    public LocalDateTime getArrivalDate() {
        return arrivalDate;
    }

    public String getArrivalTerminal() {
        return arrivalTerminal;
    }

    public String getDepartureTerminal() {
        return departureTerminal;
    }

    public CabinClass getCabinClass() {
        return cabinClass;
    }

    public BaggageAllowance getBaggageAllowance() {
        return baggageAllowance;
    }

    public String getProductType() {
        return productType;
    }

    public static Builder builder() {
        return new Builder();
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static class Builder<T extends Builder<T>> {

        private ItineraryId id;
        private Integer segmentPosition;
        private Integer departureGeoNodeId;
        private Integer arrivalGeoNodeId;
        private LocalDateTime departureDate;
        private LocalDateTime arrivalDate;
        private String arrivalTerminal;
        private String departureTerminal;
        private CabinClass cabinClass;
        private BaggageAllowance baggageAllowance;
        private String productType;

        public T setId(ItineraryId id) {
            this.id = id;
            return (T) this;
        }

        public T setSegmentPosition(Integer segmentPosition) {
            this.segmentPosition = segmentPosition;
            return (T) this;
        }

        public T setDepartureGeoNodeId(Integer departureGeoNodeId) {
            this.departureGeoNodeId = departureGeoNodeId;
            return (T) this;
        }

        public T setArrivalGeoNodeId(Integer arrivalGeoNodeId) {
            this.arrivalGeoNodeId = arrivalGeoNodeId;
            return (T) this;
        }

        public T setDepartureDate(LocalDateTime departureDate) {
            this.departureDate = departureDate;
            return (T) this;
        }

        public T setArrivalDate(LocalDateTime arrivalDate) {
            this.arrivalDate = arrivalDate;
            return (T) this;
        }

        public T setArrivalTerminal(String arrivalTerminal) {
            this.arrivalTerminal = arrivalTerminal;
            return (T) this;
        }

        public T setDepartureTerminal(String departureTerminal) {
            this.departureTerminal = departureTerminal;
            return (T) this;
        }

        public T setCabinClass(CabinClass cabinClass) {
            this.cabinClass = cabinClass;
            return (T) this;
        }

        public T setBaggageAllowance(BaggageAllowance baggageAllowance) {
            this.baggageAllowance = baggageAllowance;
            return (T) this;
        }

        public T setProductType(String productType) {
            this.productType = productType;
            return (T) this;
        }

        public Section build() {
            return new Section(this);
        }
    }
}
