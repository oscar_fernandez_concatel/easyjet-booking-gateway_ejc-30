package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.util.List;
import java.util.Map;

@JsonDeserialize(builder = FareDetails.Builder.class)
public class FareDetails {

    private final ItineraryId itineraryId;
    private final Map<TravellerType, PassengerTypeFare> passengerFares;
    private final Money totalFareAmount;
    private final List<String> corporateCodes;
    private final Money totalTaxAmount;

    public FareDetails(final FareDetails.Builder builder) {
        this.itineraryId = builder.itineraryId;
        this.passengerFares = builder.passengerFares;
        this.totalFareAmount = builder.totalFareAmount;
        this.corporateCodes = builder.corporateCodes;
        this.totalTaxAmount = builder.totalTaxAmount;
    }

    public static FareDetails.Builder builder() {
        return new FareDetails.Builder();
    }

    public ItineraryId getItineraryId() {
        return itineraryId;
    }

    public Map<TravellerType, PassengerTypeFare> getPassengerFares() {
        return passengerFares;
    }

    public Money getTotalFareAmount() {
        return totalFareAmount;
    }

    public List<String> getCorporateCodes() {
        return corporateCodes;
    }

    public Money getTotalTaxAmount() {
        return totalTaxAmount;
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static final class Builder {
        private ItineraryId itineraryId;
        private Map<TravellerType, PassengerTypeFare> passengerFares;
        private Money totalFareAmount;
        private List<String> corporateCodes;
        private Money totalTaxAmount;

        public Builder setItineraryId(ItineraryId itineraryId) {
            this.itineraryId = itineraryId;
            return this;
        }

        public Builder setPassengerFares(Map<TravellerType, PassengerTypeFare> passengerFares) {
            this.passengerFares = passengerFares;
            return this;
        }

        public Builder setTotalFareAmount(Money totalFareAmount) {
            this.totalFareAmount = totalFareAmount;
            return this;
        }

        public Builder setCorporateCodes(List<String> corporateCodes) {
            this.corporateCodes = corporateCodes;
            return this;
        }

        public Builder setTotalTaxAmount(Money totalTaxAmount) {
            this.totalTaxAmount = totalTaxAmount;
            return this;
        }

        public FareDetails build() {
            return new FareDetails(this);
        }
    }
}
