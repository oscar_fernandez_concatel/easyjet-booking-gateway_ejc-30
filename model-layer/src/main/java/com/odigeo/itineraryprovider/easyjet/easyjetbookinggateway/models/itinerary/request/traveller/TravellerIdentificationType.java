package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller;

public enum TravellerIdentificationType {
    PASSPORT,
    NIE,
    NIF,
    NATIONAL_ID_CARD,
    BIRTH_DATE
}
