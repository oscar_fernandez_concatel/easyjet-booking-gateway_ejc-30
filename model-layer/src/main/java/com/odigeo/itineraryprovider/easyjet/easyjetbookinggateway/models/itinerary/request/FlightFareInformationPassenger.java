package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = FlightFareInformationPassenger.Builder.class)
public class FlightFareInformationPassenger {

    private final String serviceClass;
    private final CabinClass cabinClass;
    private final String fareBasisId;
    private final boolean isResident;
    private final Integer availableSeats;
    private final FareType fareType;
    private final String agreementsType;
    private final String fareRules;
    private final String subFareType;

    public FlightFareInformationPassenger(final FlightFareInformationPassenger.Builder builder) {
        this.serviceClass = builder.serviceClass;
        this.cabinClass = builder.cabinClass;
        this.fareBasisId = builder.fareBasisId;
        this.isResident = builder.isResident;
        this.availableSeats = builder.availableSeats;
        this.fareType = builder.fareType;
        this.agreementsType = builder.agreementsType;
        this.fareRules = builder.fareRules;
        this.subFareType = builder.subFareType;
    }

    public static FlightFareInformationPassenger.Builder builder() {
        return new FlightFareInformationPassenger.Builder();
    }

    public String getServiceClass() {
        return serviceClass;
    }

    public CabinClass getCabinClass() {
        return cabinClass;
    }

    public String getFareBasisId() {
        return fareBasisId;
    }

    public boolean isResident() {
        return isResident;
    }

    public int getAvailableSeats() {
        return availableSeats;
    }

    public FareType getFareType() {
        return fareType;
    }

    public String getAgreementsType() {
        return agreementsType;
    }

    public String getFareRules() {
        return fareRules;
    }

    public String getSubFareType() {
        return subFareType;
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static final class Builder {

        private String serviceClass;
        private CabinClass cabinClass;
        private String fareBasisId;
        private boolean isResident;
        private int availableSeats = -1;
        private FareType fareType;
        private String agreementsType;
        private String fareRules;
        private String subFareType;

        public Builder setServiceClass(String serviceClass) {
            this.serviceClass = serviceClass;
            return this;
        }

        public Builder setCabinClass(CabinClass cabinClass) {
            this.cabinClass = cabinClass;
            return this;
        }

        public Builder setFareBasisId(String fareBasisId) {
            this.fareBasisId = fareBasisId;
            return this;
        }

        public Builder setIsResident(boolean isResident) {
            this.isResident = isResident;
            return this;
        }

        public Builder setAvailableSeats(int availableSeats) {
            this.availableSeats = availableSeats;
            return this;
        }

        public Builder setFareType(FareType fareType) {
            this.fareType = fareType;
            return this;
        }

        public Builder setAgreementsType(String agreementsType) {
            this.agreementsType = agreementsType;
            return this;
        }

        public Builder setFareRules(String fareRules) {
            this.fareRules = fareRules;
            return this;
        }

        public Builder setSubFareType(String subFareType) {
            this.subFareType = subFareType;
            return this;
        }

        public FlightFareInformationPassenger build() {
            return new FlightFareInformationPassenger(this);
        }
    }
}
