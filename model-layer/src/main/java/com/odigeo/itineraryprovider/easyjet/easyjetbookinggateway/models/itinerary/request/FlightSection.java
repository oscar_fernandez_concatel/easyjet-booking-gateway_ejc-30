package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = FlightSection.Builder.class)
public class FlightSection extends Section {

    private final Flight flight;
    private final FlightFareInformation flightFareInformation;
    private final ProviderSection providerSection;

    public FlightSection(final FlightSection.Builder builder) {
        super(builder);
        this.flight = builder.flight;
        this.flightFareInformation = builder.flightFareInformation;
        this.providerSection = builder.providerSection;
    }

    public static FlightSection.Builder builder() {
        return new FlightSection.Builder();
    }

    public Flight getFlight() {
        return flight;
    }

    public FlightFareInformation getFlightFareInformation() {
        return flightFareInformation;
    }

    public ProviderSection getProviderSection() {
        return providerSection;
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static class Builder extends Section.Builder<Builder> {

        private Flight flight;
        private FlightFareInformation flightFareInformation;
        private ProviderSection providerSection;

        public FlightSection.Builder setFlight(Flight flight) {
            this.flight = flight;
            return this;
        }

        public FlightSection.Builder setFlightFareInformation(FlightFareInformation flightFareInformation) {
            this.flightFareInformation = flightFareInformation;
            return this;
        }

        public FlightSection.Builder setProviderSection(ProviderSection providerSection) {
            this.providerSection = providerSection;
            return this;
        }

        public FlightSection build() {
            return new FlightSection(this);
        }
    }
}
