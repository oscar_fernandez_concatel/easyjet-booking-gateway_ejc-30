package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller;

public class Phone {
    private final Country country;
    private final String number;
    private final String phoneType;

    public Phone(final Builder builder) {
        this.country = builder.country;
        this.number = builder.number;
        this.phoneType = builder.phoneType;
    }

    public static Builder builder() {
        return new Builder();
    }

    public Country getCountry() {
        return country;
    }

    public String getNumber() {
        return number;
    }

    public String getPhoneType() {
        return phoneType;
    }


    public static final class Builder {
        private Country country;
        private String number;
        private String phoneType;

        public Builder setCountry(Country country) {
            this.country = country;
            return this;
        }

        public Builder setNumber(String number) {
            this.number = number;
            return this;
        }

        public Builder setPhoneType(String phoneType) {
            this.phoneType = phoneType;
            return this;
        }

        public Phone build() {
            return new Phone(this);
        }
    }
}
