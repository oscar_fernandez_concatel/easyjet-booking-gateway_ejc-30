package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

public enum CreditCardCategory {
    CREDIT,
    DEBIT,
    PREPAID
}
