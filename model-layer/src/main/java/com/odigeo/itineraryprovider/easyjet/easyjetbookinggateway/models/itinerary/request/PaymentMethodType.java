package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

public enum PaymentMethodType {
    CASH,
    CREDITCARD
}
