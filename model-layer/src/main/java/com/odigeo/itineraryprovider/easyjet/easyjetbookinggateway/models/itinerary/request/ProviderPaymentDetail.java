package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

public class ProviderPaymentDetail {
    private final ProviderPaymentMethod providerPaymentMethod;

    public ProviderPaymentDetail(final Builder builder) {
        this.providerPaymentMethod = builder.providerPaymentMethod;
    }

    public static Builder builder() {
        return new Builder();
    }

    public ProviderPaymentMethod getProviderPaymentMethod() {
        return providerPaymentMethod;
    }

    public static final class Builder {
        private ProviderPaymentMethod providerPaymentMethod;

        public Builder setProviderPaymentMethod(ProviderPaymentMethod providerPaymentMethod) {
            this.providerPaymentMethod = providerPaymentMethod;
            return this;
        }

        public ProviderPaymentDetail build() {
            return new ProviderPaymentDetail(this);
        }
    }
}
