package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import java.util.UUID;

public class TaxDetailId {
    private final UUID id;

    public TaxDetailId(final Builder builder) {
        this.id = builder.id;
    }

    public static Builder builder() {
        return new Builder();
    }

    public UUID getId() {
        return id;
    }


    public static final class Builder {
        private UUID id;

        public Builder setId(UUID id) {
            this.id = id;
            return this;
        }

        public TaxDetailId build() {
            return new TaxDetailId(this);
        }
    }
}
