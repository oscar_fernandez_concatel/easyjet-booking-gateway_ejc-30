package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import java.util.UUID;

public class TicketId {
    private final UUID id;

    public TicketId(final Builder builder) {
        this.id = builder.id;
    }

    public static Builder builder() {
        return new Builder();
    }

    public UUID getId() {
        return id;
    }


    public static final class Builder {
        private UUID id;

        public Builder setId(UUID id) {
            this.id = id;
            return this;
        }

        public TicketId build() {
            return new TicketId(this);
        }
    }
}
