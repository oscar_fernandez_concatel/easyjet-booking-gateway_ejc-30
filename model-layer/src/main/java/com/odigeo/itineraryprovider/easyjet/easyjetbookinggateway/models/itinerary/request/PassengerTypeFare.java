package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = PassengerTypeFare.Builder.class)
public class PassengerTypeFare {

    private final Money fareAmount;
    private final Money taxAmount;
    private final TravellerType travellerType;
    private final Integer passengersPerType;
    private final boolean discount;

    public PassengerTypeFare(final PassengerTypeFare.Builder builder) {
        this.fareAmount = builder.fareAmount;
        this.taxAmount = builder.taxAmount;
        this.travellerType = builder.travellerType;
        this.passengersPerType = builder.passengersPerType;
        this.discount = builder.discount;
    }

    public static PassengerTypeFare.Builder builder() {
        return new PassengerTypeFare.Builder();
    }

    public Money getFareAmount() {
        return fareAmount;
    }

    public Money getTaxAmount() {
        return taxAmount;
    }

    public TravellerType getTravellerType() {
        return travellerType;
    }

    public Integer getPassengersPerType() {
        return passengersPerType;
    }

    public boolean isDiscount() {
        return discount;
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static final class Builder {

        private Money fareAmount;
        private Money taxAmount;
        private TravellerType travellerType;
        private Integer passengersPerType;
        private boolean discount;

        public Builder setFareAmount(Money fareAmount) {
            this.fareAmount = fareAmount;
            return this;
        }

        public Builder setTaxAmount(Money taxAmount) {
            this.taxAmount = taxAmount;
            return this;
        }

        public Builder setTravellerType(TravellerType travellerType) {
            this.travellerType = travellerType;
            return this;
        }

        public Builder setPassengersPerType(Integer passengersPerType) {
            this.passengersPerType = passengersPerType;
            return this;
        }

        public Builder setDiscount(boolean discount) {
            this.discount = discount;
            return this;
        }

        public PassengerTypeFare build() {
            return new PassengerTypeFare(this);
        }
    }
}
