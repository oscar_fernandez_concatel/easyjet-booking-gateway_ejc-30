package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.util.ArrayList;
import java.util.List;

@JsonDeserialize(builder = Segment.Builder.class)
public class Segment {

    private final ItineraryId itineraryId;
    private final List<FlightSection> sections;
    private final Integer position;

    public Segment(final Segment.Builder builder) {
        this.itineraryId = builder.itineraryId;
        this.sections = builder.sections != null ? builder.sections : new ArrayList<>();
        this.position = builder.position;
    }

    public static Segment.Builder builder() {
        return new Segment.Builder();
    }

    public ItineraryId getItineraryId() {
        return itineraryId;
    }

    public List<FlightSection> getSections() {
        return sections;
    }

    public Integer getPosition() {
        return position;
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static final class Builder {

        private ItineraryId itineraryId;
        private List<FlightSection> sections;
        private Integer position;

        public Builder setItineraryId(ItineraryId itineraryId) {
            this.itineraryId = itineraryId;
            return this;
        }

        public Builder setSections(List<FlightSection> sections) {
            this.sections = sections;
            return this;
        }

        public Builder setPosition(Integer position) {
            this.position = position;
            return this;
        }

        public Segment build() {
            return new Segment(this);
        }
    }
}
