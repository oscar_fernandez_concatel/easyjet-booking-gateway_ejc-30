package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

public enum ActionType {
    REFUNDED,
    VOIDED,
    ISSUED
}
