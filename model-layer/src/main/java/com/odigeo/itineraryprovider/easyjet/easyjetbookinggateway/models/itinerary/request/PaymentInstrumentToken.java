package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import java.util.UUID;

public class PaymentInstrumentToken {
    private final PaymentMethodType type;
    private final UUID uuid;

    public PaymentInstrumentToken(final Builder builder) {
        this.type = builder.type;
        this.uuid = builder.uuid;
    }

    public static Builder builder() {
        return new Builder();
    }

    public PaymentMethodType getType() {
        return type;
    }

    public UUID getUuid() {
        return uuid;
    }


    public static final class Builder {
        private PaymentMethodType type;
        private UUID uuid;

        public Builder setType(PaymentMethodType type) {
            this.type = type;
            return this;
        }

        public Builder setUuid(UUID uuid) {
            this.uuid = uuid;
            return this;
        }

        public PaymentInstrumentToken build() {
            return new PaymentInstrumentToken(this);
        }
    }
}
