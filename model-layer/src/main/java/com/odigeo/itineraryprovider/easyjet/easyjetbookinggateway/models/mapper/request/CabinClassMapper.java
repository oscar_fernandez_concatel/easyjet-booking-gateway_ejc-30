package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CabinClass;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CabinClassMapper {
    CabinClassMapper INSTANCE = Mappers.getMapper(CabinClassMapper.class);

    CabinClass toModel(String cabinClass);
}
