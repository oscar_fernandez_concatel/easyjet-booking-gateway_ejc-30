package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightFareInformationPassenger;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.Map;

@Mapper
public interface FlightFareInformationMapper {

    FlightFareInformationMapper INSTANCE = Mappers.getMapper(FlightFareInformationMapper.class);

    Map<String, FlightFareInformationPassenger> toModel(Map<String, com.odigeo.suppliergatewayapi.v25.itinerary.fare.FlightFareInformationPassenger> flightFareInformationPassenger);

    Map<String, com.odigeo.suppliergatewayapi.v25.itinerary.fare.FlightFareInformationPassenger> toApi(Map<String, FlightFareInformationPassenger> flightFareInformationPassenger);

}
