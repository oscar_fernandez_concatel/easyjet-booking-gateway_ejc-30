package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CashPaymentOption;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardPaymentOption;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderPaymentOptions;
import com.odigeo.suppliergatewayapi.v25.itinerary.paymentoption.CashProviderPaymentOption;
import com.odigeo.suppliergatewayapi.v25.itinerary.paymentoption.CreditCardProviderPaymentOption;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.math.BigDecimal;
import java.util.Currency;

@Mapper
public interface ProviderPaymentOptionsMapper {

    ProviderPaymentOptionsMapper INSTANCE = Mappers.getMapper(ProviderPaymentOptionsMapper.class);

    com.odigeo.suppliergatewayapi.v25.itinerary.fare.Money DEFAULT_FEE = new com.odigeo.suppliergatewayapi.v25.itinerary.fare.Money(BigDecimal.ZERO, Currency.getInstance("EUR"));

    @Mapping(source = "cashPaymentOptions", target = "cashProviderPaymentOptions")
    @Mapping(source = "creditCardPaymentOptions", target = "creditCardProviderPaymentOptions")
    com.odigeo.suppliergatewayapi.v25.itinerary.paymentoption.ProviderPaymentOptions toApi(ProviderPaymentOptions providerPaymentOptions);

    @Mapping(target = "fee", expression = "java(DEFAULT_FEE)")
    CashProviderPaymentOption toApi(CashPaymentOption cashPaymentOption);

    @Mapping(target = "fee", expression = "java(DEFAULT_FEE)")
    CreditCardProviderPaymentOption toApi(CreditCardPaymentOption creditCardPaymentOption);
}
