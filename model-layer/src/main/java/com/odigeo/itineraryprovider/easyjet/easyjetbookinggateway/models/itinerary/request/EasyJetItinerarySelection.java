package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

public class EasyJetItinerarySelection {

    private final String visitId;
    private final String visitInformation;
    private final Itinerary itinerary;
    private final ProviderPaymentDetail providerPaymentDetail;
    private final ProviderPaymentOptions providerPaymentOptions;

    public EasyJetItinerarySelection(final Builder builder) {
        this.visitId = builder.visitId;
        this.visitInformation = builder.visitInformation;
        this.itinerary = builder.itinerary;
        this.providerPaymentDetail = builder.providerPaymentDetail;
        this.providerPaymentOptions = builder.providerPaymentOptions;
    }

    public static EasyJetItinerarySelection.Builder builder() {
        return new EasyJetItinerarySelection.Builder();
    }

    public String getVisitId() {
        return visitId;
    }

    public String getVisitInformation() {
        return visitInformation;
    }

    public Itinerary getItinerary() {
        return itinerary;
    }

    public ProviderPaymentDetail getProviderPaymentDetail() {
        return providerPaymentDetail;
    }

    public ProviderPaymentOptions getProviderPaymentOptions() {
        return providerPaymentOptions;
    }

    public static class Builder {

        private String visitId;
        private String visitInformation;
        private Itinerary itinerary;
        private ProviderPaymentDetail providerPaymentDetail;
        private ProviderPaymentOptions providerPaymentOptions;

        public Builder setVisitId(final String visitId) {
            this.visitId = visitId;
            return this;
        }

        public Builder setVisitInformation(final String visitInformation) {
            this.visitInformation = visitInformation;
            return this;
        }

        public Builder setItinerary(final Itinerary itinerary) {
            this.itinerary = itinerary;
            return this;
        }

        public Builder setProviderPaymentDetail(final ProviderPaymentDetail providerPaymentDetail) {
            this.providerPaymentDetail = providerPaymentDetail;
            return this;
        }

        public Builder setProviderPaymentOptions(final ProviderPaymentOptions providerPaymentOptions) {
            this.providerPaymentOptions = providerPaymentOptions;
            return this;
        }

        public EasyJetItinerarySelection build() {
            return new EasyJetItinerarySelection(this);
        }

    }
}
