package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

public enum CreditCardProduct {

    DEFAULT("Default Product");

    private final String name;

    CreditCardProduct(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
