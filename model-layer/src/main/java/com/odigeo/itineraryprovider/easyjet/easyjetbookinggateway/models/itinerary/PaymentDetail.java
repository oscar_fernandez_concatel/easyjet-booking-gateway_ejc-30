package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary;

import java.util.UUID;

public class PaymentDetail {

    private final UUID id;
    private final String paymentInstrumentToken;
    private final UUID itineraryBookingId;
    private final String cardVendorCode;

    public PaymentDetail(PaymentDetail.Builder builder) {
        this.id = builder.id;
        this.paymentInstrumentToken = builder.paymentInstrumentToken;
        this.itineraryBookingId = builder.itineraryBookingId;
        this.cardVendorCode = builder.cardVendorCode;
    }

    public static PaymentDetail.Builder builder() {
        return new PaymentDetail.Builder();
    }

    public UUID getId() {
        return id;
    }

    public String getPaymentInstrumentToken() {
        return paymentInstrumentToken;
    }

    public UUID getItineraryBookingId() {
        return itineraryBookingId;
    }

    public String getCardVendorCode() {
        return cardVendorCode;
    }

    public static final class Builder {

        private UUID id;
        private String paymentInstrumentToken;
        private UUID itineraryBookingId;
        private String cardVendorCode;

        public PaymentDetail.Builder setId(UUID id) {
            this.id = id;
            return this;
        }

        public PaymentDetail.Builder setPaymentInstrumentToken(String paymentInstrumentToken) {
            this.paymentInstrumentToken = paymentInstrumentToken;
            return this;
        }

        public PaymentDetail.Builder setItineraryBookingId(UUID itineraryBookingId) {
            this.itineraryBookingId = itineraryBookingId;
            return this;
        }

        public PaymentDetail.Builder setCardVendorCode(String cardVendorCode) {
            this.cardVendorCode = cardVendorCode;
            return this;
        }

        public PaymentDetail build() {
            return new PaymentDetail(this);
        }
    }
}
