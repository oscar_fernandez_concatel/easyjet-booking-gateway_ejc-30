package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

public enum CreditCardScheme {
    VISA,
    MASTERCARD,
    MAESTRO,
    AMEX,
    DINERS_CLUB,
    JCB,
    AIRPLUS
}
