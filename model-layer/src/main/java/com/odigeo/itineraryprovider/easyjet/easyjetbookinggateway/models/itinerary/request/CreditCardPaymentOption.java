package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = CreditCardPaymentOption.Builder.class)
public class CreditCardPaymentOption {

    private final CreditCardSpec creditCardSpec;

    public CreditCardPaymentOption(final CreditCardPaymentOption.Builder builder) {
        this.creditCardSpec = builder.creditCardSpec;
    }

    public static CreditCardPaymentOption.Builder builder() {
        return new CreditCardPaymentOption.Builder();
    }

    public CreditCardSpec getCreditCardSpec() {
        return creditCardSpec;
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static final class Builder {

        private CreditCardSpec creditCardSpec;

        public Builder setCreditCardSpec(CreditCardSpec creditCardSpec) {
            this.creditCardSpec = creditCardSpec;
            return this;
        }

        public CreditCardPaymentOption build() {
            return new CreditCardPaymentOption(this);
        }
    }
}
