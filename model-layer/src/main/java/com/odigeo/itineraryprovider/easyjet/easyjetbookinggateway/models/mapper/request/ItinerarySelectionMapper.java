package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request;


import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayErrorType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.ItineraryNotFoundException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.EasyJetItinerarySelection;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FareDetails;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Itinerary;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Money;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PassengerTypeFare;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TripType;
import com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.Carrier;
import com.odigeo.suppliergatewayapi.v25.itinerary.selection.ItinerarySelection;
import org.apache.commons.collections.CollectionUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.stream.Collectors;

@Mapper(uses = {FlightSectionMapper.class})
public interface ItinerarySelectionMapper {

    ItinerarySelectionMapper INSTANCE = Mappers.getMapper(ItinerarySelectionMapper.class);

    default EasyJetItinerarySelection toModel(final ItinerarySelection itinerarySelection) {
        return EasyJetItinerarySelection.builder()
                .setVisitId(itinerarySelection.getVisitId())
                .setVisitInformation(itinerarySelection.getVisitInformation())
                .setItinerary(toItinerary(itinerarySelection)).build();
    }

    @Mappings({
            @Mapping(target = "departureGeoNodeId", source = "departureLocation"),
            @Mapping(target = "arrivalGeoNodeId", source = "arrivalLocation")
        })
    default Itinerary toItinerary(ItinerarySelection itinerarySelection) {

        if (itinerarySelection.getSegments().isEmpty()) {
            throw new ItineraryNotFoundException("Itinerary Selection doesnt have any segments so we cannot get the itineraryId",
                    EasyJetBookingGatewayErrorType.ITINERARY_NOT_FOUND);
        }

        return Itinerary.builder()
                .setId(ItineraryId.builder().setId(itinerarySelection.getSegments().get(0).getItineraryId().getId()).build())
                .setSegments(itinerarySelection.getSegments().stream()
                        .map(SegmentMapper.INSTANCE::toModel)
                        .collect(Collectors.toList()))
                .setTripType(TripType.getTripType(itinerarySelection.getTripType()))
                .setReturnDate(itinerarySelection.getReturnDate() != null ? itinerarySelection.getReturnDate() : null)
                .setDepartureDate(itinerarySelection.getDepartureDate() != null ? itinerarySelection.getDepartureDate() : null)
                .setDepartureGeoNodeId(itinerarySelection.getDepartureLocation())
                .setArrivalGeoNodeId(itinerarySelection.getArrivalLocation())
                .setDefaultValidatingCarrier(CarrierMapper.INSTANCE.toModel(getCarrier(itinerarySelection)))
                .setFareDetails(FareDetailsMapper.INSTANCE.toModel(itinerarySelection.getFareDetails()))
                .build();
    }

    default Carrier getCarrier(ItinerarySelection itinerarySelection) {

        if (CollectionUtils.isEmpty(itinerarySelection.getSegments().get(0).getSections())) {
            return null;
        }

        return itinerarySelection.getSegments().get(0).getSections()
                .stream().filter(section -> section instanceof com.odigeo.suppliergatewayapi.v25.
                        itinerary.segment.section.FlightSection)
                .map(section ->
                        ((com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.FlightSection) section)
                                .getFlight().getOperatingCarrier())
                .findFirst().orElse(null);
    }

    @Mappings({
            @Mapping(target = "passengerFares", source = "passengerFares"),
            @Mapping(target = "totalFareAmount", source = "totalFareAmount"),
            @Mapping(target = "totalTaxAmount", source = "totalTaxAmount"),
            @Mapping(target = "corporateCodes", source = "corporateCodes")
        })
    FareDetails toItinerary(com.odigeo.suppliergatewayapi.v25.itinerary.fare.FareDetails fareDetails);

    @Mappings({
            @Mapping(target = "amount", source = "amount"),
            @Mapping(target = "currency", source = "currency")
        })
    Money toItinerary(com.odigeo.suppliergatewayapi.v25.itinerary.fare.Money money);

    @Mappings({
            @Mapping(target = "fareAmount", source = "fareAmount"),
            @Mapping(target = "taxAmount", source = "taxAmount"),
            @Mapping(target = "travellerType", source = "travellerType"),
            @Mapping(target = "passengersPerType", source = "passengersPerType"),
            @Mapping(target = "discount", source = "discount")
        })
    PassengerTypeFare toItinerary(com.odigeo.suppliergatewayapi.v25.itinerary.fare.PassengerTypeFare passengerTypeFare);

}
