package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardVendorCode;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CreditCardVendorCodeMapper {
    CreditCardVendorCodeMapper INSTANCE = Mappers.getMapper(CreditCardVendorCodeMapper.class);

    default CreditCardVendorCode toModel(String creditCardVendorCode) {
        return CreditCardVendorCode.getValue(creditCardVendorCode);
    }
}
