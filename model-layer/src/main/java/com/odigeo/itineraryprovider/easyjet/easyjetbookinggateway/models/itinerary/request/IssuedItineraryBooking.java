package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import java.util.List;

public class IssuedItineraryBooking {

    private final IssuedItineraryBookingId id;
    private final ItineraryBooking itineraryBooking;
    private final List<ItineraryTicket> itineraryTickets;
    private final String status;

    public IssuedItineraryBooking(final IssuedItineraryBooking.Builder builder) {
        this.id = builder.id;
        this.itineraryBooking = builder.itineraryBooking;
        this.itineraryTickets = builder.itineraryTickets;
        this.status = builder.status;
    }

    public static IssuedItineraryBooking.Builder builder() {
        return new IssuedItineraryBooking.Builder();
    }

    public IssuedItineraryBookingId getId() {
        return id;
    }

    public ItineraryBooking getItineraryBooking() {
        return itineraryBooking;
    }

    public List<ItineraryTicket> getItineraryTickets() {
        return itineraryTickets;
    }

    public String getStatus() {
        return status;
    }

    public static final class Builder {
        private IssuedItineraryBookingId id;
        private ItineraryBooking itineraryBooking;
        private List<ItineraryTicket> itineraryTickets;
        private String status;

        public Builder setId(IssuedItineraryBookingId id) {
            this.id = id;
            return this;
        }

        public Builder setItineraryBooking(ItineraryBooking itineraryBooking) {
            this.itineraryBooking = itineraryBooking;
            return this;
        }

        public Builder setItineraryTickets(List<ItineraryTicket> itineraryTickets) {
            this.itineraryTickets = itineraryTickets;
            return this;
        }

        public Builder setStatus(String status) {
            this.status = status;
            return this;
        }

        public IssuedItineraryBooking build() {
            return new IssuedItineraryBooking(this);
        }
    }
}
