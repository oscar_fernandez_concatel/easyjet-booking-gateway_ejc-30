package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions;

public class EasyJetBookingProviderException extends EasyJetBookingGatewayException {

    public EasyJetBookingProviderException(final String message) {
        super(message, null);
    }

}
