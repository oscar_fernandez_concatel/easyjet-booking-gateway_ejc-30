package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.math.BigDecimal;
import java.util.Currency;

@JsonDeserialize(builder = Money.Builder.class)
public class Money {

    private final BigDecimal amount;
    private final Currency currency;

    public Money(final Money.Builder builder) {
        this.amount = builder.amount;
        this.currency = builder.currency;
    }

    public static Money.Builder builder() {
        return new Money.Builder();
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static final class Builder {

        private BigDecimal amount;
        private Currency currency;

        public Builder setAmount(BigDecimal amount) {
            this.amount = amount;
            return this;
        }

        public Builder setCurrency(Currency currency) {
            this.currency = currency;
            return this;
        }

        public Money build() {
            return new Money(this);
        }
    }
}
