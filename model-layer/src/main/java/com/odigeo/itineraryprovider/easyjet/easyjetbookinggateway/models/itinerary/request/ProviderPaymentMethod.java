package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

public interface ProviderPaymentMethod {
    ProviderPaymentMethodType getProviderPaymentMethodType();
}
