package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.PaymentDetail;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Traveller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProviderItineraryBooking extends ProviderItinerary {

    private final UUID itineraryBookingId;
    private final String pnr;
    private final String universalLocator;
    private final String reservationLocator;
    private final Buyer buyer;
    private final List<Traveller> travellers;
    private final BookingStatus status;
    private final List<ItineraryTicket> itineraryTickets;
    private final List<SupplierLocator> supplierLocators;
    private final List<PaymentDetail> paymentDetails;
    private final BigDecimal totalFarePrice;

    public ProviderItineraryBooking(Builder builder) {
        super(builder);
        this.itineraryBookingId = builder.itineraryBookingId;
        this.pnr = builder.pnr;
        this.universalLocator = builder.universalLocator;
        this.reservationLocator = builder.reservationLocator;
        this.buyer = builder.buyer;
        this.travellers = builder.travellers == null ? new ArrayList<>() : builder.travellers;
        this.status = builder.status;
        this.itineraryTickets = builder.itineraryTickets == null ? new ArrayList<>() : builder.itineraryTickets;
        this.supplierLocators = builder.supplierLocators == null ? new ArrayList<>() : builder.supplierLocators;
        this.paymentDetails = builder.paymentDetails == null ? new ArrayList<>() : builder.paymentDetails;
        this.totalFarePrice = builder.totalFarePrice;
    }

    public static Builder builder() {
        return new Builder();
    }

    public UUID getItineraryBookingId() {
        return itineraryBookingId;
    }

    public String getPnr() {
        return pnr;
    }

    public String getUniversalLocator() {
        return universalLocator;
    }

    public String getReservationLocator() {
        return reservationLocator;
    }

    public Buyer getBuyer() {
        return buyer;
    }

    public List<Traveller> getTravellers() {
        return new ArrayList<>(travellers);
    }

    public BookingStatus getStatus() {
        return status;
    }

    public List<ItineraryTicket> getItineraryTickets() {
        return new ArrayList<>(itineraryTickets);
    }

    public List<SupplierLocator> getSupplierLocators() {
        return new ArrayList<>(supplierLocators);
    }

    public List<PaymentDetail> getPaymentDetails() {
        return paymentDetails;
    }

    public BigDecimal getTotalFarePrice() {
        return totalFarePrice;
    }

    public static class Builder extends ProviderItinerary.Builder<Builder> {
        private UUID itineraryBookingId;
        private String pnr;
        private String universalLocator;
        private String reservationLocator;
        private Buyer buyer;
        private List<Traveller> travellers;
        private BookingStatus status;
        private List<ItineraryTicket> itineraryTickets;
        private List<SupplierLocator> supplierLocators;
        private List<PaymentDetail> paymentDetails;
        private BigDecimal totalFarePrice;

        public Builder setItineraryBookingId(UUID itineraryBookingId) {
            this.itineraryBookingId = itineraryBookingId;
            return this;
        }

        public Builder setPnr(String pnr) {
            this.pnr = pnr;
            return this;
        }

        public Builder setUniversalLocator(String universalLocator) {
            this.universalLocator = universalLocator;
            return this;
        }

        public Builder setReservationLocator(String reservationLocator) {
            this.reservationLocator = reservationLocator;
            return this;
        }

        public Builder setBuyer(Buyer buyer) {
            this.buyer = buyer;
            return this;
        }

        public Builder setTravellers(List<Traveller> travellers) {
            this.travellers = new ArrayList<>(travellers);
            return this;
        }

        public Builder setStatus(BookingStatus status) {
            this.status = status;
            return this;
        }

        public Builder setItineraryTickets(List<ItineraryTicket> itineraryTickets) {
            this.itineraryTickets = new ArrayList<>(itineraryTickets);
            return this;
        }

        public Builder setSupplierLocators(List<SupplierLocator> supplierLocators) {
            this.supplierLocators = new ArrayList<>(supplierLocators);
            return this;
        }

        public Builder setPaymentDetails(List<PaymentDetail> paymentDetails) {
            this.paymentDetails = new ArrayList<>(paymentDetails);
            return this;
        }

        public Builder setTotalFarePrice(BigDecimal totalFarePrice) {
            this.totalFarePrice = totalFarePrice;
            return this;
        }

        @Override
        public ProviderItineraryBooking build() {
            return new ProviderItineraryBooking(this);
        }
    }
}
