package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.command;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;
import com.odigeo.suppliergatewayapi.v25.ancillaries.baggage.entity.selection.BaggageSelection;
import com.odigeo.suppliergatewayapi.v25.ancillaries.seats.entity.selection.SeatSelection;

import java.util.ArrayList;
import java.util.List;

public class CreateItineraryBookingCommand {

    private final ProviderItineraryBooking itineraryBooking;
    private final List<SeatSelection> seatSelections;
    private final List<BaggageSelection> baggageSelections;

    public CreateItineraryBookingCommand(final CreateItineraryBookingCommand.Builder builder) {
        this.itineraryBooking = builder.itineraryBooking;
        this.seatSelections = builder.seatSelections;
        this.baggageSelections = builder.baggageSelections;
    }

    public static CreateItineraryBookingCommand.Builder builder() {
        return new CreateItineraryBookingCommand.Builder();
    }

    public ProviderItineraryBooking getItineraryBooking() {
        return itineraryBooking;
    }

    public List<SeatSelection> getSeatSelections() {
        return seatSelections;
    }

    public List<BaggageSelection> getBaggageSelections() {
        return baggageSelections;
    }

    public static final class Builder {

        private ProviderItineraryBooking itineraryBooking;
        private List<SeatSelection> seatSelections;
        private List<BaggageSelection> baggageSelections;

        public Builder itineraryBooking(ProviderItineraryBooking itineraryBooking) {
            this.itineraryBooking = itineraryBooking;
            return this;
        }

        public Builder seatSelections(List<SeatSelection> seatSelections) {
            this.seatSelections = new ArrayList<>(seatSelections);
            return this;
        }

        public Builder baggageSelections(List<BaggageSelection> baggageSelections) {
            this.baggageSelections = new ArrayList<>(baggageSelections);
            return this;
        }

        public CreateItineraryBookingCommand build() {
            return new CreateItineraryBookingCommand(this);
        }
    }
}
