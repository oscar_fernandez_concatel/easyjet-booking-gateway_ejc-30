package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = CreditCardSpec.Builder.class)
public class CreditCardSpec {

    private final CreditCardScheme creditCardScheme;
    private final CreditCardCategory creditCardCategory;
    private final CreditCardProduct creditCardProduct;
    private final CreditCardLevel creditCardLevel;

    public CreditCardSpec(final CreditCardSpec.Builder builder) {
        this.creditCardScheme = builder.creditCardScheme;
        this.creditCardCategory = builder.creditCardCategory;
        this.creditCardProduct = builder.creditCardProduct;
        this.creditCardLevel = builder.creditCardLevel;
    }

    public static CreditCardSpec.Builder builder() {
        return new CreditCardSpec.Builder();
    }

    public CreditCardScheme getCreditCardScheme() {
        return creditCardScheme;
    }

    public CreditCardCategory getCreditCardCategory() {
        return creditCardCategory;
    }

    public CreditCardProduct getCreditCardProduct() {
        return creditCardProduct;
    }

    public CreditCardLevel getCreditCardLevel() {
        return creditCardLevel;
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static final class Builder {

        private CreditCardScheme creditCardScheme;
        private CreditCardCategory creditCardCategory;
        private CreditCardProduct creditCardProduct;
        private CreditCardLevel creditCardLevel;

        public Builder setCreditCardScheme(CreditCardScheme creditCardScheme) {
            this.creditCardScheme = creditCardScheme;
            return this;
        }

        public Builder setCreditCardCategory(CreditCardCategory creditCardCategory) {
            this.creditCardCategory = creditCardCategory;
            return this;
        }

        public Builder setCreditCardProduct(CreditCardProduct creditCardProduct) {
            this.creditCardProduct = creditCardProduct;
            return this;
        }

        public Builder setCreditCardLevel(CreditCardLevel creditCardLevel) {
            this.creditCardLevel = creditCardLevel;
            return this;
        }

        public CreditCardSpec build() {
            return new CreditCardSpec(this);
        }
    }
}
