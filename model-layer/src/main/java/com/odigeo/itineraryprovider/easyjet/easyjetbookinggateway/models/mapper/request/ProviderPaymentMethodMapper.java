package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCard;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderPaymentMethod;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.payment.PaymentMethod;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(uses = CreditCardMapper.class)
public interface ProviderPaymentMethodMapper {
    ProviderPaymentMethodMapper INSTANCE = Mappers.getMapper(ProviderPaymentMethodMapper.class);

    default ProviderPaymentMethod toModel(PaymentMethod paymentMethod) {
        return CreditCardMapper.INSTANCE.toModel((com.odigeo.suppliergatewayapi.v25.itinerary.booking.payment.CreditCard) paymentMethod);
    }

    default PaymentMethod toApi(ProviderPaymentMethod paymentMethod) {
        CreditCard providerPaymentMethod = ((CreditCard) paymentMethod);
        com.odigeo.suppliergatewayapi.v25.itinerary.booking.payment.CreditCard creditCard = new com.odigeo.suppliergatewayapi.v25.itinerary.booking.payment.CreditCard();
        creditCard.setCardType(providerPaymentMethod.getCardType());
        creditCard.setExpirationDate(providerPaymentMethod.getExpirationDate());
        creditCard.setHolderName(providerPaymentMethod.getHolderName());
        creditCard.setNumber(providerPaymentMethod.getNumber());
        creditCard.setVendorCode(providerPaymentMethod.getVendorCode().name());
        creditCard.setVerificationValue(providerPaymentMethod.getVerificationValue());
        return creditCard;
    }
}
