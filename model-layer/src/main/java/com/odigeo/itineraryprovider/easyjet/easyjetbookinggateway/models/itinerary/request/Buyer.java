package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

public class Buyer {

    private final String name;
    private final String mail;
    private final String phoneNumber;
    private final String countryCode;
    private final String address;
    private final String cityName;
    private final String zipCode;
    private final String lastNames;
    private final boolean primeMember;

    public Buyer(final Builder builder) {
        this.name = builder.name;
        this.mail = builder.mail;
        this.phoneNumber = builder.phoneNumber;
        this.countryCode = builder.countryCode;
        this.address = builder.address;
        this.cityName = builder.cityName;
        this.zipCode = builder.zipCode;
        this.lastNames = builder.lastNames;
        this.primeMember = builder.primeMember;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getName() {
        return name;
    }

    public String getMail() {
        return mail;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getAddress() {
        return address;
    }

    public String getCityName() {
        return cityName;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getLastNames() {
        return lastNames;
    }

    public boolean isPrimeMember() {
        return primeMember;
    }


    public static final class Builder {
        private String name;
        private String mail;
        private String phoneNumber;
        private String countryCode;
        private String address;
        private String cityName;
        private String zipCode;
        private String lastNames;
        private boolean primeMember;

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setMail(String mail) {
            this.mail = mail;
            return this;
        }

        public Builder setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        public Builder setCountryCode(String countryCode) {
            this.countryCode = countryCode;
            return this;
        }

        public Builder setAddress(String address) {
            this.address = address;
            return this;
        }

        public Builder setCityName(String cityName) {
            this.cityName = cityName;
            return this;
        }

        public Builder setZipCode(String zipCode) {
            this.zipCode = zipCode;
            return this;
        }

        public Builder setLastNames(String lastNames) {
            this.lastNames = lastNames;
            return this;
        }

        public Builder setPrimeMember(boolean primeMember) {
            this.primeMember = primeMember;
            return this;
        }

        public Buyer build() {
            return new Buyer(this);
        }
    }
}
