package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

public class ItineraryTicket extends Ticket {
    private final ItineraryId itineraryId;

    public ItineraryTicket(final Builder builder) {
        super(builder);
        this.itineraryId = builder.itineraryId;
    }

    public static Builder builder() {
        return new Builder();
    }

    public ItineraryId getItineraryId() {
        return itineraryId;
    }


    public static final class Builder extends Ticket.Builder<Builder> {
        private ItineraryId itineraryId;

        public Builder setItineraryId(ItineraryId itineraryId) {
            this.itineraryId = itineraryId;
            return this;
        }

        public ItineraryTicket build() {
            return new ItineraryTicket(this);
        }
    }
}
