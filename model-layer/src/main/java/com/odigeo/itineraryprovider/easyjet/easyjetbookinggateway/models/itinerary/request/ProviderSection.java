package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = ProviderSection.Builder.class)
public class ProviderSection {

    private final String key;
    private final String fareKey;
    private final String bookingCode;
    private final String classOfService;

    public ProviderSection(final Builder builder) {
        this.key = builder.key;
        this.fareKey = builder.fareKey;
        this.bookingCode = builder.bookingCode;
        this.classOfService = builder.classOfService;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getKey() {
        return key;
    }

    public String getFareKey() {
        return fareKey;
    }

    public String getBookingCode() {
        return bookingCode;
    }

    public String getClassOfService() {
        return classOfService;
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static final class Builder {
        private String key;
        private String fareKey;
        private String bookingCode;
        private String classOfService;

        public Builder setKey(String key) {
            this.key = key;
            return this;
        }

        public Builder setFareKey(String fareKey) {
            this.fareKey = fareKey;
            return this;
        }

        public Builder setBookingCode(String bookingCode) {
            this.bookingCode = bookingCode;
            return this;
        }

        public Builder setClassOfService(String classOfService) {
            this.classOfService = classOfService;
            return this;
        }

        public ProviderSection build() {
            return new ProviderSection(this);
        }
    }
}
