package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.odigeo.commons.uuid.UUIDGenerator;

import java.io.Serializable;
import java.util.UUID;

@JsonDeserialize(builder = ItineraryId.Builder.class)
public class ItineraryId implements Serializable {

    private final UUID id;

    public ItineraryId(final ItineraryId.Builder builder) {
        this.id = builder.id;
    }

    public static ItineraryId.Builder builder() {
        return new ItineraryId.Builder();
    }

    public UUID getId() {
        return id;
    }

    public static ItineraryId valueOf(String id) {
        UUID uuid = UUID.fromString(id);
        return new ItineraryId.Builder().setId(uuid).build();
    }

    public static ItineraryId generate() {
        return new ItineraryId.Builder().setId(UUIDGenerator.getInstance().generateUUID()).build();
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static final class Builder {

        private UUID id;

        public Builder setId(UUID id) {
            this.id = id;
            return this;
        }

        public ItineraryId build() {
            return new ItineraryId(this);
        }
    }
}
