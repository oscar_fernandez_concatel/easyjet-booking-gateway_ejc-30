package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.util.Set;

@JsonDeserialize(builder = ProviderPaymentOptions.Builder.class)
public class ProviderPaymentOptions {

    private final Set<CashPaymentOption> cashPaymentOptions;
    private final Set<CreditCardPaymentOption> creditCardPaymentOptions;

    public ProviderPaymentOptions(final ProviderPaymentOptions.Builder builder) {
        this.cashPaymentOptions = builder.cashPaymentOptions;
        this.creditCardPaymentOptions = builder.creditCardPaymentOptions;
    }

    public static ProviderPaymentOptions.Builder builder() {
        return new ProviderPaymentOptions.Builder();
    }

    public Set<CashPaymentOption> getCashPaymentOptions() {
        return cashPaymentOptions;
    }

    public Set<CreditCardPaymentOption> getCreditCardPaymentOptions() {
        return creditCardPaymentOptions;
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static final class Builder {

        private Set<CashPaymentOption> cashPaymentOptions;
        private Set<CreditCardPaymentOption> creditCardPaymentOptions;

        public Builder setCashPaymentOptions(Set<CashPaymentOption> cashPaymentOptions) {
            this.cashPaymentOptions = cashPaymentOptions;
            return this;
        }

        public Builder setCreditCardPaymentOptions(Set<CreditCardPaymentOption> creditCardPaymentOptions) {
            this.creditCardPaymentOptions = creditCardPaymentOptions;
            return this;
        }

        public ProviderPaymentOptions build() {
            return new ProviderPaymentOptions(this);
        }
    }
}
