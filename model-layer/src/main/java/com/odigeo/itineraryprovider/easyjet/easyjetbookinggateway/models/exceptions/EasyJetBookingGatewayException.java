package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions;

public class EasyJetBookingGatewayException extends RuntimeException {

    public static final String PROVIDER_ID = "U2";
    private final EasyJetBookingGatewayErrorType errorType;
    private final String message;

    public EasyJetBookingGatewayException(Throwable cause, EasyJetBookingGatewayErrorType errorType, String message) {
        super(cause);
        this.errorType = errorType;
        this.message = message;
    }

    public EasyJetBookingGatewayException(String message, EasyJetBookingGatewayErrorType errorType) {
        super(message);
        this.errorType = errorType;
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public String getProviderId() {
        return PROVIDER_ID;
    }

    public EasyJetBookingGatewayErrorType getErrorType() {
        return errorType;
    }

}
