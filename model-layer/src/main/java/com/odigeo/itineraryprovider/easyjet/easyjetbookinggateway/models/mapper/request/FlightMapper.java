package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Flight;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(uses = CarrierMapper.class)
public interface FlightMapper {
    FlightMapper INSTANCE = Mappers.getMapper(FlightMapper.class);

    Flight toModel(com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.Flight flight);

    com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.Flight toApi(Flight flight);

}
