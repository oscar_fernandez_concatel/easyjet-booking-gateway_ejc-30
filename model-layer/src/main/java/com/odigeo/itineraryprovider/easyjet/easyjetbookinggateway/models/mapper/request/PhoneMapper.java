package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Phone;
import org.apache.commons.lang3.StringUtils;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(uses = CountryMapper.class)
public interface PhoneMapper {

    PhoneMapper INSTANCE = Mappers.getMapper(PhoneMapper.class);

    default com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Phone map(Phone phone) {
        if (phone == null) {
            return null;
        }

        if (StringUtils.isBlank(phone.getNumber())) {
            return new com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Phone();
        }

        if (StringUtils.isBlank(phone.getPhoneType())) {
            return new com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Phone(
                    CountryMapper.INSTANCE.map(phone.getCountry()),
                    phone.getNumber());
        }

        return new com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Phone(
                CountryMapper.INSTANCE.map(phone.getCountry()),
                phone.getNumber(),
                phone.getPhoneType());
    }

    Phone map(com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Phone phone);

}
