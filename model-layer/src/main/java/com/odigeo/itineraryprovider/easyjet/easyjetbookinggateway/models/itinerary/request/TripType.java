package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

public enum TripType {

    ONE_WAY("ONE_WAY"),
    ROUND_TRIP("ROUNT_TRIP"),
    MULTI_SEGMENT("MULTI_SEGMENT");

    private final String value;

    TripType(String tripType) {
        this.value = tripType;
    }

    public String getValue() {
        return this.value;
    }

    public static TripType getTripType(String value) {

        TripType[] tripTypes = values();

        for (TripType currentTripType : tripTypes) {
            if (currentTripType.value.equals(value)) {
                return currentTripType;
            }
        }

        throw new IllegalArgumentException(value);
    }
}
