package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request;


import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CabinClass;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FareType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Flight;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightFareInformation;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightFareInformationPassenger;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightSection;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;
import com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.Section;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.Map;

@Mapper
public interface FlightSectionMapper {

    FlightSectionMapper INSTANCE = Mappers.getMapper(FlightSectionMapper.class);

    default FlightSection toModel(Section section) {

        Flight flight = FlightMapper.INSTANCE.toModel(((com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.FlightSection) section).getFlight());

        Map<String, FlightFareInformationPassenger> map = FlightFareInformationMapper.INSTANCE.toModel(((com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.FlightSection) section).getFlightFareInformations());
        FlightFareInformation flightFareInformation = FlightFareInformation.builder()
                .setFlightFareInformationPassenger(map)
                .setFareType(getSectionFareType(map))
                .build();

        return FlightSection.builder()
                .setId(itineraryIdToItineraryId(section.getId()))
                .setArrivalGeoNodeId(section.getArrivalGeoNodeId())
                .setDepartureGeoNodeId(section.getDepartureGeoNodeId())
                .setArrivalTerminal(section.getArrivalTerminal())
                .setDepartureTerminal(section.getDepartureTerminal())
                .setCabinClass(CabinClass.getCabinClass(section.getCabinClass()))
                .setDepartureDate(section.getDepartureDate())
                .setArrivalDate(section.getArrivalDate())
                .setSegmentPosition(section.getSegmentPosition())
                .setProductType(section.getProductType())
                .setBaggageAllowance(BaggageAllowanceMapper.INSTANCE.toModel(section.getBaggageAllowance()))
                .setFlight(flight)
                .setFlightFareInformation(flightFareInformation)
                .build();
    }

    default FareType getSectionFareType(Map<String, FlightFareInformationPassenger> map) {
        if (map == null) {
            return FareType.PUBLIC;
        }
        return map.values().stream()
                .map(FlightFareInformationPassenger::getFareType)
                .reduce(FareType.PUBLIC, FareType::compose);
    }

    ItineraryId itineraryIdToItineraryId(com.odigeo.suppliergatewayapi.v25.itinerary.ItineraryId itineraryId);


    default com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.FlightSection toApi(FlightSection flightSection) {
        com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.Flight flight = FlightMapper.INSTANCE.toApi(flightSection.getFlight());

        com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.FlightSection fSection = new com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.FlightSection();
        fSection.setFlight(flight);
        fSection.setId(itineraryIdToItineraryId(flightSection.getId()));
        fSection.setArrivalGeoNodeId(flightSection.getArrivalGeoNodeId());
        fSection.setDepartureGeoNodeId(flightSection.getDepartureGeoNodeId());
        fSection.setArrivalTerminal(flightSection.getArrivalTerminal());
        fSection.setDepartureTerminal(flightSection.getDepartureTerminal());
        fSection.setCabinClass(flightSection.getCabinClass().getName());
        fSection.setArrivalDate(flightSection.getArrivalDate());
        fSection.setDepartureDate(flightSection.getDepartureDate());
        fSection.setSegmentPosition(flightSection.getSegmentPosition());
        fSection.setProductType(flightSection.getProductType());

        fSection.setFlightFareInformations(FlightFareInformationMapper.INSTANCE.toApi(flightSection.getFlightFareInformation().getFlightFareInformationPassenger()));
        fSection.setBaggageAllowance(BaggageAllowanceMapper.INSTANCE.toApi(flightSection.getBaggageAllowance()));
        return fSection;
    }

    com.odigeo.suppliergatewayapi.v25.itinerary.ItineraryId itineraryIdToItineraryId(ItineraryId itineraryId);
}
