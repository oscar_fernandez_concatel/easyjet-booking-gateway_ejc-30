package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import java.util.UUID;

public class ItineraryBookingId {

    private final UUID id;

    public ItineraryBookingId(final ItineraryBookingId.Builder<?> builder) {
        this.id = builder.id;
    }

    public static ItineraryBookingId.Builder builder() {
        return new ItineraryBookingId.Builder();
    }

    public UUID getId() {
        return id;
    }

    public static class Builder<T extends Builder<T>> {

        private UUID id;

        public T setId(UUID id) {
            this.id = id;
            return (T) this;
        }

        public ItineraryBookingId build() {
            return new ItineraryBookingId(this);
        }
    }
}
