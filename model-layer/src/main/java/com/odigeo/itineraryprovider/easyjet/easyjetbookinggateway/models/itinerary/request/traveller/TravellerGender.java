package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller;

public enum TravellerGender {
    MALE,
    FEMALE;
}
