package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PaymentItineraryBookingId;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PaymentItineraryBookingIdMapper {

    PaymentItineraryBookingIdMapper INSTANCE = Mappers.getMapper(PaymentItineraryBookingIdMapper.class);

    PaymentItineraryBookingId toModel(final com.odigeo.suppliergatewayapi.v25.itinerary.booking.payment.PaymentItineraryBookingId paymentItineraryBookingId);
}
