package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request;

import com.odigeo.suppliergatewayapi.v25.itinerary.booking.closing.IssuedItineraryBooking;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {FlightSectionMapper.class, PhoneMapper.class})
public interface IssuedItineraryBookingMapper {

    IssuedItineraryBookingMapper INSTANCE = Mappers.getMapper(IssuedItineraryBookingMapper.class);

    IssuedItineraryBooking toApi(final com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.IssuedItineraryBooking issuedItineraryBooking);
}
