package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;


public enum CabinClass {

    ECONOMIC_DISCOUNTED,
    PREMIUM_ECONOMY,
    TOURIST,
    BUSINESS,
    FIRST;

    public static CabinClass getCabinClass(String name) {
        if (name != null) {
            for (CabinClass cabinClass : CabinClass.values()) {
                if (cabinClass.toString().equalsIgnoreCase(name)) {
                    return cabinClass;
                }
            }
        }
        return null;
    }

    public String getName() {
        return this.name();
    }

}
