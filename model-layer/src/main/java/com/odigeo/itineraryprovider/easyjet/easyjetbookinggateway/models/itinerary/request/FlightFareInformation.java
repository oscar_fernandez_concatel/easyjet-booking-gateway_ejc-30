package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.util.Map;

@JsonDeserialize(builder = FlightFareInformation.Builder.class)
public class FlightFareInformation {

    private final FareType fareType;
    private final Map<String, FlightFareInformationPassenger> flightFareInformationPassenger;

    public FlightFareInformation(final FlightFareInformation.Builder builder) {
        this.fareType = builder.fareType;
        this.flightFareInformationPassenger = builder.flightFareInformationPassenger;
    }

    public static FlightFareInformation.Builder builder() {
        return new FlightFareInformation.Builder();
    }

    public FareType getFareType() {
        return fareType;
    }

    public Map<String, FlightFareInformationPassenger> getFlightFareInformationPassenger() {
        return flightFareInformationPassenger;
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static final class Builder {

        private FareType fareType;
        private Map<String, FlightFareInformationPassenger> flightFareInformationPassenger;

        public Builder setFareType(FareType fareType) {
            this.fareType = fareType;
            return this;
        }

        public Builder setFlightFareInformationPassenger(Map<String, FlightFareInformationPassenger> flightFareInformationPassenger) {
            this.flightFareInformationPassenger = flightFareInformationPassenger;
            return this;
        }

        public FlightFareInformation build() {
            return new FlightFareInformation(this);
        }
    }
}
