package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import java.util.UUID;

public class SupplierLocator {
    private final String supplierCode;
    private final String supplierLocatorCode;
    private final UUID itineraryBookingId;

    public SupplierLocator(final Builder builder) {
        this.supplierCode = builder.supplierCode;
        this.supplierLocatorCode = builder.supplierLocatorCode;
        this.itineraryBookingId = builder.itineraryBookingId;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getSupplierCode() {
        return supplierCode;
    }

    public String getSupplierLocatorCode() {
        return supplierLocatorCode;
    }

    public UUID getItineraryBookingId() {
        return itineraryBookingId;
    }

    public static final class Builder {
        private String supplierCode;
        private String supplierLocatorCode;
        private UUID itineraryBookingId;

        public Builder setSupplierCode(String supplierCode) {
            this.supplierCode = supplierCode;
            return this;
        }

        public Builder setSupplierLocatorCode(String supplierLocatorCode) {
            this.supplierLocatorCode = supplierLocatorCode;
            return this;
        }

        public Builder setItineraryBookingId(UUID itineraryBookingId) {
            this.itineraryBookingId = itineraryBookingId;
            return this;
        }

        public SupplierLocator build() {
            return new SupplierLocator(this);
        }
    }
}
