package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions;

import java.util.HashMap;
import java.util.Map;

public enum EasyJetBookingGatewayErrorType {

    UNKNOWN(-1),
    NOT_EXISTING_CODE(0),
    ERROR_IN_ADAPTER(1),
    PAYMENT_TYPE_UNDEFINED(2),
    RUNTIME_ERROR(3),
    DATABASE_ERROR(4),
    REDIS_DESERIALIZATION_EXCEPTION(5),
    ITINERARY_NOT_FOUND(6),
    ERROR_ON_BUILD_ADAPTER_REQUEST(7),
    PAYMENT_VENDOR_CODE_NOT_SUPPORTED(8),
    PAYMENT_METHOD_NOT_SPECIFIED(9),
    PAYMENT_INSTRUMENT_EXCEPTION(10);

    private static final Map<Integer, EasyJetBookingGatewayErrorType> VALUES_MAP = new HashMap<>();

    static {
        for (final EasyJetBookingGatewayErrorType errorType : EasyJetBookingGatewayErrorType.values()) {
            VALUES_MAP.put(errorType.getErrorCode(), errorType);
        }
    }

    private final int errorCode;

    EasyJetBookingGatewayErrorType(final int errorCode) {
        this.errorCode = errorCode;
    }


    public int getErrorCode() {
        return errorCode;
    }

    public static EasyJetBookingGatewayErrorType getErrorType(final int id) {
        EasyJetBookingGatewayErrorType errorType = VALUES_MAP.get(id);
        if (errorType == null) {
            errorType = NOT_EXISTING_CODE;
        }
        return errorType;
    }

}
