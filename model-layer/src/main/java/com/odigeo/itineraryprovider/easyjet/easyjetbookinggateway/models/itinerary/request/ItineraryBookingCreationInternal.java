package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Traveller;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ItineraryBookingCreationInternal {

    private final UUID itineraryId;
    private final List<Traveller> travellers;
    private final Buyer buyer;


    private ItineraryBookingCreationInternal(final ItineraryBookingCreationInternal.Builder builder) {
        this.itineraryId = builder.itineraryId;
        this.travellers = builder.travellers;
        this.buyer = builder.buyer;
    }

    public static ItineraryBookingCreationInternal.Builder builder() {
        return new ItineraryBookingCreationInternal.Builder();
    }

    public UUID getItineraryId() {
        return itineraryId;
    }

    public List<Traveller> getTravellers() {
        return new ArrayList<>(travellers);
    }

    public Buyer getBuyer() {
        return buyer;
    }

    public static final class Builder {

        private UUID itineraryId;
        private List<Traveller> travellers;
        private Buyer buyer;

        public ItineraryBookingCreationInternal.Builder setSelectedItineraryId(UUID itineraryId) {
            this.itineraryId = itineraryId;
            return this;
        }

        public ItineraryBookingCreationInternal.Builder setTravellers(List<Traveller> travellers) {
            this.travellers = new ArrayList<>(travellers);
            return this;
        }

        public ItineraryBookingCreationInternal.Builder setBuyer(Buyer buyer) {
            this.buyer = buyer;
            return this;
        }

        public ItineraryBookingCreationInternal build() {
            return new ItineraryBookingCreationInternal(this);
        }
    }
}
