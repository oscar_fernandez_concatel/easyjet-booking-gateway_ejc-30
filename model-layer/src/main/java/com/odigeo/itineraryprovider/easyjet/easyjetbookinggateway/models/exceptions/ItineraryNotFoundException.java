package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions;

public class ItineraryNotFoundException extends EasyJetBookingGatewayException {

    public ItineraryNotFoundException(final String message, final EasyJetBookingGatewayErrorType errorType) {
        super(message, errorType);
    }
}
