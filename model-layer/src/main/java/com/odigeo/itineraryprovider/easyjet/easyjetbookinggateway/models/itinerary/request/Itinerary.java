package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

@JsonDeserialize(builder = Itinerary.Builder.class)
public class Itinerary implements Serializable {

    private final ItineraryId id;
    private final List<Segment> segments;
    private final Integer departureGeoNodeId;
    private final Integer arrivalGeoNodeId;
    private final LocalDateTime departureDate;
    private final LocalDateTime returnDate;
    private final FareDetails fareDetails;
    private final TripType tripType;
    private final ProviderPaymentOptions providerPaymentOptions;
    private final Carrier defaultValidatingCarrier;

    public Itinerary(final Itinerary.Builder builder) {
        this.id = builder.id;
        this.segments = builder.segments != null ? builder.segments : new ArrayList<>();
        this.departureGeoNodeId = builder.departureGeoNodeId;
        this.arrivalGeoNodeId = builder.arrivalGeoNodeId;
        this.departureDate = builder.departureDate;
        this.returnDate = builder.returnDate;
        this.fareDetails = builder.fareDetails;
        this.tripType = builder.tripType;
        this.providerPaymentOptions = builder.providerPaymentOptions;
        this.defaultValidatingCarrier = builder.defaultValidatingCarrier;
    }

    public static Itinerary.Builder builder() {
        return new Itinerary.Builder();
    }

    public LocalDateTime getDepartureDate() {
        return departureDate;
    }

    public LocalDateTime getReturnDate() {
        return returnDate;
    }

    public Integer getDepartureGeoNodeId() {
        return departureGeoNodeId;
    }

    public Integer getArrivalGeoNodeId() {
        return arrivalGeoNodeId;
    }

    public TripType getTripType() {
        return tripType;
    }

    public FareDetails getFareDetails() {
        return fareDetails;
    }

    public ItineraryId getId() {
        return id;
    }

    public List<Segment> getSegments() {
        return new ArrayList<>(segments);
    }

    public ProviderPaymentOptions getProviderPaymentOptions() {
        return providerPaymentOptions;
    }

    public Carrier getDefaultValidatingCarrier() {
        return defaultValidatingCarrier;
    }

    public List<FlightSection> getSections() {
        return getSegments().stream()
                .flatMap(segment -> segment.getSections().stream())
                .collect(toList());
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static final class Builder {

        private ItineraryId id;
        private List<Segment> segments;
        private Integer departureGeoNodeId;
        private Integer arrivalGeoNodeId;
        private LocalDateTime departureDate;
        private LocalDateTime returnDate;
        private FareDetails fareDetails;
        private TripType tripType;
        private ProviderPaymentOptions providerPaymentOptions;
        private Carrier defaultValidatingCarrier;

        public Builder setId(ItineraryId id) {
            this.id = id;
            return this;
        }

        public Builder setSegments(List<Segment> segments) {
            this.segments = segments;
            return this;
        }

        public Builder setDepartureGeoNodeId(Integer departureGeoNodeId) {
            this.departureGeoNodeId = departureGeoNodeId;
            return this;
        }

        public Builder setArrivalGeoNodeId(Integer arrivalGeoNodeId) {
            this.arrivalGeoNodeId = arrivalGeoNodeId;
            return this;
        }

        public Builder setDepartureDate(LocalDateTime departureDate) {
            this.departureDate = departureDate;
            return this;
        }

        public Builder setReturnDate(LocalDateTime returnDate) {
            this.returnDate = returnDate;
            return this;
        }

        public Builder setFareDetails(FareDetails fareDetails) {
            this.fareDetails = fareDetails;
            return this;
        }

        public Builder setTripType(TripType tripType) {
            this.tripType = tripType;
            return this;
        }

        public Builder setProviderPaymentOptions(ProviderPaymentOptions providerPaymentOptions) {
            this.providerPaymentOptions = providerPaymentOptions;
            return this;
        }

        public Builder setDefaultValidatingCarrier(Carrier defaultValidatingCarrier) {
            this.defaultValidatingCarrier = defaultValidatingCarrier;
            return this;
        }

        public Itinerary build() {
            return new Itinerary(this);
        }

    }

}
