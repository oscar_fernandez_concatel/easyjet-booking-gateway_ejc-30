package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

public enum FareClass {
    Y, // Standard Fare Class
    M, // Flexi Fare Class
    W, // Flexi Fare Class
    B; // Inclusive Fare Class

    public static FareClass getValue(String representation) {
        switch (representation) {
        case "Y":
            return Y;
        case "M":
            return M;
        case "W":
            return W;
        case "B":
            return B;
        default:
            return Y;
        }
    }

}
