package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller;

public class Continent {
    private final int continentId;

    public Continent(final Builder builder) {
        this.continentId = builder.continentId;
    }

    public static Builder builder() {
        return new Builder();
    }

    public int getContinentId() {
        return continentId;
    }

    public static final class Builder {
        private int continentId;

        public Builder setContinentId(int continentId) {
            this.continentId = continentId;
            return this;
        }

        public Continent build() {
            return new Continent(this);
        }
    }
}
