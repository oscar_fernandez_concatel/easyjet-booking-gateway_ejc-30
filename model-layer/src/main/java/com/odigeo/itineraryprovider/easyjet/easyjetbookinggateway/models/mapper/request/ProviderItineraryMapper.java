package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItinerary;
import com.odigeo.suppliergatewayapi.v25.itinerary.selection.SelectedItinerary;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {FlightSectionMapper.class, ProviderPaymentOptionsMapper.class})
public interface ProviderItineraryMapper {
    ProviderItineraryMapper INSTANCE = Mappers.getMapper(ProviderItineraryMapper.class);

    @Mapping(target = "selectedItineraryId", source = "itinerary.id")
    @Mapping(target = "itinerary.fareDetails.passengerFares", source = "itinerary.fareDetails.passengerFares")
    @Mapping(target = "itinerary.providerPaymentOptions", source = "itinerary.providerPaymentOptions")
    SelectedItinerary toApi(final ProviderItinerary itinerary);

}
