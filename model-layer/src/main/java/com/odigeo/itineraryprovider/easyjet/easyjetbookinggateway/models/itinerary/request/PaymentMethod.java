package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

public class PaymentMethod {
    private final PaymentMethodType paymentMethodType;

    public PaymentMethod(final Builder builder) {
        this.paymentMethodType = builder.paymentMethodType;
    }

    public static Builder builder() {
        return new Builder();
    }

    public PaymentMethodType getPaymentMethodType() {
        return paymentMethodType;
    }


    public static final class Builder {
        private PaymentMethodType paymentMethodType;

        public Builder setPaymentMethodType(PaymentMethodType paymentMethodType) {
            this.paymentMethodType = paymentMethodType;
            return this;
        }

        public PaymentMethod build() {
            return new PaymentMethod(this);
        }
    }
}
