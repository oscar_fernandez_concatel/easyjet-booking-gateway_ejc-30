package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller;

public class Country {
    private final String countryCode;
    private final String countryCode3Letters;
    private final int numCountryCode;
    private final Continent continent;
    private final String phonePrefix;

    public Country(final Builder builder) {
        this.countryCode = builder.countryCode;
        this.countryCode3Letters = builder.countryCode3Letters;
        this.numCountryCode = builder.numCountryCode;
        this.continent = builder.continent;
        this.phonePrefix = builder.phonePrefix;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getCountryCode3Letters() {
        return countryCode3Letters;
    }

    public int getNumCountryCode() {
        return numCountryCode;
    }

    public Continent getContinent() {
        return continent;
    }

    public String getPhonePrefix() {
        return phonePrefix;
    }


    public static final class Builder {
        private String countryCode;
        private String countryCode3Letters;
        private int numCountryCode;
        private Continent continent;
        private String phonePrefix;

        public Builder setCountryCode(String countryCode) {
            this.countryCode = countryCode;
            return this;
        }

        public Builder setCountryCode3Letters(String countryCode3Letters) {
            this.countryCode3Letters = countryCode3Letters;
            return this;
        }

        public Builder setNumCountryCode(int numCountryCode) {
            this.numCountryCode = numCountryCode;
            return this;
        }

        public Builder setContinent(Continent continent) {
            this.continent = continent;
            return this;
        }

        public Builder setPhonePrefix(String phonePrefix) {
            this.phonePrefix = phonePrefix;
            return this;
        }

        public Country build() {
            return new Country(this);
        }
    }
}
