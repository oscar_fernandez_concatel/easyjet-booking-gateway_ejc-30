package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TravellerType;

import java.time.LocalDate;
import java.util.UUID;

public class Traveller {

    private final TravellerId id;
    private final Integer travellerNumber;
    private final TravellerType travellerType;
    private final TravellerTitle title;
    private final String name;
    private final String middleName;
    private final String firstLastName;
    private final String secondLastName;
    private final TravellerGender travellerGender;
    private final LocalDate dateOfBirth;
    private final Integer age;
    private final String nationalityCountryCode;
    private final String countryCodeOfResidence;
    private final String identification;
    private final TravellerIdentificationType identificationType;
    private final LocalDate identificationExpirationDate;
    private final String identificationIssueCountryCode;
    private final String localityCodeOfResidence;
    private final Phone phone;
    private final String key;
    private final UUID itineraryBookingId;

    public Traveller(final Builder builder) {
        this.id = builder.id;
        this.travellerNumber = builder.travellerNumber;
        this.travellerType = builder.travellerType;
        this.title = builder.title;
        this.name = builder.name;
        this.middleName = builder.middleName;
        this.firstLastName = builder.firstLastName;
        this.secondLastName = builder.secondLastName;
        this.travellerGender = builder.travellerGender;
        this.dateOfBirth = builder.dateOfBirth;
        this.age = builder.age;
        this.nationalityCountryCode = builder.nationalityCountryCode;
        this.countryCodeOfResidence = builder.countryCodeOfResidence;
        this.identification = builder.identification;
        this.identificationType = builder.identificationType;
        this.identificationExpirationDate = builder.identificationExpirationDate;
        this.identificationIssueCountryCode = builder.identificationIssueCountryCode;
        this.localityCodeOfResidence = builder.localityCodeOfResidence;
        this.phone = builder.phone;
        this.key = builder.key;
        this.itineraryBookingId = builder.itineraryBookingId;
    }

    public static Builder builder() {
        return new Builder();
    }

    public TravellerId getId() {
        return id;
    }

    public Integer getTravellerNumber() {
        return travellerNumber;
    }

    public TravellerType getTravellerType() {
        return travellerType;
    }

    public TravellerTitle getTitle() {
        return title;
    }

    public String getName() {
        return name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getFirstLastName() {
        return firstLastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public TravellerGender getTravellerGender() {
        return travellerGender;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public Integer getAge() {
        return age;
    }

    public String getNationalityCountryCode() {
        return nationalityCountryCode;
    }

    public String getCountryCodeOfResidence() {
        return countryCodeOfResidence;
    }

    public String getIdentification() {
        return identification;
    }

    public TravellerIdentificationType getIdentificationType() {
        return identificationType;
    }

    public LocalDate getIdentificationExpirationDate() {
        return identificationExpirationDate;
    }

    public String getIdentificationIssueCountryCode() {
        return identificationIssueCountryCode;
    }

    public String getLocalityCodeOfResidence() {
        return localityCodeOfResidence;
    }

    public Phone getPhone() {
        return phone;
    }

    public String getKey() {
        return key;
    }

    public UUID getItineraryBookingId() {
        return itineraryBookingId;
    }


    public static final class Builder {
        private TravellerId id;
        private Integer travellerNumber;
        private TravellerType travellerType;
        private TravellerTitle title;
        private String name;
        private String middleName;
        private String firstLastName;
        private String secondLastName;
        private TravellerGender travellerGender;
        private LocalDate dateOfBirth;
        private Integer age;
        private String nationalityCountryCode;
        private String countryCodeOfResidence;
        private String identification;
        private TravellerIdentificationType identificationType;
        private LocalDate identificationExpirationDate;
        private String identificationIssueCountryCode;
        private String localityCodeOfResidence;
        private Phone phone;
        private String key;
        private UUID itineraryBookingId;

        public Builder setId(TravellerId id) {
            this.id = id;
            return this;
        }

        public Builder setTravellerNumber(Integer travellerNumber) {
            this.travellerNumber = travellerNumber;
            return this;
        }

        public Builder setTravellerType(TravellerType travellerType) {
            this.travellerType = travellerType;
            return this;
        }

        public Builder setTitle(TravellerTitle title) {
            this.title = title;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setMiddleName(String middleName) {
            this.middleName = middleName;
            return this;
        }

        public Builder setFirstLastName(String firstLastName) {
            this.firstLastName = firstLastName;
            return this;
        }

        public Builder setSecondLastName(String secondLastName) {
            this.secondLastName = secondLastName;
            return this;
        }

        public Builder setTravellerGender(TravellerGender travellerGender) {
            this.travellerGender = travellerGender;
            return this;
        }

        public Builder setDateOfBirth(LocalDate dateOfBirth) {
            this.dateOfBirth = dateOfBirth;
            return this;
        }

        public Builder setAge(Integer age) {
            this.age = age;
            return this;
        }

        public Builder setNationalityCountryCode(String nationalityCountryCode) {
            this.nationalityCountryCode = nationalityCountryCode;
            return this;
        }

        public Builder setCountryCodeOfResidence(String countryCodeOfResidence) {
            this.countryCodeOfResidence = countryCodeOfResidence;
            return this;
        }

        public Builder setIdentification(String identification) {
            this.identification = identification;
            return this;
        }

        public Builder setIdentificationType(TravellerIdentificationType identificationType) {
            this.identificationType = identificationType;
            return this;
        }

        public Builder setIdentificationExpirationDate(LocalDate identificationExpirationDate) {
            this.identificationExpirationDate = identificationExpirationDate;
            return this;
        }

        public Builder setIdentificationIssueCountryCode(String identificationIssueCountryCode) {
            this.identificationIssueCountryCode = identificationIssueCountryCode;
            return this;
        }

        public Builder setLocalityCodeOfResidence(String localityCodeOfResidence) {
            this.localityCodeOfResidence = localityCodeOfResidence;
            return this;
        }

        public Builder setPhone(Phone phone) {
            this.phone = phone;
            return this;
        }

        public Builder setKey(String key) {
            this.key = key;
            return this;
        }

        public Builder setItineraryBookingId(UUID itineraryBookingId) {
            this.itineraryBookingId = itineraryBookingId;
            return this;
        }

        public Traveller build() {
            return new Traveller(this);
        }
    }
}
