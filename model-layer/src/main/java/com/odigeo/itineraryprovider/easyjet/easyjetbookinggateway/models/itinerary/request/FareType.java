package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

public enum FareType {
    PUBLIC("P"),
    NEGOTIATED("N"),
    PRIVATE("V");

    private String code;

    FareType(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }

    public FareType compose(FareType aFareType) {
        return aFareType.compareTo(this) > 0 ? aFareType : this;
    }

    public static FareType getFareTypeByCode(String fareType) {
        if (fareType != null) {
            FareType[] var1 = values();
            int var2 = var1.length;

            for (int var3 = 0; var3 < var2; ++var3) {
                FareType type = var1[var3];
                if (type.getCode().equalsIgnoreCase(fareType)) {
                    return type;
                }
            }
        }

        throw new IllegalArgumentException(fareType);
    }
}
