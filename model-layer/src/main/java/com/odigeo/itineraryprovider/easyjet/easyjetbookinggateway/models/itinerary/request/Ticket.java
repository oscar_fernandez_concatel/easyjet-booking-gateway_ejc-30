package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import java.util.List;

public class Ticket {
    private final TicketId id;
    private final String number;
    private final String travellerKey;
    private final Money totalAmount;
    private final Money taxes;
    private final Money commission;
    private final String status;
    private final ItineraryBookingId itineraryBookingId;
    private final Integer numPassenger;
    private final Carrier validatingCarrier;
    private final String iataCode;
    private final String officeId;
    private final List<TicketAction> ticketActions;
    private final List<TaxDetail> taxDetails;

    public Ticket(final Builder<?> builder) {
        this.id = builder.id;
        this.number = builder.number;
        this.travellerKey = builder.travellerKey;
        this.totalAmount = builder.totalAmount;
        this.taxes = builder.taxes;
        this.commission = builder.commission;
        this.status = builder.status;
        this.itineraryBookingId = builder.itineraryBookingId;
        this.numPassenger = builder.numPassenger;
        this.validatingCarrier = builder.validatingCarrier;
        this.iataCode = builder.iataCode;
        this.officeId = builder.officeId;
        this.ticketActions = builder.ticketActions;
        this.taxDetails = builder.taxDetails;
    }

    public static Builder builder() {
        return new Builder();
    }

    public TicketId getId() {
        return id;
    }

    public String getNumber() {
        return number;
    }

    public String getTravellerKey() {
        return travellerKey;
    }

    public Money getTotalAmount() {
        return totalAmount;
    }

    public Money getTaxes() {
        return taxes;
    }

    public Money getCommission() {
        return commission;
    }

    public String getStatus() {
        return status;
    }

    public ItineraryBookingId getItineraryBookingId() {
        return itineraryBookingId;
    }

    public Integer getNumPassenger() {
        return numPassenger;
    }

    public Carrier getValidatingCarrier() {
        return validatingCarrier;
    }

    public String getIataCode() {
        return iataCode;
    }

    public String getOfficeId() {
        return officeId;
    }

    public List<TicketAction> getTicketActions() {
        return ticketActions;
    }

    public List<TaxDetail> getTaxDetails() {
        return taxDetails;
    }


    public static class Builder<T extends Builder<T>> {
        private TicketId id;
        private String number;
        private String travellerKey;
        private Money totalAmount;
        private Money taxes;
        private Money commission;
        private String status;
        private ItineraryBookingId itineraryBookingId;
        private Integer numPassenger;
        private Carrier validatingCarrier;
        private String iataCode;
        private String officeId;
        private List<TicketAction> ticketActions;
        private List<TaxDetail> taxDetails;

        public Builder setId(TicketId id) {
            this.id = id;
            return this;
        }

        public Builder setNumber(String number) {
            this.number = number;
            return this;
        }

        public Builder setTravellerKey(String travellerKey) {
            this.travellerKey = travellerKey;
            return this;
        }

        public Builder setTotalAmount(Money totalAmount) {
            this.totalAmount = totalAmount;
            return this;
        }

        public Builder setTaxes(Money taxes) {
            this.taxes = taxes;
            return this;
        }

        public Builder setCommission(Money commission) {
            this.commission = commission;
            return this;
        }

        public Builder setStatus(String status) {
            this.status = status;
            return this;
        }

        public Builder setItineraryBookingId(ItineraryBookingId itineraryBookingId) {
            this.itineraryBookingId = itineraryBookingId;
            return this;
        }

        public Builder setNumPassenger(Integer numPassenger) {
            this.numPassenger = numPassenger;
            return this;
        }

        public Builder setValidatingCarrier(Carrier validatingCarrier) {
            this.validatingCarrier = validatingCarrier;
            return this;
        }

        public Builder setIataCode(String iataCode) {
            this.iataCode = iataCode;
            return this;
        }

        public Builder setOfficeId(String officeId) {
            this.officeId = officeId;
            return this;
        }

        public Builder setTicketActions(List<TicketAction> ticketActions) {
            this.ticketActions = ticketActions;
            return this;
        }

        public Builder setTaxDetails(List<TaxDetail> taxDetails) {
            this.taxDetails = taxDetails;
            return this;
        }

        public Ticket build() {
            return new Ticket(this);
        }
    }
}
