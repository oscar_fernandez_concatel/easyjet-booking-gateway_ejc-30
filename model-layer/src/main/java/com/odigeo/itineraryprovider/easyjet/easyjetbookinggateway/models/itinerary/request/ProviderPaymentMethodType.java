package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

public enum ProviderPaymentMethodType {
    CASH("Cash"),
    CREDITCARD("Credit"),
    SECURE3D("Secure3D"),
    INSTALLMENT("Installment"),
    DELAYED_PAYMENT("DelayedPayment"),
    NO_PAYMENT_NEEDED("NoPaymentNeeded"),
    TRAVEL_ACCOUNT("TravelAccount");

    private final String code;

    ProviderPaymentMethodType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
