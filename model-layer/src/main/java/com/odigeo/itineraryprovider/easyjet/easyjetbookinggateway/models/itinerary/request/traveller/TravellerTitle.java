package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller;

import com.edreams.base.MissingElementException;

public enum TravellerTitle {
    MR("Mr"),
    MRS("Mrs"),
    MS("Ms");

    private final String databaseCode;

    TravellerTitle(String databaseCode) {
        this.databaseCode = databaseCode;
    }

    public String getDatabaseCode() {
        return databaseCode;
    }

    public static TravellerTitle getValue(String type) throws MissingElementException {
        if (MR.databaseCode.equals(type)) {
            return MR;
        } else if (MRS.databaseCode.equals(type)) {
            return MRS;
        } else if (MS.databaseCode.equals(type)) {
            return MS;
        }
        throw new MissingElementException("Missing TravellerTitle Element for string code: " + type);
    }
}
