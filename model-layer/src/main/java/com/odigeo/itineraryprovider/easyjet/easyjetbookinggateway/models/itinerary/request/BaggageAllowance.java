package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = BaggageAllowance.Builder.class)
public class BaggageAllowance {

    private final BaggageAllowanceType baggageAllowanceType;
    private final Integer quantity;

    public BaggageAllowance(final BaggageAllowance.Builder builder) {
        this.baggageAllowanceType = builder.baggageAllowanceType;
        this.quantity = builder.quantity;
    }

    public static BaggageAllowance.Builder builder() {
        return new BaggageAllowance.Builder();
    }

    public BaggageAllowanceType getBaggageAllowanceType() {
        return baggageAllowanceType;
    }

    public Integer getQuantity() {
        return quantity;
    }

    @JsonPOJOBuilder(withPrefix = "set")
    public static final class Builder {

        private BaggageAllowanceType baggageAllowanceType;
        private Integer quantity;

        public Builder setBaggageAllowanceType(BaggageAllowanceType baggageAllowanceType) {
            this.baggageAllowanceType = baggageAllowanceType;
            return this;
        }

        public Builder setQuantity(Integer quantity) {
            this.quantity = quantity;
            return this;
        }

        public BaggageAllowance build() {
            return new BaggageAllowance(this);
        }
    }
}
