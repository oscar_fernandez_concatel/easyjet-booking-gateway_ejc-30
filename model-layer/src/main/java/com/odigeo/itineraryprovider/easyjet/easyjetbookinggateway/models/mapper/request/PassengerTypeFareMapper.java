package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PassengerTypeFare;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {MoneyMapper.class})
public interface PassengerTypeFareMapper {
    PassengerTypeFareMapper INSTANCE = Mappers.getMapper(PassengerTypeFareMapper.class);

    PassengerTypeFare toModel(final com.odigeo.suppliergatewayapi.v25.itinerary.fare.PassengerTypeFare passengerTypeFare);
}
