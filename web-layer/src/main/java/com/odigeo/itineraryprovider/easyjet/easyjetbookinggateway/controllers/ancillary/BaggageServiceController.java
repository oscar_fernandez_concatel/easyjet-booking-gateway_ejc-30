package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.controllers.ancillary;

import com.odigeo.suppliergatewayapi.v25.ancillaries.baggage.BaggageService;
import com.odigeo.suppliergatewayapi.v25.ancillaries.baggage.entity.BaggageProduct;
import com.odigeo.suppliergatewayapi.v25.ancillaries.baggage.entity.option.combined.CombinedCloseBaggageOptions;
import com.odigeo.suppliergatewayapi.v25.ancillaries.baggage.entity.option.combined.CombinedCloseItineraryBaggageOption;
import com.odigeo.suppliergatewayapi.v25.ancillaries.baggage.entity.option.combined.CombinedCloseSectionBaggageOption;
import com.odigeo.suppliergatewayapi.v25.ancillaries.baggage.entity.option.combined.CombinedCloseSegmentBaggageOption;
import com.odigeo.suppliergatewayapi.v25.ancillaries.baggage.entity.option.standalone.StandAloneCloseBaggageOptions;
import com.odigeo.suppliergatewayapi.v25.ancillaries.baggage.entity.option.standalone.StandAloneCloseItineraryBaggageOption;
import com.odigeo.suppliergatewayapi.v25.ancillaries.baggage.entity.option.standalone.StandAloneCloseSectionBaggageOption;
import com.odigeo.suppliergatewayapi.v25.ancillaries.baggage.entity.option.standalone.StandAloneCloseSegmentBaggageOption;
import com.odigeo.suppliergatewayapi.v25.ancillaries.baggage.entity.payment.PaymentBaggageSelections;
import com.odigeo.suppliergatewayapi.v25.ancillaries.baggage.entity.payment.PaymentBaggageSelectionsId;
import com.odigeo.suppliergatewayapi.v25.ancillaries.baggage.entity.selection.combined.CombinedCloseItineraryBaggageSelection;
import com.odigeo.suppliergatewayapi.v25.ancillaries.baggage.entity.selection.combined.CombinedCloseSectionBaggageSelection;
import com.odigeo.suppliergatewayapi.v25.ancillaries.baggage.entity.selection.combined.CombinedCloseSegmentBaggageSelection;
import com.odigeo.suppliergatewayapi.v25.ancillaries.baggage.entity.selection.standalone.StandAloneCloseItineraryBaggageSelection;
import com.odigeo.suppliergatewayapi.v25.ancillaries.baggage.entity.selection.standalone.StandAloneCloseSectionBaggageSelection;
import com.odigeo.suppliergatewayapi.v25.ancillaries.baggage.entity.selection.standalone.StandAloneCloseSegmentBaggageSelection;
import com.odigeo.suppliergatewayapi.v25.itinerary.ItineraryId;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.closing.IssuedItineraryBookingId;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.payment.PaymentDetails;
import com.odigeo.suppliergatewayapi.v25.itinerary.exceptions.SupplierGatewayException;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.util.List;

public class BaggageServiceController implements BaggageService {

    @Inject
    BaggageServiceController() {

    }

    @Override
    public CombinedCloseBaggageOptions optionsCombinedClose(@NotNull ItineraryId itineraryId) throws SupplierGatewayException {
        return null;
    }

    @Override
    public List<CombinedCloseSectionBaggageSelection> addCombinedCloseBySection(@NotNull ItineraryId itineraryId, @NotNull List<CombinedCloseSectionBaggageOption> list) throws SupplierGatewayException {
        return null;
    }

    @Override
    public List<BaggageProduct> product(@NotNull IssuedItineraryBookingId issuedItineraryBookingId) throws SupplierGatewayException {
        return null;
    }

    @Override
    public List<CombinedCloseSegmentBaggageSelection> addCombinedCloseBySegment(@NotNull ItineraryId itineraryId, @NotNull List<CombinedCloseSegmentBaggageOption> list) throws SupplierGatewayException {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<CombinedCloseItineraryBaggageSelection> addCombinedCloseByItinerary(@NotNull ItineraryId itineraryId, @NotNull List<CombinedCloseItineraryBaggageOption> list) throws SupplierGatewayException {
        throw new UnsupportedOperationException();
    }

    @Override
    public PaymentBaggageSelections setPaymentDetails(@NotNull IssuedItineraryBookingId issuedItineraryBookingId, @NotNull List<StandAloneCloseSectionBaggageSelection> list, @NotNull List<StandAloneCloseSegmentBaggageSelection> list1, @NotNull List<StandAloneCloseItineraryBaggageSelection> list2, @NotNull PaymentDetails paymentDetails) throws SupplierGatewayException {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<StandAloneCloseItineraryBaggageSelection> addStandAloneCloseByItinerary(@NotNull ItineraryId itineraryId, @NotNull List<StandAloneCloseItineraryBaggageOption> list) throws SupplierGatewayException {
        throw new UnsupportedOperationException();
    }

    @Override
    public StandAloneCloseBaggageOptions optionsStandAloneClose(@NotNull ItineraryId itineraryId) throws SupplierGatewayException {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<StandAloneCloseSectionBaggageSelection> addStandAloneCloseBySection(@NotNull ItineraryId itineraryId, @NotNull List<StandAloneCloseSectionBaggageOption> list) throws SupplierGatewayException {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<StandAloneCloseSegmentBaggageSelection> addStandAloneCloseBySegment(@NotNull ItineraryId itineraryId, @NotNull List<StandAloneCloseSegmentBaggageOption> list) throws SupplierGatewayException {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<BaggageProduct> close(@NotNull PaymentBaggageSelectionsId paymentBaggageSelectionsId) throws SupplierGatewayException {
        throw new UnsupportedOperationException();
    }
}
