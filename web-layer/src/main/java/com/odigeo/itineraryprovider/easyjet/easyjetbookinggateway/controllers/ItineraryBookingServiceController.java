package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.controllers;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.itinerary.ProviderItineraryBookingRepository;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper.CreatedItineraryBookingMapper;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper.ItineraryBookingCreationMapper;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.mapper.PaymentItineraryBookingResponseMapper;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayErrorType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryBookingCreationInternal;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryBookingId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Money;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderPaymentDetail;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.command.CreateItineraryBookingCommand;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.exceptions.ProviderExceptionMapper;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request.CreatedItineraryBookingIdMapper;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request.IssuedItineraryBookingMapper;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request.PaymentItineraryBookingIdMapper;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request.ProviderPaymentDetailsMapper;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.services.CloseBookingService;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.services.CreateProviderItineraryBookingService;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.services.PaymentDetailsService;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.ItineraryBooking;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.ItineraryBookingService;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.cancellation.CancelledItineraryBooking;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.closing.IssuedItineraryBooking;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.closing.IssuedItineraryBookingId;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.creation.CreatedItineraryBooking;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.creation.CreatedItineraryBookingId;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.creation.ItineraryBookingCreation;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.payment.ItineraryBookingPaymentDetails;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.payment.PaymentItineraryBooking;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.payment.PaymentItineraryBookingId;
import com.odigeo.suppliergatewayapi.v25.itinerary.exceptions.SupplierGatewayException;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@SuppressWarnings("PMD.AvoidCatchingGenericException")
public class ItineraryBookingServiceController implements ItineraryBookingService {

    private final PaymentDetailsService paymentDetailsService;
    private final ProviderItineraryBookingRepository providerItineraryBookingRepository;
    private final CreateProviderItineraryBookingService createProviderItineraryBookingService;
    private final CloseBookingService closeBookingService;

    @Inject
    public ItineraryBookingServiceController(
            PaymentDetailsService paymentDetailsService,
            ProviderItineraryBookingRepository providerItineraryBookingRepository,
            CreateProviderItineraryBookingService createProviderItineraryBookingService,
            CloseBookingService closeBookingService) {
        this.paymentDetailsService = paymentDetailsService;
        this.providerItineraryBookingRepository = providerItineraryBookingRepository;
        this.createProviderItineraryBookingService = createProviderItineraryBookingService;
        this.closeBookingService = closeBookingService;
    }

    @Override
    public CreatedItineraryBooking create(
            @NotNull @Valid ItineraryBookingCreation itineraryBookingCreation) throws SupplierGatewayException {
        try {
            ItineraryBookingCreationInternal itineraryBookingCreationInternal =
                    ItineraryBookingCreationMapper.INSTANCE.toModel(itineraryBookingCreation);

            CreateItineraryBookingCommand createItineraryBookingCommand =
                    createProviderItineraryBookingService
                            .createProviderItineraryBooking(itineraryBookingCreationInternal);

            CreatedItineraryBooking createdItineraryBooking =
                    CreatedItineraryBookingMapper.INSTANCE.toApi(
                            createItineraryBookingCommand.getItineraryBooking()
                    );

            createdItineraryBooking.getItineraryBooking().setBookingWithoutPaymentAllowed(Boolean.FALSE);

            return createdItineraryBooking;
        } catch (RuntimeException e) {
            throw ProviderExceptionMapper.INSTANCE.fromModelToApi(
                    new EasyJetBookingGatewayException(e,
                            EasyJetBookingGatewayErrorType.RUNTIME_ERROR, e.getMessage()));
        }
    }

    @Override
    public PaymentItineraryBooking setPaymentDetail(
            @NotNull CreatedItineraryBookingId createdItineraryBookingId,
            @NotNull @Valid ItineraryBookingPaymentDetails itineraryBookingPaymentDetails)
            throws SupplierGatewayException {
        try {
            ItineraryBookingId itineraryBookingId =
                    CreatedItineraryBookingIdMapper.INSTANCE.toModel(createdItineraryBookingId);

            ProviderItineraryBooking providerItineraryBooking =
                    providerItineraryBookingRepository.find(itineraryBookingId.getId());

            ProviderPaymentDetail providerPaymentDetail =
                    ProviderPaymentDetailsMapper.INSTANCE.toModel(itineraryBookingPaymentDetails);

            Money fee = paymentDetailsService.setPaymentDetails(
                    providerItineraryBooking, providerPaymentDetail);


            return PaymentItineraryBookingResponseMapper.INSTANCE.toApi(
                    providerItineraryBooking, providerPaymentDetail, fee);
        } catch (RuntimeException e) {
            throw ProviderExceptionMapper.INSTANCE.fromModelToApi(
                    new EasyJetBookingGatewayException(e, EasyJetBookingGatewayErrorType.RUNTIME_ERROR, e.getMessage()));
        }
    }

    @Override
    public PaymentItineraryBooking changePaymentDetail(@NotNull @Valid PaymentItineraryBookingId paymentItineraryBookingId, @NotNull @Valid ItineraryBookingPaymentDetails itineraryBookingPaymentDetails) throws SupplierGatewayException {
        return null;
    }

    @Override
    public IssuedItineraryBooking close(
            @NotNull @Valid PaymentItineraryBookingId paymentItineraryBookingId) throws SupplierGatewayException {
        try {

            com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary
                    .request.PaymentItineraryBookingId paymentItineraryBookingIdMapped =
                    PaymentItineraryBookingIdMapper.INSTANCE.toModel(paymentItineraryBookingId);

            com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models
                    .itinerary.request.IssuedItineraryBooking booking =
                    closeBookingService.close(paymentItineraryBookingIdMapped);

            return IssuedItineraryBookingMapper.INSTANCE.toApi(booking);
        } catch (RuntimeException e) {
            throw ProviderExceptionMapper.INSTANCE.fromModelToApi(
                    new EasyJetBookingGatewayException(e,
                            EasyJetBookingGatewayErrorType.RUNTIME_ERROR, e.getMessage()));
        }
    }

    @Override
    public CancelledItineraryBooking cancel(@NotNull @Valid CreatedItineraryBookingId createdItineraryBookingId) throws SupplierGatewayException {
        throw new UnsupportedOperationException();
    }

    @Override
    public CancelledItineraryBooking cancel(@NotNull @Valid IssuedItineraryBookingId issuedItineraryBookingId) throws SupplierGatewayException {
        throw new UnsupportedOperationException();
    }

    @Override
    public ItineraryBooking itineraryBooking(@NotNull @Valid CreatedItineraryBookingId createdItineraryBookingId) throws SupplierGatewayException {
        return null;
    }

    @Override
    public ItineraryBooking itineraryBooking(@NotNull @Valid PaymentItineraryBookingId paymentItineraryBookingId) throws SupplierGatewayException {
        return null;
    }

    @Override
    public ItineraryBooking itineraryBooking(@NotNull @Valid IssuedItineraryBookingId issuedItineraryBookingId) throws SupplierGatewayException {
        return null;
    }
}
