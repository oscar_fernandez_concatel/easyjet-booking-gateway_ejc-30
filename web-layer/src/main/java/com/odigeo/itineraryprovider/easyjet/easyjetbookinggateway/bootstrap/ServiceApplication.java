package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.bootstrap;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.rest.error.UnhandledExceptionMapper;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.controllers.ItineraryBookingServiceController;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.controllers.SelectedItineraryController;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.controllers.ancillary.BaggageServiceController;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.controllers.ancillary.SeatsServiceController;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

public class ServiceApplication extends Application {

    private final Set<Object> singletons = new HashSet<>();

    public ServiceApplication() {
        setControllers();
        setMappers();
        setInterceptors();
        setProviders();
    }

    private void setProviders() {
        singletons.add(ConfigurationEngine.getInstance(com.odigeo.commons.rest.guice.configuration.JacksonJsonMediaTypedProvider.class));
    }

    private void setInterceptors() {
        singletons.add(ConfigurationEngine.getInstance(org.jboss.resteasy.plugins.interceptors.encoding.GZIPEncodingInterceptor.class));
        singletons.add(ConfigurationEngine.getInstance(org.jboss.resteasy.plugins.interceptors.encoding.GZIPDecodingInterceptor.class));
    }

    private void setMappers() {
        singletons.add(ConfigurationEngine.getInstance(com.odigeo.suppliergatewayapi.v25.json.SupplierGatewayApiJacksonConfig.class));
        singletons.add(ConfigurationEngine.getInstance(com.odigeo.suppliergatewayapi.v25.itinerary.exceptions.mappers.RuntimeExceptionMapper.class));
        singletons.add(ConfigurationEngine.getInstance(com.odigeo.suppliergatewayapi.v25.itinerary.exceptions.mappers.SupplierGatewayExceptionMapper.class));
        singletons.add(ConfigurationEngine.getInstance(com.odigeo.suppliergatewayapi.v25.json.ThrowableJsonDeserializer.class));
        singletons.add(new UnhandledExceptionMapper());
    }

    private void setControllers() {
        singletons.add(ConfigurationEngine.getInstance(SelectedItineraryController.class));
        singletons.add(ConfigurationEngine.getInstance(ItineraryBookingServiceController.class));
        singletons.add(ConfigurationEngine.getInstance(BaggageServiceController.class));
        singletons.add(ConfigurationEngine.getInstance(SeatsServiceController.class));
    }
    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
}
