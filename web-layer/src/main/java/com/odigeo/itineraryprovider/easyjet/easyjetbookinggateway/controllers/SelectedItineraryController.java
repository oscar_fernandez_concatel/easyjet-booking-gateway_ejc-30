package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.controllers;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.EasyJetItinerarySelection;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItinerary;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.exceptions.ProviderExceptionMapper;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request.ItinerarySelectionMapper;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.mapper.request.ProviderItineraryMapper;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.services.EasyJetSelectedItineraryService;
import com.odigeo.suppliergatewayapi.v25.itinerary.exceptions.SupplierGatewayException;
import com.odigeo.suppliergatewayapi.v25.itinerary.selection.ItinerarySelection;
import com.odigeo.suppliergatewayapi.v25.itinerary.selection.SelectedItinerary;
import com.odigeo.suppliergatewayapi.v25.itinerary.selection.SelectedItineraryService;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@SuppressWarnings("PMD.AvoidCatchingGenericException")
public class SelectedItineraryController implements SelectedItineraryService {

    private final EasyJetSelectedItineraryService service;

    @Inject
    public SelectedItineraryController(final EasyJetSelectedItineraryService service) {
        this.service = service;
    }

    @Override
    public SelectedItinerary createSelectedItinerary(@NotNull @Valid ItinerarySelection itinerarySelection) throws SupplierGatewayException {
        try {
            final EasyJetItinerarySelection easyJetItinerarySelection = ItinerarySelectionMapper.INSTANCE.toModel(itinerarySelection);
            ProviderItinerary itinerary = service.createSelectedItinerary(easyJetItinerarySelection);
            return ProviderItineraryMapper.INSTANCE.toApi(itinerary);
        } catch (EasyJetBookingGatewayException easyJetBookingGatewayException) {
            throw ProviderExceptionMapper.INSTANCE.fromModelToApi(easyJetBookingGatewayException);
        }

    }
}
