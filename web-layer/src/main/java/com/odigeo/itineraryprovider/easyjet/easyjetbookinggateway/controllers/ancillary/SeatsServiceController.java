package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.controllers.ancillary;

import com.google.inject.Inject;
import com.odigeo.suppliergatewayapi.v25.ancillaries.seats.SeatsService;
import com.odigeo.suppliergatewayapi.v25.ancillaries.seats.entity.SeatProduct;
import com.odigeo.suppliergatewayapi.v25.ancillaries.seats.entity.option.combined.CombinedCloseSeatMapCondition;
import com.odigeo.suppliergatewayapi.v25.ancillaries.seats.entity.option.combined.CombinedCloseSeatOption;
import com.odigeo.suppliergatewayapi.v25.ancillaries.seats.entity.option.standalone.StandAloneCloseSeatMapCondition;
import com.odigeo.suppliergatewayapi.v25.ancillaries.seats.entity.option.standalone.StandAloneCloseSeatOption;
import com.odigeo.suppliergatewayapi.v25.ancillaries.seats.entity.payment.PaymentSeatSelections;
import com.odigeo.suppliergatewayapi.v25.ancillaries.seats.entity.payment.PaymentSeatSelectionsId;
import com.odigeo.suppliergatewayapi.v25.ancillaries.seats.entity.selection.combined.CombinedCloseSeatSelection;
import com.odigeo.suppliergatewayapi.v25.ancillaries.seats.entity.selection.standalone.StandAloneCloseSeatSelection;
import com.odigeo.suppliergatewayapi.v25.ancillaries.seats.entity.selection.standalone.StandAloneCloseSeatSelectionId;
import com.odigeo.suppliergatewayapi.v25.itinerary.ItineraryId;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.closing.IssuedItineraryBookingId;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.payment.PaymentDetails;
import com.odigeo.suppliergatewayapi.v25.itinerary.exceptions.SupplierGatewayException;


import javax.validation.constraints.NotNull;
import java.util.List;

public class SeatsServiceController implements SeatsService {

    @Inject
    public SeatsServiceController() {
    }

    @Override
    public List<CombinedCloseSeatMapCondition> optionsCombinedClose(@NotNull ItineraryId itineraryId) throws SupplierGatewayException {
        return null;
    }

    @Override
    public List<CombinedCloseSeatSelection> addCombinedClose(@NotNull ItineraryId itineraryId, @NotNull List<CombinedCloseSeatOption> list) throws SupplierGatewayException {
        return null;
    }

    @Override
    public List<SeatProduct> product(@NotNull IssuedItineraryBookingId issuedItineraryBookingId) throws SupplierGatewayException {
        return null;
    }

    @Override
    public List<StandAloneCloseSeatMapCondition> optionsStandAloneClose(@NotNull ItineraryId itineraryId) throws SupplierGatewayException {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<StandAloneCloseSeatSelection> addStandAloneClose(@NotNull ItineraryId itineraryId, @NotNull List<StandAloneCloseSeatOption> list) throws SupplierGatewayException {
        throw new UnsupportedOperationException();
    }

    @Override
    public PaymentSeatSelections setPaymentDetails(@NotNull IssuedItineraryBookingId issuedItineraryBookingId, @NotNull List<StandAloneCloseSeatSelectionId> list, @NotNull PaymentDetails paymentDetails) throws SupplierGatewayException {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<SeatProduct> close(@NotNull PaymentSeatSelectionsId paymentSeatSelectionsId) throws SupplierGatewayException {
        throw new UnsupportedOperationException();
    }
}
