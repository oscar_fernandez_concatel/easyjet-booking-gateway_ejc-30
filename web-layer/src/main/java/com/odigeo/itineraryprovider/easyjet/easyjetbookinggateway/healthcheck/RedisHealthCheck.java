package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.healthcheck;

import com.codahale.metrics.health.HealthCheck;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.redis.SelectionRedisCacheConfiguration;
import com.odigeo.persistance.cache.impl.distributed.RedisClientFactory;
import com.odigeo.persistance.cache.impl.distributed.SerializableSerializableRedisCodec;
import io.lettuce.core.RedisClient;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RedisHealthCheck extends HealthCheck {
    private static final Logger logger = LoggerFactory.getLogger(RedisHealthCheck.class);

    private final RedisClient redisClient;
    private final SelectionRedisCacheConfiguration selectionRedisCacheConfiguration;

    public RedisHealthCheck(SelectionRedisCacheConfiguration selectionRedisCacheConfiguration, RedisClientFactory redisClientFactory) {
        this.selectionRedisCacheConfiguration = selectionRedisCacheConfiguration;

        logger.info("[REDIS] Initializing Healthcheck for Checkouts Cache using the following RedisConfiguration: {}", selectionRedisCacheConfiguration);
        this.redisClient = redisClientFactory.newInstance(selectionRedisCacheConfiguration.getHost(),
                selectionRedisCacheConfiguration.getPort(),
                selectionRedisCacheConfiguration.getTimeoutMillis(),
                selectionRedisCacheConfiguration.getExpirationTimeMillis());
        logger.info("[REDIS] Redis Client initialized");
    }

    @Override
    public Result check() {
        logger.info("[REDIS] Starting check()");
        String ping = redisClient.connect(new SerializableSerializableRedisCodec()).sync().ping();
        logger.info("[REDIS] ping check response: {}", ping);
        return getResult(StringUtils.isNotBlank(ping));
    }

    private Result getResult(boolean ok) {
        return ok ? Result.healthy("Redis is up. Configuration: " + selectionRedisCacheConfiguration) : Result.unhealthy("Redis is down. Configuration: " + selectionRedisCacheConfiguration);
    }
}
