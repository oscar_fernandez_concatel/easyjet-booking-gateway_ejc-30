package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardCategory;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardLevel;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardPaymentOption;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardProduct;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardSpec;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderPaymentOptions;

import java.util.Set;

public class ProviderPaymentOptionsObjectMother {

    public static final ProviderPaymentOptions ANY = aProviderPaymentOptions();

    private static ProviderPaymentOptions aProviderPaymentOptions() {

        return ProviderPaymentOptions.builder()
                .setCreditCardPaymentOptions(Set.of(CreditCardPaymentOption.builder()
                        .setCreditCardSpec(CreditCardSpec.builder()
                                .setCreditCardCategory(CreditCardCategory.CREDIT)
                                .setCreditCardProduct(CreditCardProduct.DEFAULT)
                                .setCreditCardCategory(CreditCardCategory.CREDIT)
                                .setCreditCardLevel(CreditCardLevel.CUSTOMER)
                                .build())
                        .build()))
                .build();
    }

}
