package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.CreditCardVendorCode;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.payment.CreditCard;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.payment.ItineraryBookingPaymentDetails;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.payment.PaymentDetails;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.payment.PaymentMethod;
import com.odigeo.suppliergatewayapi.v25.itinerary.fare.Money;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Currency;

public class ItineraryBookingPaymentDetailsObjectMother {

    public static final ItineraryBookingPaymentDetails ANY = aItineraryBookingPaymentDetails();

    private static ItineraryBookingPaymentDetails aItineraryBookingPaymentDetails() {

        ItineraryBookingPaymentDetails itineraryBookingPaymentDetails =
                new ItineraryBookingPaymentDetails();

        PaymentDetails paymentDetails = new PaymentDetails();

        String vendorCode = CreditCardVendorCode.MASTERCARD.getType();
        String method = "CREDITCARD";
        String number = "5106906595054446";
        LocalDate expirationDate = LocalDate.of(2022, 1, 1);
        String verificationValue = "111";
        String holderName = "Juan Perez Galan";
        String cardType = "MASTERCARD";

        PaymentMethod paymentMethod = new CreditCard(
                vendorCode, method,
                number, expirationDate,
                verificationValue, holderName, cardType);

        paymentMethod.setFee(new Money(BigDecimal.valueOf(91.99), Currency.getInstance("EUR")));

        paymentDetails.setPaymentMethod(paymentMethod);

        itineraryBookingPaymentDetails.setPaymentDetails(paymentDetails);

        return itineraryBookingPaymentDetails;
    }
}
