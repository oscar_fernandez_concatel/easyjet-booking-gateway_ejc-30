package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.BaggageAllowanceType;
import com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.BaggageAllowance;

public class BaggageAllowanceObjectMother {
    public static final BaggageAllowance ANY_EXTERNAL = aBaggageAllowanceExternal();
    public static final com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.BaggageAllowance ANY_INTERNAL = aBaggageAllowanceInternal();

    public static BaggageAllowance aBaggageAllowanceExternal() {
        BaggageAllowance baggageAllowance = new BaggageAllowance();
        baggageAllowance.setBaggageAllowanceType("KG");
        baggageAllowance.setQuantity(15);
        return baggageAllowance;
    }

    public static com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.BaggageAllowance aBaggageAllowanceInternal() {
        return com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.BaggageAllowance.builder()
                .setBaggageAllowanceType(BaggageAllowanceType.KG)
                .setQuantity(15)
                .build();
    }
}
