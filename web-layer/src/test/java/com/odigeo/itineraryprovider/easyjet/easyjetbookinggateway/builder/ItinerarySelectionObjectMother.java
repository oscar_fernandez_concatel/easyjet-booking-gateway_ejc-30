package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.suppliergatewayapi.v25.itinerary.fare.FareDetails;
import com.odigeo.suppliergatewayapi.v25.itinerary.segment.Segment;
import com.odigeo.suppliergatewayapi.v25.itinerary.selection.ItinerarySelection;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ItinerarySelectionObjectMother {

    public static final ItinerarySelection ANY = aItinerarySelection();

    public static ItinerarySelection aItinerarySelection() {

        FareDetails fareDetails = FareDetailsObjectMother.ANY_EXTERNAL;
        Segment segment = SegmentObjectMother.ANY_EXTERNAL;
        List<Segment> segments = new ArrayList<Segment>();
        segments.add(segment);

        ItinerarySelection itinerarySelection = new ItinerarySelection();
        itinerarySelection.setArrivalLocation(5678);
        itinerarySelection.setDepartureDate(LocalDateTime.of(2021, 12, 15, 10, 0, 0));
        itinerarySelection.setDepartureLocation(1234);
        itinerarySelection.setSegments(null);
        itinerarySelection.setResident(false);
        itinerarySelection.setFareDetails(fareDetails);
        itinerarySelection.setReturnDate(LocalDateTime.of(2021, 12, 15, 10, 0, 0));
        itinerarySelection.setTripType("ONE_WAY");
        itinerarySelection.setVisitId("1");
        itinerarySelection.setVisitInformation("hello world!");
        itinerarySelection.setSegments(segments);
        return itinerarySelection;

    }
}
