package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.suppliergatewayapi.v25.itinerary.booking.creation.ItineraryBookingCreation;
import com.odigeo.suppliergatewayapi.v25.itinerary.selection.SelectedItineraryId;

import java.util.List;
import java.util.UUID;

public class ItineraryBookingCreationObjectMother {

    public static final ItineraryBookingCreation ANY = aItineraryBookingCreation();

    public static ItineraryBookingCreation aItineraryBookingCreation() {
        SelectedItineraryId selectedItineraryId = new SelectedItineraryId();
        selectedItineraryId.setId(UUID.randomUUID());
        ItineraryBookingCreation itineraryBookingCreation = new ItineraryBookingCreation();
        itineraryBookingCreation.setBookingId(UUID.randomUUID().toString());
        itineraryBookingCreation.setItineraryId(selectedItineraryId);
        itineraryBookingCreation.setBuyer(BuyerObjectMother.ANY_EXTERNAL);
        itineraryBookingCreation.setTravellers(List.of(TravellerObjectMother.ANY_EXTERNAL));
        return itineraryBookingCreation;
    }
}
