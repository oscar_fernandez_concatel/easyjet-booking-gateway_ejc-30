package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.Carrier;

public class CarrierObjectModel {
    public static final Carrier ANY_EXTERNAL = aCarrierExternal();
    public static final com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Carrier ANY_INTERNAL = aCarrierInternal();

    public static Carrier aCarrierExternal() {
        Carrier carrier = new Carrier();

        carrier.setCode("CODE");
        carrier.setIataCode("IATACODE");
        carrier.setName("NAME");
        carrier.setShowBaggagePolicy(true);
        return carrier;
    }

    public static com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Carrier aCarrierInternal() {
        return com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Carrier
                .builder()
                .setCode("CODE")
                .setIataCode("IATACODE")
                .setName("NAME")
                .setShowBaggagePolicy(true)
                .build();
    }

}
