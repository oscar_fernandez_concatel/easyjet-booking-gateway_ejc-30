package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Itinerary;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TripType;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public class ItineraryObjectMother {

    public static final Itinerary ANY = aItinerary();

    private static Itinerary aItinerary() {

        return Itinerary.builder()
                .setFareDetails(FareDetailsObjectMother.ANY)
                .setId(ItineraryId.builder().setId(UUID.randomUUID()).build())
                .setArrivalGeoNodeId(9884)
                .setDepartureGeoNodeId(9766)
                .setDepartureDate(LocalDateTime.now().plusDays(2))
                .setReturnDate(LocalDateTime.now().plusDays(3))
                .setTripType(TripType.ONE_WAY)
                .setSegments(List.of(SegmentObjectMother.ANY))
                .setProviderPaymentOptions(ProviderPaymentOptionsObjectMother.ANY)
                .build();
    }

}
