package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.BookingStatus;
import com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.Carrier;
import com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.Flight;

public class FlightObjectMother {
    public static final Flight ANY_EXTERNAL = aFlightExternal();
    public static final com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Flight ANY_INTERNAL = aFlightInternal();

    public static Flight aFlightExternal() {
        Flight flight = new Flight();

        Carrier marketing = CarrierObjectModel.ANY_EXTERNAL;
        Carrier operating = CarrierObjectModel.ANY_EXTERNAL;

        flight.setFlightNumber("AR1401");
        flight.setMarketingCarrier(marketing);
        flight.setOperatingCarrier(operating);
        flight.setStatus("CONFIRM");
        flight.setTypeOfAircraft("aircraft");

        return flight;
    }

    public static com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Flight aFlightInternal() {
        com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Carrier marketing = CarrierObjectModel.ANY_INTERNAL;
        com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Carrier operating = CarrierObjectModel.ANY_INTERNAL;
        return com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Flight.builder()
                .setFlightNumber("AR1401")
                .setMarketingCarrier(marketing)
                .setOperatingCarrier(operating)
                .setStatus(BookingStatus.CONFIRMED.name())
                .build();
    }

}
