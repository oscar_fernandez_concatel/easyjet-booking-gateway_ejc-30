package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.BookingStatus;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ProviderItineraryBooking;

import java.util.List;
import java.util.UUID;

public class ProviderItineraryBookingObjectMother {

    public static final ProviderItineraryBooking ANY_EXTERNAL = aProviderItineraryBooking();

    private static ProviderItineraryBooking aProviderItineraryBooking() {

        return ProviderItineraryBooking.builder()
                .setItineraryId(ItineraryId.builder().setId(UUID.randomUUID()).build())
                .setItinerary(ItineraryObjectMother.ANY)
                .setStatus(BookingStatus.PENDING)
                .setBuyer(BuyerObjectMother.ANY)
                .setTravellers(List.of(TravellerObjectMother.ANY))
                .setReservationLocator("8HUDHW")
                .build();
    }

}
