package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FareType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightFareInformation;

public class FlightFareInformationObjectMother {

    public static final FlightFareInformation ANY = aFlightFareInformation();

    public static FlightFareInformation aFlightFareInformation() {

        return FlightFareInformation.builder()
                .setFareType(FareType.PUBLIC)
                .setFlightFareInformationPassenger(FlightFareInformationPassengerObjectMother.mapFareFlightFareInformationPassenger())
                .build();
    }
}
