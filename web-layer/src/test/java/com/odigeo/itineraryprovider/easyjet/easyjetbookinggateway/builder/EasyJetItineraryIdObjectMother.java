package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;


import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;

public class EasyJetItineraryIdObjectMother {
    public static final ItineraryId ANY = aItineraryId();

    public static ItineraryId aItineraryId() {
        return ItineraryId.valueOf("15e45e7d-3e34-43df-9366-91c66a8cc9ae");
    }

}
