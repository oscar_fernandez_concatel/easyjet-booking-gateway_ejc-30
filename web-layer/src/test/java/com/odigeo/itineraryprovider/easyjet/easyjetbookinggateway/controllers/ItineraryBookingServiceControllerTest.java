package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.controllers;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.IssuedItineraryBookingObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryBookingCreationObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItineraryBookingPaymentDetailsObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderItineraryBookingObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.itinerary.ProviderItineraryBookingRepository;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayErrorType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.ItineraryNotFoundException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.command.CreateItineraryBookingCommand;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.services.CloseBookingService;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.services.CreateProviderItineraryBookingService;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.services.PaymentDetailsService;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.ItineraryBookingService;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.closing.IssuedItineraryBooking;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.closing.IssuedItineraryBookingId;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.creation.CreatedItineraryBooking;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.creation.CreatedItineraryBookingId;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.payment.ItineraryBookingPaymentDetails;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.payment.PaymentItineraryBooking;
import com.odigeo.suppliergatewayapi.v25.itinerary.booking.payment.PaymentItineraryBookingId;
import com.odigeo.suppliergatewayapi.v25.itinerary.exceptions.SupplierGatewayException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class ItineraryBookingServiceControllerTest {

    @Mock
    private PaymentDetailsService paymentDetailsService;

    @Mock
    private ProviderItineraryBookingRepository providerItineraryBookingRepository;

    @Mock
    private CreateProviderItineraryBookingService createProviderItineraryBookingService;

    @Mock
    private CloseBookingService closeBookingService;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);

        ConfigurationEngine.init(new AbstractModule() {
            @Override
            protected void configure() {
                bind(ItineraryBookingService.class).toInstance(new ItineraryBookingServiceController(
                        paymentDetailsService, providerItineraryBookingRepository,
                        createProviderItineraryBookingService, closeBookingService));
            }
        });
    }

    @Test
    public void whenCreate() throws SupplierGatewayException, EasyJetBookingGatewayException {

        when(createProviderItineraryBookingService
                        .createProviderItineraryBooking(any()))
                .thenReturn(CreateItineraryBookingCommand.builder()
                        .itineraryBooking(ProviderItineraryBookingObjectMother.ANY_EXTERNAL)
                        .baggageSelections(List.of())
                        .seatSelections(List.of())
                        .build());

        ItineraryBookingService itineraryBookingServiceController =
                ConfigurationEngine.getInstance(ItineraryBookingService.class);

        CreatedItineraryBooking result =
                itineraryBookingServiceController.create(ItineraryBookingCreationObjectMother.ANY);

        Assert.assertNotNull(result);
    }

    @Test(expectedExceptions = SupplierGatewayException.class)
    public void whenCreateAndRuntimeExceptionThenSupplierGateway()
            throws SupplierGatewayException, EasyJetBookingGatewayException {

        when(createProviderItineraryBookingService
                        .createProviderItineraryBooking(any()))
                .thenThrow(new RuntimeException());

        ItineraryBookingService itineraryBookingServiceController =
                ConfigurationEngine.getInstance(ItineraryBookingService.class);

        CreatedItineraryBooking result =
                itineraryBookingServiceController.create(ItineraryBookingCreationObjectMother.ANY);

        Assert.assertNotNull(result);
    }

    @Test(expectedExceptions = SupplierGatewayException.class)
    public void whenCreateAndEasyJetBookingGatewayExceptionThenSupplierGateway()
            throws SupplierGatewayException, EasyJetBookingGatewayException {

        when(createProviderItineraryBookingService
                        .createProviderItineraryBooking(any()))
                .thenThrow(new EasyJetBookingGatewayException("", EasyJetBookingGatewayErrorType.UNKNOWN));

        ItineraryBookingService itineraryBookingServiceController =
                ConfigurationEngine.getInstance(ItineraryBookingService.class);

        CreatedItineraryBooking result =
                itineraryBookingServiceController.create(ItineraryBookingCreationObjectMother.ANY);

        Assert.assertNotNull(result);
    }

    @Test
    public void whenSetPaymentDetail() throws SupplierGatewayException {

        when(providerItineraryBookingRepository.find(any()))
                .thenReturn(ProviderItineraryBookingObjectMother.ANY_EXTERNAL);

        ItineraryBookingService itineraryBookingServiceController =
                ConfigurationEngine.getInstance(ItineraryBookingService.class);

        CreatedItineraryBookingId createdItineraryBookingId = new CreatedItineraryBookingId();
        createdItineraryBookingId.setId(UUID.randomUUID());

        PaymentItineraryBooking result =
                itineraryBookingServiceController.setPaymentDetail(
                        createdItineraryBookingId,
                        ItineraryBookingPaymentDetailsObjectMother.ANY);

        Assert.assertNotNull(result);
    }

    @Test(expectedExceptions = SupplierGatewayException.class)
    public void whenSetPaymentDetailThenReturnException() throws SupplierGatewayException {

        when(providerItineraryBookingRepository.find(any()))
                .thenThrow(new ItineraryNotFoundException("", EasyJetBookingGatewayErrorType.UNKNOWN));

        ItineraryBookingService itineraryBookingServiceController =
                ConfigurationEngine.getInstance(ItineraryBookingService.class);

        CreatedItineraryBookingId createdItineraryBookingId = new CreatedItineraryBookingId();
        createdItineraryBookingId.setId(UUID.randomUUID());

        PaymentItineraryBooking result =
                itineraryBookingServiceController.setPaymentDetail(
                        createdItineraryBookingId,
                        ItineraryBookingPaymentDetailsObjectMother.ANY);

        Assert.assertNotNull(result);
    }

    @Test
    public void whenClose() throws SupplierGatewayException {

        com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models
                .itinerary.request.IssuedItineraryBooking issuedItineraryBooking = IssuedItineraryBookingObjectMother.ANY;
        when(closeBookingService.close(any()))
                .thenReturn(issuedItineraryBooking);

        ItineraryBookingService itineraryBookingServiceController =
                ConfigurationEngine.getInstance(ItineraryBookingService.class);

        PaymentItineraryBookingId paymentItineraryBookingId = new PaymentItineraryBookingId();
        paymentItineraryBookingId.setId(UUID.randomUUID());

        IssuedItineraryBooking result =
                itineraryBookingServiceController.close(
                        paymentItineraryBookingId);

        Assert.assertNotNull(result);
    }

    @Test(expectedExceptions = SupplierGatewayException.class)
    public void whenCloseThenReturnException() throws SupplierGatewayException {

        when(closeBookingService.close(any()))
                .thenThrow(new NullPointerException());

        ItineraryBookingService itineraryBookingServiceController =
                ConfigurationEngine.getInstance(ItineraryBookingService.class);

        PaymentItineraryBookingId paymentItineraryBookingId = new PaymentItineraryBookingId();
        paymentItineraryBookingId.setId(UUID.randomUUID());

        IssuedItineraryBooking result =
                itineraryBookingServiceController.close(
                        paymentItineraryBookingId);

        Assert.assertNotNull(result);
    }

    @Test
    public void whenChangePaymentDetail() throws SupplierGatewayException {

        PaymentItineraryBookingId paymentItineraryBookingId = new PaymentItineraryBookingId();
        paymentItineraryBookingId.setId(UUID.randomUUID());
        ItineraryBookingPaymentDetails itineraryBookingPaymentDetails = new ItineraryBookingPaymentDetails();
        ItineraryBookingService itineraryBookingServiceController =
                ConfigurationEngine.getInstance(ItineraryBookingService.class);
        itineraryBookingServiceController.changePaymentDetail(
                        paymentItineraryBookingId, itineraryBookingPaymentDetails);
    }

    @Test(expectedExceptions = UnsupportedOperationException.class)
    public void whenCancelCreated() throws SupplierGatewayException {

        CreatedItineraryBookingId createdItineraryBookingId = new CreatedItineraryBookingId();
        ItineraryBookingService itineraryBookingServiceController =
                ConfigurationEngine.getInstance(ItineraryBookingService.class);
        itineraryBookingServiceController.cancel(
                createdItineraryBookingId);
    }

    @Test(expectedExceptions = UnsupportedOperationException.class)
    public void whenItineraryBookingIssued() throws SupplierGatewayException {

        IssuedItineraryBookingId issuedItineraryBookingId = new IssuedItineraryBookingId();
        ItineraryBookingService itineraryBookingServiceController =
                ConfigurationEngine.getInstance(ItineraryBookingService.class);
        itineraryBookingServiceController.cancel(
                issuedItineraryBookingId);
    }

    @Test
    public void whenItineraryBookingPayment() throws SupplierGatewayException {

        PaymentItineraryBookingId paymentItineraryBookingId = new PaymentItineraryBookingId();
        ItineraryBookingService itineraryBookingServiceController =
                ConfigurationEngine.getInstance(ItineraryBookingService.class);
        itineraryBookingServiceController.itineraryBooking(
                paymentItineraryBookingId);
    }

    @Test
    public void whenItineraryBookingCreated() throws SupplierGatewayException {

        CreatedItineraryBookingId createdItineraryBookingId = new CreatedItineraryBookingId();
        ItineraryBookingService itineraryBookingServiceController =
                ConfigurationEngine.getInstance(ItineraryBookingService.class);
        itineraryBookingServiceController.itineraryBooking(
                createdItineraryBookingId);
    }

    @Test
    public void whenCancelIssued() throws SupplierGatewayException {

        IssuedItineraryBookingId issuedItineraryBookingId = new IssuedItineraryBookingId();
        ItineraryBookingService itineraryBookingServiceController =
                ConfigurationEngine.getInstance(ItineraryBookingService.class);
        itineraryBookingServiceController.itineraryBooking(
                issuedItineraryBookingId);
    }

}
