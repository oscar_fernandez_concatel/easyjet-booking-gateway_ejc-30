package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;


import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Country;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.traveller.Phone;

public class PhoneObjectMother {

    public static final com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Phone ANY_EXTERNAL = aPhoneExternal();
    public static final Phone ANY = aPhone();

    private static com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Phone aPhoneExternal() {
        com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Country country = CountryObjectMother.ANY_EXTERNAL;
        return new com.odigeo.suppliergatewayapi.v25.itinerary.booking.traveller.Phone(country, "123456789");
    }

    private static Phone aPhone() {
        Country country = CountryObjectMother.ANY;
        return Phone.builder()
                .setPhoneType("Movil")
                .setNumber("666888777")
                .setCountry(country)
                .build();
    }


}
