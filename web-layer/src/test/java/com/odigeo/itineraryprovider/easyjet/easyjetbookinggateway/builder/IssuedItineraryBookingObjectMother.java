package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.BookingStatus;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.IssuedItineraryBooking;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.IssuedItineraryBookingId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryBooking;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryBookingId;

import java.util.List;
import java.util.UUID;

public class IssuedItineraryBookingObjectMother {

    public static final IssuedItineraryBooking ANY = aIssuedItineraryBooking();

    public static final String ISSUED_PNR = "9HJDFG";

    public static IssuedItineraryBooking aIssuedItineraryBooking() {

        return IssuedItineraryBooking.builder()
                .setId(IssuedItineraryBookingId.builder()
                        .setId(UUID.randomUUID())
                        .build())
                .setStatus(BookingStatus.CONFIRMED.name())
                .setItineraryBooking(ItineraryBooking.builder()
                        .setId(ItineraryBookingId.builder().setId(UUID.randomUUID()).build())
                        .setItinerary(ItineraryObjectMother.ANY)
                        .setBuyer(BuyerObjectMother.ANY)
                        .setPnr(ISSUED_PNR)
                        .setTravellers(List.of(TravellerObjectMother.ANY))
                        .setBookingWithoutPaymentAllowed(false)
                        .build())
                .build();
    }
}
