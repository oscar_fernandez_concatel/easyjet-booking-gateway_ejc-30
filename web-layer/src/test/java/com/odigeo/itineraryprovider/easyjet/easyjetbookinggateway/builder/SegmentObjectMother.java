package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;


import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.FlightSection;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.ItineraryId;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Segment;

import java.util.ArrayList;
import java.util.List;

public class SegmentObjectMother {

    public static final com.odigeo.suppliergatewayapi.v25.itinerary.segment.Segment ANY_EXTERNAL = aSegmentExternal();
    public static final Segment ANY = aSegment();

    public static com.odigeo.suppliergatewayapi.v25.itinerary.segment.Segment aSegmentExternal() {

        com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.FlightSection section =
                FlightSectionObjectMother.ANY_EXTERNAL;

        List<com.odigeo.suppliergatewayapi.v25.itinerary.segment.section.Section> sections = new ArrayList<>();
        sections.add(section);

        com.odigeo.suppliergatewayapi.v25.itinerary.ItineraryId itineraryId = ItineraryIdObjectMother.ANY_EXTERNAL;

        com.odigeo.suppliergatewayapi.v25.itinerary.segment.Segment segment =
                new com.odigeo.suppliergatewayapi.v25.itinerary.segment.Segment();

        segment.setItineraryId(itineraryId);
        segment.setPosition(1);
        segment.setSections(sections);

        return segment;
    }

    public static Segment aSegment() {

        FlightSection section = FlightSectionObjectMother.ANY_INTERNAL;

        List<FlightSection> sections = new ArrayList<>();
        sections.add(section);

        ItineraryId itineraryId = ItineraryIdObjectMother.ANY_INTERNAL;

        return Segment.builder()
                .setItineraryId(itineraryId)
                .setPosition(1)
                .setSections(sections)
                .build();
    }
}
