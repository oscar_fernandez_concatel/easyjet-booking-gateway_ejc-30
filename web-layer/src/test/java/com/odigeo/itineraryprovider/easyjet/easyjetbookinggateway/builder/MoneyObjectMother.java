package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;


import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Money;

import java.math.BigDecimal;
import java.util.Currency;

public class MoneyObjectMother {

    public static final com.odigeo.suppliergatewayapi.v25.itinerary.fare.Money ANY_EXTERNAL = aMoneyExternal();
    public static final Money ANY = aMoney();

    public static com.odigeo.suppliergatewayapi.v25.itinerary.fare.Money aMoneyExternal() {
        return new com.odigeo.suppliergatewayapi.v25.itinerary.fare.Money(BigDecimal.ONE, Currency.getInstance("EUR"));
    }

    public static Money aMoney() {
        return Money.builder()
                .setAmount(BigDecimal.ONE)
                .setCurrency(Currency.getInstance("EUR"))
                .build();
    }

}


