package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder;


import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.Money;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.PassengerTypeFare;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.itinerary.request.TravellerType;

public class PassengerTypeFareObjectMother {

    public static final com.odigeo.suppliergatewayapi.v25.itinerary.fare.PassengerTypeFare ANY_EXTERNAL = aPassengerTypeFareExternal();
    public static final PassengerTypeFare ANY = aPassengerTypeFare();

    public static com.odigeo.suppliergatewayapi.v25.itinerary.fare.PassengerTypeFare aPassengerTypeFareExternal() {

        com.odigeo.suppliergatewayapi.v25.itinerary.fare.Money fareAmount = MoneyObjectMother.ANY_EXTERNAL;
        com.odigeo.suppliergatewayapi.v25.itinerary.fare.Money taxAmount = MoneyObjectMother.ANY_EXTERNAL;

        com.odigeo.suppliergatewayapi.v25.itinerary.fare.PassengerTypeFare passengerTypeFare =
                new com.odigeo.suppliergatewayapi.v25.itinerary.fare.PassengerTypeFare();

        passengerTypeFare.setDiscount(false);
        passengerTypeFare.setPassengersPerType(1);
        passengerTypeFare.setFareAmount(fareAmount);
        passengerTypeFare.setTaxAmount(taxAmount);
        passengerTypeFare.setTravellerType("ADULT");
        return passengerTypeFare;
    }

    public static PassengerTypeFare aPassengerTypeFare() {

        Money fareAmount = MoneyObjectMother.ANY;
        Money taxAmount = MoneyObjectMother.ANY;

        return PassengerTypeFare.builder()
                .setDiscount(false)
                .setPassengersPerType(1)
                .setFareAmount(fareAmount)
                .setTaxAmount(taxAmount)
                .setTravellerType(TravellerType.ADULT)
                .build();
    }


}
