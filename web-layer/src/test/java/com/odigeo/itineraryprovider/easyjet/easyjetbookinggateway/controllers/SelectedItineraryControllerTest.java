package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.controllers;

import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ItinerarySelectionObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.builder.ProviderItineraryObjectMother;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayErrorType;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.models.exceptions.EasyJetBookingGatewayException;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.services.EasyJetSelectedItineraryService;
import com.odigeo.suppliergatewayapi.v25.itinerary.exceptions.SupplierGatewayException;
import com.odigeo.suppliergatewayapi.v25.itinerary.selection.ItinerarySelection;
import com.odigeo.suppliergatewayapi.v25.itinerary.selection.SelectedItinerary;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class SelectedItineraryControllerTest {


    @Mock
    private EasyJetSelectedItineraryService easyJetSelectedItineraryService;

    private SelectedItineraryController sut;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        sut = new SelectedItineraryController(easyJetSelectedItineraryService);
    }

    @Test
    public void testWhenCreateSelectedItinerary() throws EasyJetBookingGatewayException, SupplierGatewayException {
        when(easyJetSelectedItineraryService.createSelectedItinerary(any())).thenReturn(ProviderItineraryObjectMother.ANY);

        ItinerarySelection input = ItinerarySelectionObjectMother.ANY;
        SelectedItinerary response = sut.createSelectedItinerary(input);

        Assert.assertNotNull(response);
        Assert.assertEquals(input.getTripType(), response.getItinerary().getTripType());
        Assert.assertEquals(input.getDepartureDate(), response.getItinerary().getDepartureDate());
        Assert.assertEquals(input.getReturnDate(), response.getItinerary().getReturnDate());
    }

    @Test(expectedExceptions = SupplierGatewayException.class)
    public void testWhenCreateSelectedItineraryWithErrorOnService() throws EasyJetBookingGatewayException, SupplierGatewayException {
        when(easyJetSelectedItineraryService.createSelectedItinerary(any())).thenThrow(new EasyJetBookingGatewayException("...", EasyJetBookingGatewayErrorType.NOT_EXISTING_CODE) {
        });

        ItinerarySelection input = ItinerarySelectionObjectMother.ANY;
        sut.createSelectedItinerary(input);
    }
}
