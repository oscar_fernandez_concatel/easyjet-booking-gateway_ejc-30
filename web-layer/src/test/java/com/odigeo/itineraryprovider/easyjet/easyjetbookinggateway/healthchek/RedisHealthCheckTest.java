package com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.healthchek;

import com.codahale.metrics.health.HealthCheck;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.healthcheck.RedisHealthCheck;
import com.odigeo.itineraryprovider.easyjet.easyjetbookinggateway.redis.SelectionRedisCacheConfiguration;
import com.odigeo.persistance.cache.impl.distributed.RedisClientFactory;
import com.odigeo.persistance.cache.impl.distributed.SerializableSerializableRedisCodec;
import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class RedisHealthCheckTest {

    @Mock
    private RedisClientFactory redisClientFactory;

    @Mock
    private RedisClient redisClient;

    @Mock
    private StatefulRedisConnection statefulRedisConnection;

    @Mock
    private RedisCommands redisCommands;

    @Mock
    private SelectionRedisCacheConfiguration selectionRedisCacheConfiguration;

    private RedisHealthCheck sut;

    @BeforeMethod
    public void init() {
        MockitoAnnotations.openMocks(this);
        when(redisClientFactory.newInstance(anyString(), anyInt(), anyInt(), anyInt())).thenReturn(redisClient);
        when(selectionRedisCacheConfiguration.getExpirationTimeMillis()).thenReturn(1);
        when(selectionRedisCacheConfiguration.getHost()).thenReturn("HOST");
        when(selectionRedisCacheConfiguration.getPort()).thenReturn(1);
        when(selectionRedisCacheConfiguration.getTimeoutMillis()).thenReturn(1);
        sut = new RedisHealthCheck(selectionRedisCacheConfiguration, redisClientFactory);
    }

    @Test
    public void testWhenCheck() {
        when(redisClient.connect(any(SerializableSerializableRedisCodec.class))).thenReturn(statefulRedisConnection);
        when(statefulRedisConnection.sync()).thenReturn(redisCommands);
        when(redisCommands.ping()).thenReturn("PINGED!");

        HealthCheck.Result result = sut.check();

        Assert.assertNotNull(result);
        Assert.assertTrue(result.isHealthy());
    }

    @Test
    public void testWhenCheckUnhealthy() {
        when(redisClientFactory.newInstance(anyString(), anyInt())).thenReturn(redisClient);
        when(redisClient.connect(any(SerializableSerializableRedisCodec.class))).thenReturn(statefulRedisConnection);
        when(statefulRedisConnection.sync()).thenReturn(redisCommands);
        when(redisCommands.ping()).thenReturn("");

        HealthCheck.Result result = sut.check();

        Assert.assertNotNull(result);
        Assert.assertFalse(result.isHealthy());
    }

}
